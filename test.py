import functools
import operator


def getFromDict(dataDict, mapList):
    return functools.reduce(operator.getitem, mapList, dataDict)


def setInDict(dataDict, mapList, value):
    getFromDict(dataDict, mapList[:-1])[mapList[-1]] = value


def addToDict(dataDict, mapList, value):
    getFromDict(dataDict, mapList[:-1])[mapList[-1]].update(value)


def remFromDict(dataDict, mapList):
    rem = mapList.pop()
    ob = getFromDict(dataDict, mapList)
    ob.pop(rem)



a = {'key' : 0.3, 'nested': {'val': 4}}
b = {'type': 'E', 'path': ['nested'], 'rhs': {'second val': 5}}
c = {'type': 'E', 'path': ['nested', 'val'], 'rhs': 5}
d = {'type': 'N', 'path': ['nested'], 'rhs': {'third val': 3}}
e = {'type': 'D', 'path': ['nested', 'second val'], 'lhs': {'third val': 3}}
f = {'type': 'D', 'path': ['nested'], 'lhs': {'third val': 3}}

print("start", a)
print("modify", c)
setInDict(a, c['path'], c['rhs'])

print(a)
print("======")
print("modify", b)
setInDict(a, b['path'], b['rhs'])

print(a)
print("======")
print("add", d)
addToDict(a, d['path'], d['rhs'])

print(a)
print("======")
print("rem", e)
remFromDict(a, e['path'])

print (a)
print("======")
print("rem", f)
remFromDict(a, f['path'])

print(a)

