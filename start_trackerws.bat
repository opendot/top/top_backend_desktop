@ECHO OFF
start /min cmd /c "venv\Scripts\activate & python manage.py startserver
ping 127.0.0.1 -n 3 > nul
start /min cmd /c "venv\Scripts\activate && python manage.py runbrowser"
start /min cmd /c "venv\Scripts\activate && python manage.py trackerws"
start /min cmd /c "venv\Scripts\activate && python manage.py eventws"
exit
