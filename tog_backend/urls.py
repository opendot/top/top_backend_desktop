"""tog_backend URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf.urls import url, include
from rest_framework import routers
from tog import views
from tog.views import login, getServerIP, showQRIp, pingview, checkClosedSession, syncupload, receivesync, syncdownload

router = routers.DefaultRouter()
router.register(r'users', views.UserViewSet)
router.register(r'carereceivers', views.CareReceiverViewSet,basename='CareReceiver')
router.register(r'games', views.GameViewSet)
router.register(r'sessions', views.SessionViewSet,basename='Session')

# Wire up our API using automatic URL routing.
# Additionally, we include qr URLs for the browsable API.
urlpatterns = [
    path('login/', login),
    path('qr/', showQRIp),
    path('ip/', getServerIP),
    path('checksession/', checkClosedSession),
    path('syncupload/', syncupload),
    path('syncdownload/', syncdownload),
    path('receivesync/', receivesync),
    path('ping/', pingview),
    url(r'^', include(router.urls)),
    #url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    path('admin/', admin.site.urls),
    path('lastsync/', views.LastSyncView.as_view())
]

