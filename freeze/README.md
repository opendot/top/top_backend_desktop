## Freezing the Django app with PyInstaller

Lib files to be modified in order to make PyInstaller works:
- `tog_backend\venv\Lib\site-packages\PyInstaller\hooks\rthooks\pyi_rth_django.py`
- `tog_backend\venv\Lib\site-packages\PyInstaller\hooks\hook-django.py`
- `tog_backend\venv\Lib\site-packages\tobii_research.py`

The PyInstaller command must be executed from the partent directory of the backend repository (that should be named after the django app directory name, i.e. `tog_backend`).
Before running PyInstaller, activate the virtual environment, e.g. assuming the name of the virualenv to be `venv`:
```
.\tog_backend\venv\Scripts\activate
```
The PyInstaller command is the following:
```
pyinstaller --paths .\tog_backend\venv\Lib\site-packages --add-data "tog_backend\tog\templates;templates" --name=top tog_backend\manage.py
```

Note: after the generation of the `dist` folder, according to the state of the database at the time the commad was executed, it could be necessary to run (deactivate the virtualenv first):
```
.\dist\top\top.exe migrate
.\dist\top\top.exe getgames
.\dist\top\top.exe createsuperuser
```

## Creating setup.exe with Inno Setup
Copy the following files and directories inside the working directory (the one that contains `dist`, the output of PyInstaller):

- `nopro_driver` (copied from the repo `tog_backend`)
- `static` (copied from the repo `tog_backend`)
- `chrome-win32` (chromium distribution)
- `TOP.bat`
- `TOP.ico`
- `TOP.vbs`
- `splashscreen.hta`
- `splashscreen.jpg`
- `vc_redist.x64.exe` (Microsoft Visual C++ Redistributable Packages for Visual Studio 2015)
- `setup.iss` (Inno Setup script)

The tree of the working directory now looks like this:
```
- workingdir
	- dist\
		- top\
			- top.exe
			- [...]
	- nopro_driver\
	- static\
	- chrome-win32\
		- chrome.exe
		- [...]
	- vc_redist.x64.exe
	- TOP.bat
	- TOP.ico
	- TOP.vbs
	- splashscreen.hta
	- splashscreen.jpg
	- setup.iss
```

Run the Inno Setup script, the output `TOP_setup.exe` will be created in an `Output` folder inside the working directory.