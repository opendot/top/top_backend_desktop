/*
  This class represents a cubic Bézier curve.
  getPointAtParameter() method works the same as bezierPoint().
  Points returned from this method are closer to each other
  at places where the curve bends and farther apart where the
  curve runs straight.
  On the orther hand, getPointAtFraction() and getPointAtLength()
  return points at fixed distances. This is useful in many scenarios:
  you may want to move an object along the curve at some speed
  or you may want to draw dashed Bézier curves.
*/


class BezierCurve {
  
  constructor(a,b,c,d,rtnLastPoint) {

    this.SEGMENT_COUNT = 100;
  
  // private PVector v0, v1, v2, v3;
    this.v0 = a; // curve begins here
    this.v1 = b;
    this.v2 = c;
    this.v3 = d; // curve ends here

    //the optional last boolean argument specify if the last point is returned
    (rtnLastPoint != null) ? this.rtnLastPoint = rtnLastPoint : this.rtnLastPoint = true;
  
    this.arcLengths = []; // there are n segments between n+1 points
  
    this.curveLength;
    
    // The idea here is to make a handy look up table, which contains
    // parameter values with their arc lengths along the curve. Later,
    // when we want a point at some arc length, we can go through our
    // table, pick the place where the point is going to be located and
    // interpolate the value of parameter from two surrounding parameters
    // in our table.

    // we will keep current length along the curve here
    var arcLength = 0;
    
    var prev = new p5.Vector();
    prev = this.v0;
    
    // i goes from 0 to SEGMENT_COUNT
    for (let i = 0; i <= this.SEGMENT_COUNT; i++) {
      
      // map index from range (0, this.SEGMENT_COUNT) to parameter in range (0.0, 1.0)
      var t = i / this.SEGMENT_COUNT; 
      
      // get point on the curve at this parameter value
      var point = this.pointAtParameter(t);
      
      // get distance from previous point
      var distanceFromPrev = p5.Vector.dist(prev, point);
      
      // add arc length of last segment to total length
      arcLength += distanceFromPrev;
      
      // save current arc length to the look up table
      this.arcLengths[i] = arcLength;
      
      // keep this point to compute length of next segment
      prev = point;
    }
    
    // Here we have sum of all segment lengths, which should be
    // very close to the actual length of the curve. The more
    // segments we use, the more accurate it becomes.
    this.curveLength = arcLength;
  }
  //--------------------------------------------------------------------------------------------------------------------
  
  // Returns the length of this curve
  length() {
    return this.curveLength;
  }
  
  
  // Returns a point along the curve at a specified parameter value.
  pointAtParameter(t) {
    var result = new p5.Vector();
    result.x = bezierPoint(this.v0.x, this.v1.x, this.v2.x, this.v3.x, t);
    result.y = bezierPoint(this.v0.y, this.v1.y, this.v2.y, this.v3.y, t);
    result.z = bezierPoint(this.v0.z, this.v1.z, this.v2.z, this.v3.z, t);
    return result;
  }

  
  
  // Returns a point at a fraction of curve's length.
  // Example: pointAtFraction(0.25) returns point at one quarter of curve's length.
  pointAtFraction(r) {
    var wantedLength = this.curveLength * r;
    return this.pointAtLength(wantedLength);
  }
  
  
  // Returns a point at a specified arc length along the curve.
  pointAtLength(wantedLength) {
    wantedLength = constrain(wantedLength, 0.0, this.curveLength);
    
    // look up the length in our look up table
    var index = java.util.Arrays.binarySearch(this.arcLengths, wantedLength);
    
    var mappedIndex;
    
    if (index < 0) {
      // if the index is negative, exact length is not in the table,
      // but it tells us where it should be in the table
      // see http://docs.oracle.com/javase/7/docs/api/java/util/Arrays.html#binarySearch(float[], float)
      
      // interpolate two surrounding indexes
      var nextIndex = -(index + 1);
      var prevIndex = nextIndex - 1;
      var prevLength = this.arcLengths[prevIndex];
      var nextLength = this.arcLengths[nextIndex];
      mappedIndex = map(wantedLength, prevLength, nextLength, prevIndex, nextIndex);
      
    } else {
      // wanted length is in the table, we know the index right away
      mappedIndex = index;
    }
    
    // map index from range (0, SEGMENT_COUNT) to parameter in range (0.0, 1.0)
    var parameter = mappedIndex / this.SEGMENT_COUNT;
    
    return this.pointAtParameter(parameter);
  }
  

  // Returns an array of equidistant point on the curve
  equidistantPoints(howMany) {
    
    // PVector[] resultPoints = new PVector[howMany];
    var resultPoints = [];
    
    // we already know the beginning and the end of the curve
    resultPoints[0] = this.v0;
    resultPoints[howMany - 1] = this.v3; 
    
    var arcLengthIndex = 1;
    for (let i = 1; i < howMany - 1; i++) {
      
      // compute wanted arc length
      var fraction = i / (howMany - 1);
      var wantedLength = fraction * this.curveLength;
      
      // move through the look up table until we find greater length
      while (wantedLength > this.arcLengths[arcLengthIndex] && arcLengthIndex < this.arcLengths.length) {
        arcLengthIndex++;
      }
      
      // interpolate two surrounding indexes
      var nextIndex = arcLengthIndex;
      var prevIndex = arcLengthIndex - 1;
      var prevLength = this.arcLengths[prevIndex];
      var nextLength = this.arcLengths[nextIndex];
      var mappedIndex = map(wantedLength, prevLength, nextLength, prevIndex, nextIndex);
      
      // map index from range (0, SEGMENT_COUNT) to parameter in range (0.0, 1.0)
      var parameter = mappedIndex / this.SEGMENT_COUNT;
      
      resultPoints[i] = this.pointAtParameter(parameter);
    }
    
    if(!this.rtnLastPoint) resultPoints.splice(resultPoints.length-1,1);
   
    return resultPoints;
  }
  
  
  // Returns an array of points on the curve.
  points(howMany) {
    
    var resultPoints = [];
    
    // we already know the first and the last point of the curve
    resultPoints[0] = this.v0;
    resultPoints[howMany - 1] = this.v3;
    
    for (let i = 1; i < howMany - 1; i++) {
      
      // map index to parameter in range (0.0, 1.0)
      var parameter = i / (howMany - 1);
      
      resultPoints[i] = pointAtParameter(parameter);
    }
    
    if(!this.rtnLastPoint) resultPoints.splice(resultPoints.length-1,1);

    return resultPoints;
  }
  
}