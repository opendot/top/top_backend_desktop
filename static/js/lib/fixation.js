Fixation = function (frameRate,fixationTime,cb) {
    this.fixationTime = fixationTime;
    this.frameRate = frameRate;
    this.cb = cb;
    this.tg = [];
    this.counter = 0;
    this.log = false;


    this.tgtGazed = function (tgtId) {
        var tg = this.tg.find(el=>el.id==tgtId);
        if (tg == undefined) {
            this.tg.push({id:tgtId,isGazed:true,wasGazed:true});
            //start gazing
            if(this.log) console.log("start gazing the target ",tgtId);
            monit.sendEvent({ts: new Date().getTime(), type: "event", event_type: "start_fixation", data: tgtId});
            this.counter = 0;
        } else {
            tg.isGazed = true;
            if (tg.wasGazed) {
                //keep gazing
                if (this.counter >= this.fixationTime) {
                    //target triggered
                    if(this.log) console.log("target ",tg.id," triggered");
                    monit.sendEvent({ts: new Date().getTime(), type: "event", event_type: "reach_fixation_time", data: tg.id});
                    this.cb(tg.id);
                    this.tg = [];
                    this.counter = 0;
                }
            } else {
                //start gazing
                if(this.log) console.log("start gazing the target ",tg.id);
                monit.sendEvent({ts: new Date().getTime(), type: "event", event_type: "start_fixation", data: tg.id});
                this.counter = 0;
            }
            tg.wasGazed = true;
        }
        this.counter += 1000/this.frameRate;
    };

    this.tgtNotGazed = function (tgtId) {
        var tg = this.tg.find(el=>el.id==tgtId);
        if (tg == undefined) {
            this.tg.push({id:tgtId,isGazed:false,wasGazed:false});
        } else {
            tg.isGazed = false;
            if (tg.wasGazed) {
                //stop gazing a target
                if(this.log) console.log("stop gazing the target ",tg.id);
                monit.sendEvent({ts: new Date().getTime(), type: "event", event_type: "end_fixation", data: tg.id});
                this.counter = 0;
            }
            tg.wasGazed = false;
        }
    }

    this.init = function () {
        this.tg = [];
        this.counter = 0;
    }

    this.setFixationTime = function (fixationTime) {
        this.fixationTime = fixationTime;
    }
}