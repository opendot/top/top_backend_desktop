//=============================================================
// one dimensional

// // usage example:
// filterX =  new movingAverageVect(5);
// filterY =  new movingAverageVect(5);
// -----------------------------------
// character.posX = filterX.getNextValue(gazeX);
// character.posY = filterY.getNextValue(gazeY);

//the argument is the size of the moving average window
movingAverage = function(size) {

	this.data = [];
	this.size = size;
	this.total = 0;
	this.average = 0;
	this.p = 0;
	this.n = 0;

	for(let i=0;i<this.size;i++) {
		this.data[i] = 0;
	}
}

movingAverage.prototype.getNextValue = function(value) {

	this.total -= this.data[this.p];
	this.data[this.p] = value;
	this.total += value;
	this.p = ++this.p % this.size;
	if(this.n < this.size) this.n++;
	this.average = this.total / this.n;	

	return this.average;
}

//=============================================================
// version using p5 Vector

// // usage example:
// filters =  new movingAverageVect(5);
// -----------------------------------
// var newPos = filters.getNextValue(createVector(gazeX,gazeY));
// character.posX = newPos.x;
// character.posY = newPos.y;

//the argument is the size of the moving average window (equal for x and y)
movingAverageVect = function(size) {

	this.data = createVector([],[]);
	this.size = size;
	this.total = createVector(0,0);
	this.average = createVector(0,0);
	this.p = createVector(0,0);
	this.n = createVector(0,0);

	for(let i=0;i<this.size;i++) {
		this.data.x[i] = 0;
		this.data.y[i] = 0;
	}
}

movingAverageVect.prototype.getNextValue = function(value) {

	this.total.x -= this.data.x[this.p.x];
	this.total.y -= this.data.y[this.p.y];

	this.data.x[this.p.x] = value.x;
	this.data.y[this.p.y] = value.y;

	this.total.x += value.x;
	this.total.y += value.y;

	this.p.x = ++this.p.x % this.size;
	this.p.y = ++this.p.y % this.size;

	if(this.n.x < this.size) this.n.x++;
	if(this.n.y < this.size) this.n.y++;

	this.average.x = this.total.x / this.n.x;	
	this.average.y = this.total.y / this.n.y;	

	return this.average;
}