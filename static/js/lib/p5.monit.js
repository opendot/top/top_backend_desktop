
class State{

    constructor() {

        /* PUBLIC FIELDS */
        this.loaded = false;
        this.period = 66;
        this.images = {};
        this.trackerActive = true;
        this.gaze = [0,0];
        this.dirty = false;
        /* ---------end fields--------- */

        let me = this;
        this.mode = 'eye';
        this.internalState = {};
        this.lastState = _.cloneDeep(this.internalState);
 
        this.pState = {};
        this.counterUuid;
        this.attentionSig = 0;
        this.eyeTrackerToggle = 1;
        // FIXATION TIME
        this.fixationTime = 500;
        // scene of the game
        this.oldGameScene;
        this.GameScene;
        this.skipIntro;
        this.shuffle;
        // level of the game
        this.GameLevel;
        // json data
        this.json = {};
        this.animationRate = 2;
        this.speedTarget = 5;
        this.loadCharEnable = true;
        // element dimensions
        this.elDimensions = 1; 
        this.isReceivingGaze = 0;
        this.isReceivingGazeTm = 0;
        this.isGazeOver = 0;
        this.animationFramesAmount = 4;
        this.eyeTracker = {
            posX: window.innerWidth / 4, 
            posY: window.innerHeight / 4,
            sizX: 20,
            sizY: 20,
            sizCharX: 0,
            sizCharY: 0,
            easing: 0.2,
            contacted: []
        };
        this.idealRes = {width: 1920, height: 1080};
        this.screenScaleFactor = getScreenScaleFactor(this.idealRes.width, this.idealRes.height);
        this.itemsNames = {};
        this.levelsJson = {};
        this.sounds = {};
               /* EVENT MESSAGE LISTENERS */
        // these can be overridden to fit your needs
        this.onAddTarget = (x,y,type) => {
            this.addTarget(x*window.innerWidth,y*window.innerHeight, type)
        };
        this.onRemoveTarget = (id) => {
            this.removeTargetById(id)
        };
        this.onMoveTarget = (ob) => {
            this.moveTarget(ob) 
        };
        this.onToggleTracker = (val) => {
            if(val == 1) this.trackerActive = true;
            else this.trackerActive = false;
        };
        this.onEndSession= () => {
            console.log("end session")
            let getUrl = window.location;
            let baseUrl = getUrl .protocol + "//" + getUrl.host
            // window.location = baseUrl + "/qr"
            window.location = baseUrl + "/app/homepage/"
        };
        this.onDisconnectApp = () => {
            console.log('disconnect mobile client');
            let getUrl = window.location;
            let baseUrl = getUrl .protocol + "//" + getUrl.host
            // window.location = baseUrl + "/qr"
            window.location = baseUrl + "/qr"
        }
        this.onAttentionSig= () => {
            console.log("attention sig");
        };
        /* ---------end listeners--------- */


        this.handler = {
            set: function(obj, prop, value) {
                // if(obj.)
                console.log(obj, prop, value)
                obj[prop] = value;
                return true;
            }
        }
         /* SOCKETS */
        // TRACKER SOCKET
        this.trackerwsConnect()

        // EVENT WEBSOCKET
        this.eventwsConnect()

        /* ---------end sockets--------- */

        // trigger dumpstate     
        setTimeout(this.dumpState.bind(this),this.period);
        // index of target animation
        this.targetsAnimIndx = [];
        this.distractorsAnimIndx = [];
        this.counter = 0;
        this.loaded = 0;
        this.imagesNumber = {counter: 0, amount: 0};
        this.soundsNumber = {counter: 0, amount: 0};
        // array of staring objects
        this.fixationObjects = [];
        // setInterval(()=>{
            // me.eyeTrackerToggle = !me.eyeTrackerToggle;
        // }, 1000);
        this.init();
    }
      // tracker ws connection
    trackerwsConnect() {

        //instanciate
        this.trackerconnection = new WebSocket("ws://localhost:5001");

        // on open
        this.trackerconnection.onopen = () => {
            this.trackerconnection.send(JSON.stringify({type:"room",data:"desktop"}));
            console.log("hello tracker!");
            this.eyeTrackerToggle = 1;
        }

        //on close
        this.trackerconnection.onclose = (err) => {
            console.log("bye tracker!", err);
            this.eyeTrackerToggle = 0;
            setTimeout(()=>{this.trackerwsConnect()},3000)
        };

         //on message
        this.trackerconnection.onmessage = (message) => {
            const msg = JSON.parse(message.data);
            switch(msg.type){
                case "gaze":
                    this.gaze = msg.data;
                    this.isReceivingGazeTm = new Date().getTime();
                    if(!this.isReceivingGaze) this.isReceivingGaze = 1
                    // passing gaze data to eyetracker obj
                    if(this.mode == 'eye'){
                        this.eyeTracker.posX = this.gaze[0] * window.innerWidth;
                        this.eyeTracker.posY = this.gaze[1] * window.innerHeight;
                    }
                    break;
                case "last_client":
                    this.onDisconnectApp();
                    break;
                default:
                    break
            }
        };

        //on error
        this.trackerconnection.onerror = function (err) {console.log("error tracker!", err)};

    }

    // tracker ws connection
    eventwsConnect() {

        //instanciate
        this.eventconnection = new WebSocket("ws://localhost:5002");

        //on open
        this.eventconnection.onopen = () => {
            this.eventconnection.send(JSON.stringify({type:"room",data:"desktop"}));
            setTimeout(()=>{
                this.started = true;

                // check the session name, if different from "preview", send a start_Session message
                let params = (new URL(document.location)).searchParams;
                let session = params.get("session");
                if(session != "preview"){
                    this.eventconnection.send(JSON.stringify({type:"start_session",data:session}));
                    this.trackerconnection.send(JSON.stringify({type:"start_session",data:session}));
               }
            },500)
            setTimeout(this.dumpState.bind(this),this.period);
            console.log("hello events!")
        }

        //on close
        this.eventconnection.onclose = (err) => {
            console.log("bye events!", err)
            setTimeout(()=>{this.eventwsConnect()},3000)
        };

        //on message
        this.eventconnection.onmessage = (message) => {
            const msg = JSON.parse(message.data)
            switch(msg.type){
                case "event":
                    console.log('messaggione', msg)
                    switch(msg.event_type){
                        case "add_target":
                            console.log('addTarget', msg.data.type);
                            this.onAddTarget(msg.data.x, msg.data.y, msg.data.type);
                            break;
                        case "remove_target":
                             this.onRemoveTarget(msg.data.id);
                             break;
                        case "move_target":
                             this.onMoveTarget(msg.data);
                             break;
                        case "toggle_tracker":
                            this.onToggleTracker(msg.data);
                            break;
                        case "end_session":
                            this.onEndSession();
                            break;
                        case "attention_sig":
                            this.onAttentionSig();
                            break;
                        case "change_dynamics":
                            this.onChangeDynamics(msg.data)
                            break;
                        case "back_scene":
                            this.onBackNext("back");
                            break;
                        case "next_scene":
                            this.onBackNext("next");
                            break;
                        case "toggle_fatigue":
                            this.onToggleFatigue();
                            break;
                        default:
                            break;
                    }
                    break;
                default:
                    break
            }
        };

        //on error
        this.eventconnection.onerror = function (err) {console.log("error events!", err)};
    }

    getMonitoringPeriod(ms){
        return this.period;
    }

    setMonitoringPeriod(ms){
        this.period = ms;
    }

    init(){
        this.pState.targets = {};
        this.pState.distractors = {};
        this.pState.res = [window.innerWidth,window.innerHeight];
        this.dumpState()
        this.counterUuid = 0;
        // settings passed by URL
        let args = (new URL(location.href)).searchParams;
        this.GameLevel = parseInt(args.get('level') !== null ? args.get('level') : 0);
        this.GameScene = 0;
        this.skipIntro = args.get('skipintro') == 'true' ? true : false;
        this.fixationTime = args.get('ft') !== null ? parseInt(args.get('ft')) : this.fixationTime;
        this.oldGameScene = 0;
        this.dirty = true;
        // this.onChangeDynamic({characterFollow: args.get('characterFollow')})
        // console.log('parametri gioco', this.GameLevel, this.skipIntro, this.eyeTracker['easing']);
    }
    loadJsonItems(){
        let me = this;
        var currChar;
        fetch('./gamespecs.json')
            .then(response => {
                return response.json();
            })
            .then(data => {         
                me.json = _.cloneDeep(data);
                console.log(me.json);
                me.imagesNumber.amount = data.details.ingame_assets.length;
                console.log('amount assets', me.imagesNumber.amount);
                // load all IMAGES
                data.details.ingame_assets.forEach( d =>{
                    // console.log(d.name, d.sizX, d.sizY)
                    if(d.name.indexOf('char') !== -1){
                        var defaultChar = 'char-A'; 
                        if(localStorage.character !== undefined && me.loadCharEnable) {
                            var defaultChar = d.name.replace('char', localStorage.character.replace('.png', ''));
                        } else {
                            var defaultChar = d.name.replace('char', defaultChar);
                        }
                        // console.log(defaultChar);
                        me.addImage(
                            location.origin + '/app/imgs/characters/' + defaultChar, 
                            d.name
                        );
                    }else{
                        me.addImage(
                            'img/' + d.name, 
                            d.name
                        );
                    }
                });
                // load all SOUNDS and Tone.js objects
                if(data.details.ingame_sounds !== undefined){
                    me.soundsNumber.amount = data.details.ingame_sounds.length;
                    console.log('amount sounds', me.soundsNumber.amount);
                    data.details.ingame_sounds.forEach( s => {
                        me.addSound(s)
                    });
                    me.levelsJson = data.levels;
                }
            });
    }
    setCharacterSize(x, y){
        this.eyeTracker.sizCharX = x;
        this.eyeTracker.sizCharY = y;
    }


    setBg(name){
        if(!name.startsWith('#') && (!this.images[name] || this.images[name] == "null")){
            console.error(name + " not loaded");
            return;
        }
        this.pState.bg = name;
        //background(this.images[name]);
    }
    setProcBg(col){
        this.setBg(col);
    }

    drawBg(){
        if(this.pState.bg && this.pState.bg != "null"){
            if(this.pState.bg.startsWith("#")){
                background(color(this.pState.bg))
            }
            else{
                imageMode(CORNER);
                background(this.images[this.pState.bg])
            }
        }
    }

    // deprecated, use drawBg in any case
    drawPrBg(){
        if(this.pState.bg && this.pState.bg != "null" && this.pState.bg.startsWith("#")){
            background(color(this.pState.bg))
        }
    }


    addImage(url, name){
        let me = this;
        loadImage(url,function(data){
            me.images[name] = data;
            me.imagesNumber.counter++;
            if(me.imagesNumber.counter == me.imagesNumber.amount && me.soundsNumber.counter == me.soundsNumber.amount){
                me.loaded = 1;
                console.log('caricato tutto', me.images, me.sounds);
            }
        })
    }

    addSound(url){
        this.sounds[url] = {src: url, pl: new Tone.Player({'url': 'Audio/' + url, 'autostart': false, 'loop': false}), gain: new Tone.Gain({gain: 1}).toMaster(), interval: undefined};
        this.sounds[url].pl.connect(this.sounds[url].gain);
        this.soundsNumber.counter++;
        if(this.soundsNumber.counter == this.soundsNumber.amount && this.imagesNumber.counter == this.imagesNumber.amount){
            this.loaded = 1;
            console.log('caricato tutto', this.sounds, this.images);
        }
    }

    addTarget(posX,posY,img, zIndex, col, sizX, sizY){
        if(zIndex == undefined) zIndex = 0
        let obj= {posX:posX, posY:posY,id:uuid(this), rot: 0, zIndex: zIndex, correct:true};
        var tgJs = this.json.details.ingame_assets.filter( d => d.name == img );
        if(tgJs.length && (!sizX || !sizY)){
            obj.sizX = tgJs[0].sizX * this.screenScaleFactor;
            obj.sizY = tgJs[0].sizY * this.screenScaleFactor;
        } else if (sizX && sizY) {
            obj.sizX = sizX;
            obj.sizY = sizY;
        }

        if(img != "rect" && img!="ellipse"){

            if (this.images[img]){
                if(img.includes('char') && localStorage.character !== undefined && this.loadCharEnable) {
                    obj.char = localStorage.character.slice(5,6);
                }
                obj.img = img;
                // if(sizX == null){
                //     obj.sizX = this.images[img].width;
                //     obj.sizY = this.images[img].height;
                // }
            }
            else {
                console.error(img + " not loaded");
                return null;
            }
        }
        else {
            obj.img = img;
            obj.col = col;
            obj.sizX = sizX;
            obj.sizY = sizY;
        }
        this.pState.targets[obj.id] = obj;
        this.itemsNames[obj.id] = obj.img;
        return this.pState.targets[obj.id];
    }
    

    addDistractor(posX,posY,img, zIndex, col, sizX, sizY){
        if(zIndex == undefined) zIndex = 0
        var obj = {posX:posX, posY:posY,id:uuid(this), rot: 0, zIndex: zIndex};
        var tgJs = this.json.details.ingame_assets.filter( d => d.name == img );
        if(tgJs.length && (!sizX || !sizY)){
            obj.sizX = tgJs[0].sizX * this.screenScaleFactor;
            obj.sizY = tgJs[0].sizY * this.screenScaleFactor;
        } else if (sizX && sizY) {
            obj.sizX = sizX;
            obj.sizY = sizY;
        }
        if(img != "rect" && img!="ellipse"){

            if (this.images[img]){
                if(img.includes('char') && localStorage.character !== undefined && this.loadCharEnable) {
                    obj.char = localStorage.character.slice(5,6);
                }
                obj.img = img;
                // if(sizX == null){
                //     obj.sizX = this.images[img].width;
                //     obj.sizY = this.images[img].height;
                // }
            }
            else {
                console.error(img + " not loaded");
                return null;
            }
        }
        else {
            obj.img = img;
            obj.col = col;
            obj.sizX = sizX;
            obj.sizY = sizY;
        }
        this.pState.distractors[obj.id] = obj;
        this.itemsNames[obj.id] = obj.img;
        return this.pState.distractors[obj.id];
    }

    animateTarget(obj, suff, framesAmount){
        var id = obj.id;
        framesAmount = framesAmount === undefined ? this.animationFramesAmount : framesAmount;
        if(this.counter % this.animationRate == 0){
            if(this.targetsAnimIndx[id] === undefined){
                this.targetsAnimIndx[id] = {id: id, inc: 1, val:1};
            }
            var counter = this.targetsAnimIndx[id].val;
            if(counter == framesAmount){
                this.targetsAnimIndx[id].inc = -1;
            }else if(counter == 1){
                this.targetsAnimIndx[id].inc = 1;
            } 
            this.targetsAnimIndx[id].val += this.targetsAnimIndx[id].inc;
            suff = suff === undefined ? '' : suff;
            this.pState.targets[id].img = this.itemsNames[id].replace('.png', '') + suff + '-' + counter + '.png';
        }
    }

    animateDistractor(obj, suff, framesAmount){
        var id = obj.id;
        framesAmount = framesAmount === undefined ? this.animationFramesAmount : framesAmount;
        if(this.counter % this.animationRate == 0){
            if(this.distractorsAnimIndx[id] === undefined){
                this.distractorsAnimIndx[id] = {id: obj.id, inc: 1, val:1};
            }
            var counter = this.distractorsAnimIndx[id].val;
            if(counter == framesAmount){
                this.distractorsAnimIndx[id].inc = -1;
            }else if(counter == 1){
                this.distractorsAnimIndx[id].inc = 1;
            }           
            this.distractorsAnimIndx[id].val += this.distractorsAnimIndx[id].inc;
            // this.pState.distractors[id].img = this.pState.distractors[id].name.split('.')[0] + suff + '-' + counter + '.png';
            suff = suff === undefined ? '' : suff;
            this.pState.distractors[id].img = this.itemsNames[id].replace('.png', '') + suff + '-' + counter + '.png';
        }
    }

    drawTargets(){ 
        this.drawItems(_.values(this.pState.targets).sort(orderElementsCB));
    }

    drawDistractors(){
        this.drawItems(_.values(this.pState.distractors).sort(orderElementsCB));
    }

    drawAllItems(){ 
        this.drawItems(_.values(this.pState.targets).concat(_.values(this.pState.distractors)).sort(orderElementsCB));
    }

    playSound(name, now){ 
        if(this.sounds[name] !== undefined){
            var rule = now != undefined ? 1 : this.sounds[name].pl.state !== 'started'; 
            if(rule){
                this.sounds[name].gain.gain.value = 1;
                this.sounds[name].pl.start();
                // send message to event socket when play sound
                this.sendEvent({ts: new Date().getTime(), type: 'event', event_type: 'play_sound', data: name})   
            }
        }
    }
    sendEvent(obj){
        if(this.eventconnection.readyState == 1){
            this.eventconnection.send(JSON.stringify(obj))
        }
    }
    stopSound(name){
        var me = this;
        if(this.sounds[name] !== undefined){
            if(this.sounds[name].pl.state === 'started'){
                if(this.sounds[name].interval == undefined){
                    this.sounds[name].interval = setInterval(()=>{
                        if(me.sounds[name].gain.gain.value <= 0.105){
                            me.sounds[name].pl.loop = false;
                            me.sounds[name].gain.gain.value = 0;
                            me.sounds[name].pl.stop();
                            // me.sounds[name].gain.gain.value = 1;
                            clearInterval(me.sounds[name].interval);
                            me.sounds[name].interval = undefined;
                            return 
                        }else{
                            me.sounds[name].gain.gain.value -= 0.1;
                        }   
                    }, 33);
                }
            }
        }
    }
    silence(){
        _.values(this.sounds).forEach(s =>{
            if(s.pl.state == 'started'){
                s.pl.stop();
                s.pl.loop = false;
            }
        })
    }

    drawItems(items){
        let me = this;
        items.forEach(function(t,i){

            switch(t.img){

                case "rect":
                    push();
                    translate(t.posX,t.posY);
                    rotate(t.rot);
                    rectMode(CENTER);
                    noStroke();
                    fill(t.col);
                    rect(0,0,t.sizX,t.sizY);
                    pop();
                    break;

                case "ellipse":
                    push();
                    translate(t.posX,t.posY);
                    rotate(t.rot);
                    ellipseMode(CENTER);
                    noStroke();
                    fill(t.col);
                    ellipse(0,0,t.sizX,t.sizY);
                    pop();
                    break;

                default:
                    if(!me.images[t.img] || me.images[t.img] == null) {
                        console.error(t.img +" not loaded");
                        return;
                    }
                    if(t.sizX && t.sizY) {
                        push();
                        translate(t.posX,t.posY);
                        rotate(t.rot);
                        imageMode(CENTER);
                        image(me.images[t.img], 0, 0, t.sizX, t.sizY);
                        pop();                        
                    }
                    else {
                        push();
                        translate(t.posX,t.posY);
                        rotate(t.rot);
                        imageMode(CENTER);
                        image(me.images[t.img], 0, 0);
                        pop();  
                    }
            }
        })
    }

    drawTracker() {
        fill('rgba(255,0,0, 0.25)');
        if(this.mode == 'mouse'){
            this.eyeTracker.posX = mouseX;
            this.eyeTracker.posY = mouseY;
            ellipse(this.eyeTracker.posX, this.eyeTracker.posY, 20,20);
        }else{
            ellipse(this.gaze[0]*window.innerWidth, this.gaze[1]*window.innerHeight, 20,20);
        }
        noFill();
    }

    removeTarget(obj){
        delete this.pState.targets[obj.id];
        _.remove(this.targetsAnimIndx, d => d == null);
        _.remove(this.targetsAnimIndx, el => el.id.toString() == obj.id);
        delete this.itemsNames[obj.id];
    }

    removeTargetById(id){
        delete this.pState.targets[id];
        _.remove(this.targetsAnimIndx, d => d == null);
        _.remove(this.targetsAnimIndx, el => el.id.toString() == id);
        delete this.itemsNames[id];
    }
    removeDistractor(obj){
        delete this.pState.distractors[obj.id];
        _.remove(this.distractorsAnimIndx, d => d == null);
        _.remove(this.distractorsAnimIndx, el => el.id.toString() == obj.id);
        delete this.itemsNames[obj.id];
    }
    removeDistractorById(id){
        delete this.pState.distractors[id];
        _.remove(this.distractorsAnimIndx, d => d == null);
        _.remove(this.distractorsAnimIndx, el => el.id.toString() == id);
        delete this.itemsNames[id];
    }

    clearTargets(){
        let me = this;
        var tgs = _.cloneDeep(this.getTargets());
        tgs.forEach( el => {
            me.removeTarget(el)
        });
        this.pState.targets = {};
    }
    clearDistractors(){
        let me = this;  
        var ds = _.cloneDeep(this.getDistractors());
        ds.forEach( el => {
            me.removeTarget(el)
        });
        this.pState.distractors = {};
    }
    getTargets(){
        return _.values(this.pState.targets)
    }
    getDistractors(){
        return _.values(this.pState.distractors)
    }
    getDistractorByName(dis){
        return _.find(this.getDistractors(), d => this.itemsNames[d.id] === dis);
    }
    // get specified target
    getTargetByName(tg){
        return _.find(this.getTargets(), d => this.itemsNames[d.id] === tg);
    }
    moveTarget(obj){
        let myobj = this.pState.targets[obj.id]
        myobj.posX = obj.x * window.innerWidth
        myobj.posY = obj.y * window.innerHeight
    }

    //the argument toWhat specify to what kind of item the item is to be converted (it will be assumed that it starts from the other kind)
    //the item will change index
    convertItem(id,toWhat){
        var newItem = {};
        switch(toWhat) {
            case "target":
                var item = this.getDistractors().find(el=>el.id === id);
                newItem = this.addTarget(item.posX,item.posY,item.img, item.zIndex, item.col, item.sizX, item.sizY);
                newItem.rot = item.rot;
                newItem.sizX = item.sizX;
                newItem.sizY = item.sizY;
                this.removeDistractor(item);
                break;
            case "distractor":
                var item = this.getTargets().find(el=>el.id === id);
                newItem = this.addDistractor(item.posX,item.posY,item.img, item.zIndex, item.col, item.sizX, item.sizY);
                newItem.rot = item.rot;
                newItem.sizX = item.sizX;
                newItem.sizY = item.sizY;
                this.removeTarget(item);
                break;
            default: 
                console.error(toWhat," is not a valid item type (valid types are 'target' or 'distractor')");
                break;
        }
        return newItem;
    }

    dumpState(){
        // console.log('dumpstate', this.internalState);
        if(this.dirty){            
            let currdiff = DeepDiff.diff(this.lastState, this.pState);
            if(this.gaze[0] > 1 || this.gaze[0] < 0 || this.gaze[1] > 1 || this.gaze[1] < 0){this.isGazeOver = 1;}
            else {this.isGazeOver = 0;} 
            if((new Date().getTime()) - this.isReceivingGazeTm > 1000 && this.isReceivingGaze == 1){
                this.isReceivingGaze = 0;
            }
            if(currdiff && currdiff !== null && this.loaded && this.started) {
                currdiff.forEach(function(d){
                    d.ts = new Date().getTime();
                })
                this.sendEvent({type:"render",data:currdiff})
                this.lastState = _.cloneDeep(this.pState);
            }
            // this.dirty = false;
            setTimeout(this.dumpState.bind(this), this.period);
        }
    }
    monitoring(){
        push();
        fill(0);
        text(frameRate(),10,10);
        text('targets: ' + Object.keys(this.pState.targets).length, 10, windowHeight - 10); 
        text('distractors: ' + Object.keys(this.pState.distractors).length, 100, windowHeight - 10); 
        pop();
    }
    onChangeDynamics(data){
        console.log('data', data);
        var type = data.id;
        switch(type){
            case 'ft':
                this.fixationTime = parseInt(data.value);
                break;
        }
    }
    onBackNext(button){
        switch(button){
            case "back":
                console.log("back button pressed");
                break;
            case "next":
                console.log("next button pressed");
                break;
            default:
                break;
        }
    }
    onToggleFatigue(){
        console.log("fatigue mode toggled");
    }
}


function difference(object, base) {
    function changes(object, base) {
        return _.transform(object, function(result, value, key) {
            if (!_.isEqual(value, base[key])) {
                result[key] = (_.isObject(value) && _.isObject(base[key])) ? changes(value, base[key]) : value;
            }
        });
    }
    return changes(object, base);
}
// convert hex colors to rgba
function hexToRgbA(hex){
    hex = hex.toString();
    var c;
    if(/^#([A-Fa-f0-9]{3}){1,2}$/.test(hex)){
        c= hex.substring(1).split('');
        if(c.length== 3){
            c= [c[0], c[0], c[1], c[1], c[2], c[2]];
        }
        c= '0x'+c.join('');
        return [(c>>16)&255, (c>>8)&255, c&255];
    }
    throw new Error('Bad Hex');
}
// check if two p5 items are overlapped
function checkOverlap(el1, el2){
    return el1.posX + el1.sizX  >= el2.posX &&
            el1.posX <= el2.posX + el2.sizX &&
            el1.posY + el1.sizY >= el2.posY && 
            el1.posY <= el2.posY + el2.sizY;
}
// get current timestamp
function getCurrTM(){
    return parseFloat(moment().format('x'));
}
    // check if an object is empty 
function isEmpty(obj) {
    for(var key in obj) {
        if(obj.hasOwnProperty(key))
            return false;
    }
    return true;
}
function orderElementsCB(a, b){
    if(a.zIndex < b.zIndex){
        return -1;
    }else if(a.zIndex > b.zIndex){
        return 1;
    }
}

function getScreenScaleFactor(rx, ry) {
    return getDiagonal(window.innerWidth, window.innerHeight) / getDiagonal(rx, ry);
}

function getDiagonal(x, y) {
    return Math.sqrt(x * x + y * y);
}

p5.prototype.initMonit = function () {
    p5.prototype.monit = new State();
};

p5.prototype.registerMethod('init', p5.prototype.initMonit);
// add counter incremented every loop
p5.prototype.incCounter = function(){
    p5.prototype.monit.counter = p5.prototype.monit.counter == 99 ? 0 : p5.prototype.monit.counter + 1;
    // console.log('ora', p5.prototype.monit.counter);
}
p5.prototype.registerMethod('post', p5.prototype.incCounter);
function uuid(t) {
    t.counterUuid++;
    return t.counterUuid.toString() + 't'; 
}

