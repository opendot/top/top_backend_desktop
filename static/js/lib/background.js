Bg = function (frameRate) {
    this.from;
    this.to;
    this.original;
    this.darkened;
    this.percVar = 50;
    this.duration = 500; //ms
    this.time = 0;
    this.current = 0;
    this.init = function() {
        this.original = 0;
        this.darkened = 0.5;
        this.time = 0;
        this.from = this.darkened;
        this.to = this.original;
        this.w = windowWidth*2;
        this.h = windowHeight*2;
        this.inc = this.duration / (this.duration / 33);
        this.rect = monit.addDistractor(this.w / 2, this.h / 2, 'rect', -999, 'rgba(0,0,0,0)', this.w, this.h);
    }

    this.run = function() {
        if(!monit.getDistractors().filter(d => d.id == this.rect.id).length) {
            this.rect = monit.addDistractor(this.w / 2, this.h / 2, 'rect', -999, 'rgba(0,0,0,0)', this.w, this.h);
        }
        
        if( this.time < (this.duration) - this.inc && this.current != this.to) {
            this.time += this.inc;
            this.current = map(this.time, 0, this.duration, this.from, this.to);
            this.rect.col = 'rgba(0,0,0,' + this.current + ')';
        } else {
            this.current = this.to;
            this.rect.col = 'rgba(0,0,0,' + this.current + ')';
            this.time = 0;
        }
    }

    this.fade = function() {
        this.from = monit.trackerActive ? this.darkened : this.original;
        this.to = monit.trackerActive ? this.original : this.darkened;
    }

    //to be run after a call to monit.setBg()
    this.update = function() {
        this.init();
    }
    
    this.init();
}