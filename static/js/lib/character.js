// CHARACTER OBJ
//4 animations 
Character = function(c, animations, visible){
    // this.animations = animations;
    // this.animations = svgs;
    this.visible = visible;
    // animations object for the character given from outside
    this.animationsCbs = animations;
    this.currAnimation = 'char.png';
    // create sprite and his settings
    this.currState = '';
    if(this.visible){
        this.char = monit.addDistractor(c.posX, c.posY, "char" + this.currState + '.png');
    }else{
        this.char = {
            id: 'invisible_char',
            posX: c.posX,
            posY: c.posY,          
            sizX: monit.eyeTracker.sizX,
            sizY: monit.eyeTracker.sizY,
            img: ''
        }
    }
    

    // this.char = createSprite(this.sizX, this.sizY);
    // console.log(this.char)
    this.char.posX = c.posX;
    this.char.posY = c.posY;
    this.counterLimits = 0;
    this.palette = ['#b55', '#5b5', '#55b', '#58b', '#8b5'];
    this.marginOffx = 200;
    // this.sizX = c.sizX;
    // this.sizY = c.sizY;
    // this.posX = this.char.posX;
    // this.posY = this.char.posY;
    // previous positions
    this.pposX = this.char.posX;
    this.pposY = this.char.posY;
    // canvas width and height 
    this.canvW = window.innerWidth; 
    this.canvH = window.innerHeight;
    // tail of the character
    this.tail = [{x: this.char.posX, y: this.char.posY}];
    // array of animation to perform
    this.trackingAnim = [];
    // this.animationSet = '';
    let args = (new URL(location.href)).searchParams;
    monit.eyeTracker.easing = args.get('characterFollow') !== null ? 20 / parseFloat(args.get('characterFollow')) : monit.eyeTracker.easing;

    var me = this;
    // setInterval(function(){
        // me.attentionTrigger();
    //     monit.addTarget(random(windowWidth), random(windowHeight), 100,100, targetsList[Math.round(random(targetsList.length) - 1)].name);
    // }, 1000);
} 
// CHANGE CHARACTER ANIMATION
Character.prototype.changeAnimation = function(an){
    console.log(this.currState, an, an.length)
    var anim = 'char' + (this.currState.length == 1 ? '-' + an : an) + '.png';
    if(this.currAnimation !== anim){
        this.currAnimation = anim;
        var char = _.find(monit.getDistractors(), d => d.name = monit.itemsNames[this.char.id] + this.currState + '.png');
        char.img = anim;
    }
}

// DRAW ANIMATIONS (executed as many times as necessary)
// syntax for animations object
// type -> string name of the animation
// index -> index used for gradual animation
// pos -> position of the animation element
// timestamp -> time in which the animation started
// end -> animation duration

Character.prototype.drawAnimations = function(){
    var me = this;
    // console.log(this.animationSet, me.animationSet);
    this.trackingAnim.forEach( function(el, i) {
        if(getCurrTM() - el.timestamp < el.end){
            me.animationsCbs[el.type].cb(me, el, me.animationSet);
        }else{
            if(el.type == 'attention') {
                this.resetRotationTgsDs();      
            }
            me.trackingAnim.splice(i, 1);
            // me.changeAnimation('idle' + me.animationSet);
        }
        // console.log(me.trackingAnim)
    });
    if(this.visible){
        if(monit.fixationObjects.length > 0){
            // this.changeAnimation('-happy');
            monit.animateDistractor(this.char, '-hurra')
        }else if(this.trackingAnim.filter(d => d.type === 'limitsReached').length > 0){
            this.changeAnimation(this.currState + '-sad');
        }else if(!monit.trackerActive){ 
            this.changeAnimation(this.currState + '-off');
        }else if(Math.abs(this.pposX - this.char.posX) > 4 || 
            Math.abs(this.pposY - this.char.posY) > 4){
            if(this.pposX <= this.char.posX){
                monit.animateDistractor(this.char, '-right-spostamento');
            }
            if(this.pposX >= this.char.posX){
                monit.animateDistractor(this.char, '-left-spostamento');
            }
            this.currAnimation = 'moving'
            // idle animation
        }else{
            // console.log('we are here', this.char.img, this.currState, this.currAnimation)
            if(this.char.img !== this.currAnimation) this.char.img = this.currAnimation;
            this.changeAnimation(this.currState);
            // console.log('idle', _.find(monit.getDistractors(), d => d.name = 'character').img);
        }
    }
}

// CHECK IF OVER LIMITS
Character.prototype.checkLimit = function(){
    // reaching screen limits
    if(this.char.posX >= this.canvW - (this.char.sizX / 2) || this.char.posX <= (this.char.sizX / 2) ){
        if(!this.trackingAnim.filter(d => d.type === 'limitsReached').length){
            if(monit.isReceivingGaze || !monit.isGazeOver){
                var obj = _.cloneDeep(this.animationsCbs['limitsReached'].obj)
                obj.timestamp = getCurrTM();            
                obj.param = 'x';
                obj.posX = this.char.posX >= windowWidth - this.marginOffx ? windowWidth : 0;
                obj.posY = this.char.posY;
                this.char.posX = this.char.posX >= windowWidth - this.marginOffx ? windowWidth - (this.char.sizX / 2) : this.char.sizX / 2;         
                obj.color = this.palette[parseInt(random(this.palette.length))];
                this.trackingAnim.push(obj);
                this.counterLimits++;
            }else{
                monit.eyeTracker.posX = monit.eyeTracker.posX < windowWidth / 2 ? this.marginOffx : windowWidth - this.marginOffx;
            }
        }
    }else if(this.char.posY <= (this.char.sizY / 2) || this.char.posY >= this.canvH - (this.char.sizY / 2)){
        if(!this.trackingAnim.filter(d => d.type === 'limitsReached').length){
            if(monit.isReceivingGaze || !monit.isGazeOver){
                var obj = _.cloneDeep(this.animationsCbs['limitsReached'].obj)
                obj.timestamp = getCurrTM();    
                obj.param = 'y';
                obj.posY = this.char.posY >= windowHeight - this.marginOffx ? windowHeight : 0;
                obj.posX = this.char.posX;
                this.char.posY = this.char.posY >= windowHeight - this.marginOffx ? windowHeight - (this.char.sizY / 2) : this.char.sizY / 2;     
                obj.color = this.palette[parseInt(random(this.palette.length))];
                this.trackingAnim.push(obj);
                this.counterLimits++;
            }else{
                monit.eyeTracker.posY = monit.eyeTracker.posY < windowHeight / 2 ? this.marginOffx : windowHeight - this.marginOffx;
            }
        }
    }
}
// CHECK IF OVERLAP WITH TARGETS
Character.prototype.checkInteractionTargets = function(){
    tgs = {};
    Object.assign(tgs, monit.getTargets()); 
    for(var x in tgs){
        var Tg = tgs[x];
        var eyeT = monit.eyeTracker;
        // if detect contact between character and target, remove it
        var offxOverlap = 0;
        var obj = {posX: this.char.posX + (tgs[x].sizX / 4), posY: this.char.posY + (tgs[x].sizY / 4), sizX: this.char.sizX, sizY: this.char.sizY};
        // rectMode(CENTER);
        // rect(this.char.posX + offxOverlap, this.char.posY + offxOverlap, this.char.sizX - offxOverlap * 2, this.char.sizY - offxOverlap * 2);
        if(checkOverlap(obj, Tg)){
            var tgPos = {x: (this.char.posX + Tg.posX) / 2, y: (this.char.posY + Tg.posY) / 2};
            if(monit.fixationObjects.filter(d => d.id === Tg.id).length === 0){
                if(!this.trackingAnim.filter(a => a.type == 'targetOverlap' && a.id == Tg.id).length){
                    // adding target animation
                    var temp = _.cloneDeep(this.animationsCbs['targetOverlap'].obj);
                    temp.id = Tg.id;
                    temp.pos = tgPos;
                    temp.timestamp = getCurrTM();
                    this.trackingAnim.push(temp); 
                }
                // add target to the interacted list
                monit.fixationObjects.push({id: Tg.id, counter: 0});
            }else{
                var temp = _.find(monit.fixationObjects, d => d.id === Tg.id);
                temp.counter += 33.33;
            }
        }else{
            _.remove(monit.fixationObjects, d => d.id === Tg.id);
        }
    }
}
Character.prototype.checkInteractionDistractors = function(){
    let me = this;
    var locals = monit.getDistractors();
    // locals.forEach( function(el, i) {
    //     if(!checkOverlap(monit.eyeTracker, el)){
    //         distractorEaseMove(monit.distractorsDirs[el.id], monit.distractorsDirs[el.id].direction);
    //         el.posX += monit.distractorsDirs[el.id].sx;
    //         el.posY += monit.distractorsDirs[el.id].sy;
    //         el.img = el.name;
    //     }else{
    //         me.changeAnimation('happy' + me.animationSet);
    //         monit.animateDistractor(el);
    //     }
    // });
}

// DRAW TAIL CHARACTER 
Character.prototype.drawCharacterTail = function(){
    var maxLen = 40;
    if (Math.abs(this.pposX - this.char.posX) > 10 || Math.abs(this.pposY - this.char.posY) > 10) {
        this.tail.push({
            x: this.char.posX,
            y: this.char.posY,
            indx: 0
        });
    }
    if (this.tail.length == maxLen || this.tail.filter(d => d.indx > maxLen * 2).length > 0) {
        this.tail.splice(0, 1);
    }
    for (var x = 1; x < this.tail.length; x++) {
        strokeWeight(this.tail[x].indx * 2);
        this.tail[x].indx += 2;
        var col = 'rgba(0, 0, 0, ' + (x / this.tail.length).toFixed(2) + ')';
        // console.log(col)
        stroke('rgba(200, 200, 200,' + (-this.tail[x].indx + (maxLen * 2) + 5) / maxLen + ')')
        line(this.tail[x].x, this.tail[x].y, this.tail[x - 1].x, this.tail[x - 1].y)
        noStroke();
    }
}
// ATTENTION TRIGGER ANIMATION
Character.prototype.attentionTrigger = function(){
    console.log('attention executing')
    var temp = _.cloneDeep(this.animationsCbs['attention'].obj)
    temp.timestamp = getCurrTM();
    temp.index = 255;
    this.trackingAnim.push(temp);
    monit.playSound('richiamo.mp3', 1);
}
Character.prototype.insidePadding = function(){
    if(this.char.posX > (this.char.sizX / 2) + this.marginOffx &&
        this.char.posX < this.canvW - (this.char.sizX / 2) - this.marginOffx && 
        this.char.posY > (this.char.sizY / 2) + this.marginOffx &&
        this.char.posY < this.canvH - (this.char.sizY / 2) - this.marginOffx){
        this.counterLimits = 0;
    }
}
Character.prototype.constrainMe = function(top, right, bottom, left){
        // horizontal
    if(this.char.posX - (this.char.sizX / 2) < left){
        this.char.posX = left + (this.char.sizX / 2);
    }else if(this.char.posX + (this.char.sizX / 2) > right){
        this.char.posX = right - (this.char.sizX / 2);
    }
    // vertical
    if(this.char.posY - (this.char.sizY / 2) < top){
        this.char.posY = top + (this.char.sizY / 2);
    }else if(this.char.posY + (this.char.sizY / 2) > bottom){
        this.char.posY = bottom - (this.char.sizY / 2);
    }
}

Character.prototype.toggleCharacter = function(){
        console.log('inside toggle', this.currState);
        if(this.visible){
            if(_.find(this.trackingAnim, a => a.type == 'attention') != undefined){
                resetRotationTgsDs();
            }
            var tmp = {
                id: 'invisible_char',
                posX: this.char.posX,
                posY: this.char.posY,          
                sizX: monit.eyeTracker.sizX,
                sizY: monit.eyeTracker.sizY,
                img: ''
            }
            monit.removeDistractor(this.char);
            this.char = tmp;
            this.visible = false;
        }else{
            var tmp = this.char;
            this.char = monit.addDistractor(tmp.posX, tmp.posY, "char" + (this.currState.length == 1 ? '-' + this.currState : this.currState) + '.png');
            this.visible = true;
        }
    };
// DRAW CHARACTER
Character.prototype.draw = function(){
    // instead of mouse, eyetracker
    if(!this.trackingAnim.filter(d => d.type === 'limitsReached').length){
        this.pposX = this.char.posX;
        this.pposY = this.char.posY;
        if(monit.trackerActive){
            if(this.visible){
                this.char.posX += (monit.eyeTracker.posX - this.char.posX) * monit.eyeTracker.easing;
                this.char.posY += (monit.eyeTracker.posY - this.char.posY) * monit.eyeTracker.easing;
            }else{
                this.char.posX = monit.eyeTracker.posX
                this.char.posY = monit.eyeTracker.posY
            }
        }
        this.posX = this.char.posX;
        this.posY = this.char.posY;
    }
    if(this.visible){
        if(this.counterLimits < 3){
            this.checkLimit();
        }else{
            this.constrainMe(
                0, 
                this.canvW, 
                this.canvH,
                0
                )
            this.insidePadding()
        }
        this.drawCharacterTail(); 
    }
    this.checkInteractionTargets();
    this.checkInteractionDistractors();
    // fill('black');
    // rectMode(CENTER);
    // rect(this.char.posX, this.char.posY, this.sizX, this.sizY)
    // noFill()
    
    this.drawAnimations();
}
