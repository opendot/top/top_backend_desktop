//state machine
const stateMachine = [
    init,
    play
];
const INIT = 0;
const PLAY = 1;
let currentState = 0;
let gazeX,gazeY;
const myFrameRate = 30;
let movements = [];
let movementsStack = [];
let charEng = {};
let bgEng = {};
const charactersSpecs = [
    {
        img: 'char-A.png',
        posX: 0.19,
        posY: 0.58,
        siz:500,
        zIndex:2,
        default: true
    },
    {
        img: 'char-B.png',
        posX: 0.34,
        posY: 0.37,
        siz:400,
        zIndex:1,
        default: false
    },
    {
        img: 'char-C.png',
        posX: 0.65,
        posY: 0.37,
        siz:400,
        zIndex:1,
        default: false
    },
    {
        img: 'char-D.png',
        posX: 0.78,
        posY: 0.65,
        siz:500,
        zIndex:2,
        default: false
    }
];

function preload() {
    overrideMonit();
    monit.loadJsonItems();
}

function setup() {
    createCanvas(windowWidth,windowHeight);
    noStroke();
    frameRate(myFrameRate);
    monit.setBg('#a7e4f9');
    monit.fixationTime = 1000;
    monit.animationRate = 3;
}

//do nothing until the flag monit.started is false
function checkFlag() {
    if(!monit.started || !monit.loaded) {
       setTimeout(checkFlag, 100); /* this checks the flag every 100 milliseconds*/   
    }
}

function draw() {
    if(!monit.started || !monit.loaded) {
        setTimeout(checkFlag, 100); /* this checks the flag every 100 milliseconds*/
        background('black');
        textSize(30);
        stroke('white');
        fill('white')
        textAlign(CENTER, CENTER);
        text('Caricamento in corso...', windowWidth/2, windowHeight/2); 
    } else {
        monit.drawBg();
        if (currentState < stateMachine.length) {
            stateMachine[currentState]();
        }
    }
}

function init() {
    charEng = new CharactersEngine(charactersSpecs);
    bgEng = new BackgroundEngine();
    monit.addDistractor(windowWidth/2,windowHeight/2,"sfondo.png",5, null, windowWidth, windowHeight);
    monit.addDistractor(windowWidth/2,windowHeight/6.35,"logo-01.png");
    currentState = PLAY;
}

function play() {
    gaze();
    monit.drawAllItems();
    bgEng.run();
    charEng.run();
    moveElements();
}

function overrideMonit() {
    // --------- OVERRIDE TRACKER WEBSOCKET CONNECTION --------------
    // tracker ws connection

     //on message
    monit.trackerconnection.onmessage = (message) => {
        const msg = JSON.parse(message.data);
        // console.log('tracker message', msg, 'here')
        switch(msg.type){
            case "gaze":
                monit.gaze = msg.data;
                monit.isReceivingGazeTm = new Date().getTime();
                if(!monit.isReceivingGaze) monit.isReceivingGaze = 1
                // passing gaze data to eyetracker obj
                if(monit.mode == 'eye'){
                    monit.eyeTracker.posX = monit.gaze[0] * window.innerWidth;
                    monit.eyeTracker.posY = monit.gaze[1] * window.innerHeight;
                }
                break;
            case "last_client":
                monit.onDisconnectApp();
                break;
            case "new_session":
                monit.clearTargets();
                monit.clearDistractors();
                monit.setBg('#000');
                changeLocation(msg.data);
                break;
            case "run_calibration":
                console.log(msg);
                goToCalibration();
                break;
            default:
                break
        }
    };
    monit.loadJsonItems = function() {
        let me = this;
        fetch('./gamespecs.json')
            .then(response => {
                return response.json();
            })
            .then(data => {         
                me.json = _.cloneDeep(data);
                console.log(me.json);
                me.imagesNumber.amount = data.details.ingame_assets.length;
                console.log('amount assets', me.imagesNumber.amount);
                // load all IMAGES
                data.details.ingame_assets.forEach( d =>{
                    if(d.name.startsWith('char')) {
                        me.addImage(location.origin + '/app/imgs/characters/' + d.name, d.name);
                    } else {
                        me.addImage('img/' + d.name, d.name);
                    }
                });
                // load all SOUNDS and Tone.js objects
                if(data.details.ingame_sounds !== undefined){
                    me.soundsNumber.amount = data.details.ingame_sounds.length;
                    console.log('amount sounds', me.soundsNumber.amount);
                    data.details.ingame_sounds.forEach( s => {
                        me.addSound(s)
                    });
                    me.levelsJson = data.levels;
                }
            });
    }
}

function gaze() {
    if (monit.trackerActive) {
        gazeX = monit.mode == 'eye' ? monit.gaze[0] * window.innerWidth : mouseX;
        gazeY = monit.mode == 'eye' ? monit.gaze[1] * window.innerHeight : mouseY;
    }
}

// CLASSES /////////////////////////////////////////////////////////

Character = function(posX, posY, img, siz, zIndex) {
    this.posX = posX ? posX*windowWidth : undefined;
    this.posY = posY ? posY*windowHeight : undefined;
    this.siz = siz ? siz : undefined;
    this.zIndex = 10 + zIndex;
    this.tgt = undefined;
    this.img = img ? img : undefined;
    this.doCheckOverlap = false;
    this.fixationCounter = 0;
    this.posXMem = this.posX;
    this.bouncing = false;
    this.bouncingCounter = 0;

    this.init = function () {
        this.tgt = monit.addTarget(this.posX,this.posY,this.img,this.zIndex);
        this.tgt.sizX = this.siz;
        this.tgt.sizY = this.siz;
    }

    this.run = function() {
        if(this.doCheckOverlap) this.checkOverlap();
        this.animate();
        this.bounce();
    }

    this.checkOverlap = function() {
        if (gazeX > this.tgt.posX - this.tgt.sizX / 3.7 && 
            gazeX < this.tgt.posX + this.tgt.sizX / 3.7 && 
            gazeY > this.tgt.posY - this.tgt.sizY / 2 && 
            gazeY < this.tgt.posY + this.tgt.sizY / 2) {
            this.focused();
        } else {
            this.fixationCounter = 0;
        }
    }

    this.animate = function() {
        if(Math.floor(this.tgt.posX) < Math.floor(this.posXMem)) {
            monit.animateTarget(this.tgt,"-left-spostamento");
            this.stopBouncing();
        } else if (Math.floor(this.tgt.posX) > Math.floor(this.posXMem)) {
            monit.animateTarget(this.tgt,"-right-spostamento");
            this.stopBouncing();
        } else {
            if(!this.bouncing){
                this.tgt.img = this.img;
                if(random() > 0.995) this.bouncing = true;
            }
        }
        this.posXMem = this.tgt.posX;
    }

    this.bounce = function() {
        if(this.bouncing) {
            monit.animateTarget(this.tgt,"-hurra");
            this.bouncingCounter++;
            if(this.bouncingCounter >= 30) {
                this.stopBouncing();
            }
        }
    }

    this.stopBouncing = function() {
        this.bouncingCounter = 0;
        this.bouncing = false;
    }

    this.focused = function() {
        this.fixationCounter += 1000/myFrameRate;
        if (this.fixationCounter < monit.fixationTime) {
            push();
            stroke(200);
            fill(255,100)
            ellipse(this.tgt.posX,this.tgt.posY,100,100);
            fill(255,210);
            arc(this.tgt.posX,this.tgt.posY,100,100,-PI/2+map(this.fixationCounter,50,monit.fixationTime,0,2*PI),3/2*PI,PIE);
            pop();
        } else {
            this.selected();
        }
    }

    this.selected = function() {
        this.fixationCounter = 0;
        charEng.selectCharById(this.tgt.id);
    }

    this.init();
}

CharactersEngine = function(charactersSpecs) {
    this.characters = [];
    this.selectedChar = undefined;
    this.spotlight = {
        posX: windowWidth/2,
        posY: windowHeight*0.6,
        siz: 850 * monit.screenScaleFactor
    }

    this.init = function() {
        charactersSpecs.forEach((char)=>{
            this.characters.push(new Character(char.posX,char.posY,char.img,char.siz*monit.screenScaleFactor,char.zIndex));
            if((localStorage.character == undefined && char.default == true) || localStorage.character == char.img) {
                this.selectCharById(this.characters.find(el=>el.img == char.img).tgt.id);
            }
        })
    }

    this.run = function() {
        this.characters.forEach((char)=>{
            char.run();
        })
    }

    this.selectCharById = function(id) {
        let char = this.characters.find(el=>el.tgt.id == id);
        let animDuration = 2000;
        localStorage.character = char.img;
        //char.doCheckOverlap = false;
        this.inibitAllCheckOverlapButId(id,animDuration);

        if(this.selectedChar) {
            initMovement(this.selectedChar.tgt,{
                endPosX: this.selectedChar.posX,
                endPosY: this.selectedChar.posY,
                endSizX: this.selectedChar.siz,
                endSizY: this.selectedChar.siz,
                startZIndex: this.selectedChar.zIndex,
                duration:animDuration,
                type: 'sin'
            })
            //this.selectedChar.doCheckOverlap = true;
        }

        this.selectedChar = char;

        initMovement(char.tgt,{
            endPosX: this.spotlight.posX,
            endPosY: this.spotlight.posY,
            endSizX: this.spotlight.siz,
            endSizY: this.spotlight.siz,
            startZIndex: char.zIndex,
            duration:animDuration,
            type: 'sin'
        })
    }

    this.inibitAllCheckOverlapButId = function(id,animDuration) {
    	this.characters.forEach(char=>{
    		char.doCheckOverlap = false;
    	})
    	setTimeout(()=>{
    		this.characters.forEach(char=>{
	    		if(char.tgt.id != id) char.doCheckOverlap = true;
	    	})
    	},animDuration)
    }

    this.init();
};

BackgroundEngine = function() {
    this.clouds = ["nuvola1.png","nuvola2.png","nuvola3.png","nuvola4.png"];
    this.activeClouds = [];

    this.init = function() {
        for(let i=0; i<4; i++) {
            this.activeClouds.push(new Cloud(
                this.clouds[randomNumber(0,this.clouds.length-1)],
                randomNumber(0,300),
                randomNumber(0,3),
                randomNumber(0,windowWidth)));
        }
    };

    this.run = function() {
        this.generateClouds();
        this.activeClouds.forEach((cloud)=>{
            cloud.run();
            if(cloud.ds.posX <= -cloud.ds.sizX/2) {
                _.remove(this.activeClouds, d => d.ds.id == cloud.ds.id);
                cloud.destroy();
            }
        });
    };

    this.generateClouds = function() {
        if(random() > 0.98) {
            this.activeClouds.push(new Cloud(
                this.clouds[randomNumber(0,this.clouds.length-1)],
                randomNumber(0,300),
                randomNumber(0,3)));
        }
    };

    this.init();
}

Cloud = function(img,posY,zIndex,posX) { //zIndex must be [0;3]
    this.img = img;
    this.posX = posX != undefined ? posX : windowWidth*1.2;
    this.posY = posY;
    this.zIndex = zIndex;
    this.speeds = [50,100,150,200]; // px/s
    this.ds = {};

    this.init = function() {
        this.ds = monit.addDistractor(this.posX,this.posY,this.img,this.zIndex);
    };

    this.run = function() {
        this.ds.posX -= this.speeds[this.zIndex] / myFrameRate;
    };

    this.destroy = function() {
        monit.removeDistractor(this.ds);
    };

    this.init();
}

function randomNumber(min, max) {
    return Math.floor(Math.random() * (max - min) + min);
}

// ANIMATIONS ///////////////////////////////////////////////
movements['sin'] = {
    cb: function(el){
        el.prog += el.movDescr.step;
        var index = (1+sin(PI*el.prog-PI/2))/2;
        el.item.posX = el.movDescr.startPosX +  index * el.movDescr.distPosX;
        el.item.posY = el.movDescr.startPosY +  index * el.movDescr.distPosY;
        el.item.sizX = el.movDescr.startSizX +  index * el.movDescr.distSizX;
        el.item.sizY = el.movDescr.startSizY +  index * el.movDescr.distSizY;
        el.item.rot = el.movDescr.startRot +  index * el.movDescr.distRot;
        el.item.zIndex = el.movDescr.startZIndex;
        if(el.prog >= 0.95 && el.movDescr.startSizX < el.movDescr.endSizX) el.item.zIndex = 30;
    },
    obj:{
        type: 'sin',
        item:{},
        movDescr:{},
        prog:0
    }
}
function initMovement(item,movDescr) {
    movDescr.prog = 0;
    movDescr.step = 1000/(myFrameRate * movDescr.duration);

    //POSITION
    if(movDescr.startPosX == null) movDescr.startPosX = item.posX;
    if(movDescr.endPosX == null) movDescr.endPosX = item.posX;
    if(movDescr.startPosY == null) movDescr.startPosY = item.posY;
    if(movDescr.endPosY == null) movDescr.endPosY = item.posY;

    movDescr.distPosX = movDescr.endPosX - movDescr.startPosX;
    movDescr.distPosY = movDescr.endPosY - movDescr.startPosY;

    //SIZE
    if(movDescr.startSizX == null) movDescr.startSizX = item.sizX;
    if(movDescr.endSizX == null) movDescr.endSizX = item.sizX;
    if(movDescr.startSizY == null) movDescr.startSizY = item.sizY;
    if(movDescr.endSizY == null) movDescr.endSizY = item.sizY;

    movDescr.distSizX = movDescr.endSizX - movDescr.startSizX;
    movDescr.distSizY = movDescr.endSizY - movDescr.startSizY;

    //ROTATION
    if(movDescr.startRot == null) movDescr.startRot = item.rot;
    if(movDescr.endRot == null) movDescr.endRot = item.rot; 

    movDescr.distRot = movDescr.endRot - movDescr.startRot;

    var obj = _.cloneDeep(movements[movDescr.type].obj);
    obj.id = item.id;
    obj.item = item;
    obj.movDescr = movDescr;
    movementsStack.push(obj);

    return obj;
}

function moveElements() {
    movementsStack.forEach((movObj, i)=>{
        if(movObj.prog < 1){
            movements[movObj.type].cb(movObj);
        } else {
            _.remove(movementsStack, d => d.id == movObj.id && d.type == movObj.type)
         }
    })
}

function changeLocation(data){
    let getUrl = window.location;
    let baseUrl = getUrl .protocol + "//" + getUrl.host
    let gameurl = baseUrl + data.gameurl
    gameurl += "?session="  + data.session
    gameurl += "&skipintro="  + data.skipIntro
    gameurl += "&level="  + data.level
    gameurl += "&round="  + data.round
    data.dynamics.forEach(function(d){
        gameurl += "&" + d.id + "=" + d.value
    })

    window.location.replace(gameurl);
}

function goToCalibration(){
    let getUrl = window.location;
    let baseUrl = getUrl .protocol + "//" + getUrl.host
    let calibrateUrl = baseUrl+"/app/calibration/"
    window.location.replace(calibrateUrl);
}