var animations = [],
backgroundImg = '#ffecc6',
// array if animations to draw
animationsStack = [],
// type of game (rocket, submarine, car)
GameSceneTypeFix = 0,
// game stage ('choose vehicle' -> 'game-fixation' -> 'game-transition-animation' -> 'game-return-path')
GameSceneStage = 0,
GameTransitionAnimStep = 0,
indx = 0,// index of rocket rotation
pathsJsons = {},    // car paths objects
// jump sin index and frequency
jumpSinIndx = 0, positionToReach = 0,
// VEHICLE
RoutingItem = function(ar){
    this.type = '';
    this.group = ar;
    this.number = 0;
    this.name = '';
    this.suff = '';
    this.setName = function(t, number, suff){
        if(suff !== undefined) this.suff = suff;
        this.number = number;
        this.type = t;
        this.name = this.type + '-' + this.group[this.number] + this.suff + '.png'
    };
    this.getName = function(){
        this.name = this.type + '-' + this.group[this.number] + '.png'
        return this.name;
    } 
    this.setNext = function(){
        this.number = this.number == this.group.length - 1 ? 0 : this.number + 1;
        this.name = this.type + '-' + this.group[this.number] + this.suff + '.png'
    }
},
// CURRENT VEHICLE
currVehicle = new RoutingItem(['A', 'B', 'C']),
// CURRENT ITEM TO FOLLOW
currItem = new RoutingItem(['A', 'B', 'C']),
currStar = new RoutingItem(['A', 'B', 'C']),
// BUBBLES GENERATOR OBJECT
Bubbles = {
    x: 0,
    y: 0,
    size: 4,
    body: [],
    interval: {bottom: 300, top: 3000}, 
    counter: 0,
    bubbleLife: 2000,
    speed: 2,
    bubble: function(x, y){
        this.body.push({x: x, y: y, time: 0});
    },
    draw: function(x, y){
        if(monit.trackerActive){
            var me = this;
            if(this.counter > random(this.interval.bottom, this.interval.top)){
                this.bubble(x, y);
                this.counter = 0;
            } 
            this.body.forEach((el, i)=>{
                if(el.time > me.bubbleLife){
                    me.body.splice(i, 1);
                }else{
                    push();
                    stroke('black');
                    fill('rgba(255, 255, 255, 0.4)')
                    strokeWeight(0);
                    ellipse(el.x, el.y, 40);
                    pop();
                    el.y -= me.speed;
                    el.x += random(-0.5, 0.5);
                    el.time += 33;
                }
            });
            this.counter += 33;
        }
    }
},
// dimensions of all assets of the game
assetsDim = {},
// flag loading all car path json 
pathsJsonsFl = 0,
// started fixation
fixationMoved = 0,
// array of settings 
tabletSettings = [];
// set stars of rocket fixation
currStar.setName('stella', 0);
var crTgSp = (new URL(location.href)).searchParams.get('speedTarget');
var currentTargetSpeed = parseFloat(crTgSp == undefined ? 5 : crTgSp);
// PULSE ANIMATION
var Pulse = function(obj){
    this.obj = obj;
    this.refX = obj.sizX;
    this.refY = obj.sizY;
    this.sInd = 0;
    this.amp = 10;
    this.draw = function(obj){
        this.sInd = this.sInd > 2 * PI ? 0 : this.sInd + 0.15;
        return [this.refX + this.amp * sin(this.sInd), this.refY + this.amp * sin(this.sInd) * (this.refY / this.refX)];

    }
},
HorizontalMove = function(obj){
    this.obj = obj;
    this.refX = obj.posX;
    this.sInd = 0;
    this.amp = 10;
    this.draw = function(obj){
        this.sInd = this.sInd > 2 * PI ? 0 : this.sInd + 0.15;
        return this.refX + this.amp * sin(this.sInd);
    }
},
startCarPosX = 0,
currDirection = (new URL(location.href)).searchParams.get('carDirection') == 'true' ? 'right' : 'left',
// CAR ABLE TO RUN ON CURVED SVG 
Car = function(distractor){
    this.currX = 200;
    this.obj = distractor
    this.oldP = 0;
    this.line = [];
    this.path = [];
    this.tail = [];
    this.counter = 0;
    this.ind = 0;
    this.dir = currDirection;
    this.currentSpeed = currentTargetSpeed;
    // this.oldInd;
    this.windowY = this.obj.sizY + 100;
    this.windowX = this.obj.sizX + 100;
    this.anW = {startX:200, endX: windowWidth, startY: (windowHeight / 2), endY: windowHeight};
    this.analyse = function(){
        var curr = getCurrTM();
        console.log('prima analisi');
        for (var x = this.anW.startX; x < this.anW.endX; x += 3) {
            for (var y = this.anW.startY; y < this.anW.endY; y += 3) {
                var currP = get(x, y);
                if(this.oldP[0] == 193 && currP[0] == 238){
                // if(currP[0] < 100){
                    this.line.push({
                        x: x,
                        y: y,
                        val: currP
                    });
                    x++;
                    this.oldP = currP;
                    break;
                }
                this.oldP = currP;
            }
        }
        // console.log('finito', this.line)
        console.log('dopo analisi', getCurrTM() - curr);
    };
    this.createPath = function(nPath){
            for(var x = 0; x < windowWidth; x += 3){
                switch(nPath){
                    case 1:
                        this.path.push({x: x, y: 604});
                        break;
                    case 2:
                        this.path.push({x: x, y: duneFun(x)})
                        break;
                    case 3:
                        this.path.push({x: x, y: -duneFun(x)})
                        break;
            }
        }
    };
    this.resetLine = function(){
        this.line = [];
        this.ind = 0;
        this.anW.startX = this.obj.posX;
        this.anW.endX = this.anW.endX + 500;
    };
    this.followLine = function(){
        farFromLimit = this.dir == 'right' ? this.ind < this.line.length - (currentTargetSpeed * 2) : this.ind > (currentTargetSpeed * 2)
        if(this.counter % 1 === 0 && farFromLimit){
            this.obj.posX = this.line[this.ind].x;
            this.obj.posY = this.line[this.ind].y;

            let signAngle = this.line[this.ind].y - this.line[this.ind].y > 0 ? -1 : 1,
            diffX = Math.abs(this.line[this.ind].x - this.line[this.ind].x),
            diffY = this.line[this.ind].y - this.line[this.ind].y ,
            finalRot = Math.asin(diffY / Math.sqrt(diffX**2 + diffY**2));
            if(Math.abs(this.obj.rot - finalRot) > 0){
                this.obj.rot = finalRot * 1;
            } 
            if(this.currentSpeed < currentTargetSpeed){
                this.currentSpeed += parseInt(currentTargetSpeed / 5)  
            }
            this.ind += (this.dir == 'right' ? 1 : -1) * this.currentSpeed;
            this.counter = 0;
            // this.oldInd = this.ind;
        }else{
            // this.resetLine();
            // this.analyse();
        }
        this.counter = this.counter < 3 ? this.counter + 1 : 0;
        // here = {x: el.x, y: el.y}
    }
    // draw moved line 
    this.drawLine = function(){
        this.line.forEach( el=>{
            push();
            fill('black');
            ellipse(el.x , el.y , 10);
            pop()
        });
    }
    this.setWindow = function(sx, sy, ex, ey){
        this.anW.startX = sx;
        this.anW.startY = sy;
        this.anW.endX = ex;
        this.anW.endY = ey;
    }
    this.changeDir = function(){
        this.dir = this.dir == 'right' ? 'left' : 'right'
        currDirection = this.dir;
        this.ind = this.dir == 'right' ? 80 : this.line.length - 80;
        var currentVehicleName = currVehicle.name;
        if(monit.getTargetByName(currentVehicleName) !== undefined) monit.removeTarget(monit.getTargetByName(currentVehicleName));
        currVehicle.setName('auto', currVehicle.number, '-' +  this.dir);
        this.obj = monit.addTarget(0, 0, currVehicle.name);
        // console.log('auto-', currVehicle.number, '-' +  this.dir)
        this.obj.posX = this.line[this.ind].x;
        this.obj.posY = this.line[this.ind].y;
        console.log(monit.getTargets())
    }
    // draw the distractor moving in specified direction
    this.draw = function(){
        if(monit.trackerActive){
        // analyze all layout
            // var eyeT = monit.eyeTracker;
            if(monit.fixationObjects.filter(f => monit.itemsNames[f.id] == currVehicle.name).length){
                // console.log(monit.fixationObjects.filter(f => monit.itemsNames[f.id] == currVehicle.name)[0].counter, monit.fixationTime)
                if(monit.fixationObjects.filter(f => monit.itemsNames[f.id] == currVehicle.name)[0].counter > monit.fixationTime){
                    if(!flFixationReach){
                        flFixationReach = 1;
                        monit.sendEvent({
                            ts: new Date().getTime(), 
                            type:"event", 
                            event_type: "fixation_time_reached", 
                            data: monit.fixationObjects.filter(f => monit.itemsNames[f.id] == currVehicle.name)[0].id
                        });
                    }
                    // _.remove(monit.fixationObjects, f => f.id == getTargetByName(currVehicle.name).id);
                    fixationMoved = 1;
                    this.followLine('right');
                    if(monit.sounds['car-path.wav'].pl.state !== 'started'){
                        playLoopSound('car-path.wav');
                    }
                }
            // if(checkOverlap({posX: eyeT.posX + this.obj.sizX / 2, posY: eyeT.posY + this.obj.sizY / 2, sizX: eyeT.sizX, sizY: eyeT.sizY}, this.obj)){
                // if(Math.abs(el.y - mouseY) < 30 && el.x == mouseX){
            }else{
                this.currentSpeed = 1;
            }
        }
    }
},
firstCarPosY = function(refX){
    this.oldP = 0;
    for (var y = 0; y < windowHeight; y++) {
        var currP = get(refX, y);
        if(this.oldP[0] == 193 && currP[0] == 238){
            return y 
        }
        this.oldP = currP; 
    }
},
// circle to fixation
Circle = function(x, y, dur){
    this.ind = 0;
    this.r = 100;
    this.x = x;
    this.y = y;
    this.done = 0;
    this.inc = (2 * PI) / (dur / 33);
    this.draw = function(){
        if(this.ind < 2 * PI){
            push();
            ellipseMode(CENTER)
            noStroke();
            fill('rgba(174, 174, 174, 0.7)');
            ellipse(this.x, this.y, this.r, this.r);
            fill('rgba(0, 152, 154, 0.3)');
            arc(this.x, this.y, this.r - 5, this.r - 5, -PI/2 + this.ind, (3/2)*PI, PIE);
            pop();

            this.ind += this.inc;
        }else{
            this.done = 1;
        }
    }
},
rocketFire = {
    posX: 0,
    posY: 0,
    nCircles: 3,
    vib: 2,
    maxS: 80,
    ind: 0,
    on: function(){
        if(this.ind < this.maxS){
            this.ind += 3;
        }
    },
    off: function(){
        if(this.ind > 0){
            this.ind -= 3;
        }
    },
    draw: function(){
        if(monit.trackerActive){
            for(var x = 0; x < this.nCircles; x++){
                push();
                noStroke();
                fill('rgba(255, 180, 0, ' + map(x, 0, this.nCircles, 0.9, 0.2) + ')');
                ellipse(this.posX + random(-2,2), this.posY + x * 30 + random(-2,2), (this.ind / this.maxS) * map(x, 0, this.nCircles, this.maxS, 10))
                pop();
            }
        }
    }
},
// array of pulsating objects
pulsatingObjs = [],
surfaceMov = {},
car = {},
floorCarPosY = 10,
// current car path 
currentPath = 'auto_landscape_01.png',
// targets positions at choose game part
chooseGameTargetsStartPositions = {},
fixationGameTargetsStartPositions = {},
targetChosenPos = {},
playLoopSound;
// DISPATCH ACTION ON FIXATION SUCCESS
// cbDispatchAction;
animations['chosenVehicle'] = {
    cb: function(el){
        var maxInc = 20,    // max size increment of image
        inc = 1;    // increment step
        // inc = maxInc / (el.end / 33);    // increment step
        if(el.index > maxInc){
            inc = -1;
        }
        el.index += inc;
        monit.pState.targets[el.id].sizX += inc;
    }, 
    obj:{
        type: 'chosenVehicle',
        id: '',
        timestamp: getCurrTM(),
        index: 0,
        end: 500
    }
}
// choose game
animations['chooseGame'] = {
    cb: function(el){
        // var inc = 2000 / (el.end / 30);
        // reopening
        var games = ['razzo', 'auto', 'sottomarino'],
        engGames = ['rocket', 'car', 'submarine'],
        items = ['pianeta', '', 'conchiglia'];
        // fix orrible glitch targetOverlap animation
        if(animationsStack.filter(a => a.type == 'targetOverlap').length) _.remove(animationsStack, a => a.type == 'targetOverlap');
        if(!monit.GameScene){
            monit.setBg('sfondo-' + el.game + '.png');
            setGameState(1, 'game-fixation', games.indexOf(el.game));
            // monit.fixationTime = tabletSettings[engGames[games.indexOf(el.game)]].ft;
            // currentTargetSpeed = tabletSettings[engGames[games.indexOf(el.game)]].speedTarget;
            currItem.setName(items[games.indexOf(el.game)], 0);
            monit.fixationObjects = [];
        }
        // move target
        push()
        fill(0,0,0,0);
        stroke('#ffecc6');
        strokeWeight(4000);
        let stepCinema = el.end / 33;
        if(el.index < el.end / 10){
            ellipse(monit.pState.targets[el.ref.id].posX, monit.pState.targets[el.ref.id].posY, map(el.index, 0, el.end, 0, 4000) + 4200);
        }else if(el.index > el.end / 10 && el.index < el.end / 2){
            if(!el.indexRef){
                el.indexRef = el.index + (el.end / stepCinema);
            }
            ellipse(monit.pState.targets[el.ref.id].posX, monit.pState.targets[el.ref.id].posY, el.indexRef + 4200);
            monit.getTargets().filter(t => t.id != el.ref.id).forEach(tg =>{
                if(tg.sizX <= 30 || tg.sizY <= 30){
                    monit.removeTarget(tg);
                }else{
                    tg.sizX -= incsStart[tg.img];
                    tg.sizY -= incsStart[tg.img] * (assetsDim[tg.img].sizY / assetsDim[tg.img].sizX);
                }
            });
        }else{
            monit.getTargets().filter(t => t.id != el.ref.id).forEach( tg => {
                monit.removeTarget(tg);
            });
            ellipse(monit.pState.targets[el.ref.id].posX, monit.pState.targets[el.ref.id].posY, map(el.index, el.end / 2, el.end, el.indexRef, 4000) + 4200);
        }
        pop();
        el.index += el.end / stepCinema;
        monit.drawTargets();
    }, 
    obj:{
        type: 'chooseGame',
        ref: {},
        id: {},
        index: 0,
        timestamp: 0,
        game: '',
        end: 2000
    },
    startPositions: {},
    chooseGamePositions: {}
}
// change car path scene
animations['changeCarScene'] = {
    cb: function(el){
        // remove cactus caught animation
        _.remove(animationsStack, d => d.type == 'returnObjContact');
        if(el.index <= el.end && el.inc < 0){
            el.inc *= -1;
            el.elsToMove.forEach( element => {
                var elToChange = element.type == 'target' ? getTargetByName(element.name) : getDistractorByName(element.name);
                elToChange.posX = element.posX;
                elToChange.posY = element.posY;
            });
            el.elsToDel.forEach( toDel => {
                if(toDel.type == 'target') monit.removeTarget(toDel)
                else monit.removeDistractor(toDel); 
            });
            // if next step is a number, go to next part of animation, otherwise go to next scene stage 
            if(typeof el.nextStep == 'number') GameTransitionAnimStep = el.nextStep;
            else GameSceneStage = el.nextStep;
            // console.log(GameSceneStage, GameTransitionAnimStep)
        }
        fill(0,0,0,0);
        stroke('black')
        strokeWeight(2000)
        ellipse(windowWidth / 2, windowHeight / 2,el.index);
        noFill();
        el.index += el.inc;
    }, 
    obj:{
        type: 'changeCarScene',
        index: 5000,
        inc: 5000 / (2000 / 30) * -1,
        timestamp: 0,
        nextStep: 0,
        elsToDel: [], // list of distractors/targets name to delete 
        elsToMove: [],
        end: 2000
    }
}
// targets interaction
// animations['targetOverlap'] = { 
//     cb: function(el) {
//         let col = map(el.end, 100, 1000, 150, 50),
//         dim = map(el.end, 100, 1000, 250, 400)
//         fill(map(el.index, 0, el.end, 200, 255), map(el.index, 0, el.end, 150, col), map(el.index, 0, el.end, 150, col), 150);
//         ellipse(el.pos.x, el.pos.y, map(el.index, 0, el.end, 100, dim));
//         el.index += el.end / (el.end / 33);
//         // el.index = el.index <= 0 ? 0 : el.index - (el.end / 33);
//         // me.changeAnimation('happy' + a);
//     },
//     obj: {
//         type: 'targetOverlap', 
//         id: '', 
//         index: 0,
//         pos: {}, 
//         timestamp: 0, 
//         end: 500
//     }
// };

animations['targetOverlap'] = { 
    cb: function(el) {
        el.circle.draw();
        el.index += el.end / (el.end / 33);
    },
    obj: {
        type: 'targetOverlap', 
        id: '', 
        index: 0,
        obj: {}, 
        timestamp: 0, 
        end: 500
    }
};
// targets interaction
animations['stopTargetOverlap'] = { 
    cb: function(el) {
        let col = map(el.end, 100, 1000, 150, 50),
        dim = map(el.end, 100, 1000, 250, 400)
        fill(map(el.index, 0, el.end, 255, 200), map(el.index, 0, el.end, col, 150), map(el.index, 0, el.end, col, 150), 150);
        ellipse(el.pos.x, el.pos.y, map(el.index, 0, el.end, dim, 100));
        el.index += el.end / (el.end / 33);
        // el.index = el.index <= 0 ? 0 : el.index - (el.end / 33);
        // me.changeAnimation('happy' + a);
    },
    obj: {
        type: 'stopTargetOverlap', 
        id: '', 
        index: 0,
        pos: {}, 
        timestamp: 0, 
        end: 500
    }
};
animations['getShell'] = {
    cb: function(el){
        el.obj.posY = -400 * sin(map(el.index, 0, el.end, 0, PI)) + el.posYRef;
        el.obj.rot = map(el.index, 0, el.end, 0, 10 * PI);
        el.index += el.end / 33;
    },
    obj: {
        type: 'getShell',
        obj:{},
        index: 0,
        posYRef: 0,
        timestamp: 0,
        end: 1500
    }
}
animations['returnObjContact'] = { 
    cb: function(el) {
        fill(200, 100, 100, 150);
        ellipse(el.pos.x, el.pos.y, map(el.index, 0, el.end, 100, 400));
        el.index += el.end / (el.end / 33);
        if(el.obj != undefined){
            // console.log(el.id, el.obj.sizY, el.obj.sizX)
            // el.obj.sizX -= map(el.index, el.end - 100, 0, assetsDim[el.obj.img].sizX, 0);
            // el.obj.sizY -= map(el.index, el.end - 100, 0, assetsDim[el.obj.img].sizY, 0);
            // if(el.obj.sizX < 4 || el.obj.sizY < 4){
                if(currReachedTargets.indexOf(el.obj.id) !== -1) {
                    monit.sendEvent({ts: new Date().getTime(), type: "event", event_type: "end_fixation", data: el.obj.id});
                    _.remove(currReachedTargets, e => e == el.obj.id)
                }
                monit.removeTarget(el.obj);
                el.obj = undefined;
            // }
        }
        // me.changeAnimation('happy' + a);
    },
    obj: {
        type: 'returnObjContact', 
        id: '', 
        index: 0,
        pos: {}, 
        timestamp: 0, 
        end: 700
    }
};
var isStaring = 0;
var firstStateButtons = 0;
// CHECK INTERACTION WITH TARGETS (add/remove elements to/from fixation object array)
function checkInteractionTargets(){
    isStaring = 1;
    if(monit.trackerActive){
        monit.getTargets().forEach( Tg =>{
            // var Tg = tgs[x];
            var eyeT = monit.eyeTracker,
            currTarget = {
                posX: Tg.posX,
                posY: Tg.posY,
                sizX: Tg.sizX,
                sizY: Tg.sizY
            };// if pulsating give the right object size
            if(pulsatingObjs[Tg.id] !== undefined){
                currTarget.sizX = pulsatingObjs[Tg.id].refX;
                currTarget.sizY = pulsatingObjs[Tg.id].refY;
            }
            var eyeTrackerToOverlap = {posX: eyeT.posX + Tg.sizX / 2, posY: eyeT.posY + Tg.sizY / 2, sizX: eyeT.sizX, sizY: eyeT.sizY}
            // if(currVehicle.name.indexOf('auto') !== -1){
            //     eyeTrackerToOverlap = {posX: eyeT.posX + Tg.sizX / 2, posY: eyeT.posY + Tg.sizY / 2, sizX: eyeT.sizX, sizY: eyeT.sizY}
            // }
            // compensate offset for overlap
            if(checkOverlap(eyeTrackerToOverlap, currTarget)){
                // get ref of pulsating obj
                if(pulsatingObjs[Tg.id] !== undefined){
                    Tg.sizX = pulsatingObjs[Tg.id].refX;
                    Tg.sizY = pulsatingObjs[Tg.id].refY;   
                }
                // delete pulsating object                
                delete pulsatingObjs[Tg.id];
                if(_.find(monit.fixationObjects, d => d.id === Tg.id) == undefined){
                    // Tg.img = Tg.img.replace('.png', '-overcast.png');
                    console.log(Tg.img)
                    // add target to the interacted list
                    monit.sendEvent({ts: new Date().getTime(), type: "event", event_type: "start_fixation", data: Tg.id})
                    monit.fixationObjects.push({id: Tg.id, counter: 0});
                    // _.remove(animationsStack, a => a.type == 'stopTargetOverlap' && a.id == Tg.id);
                    var temp = _.cloneDeep(animations['targetOverlap'].obj);
                    temp.timestamp = getCurrTM();
                    temp.pos = {x: Tg.posX, y: Tg.posY};
                    temp.id = Tg.id;
                    temp.circle = new Circle(Tg.posX, Tg.posY, monit.fixationTime);
                    temp.end = monit.fixationTime;
                    animationsStack.push(temp)
                }else{
                    var temp = _.find(monit.fixationObjects, d => d.id === Tg.id);
                    temp.counter += 33.33;
                }
            }else{
                // switch off sounds
                if(GameSceneStage === 'game-fixation'){
                    if(Tg.img.indexOf('razzo') !== -1){
                        monit.stopSound('rocket-path.wav');
                        monit.sounds['rocket-path.wav'].pl.loop = false;
                    }else if(Tg.img.indexOf('sottomarino') !== -1){
                        monit.stopSound('submarine-path.wav');
                        monit.sounds['submarine-path.wav'].pl.loop = false;
                    }else if(Tg.img.indexOf('auto') !== -1){
                        monit.stopSound('car-path.wav');
                        monit.sounds['car-path.wav'].pl.loop = false;
                    }
                }
                // add reverse targetOverlap animation
                if(animationsStack.filter(a => a.type == 'targetOverlap' && a.id == Tg.id).length){
                    // addStopTargetOverlapAnimation(Tg, animationsStack.filter(a => a.type == 'targetOverlap' && a.id == Tg.id)[0].index);
                }
                if(monit.fixationObjects.filter(f => f.id == Tg.id).length > 0){
                    _.remove(monit.fixationObjects, d => d.id === Tg.id);
                    monit.sendEvent({ts: new Date().getTime(), type: "event", event_type: "end_fixation", data: Tg.id});
                    flFixationReach = 0;
                }
                _.remove(animationsStack, d => d.id == Tg.id && d.type == 'targetOverlap');
                // Tg.img = Tg.img.replace('-overcast.png', '.png')
                // add pulsating part 
                if(pulsatingObjs[Tg.id] == undefined){
                    pulsatingObjs[Tg.id] = new Pulse(Tg);
                }
                var dimTg = pulsatingObjs[Tg.id].draw();
                // console.log(Tg.sizX, Tg.sizY);
                Tg.sizX = dimTg[0];
                Tg.sizY = dimTg[1];
            }
        });
        // clean up pulsatingObj array
        for(var p in pulsatingObjs){
            if(!monit.getTargets().filter(tg => tg.id == p).length){
                delete pulsatingObjs[p];
            }
        }
    }
}
function addStopTargetOverlapAnimation(obj, index){
    if(!animationsStack.filter(a => a.type == 'stopTargetOverlap').length){
        var temp = _.cloneDeep(animations['stopTargetOverlap'].obj);
        temp.timestamp = getCurrTM();
        temp.id = obj.id;
        temp.pos = {x: obj.posX, y: obj.posY};
        temp.end = monit.fixationTime;
        temp.index = map(index, 0, temp.end, temp.end, 0)
        animationsStack.push(temp);
    }
}
// interaction return elements
function checkInteractionReturnItems(man, type){
    monit.getTargets().filter(t => t.img.indexOf(type) !== -1).forEach( Tg =>{
        // overlap with offset 
        if(checkOverlap(man, Tg)){
        // if(checkOverlap({posX: parachuteMan.posX , posY: parachuteMan.posY + (parachuteMan.sizY / 5) * 2, sizX: parachuteMan.sizX / 3, sizY: parachuteMan.sizY / 5}, Tg)){
            // if(!monit.fixationObjects.filter(d => d.id === Tg.id).length){
                // add target to the interacted list
                if(!animationsStack.filter(an => an.type == 'returnObjContact').filter(an => an.id == Tg.id).length){
                    // monit.fixationObjects.push({id: Tg.id, counter: 0});
                    // monit.sendEvent({ts: new Date().getTime(), type: "event", event_type: "reach_target", data: Tg.id})
                    var temp = _.cloneDeep(animations['returnObjContact'].obj);
                    temp.timestamp = getCurrTM();
                    temp.pos = {x: Tg.posX, y: Tg.posY};
                    temp.id = Tg.id;
                    temp.obj = Tg;
                    animationsStack.push(temp);
                    monit.playSound('return-obj.wav', 1)
                    // monit.sounds['return-obj.wav'].pl.start();
                }
            // }else{
            //     var temp = _.find(monit.fixationObjects, d => d.id === Tg.id);
            //     temp.counter += 33.33;
            // }
        // }else{
            // _.remove(monit.fixationObjects, d => d.id === Tg.id);
            // _.remove(animationsStack, d => d.id == Tg.id && d.type == 'returnObjContact')
        }
    });
}
// reset targets distractor and fixation objects
function resetAll(){
    // monit.fixationObjects = [];
    monit.clearTargets();
    monit.clearDistractors();
}
function drawAnimations(){
    animationsStack.forEach(animObj =>{
        if(animObj.index > animObj.end){
        // if(getCurrTM() - animObj.timestamp > animObj.end){
            _.remove(animationsStack, d => d.id == animObj.id && d.type == animObj.type)
        }else{
            animations[animObj.type].cb(animObj);
        }
    })
}
// flag to draw one return items
var drawOnceReturnItems = 0,
counter = 0;
// draw return items 
function drawReturnItems(amount, items, numItems){
    var numItems = numItems == null ? 10 : numItems
    windowAmount = {};
    if(amount == 0){
        windowAmount.bottom = 0; 
        windowAmount.top = windowHeight;
    }else if(amount < 0){
        windowAmount.bottom = 200;
        windowAmount.top = windowHeight * 2;
    }else{
        windowAmount.bottom = -(windowHeight / 1.5);
        windowAmount.top = 200;
    }
    // console.log(windowAmount);
    if(!drawOnceReturnItems){                            
        items.forEach(el => {
            if(monit.getTargets().filter(tg => monit.itemsNames[tg.id] == el).length < parseInt(numItems / items.length)){
                console.log('cicle', counter++);
                for(var x = monit.getTargets().filter(tg => monit.itemsNames[tg.id] == el).length; x < parseInt(numItems / items.length); x++){       
                    var h = map(random(0, windowHeight), 0, windowHeight, windowAmount.bottom, windowAmount.top);
                    console.log('altezza', h)             
                    monit.addTarget(random(100, windowWidth - 100), h, el);
                }
            }else{
                drawOnceReturnItems = 1;
            }
        });
    }
    var tgs = monit.getTargets().filter(el => items.indexOf(monit.itemsNames[el.id]) !== -1); 
    tgs.forEach(el => {
        if(el.sizX < 200){
            el.sizX += 10;
            el.sizY += 10;
        }
        el.posY += amount;
        // if(el.posY < -200 || el.posY > windowHeight + 200){
        //     monit.removeTarget(el)
        // }
        // if(el.posX < -200 || el.posX > windowWidth + 200){
        //     monit.removeTarget(el)
        // }
    })
}

// CHOOSE GAME
function chooseGame(){
    monit.fixationObjects.forEach(obj=>{
        if(obj.counter > monit.fixationTime){
            monit.sendEvent({ts: new Date().getTime(), type:"event", event_type: "fixation_time_reached", data: obj.id});
            monit.sendEvent({ts: new Date().getTime(), type:"event", event_type: "end_fixation", data: obj.id});
            _.remove(monit.fixationObjects, d => d.id === obj.id);
            // monit.pState.targets[obj.id].img = monit.pState.targets[obj.id].img.replace('-overcast.png', '.png')
            var game = monit.itemsNames[obj.id].split('-')[0]; 
            // update chooseGame animation object
            var animationCinema = _.cloneDeep(animations['chooseGame'].obj);
            animationCinema.timestamp = getCurrTM();
            animationCinema.ref = _.cloneDeep(monit.pState.targets[obj.id]);
            // targetChosenPos = animationCinema.ref;
            animationCinema.id = obj.id;
            animationCinema.game = game;
            animationCinema.indexRef = 0;
            currVehicle.setName(game, 0);
            // set positions of the chosen target in its 
            animations['chooseGame'].startPositions = {
                'razzo': {
                    posX: windowWidth / 2 + 40,
                    posY: windowHeight - assetsDim[currVehicle.name].sizY / 2
                },
                'auto': {
                    posX: currDirection == 'right' ? pathsJsons[currentPath.replace('png', 'json')][80].x : 
                        pathsJsons[currentPath.replace('png', 'json')][pathsJsons[currentPath.replace('png', 'json')].length - 80].x,
                    posY: pathsJsons[currentPath.replace('png', 'json')][80].y
                },
                'sottomarino': {
                    posX: windowWidth / 2,
                    posY: assetsDim['superficie.png'].sizY / 1.3
                }
            };
            var currTgs = ['razzo', 'auto', 'sottomarino']
            animations['chooseGame'].chooseGamePositions = {
                'razzo': {
                    posX: 200,
                    posY: (windowHeight / 4) * 3
                },
                'auto': {
                    posX: (windowWidth / 3) + 200,
                    posY: (windowHeight / 4) * 3
                },
                'sottomarino': {
                    posX: (windowWidth / 3) * 2 + 200,
                    posY: (windowHeight / 4) * 3
                }
            }
            // console.log(animationCinema);
            // play game sound
            monit.playSound(game + '-start.wav');
            animationsStack.push(animationCinema);
            positionToReach = 1;
        }
    });
}
// CHOOSE VEHICLE
function chooseVehicle(game){
    monit.fixationObjects.forEach(obj=>{
        if(obj.counter > monit.fixationTime){
            var tgName = monit.itemsNames[obj.id]; 
            console.log(tgName);
            //set background
            monit.setBg('sfondo-' + game + '.png');
            // remove obj from fixation structure 
            _.remove(monit.fixationObjects, d => d.id === obj.id);
            // add vehicle
            var targ = _.find(monit.pState.targets, d => monit.itemsNames[d.id]== tgName);
            // empty targets 
            resetTargets();
            monit.addTarget(windowWidth / 2, windowHeight - 50, monit.itemsNames[targ.id]);
            // add character as distractor
            // monit.addDistractor(40, 900, 190, 200, 'character-' + game);
            GameSceneStage = 'game-fixation';
        }
    });
}
// JUMP CHARACTER
function jumpRocket(){
    monit.fixationObjects.forEach(obj => {
        if(obj.counter > monit.fixationTime){
            if(monit.itemsNames[obj.id] == currVehicle.name){
                monit.sendEvent({ts: new Date().getTime(), type:"event", event_type: "fixation_time_reached", data: obj.id});
                monit.sendEvent({ts: new Date().getTime(), type:"event", event_type: "end_fixation", data: obj.id});
                monit.addDistractor(assetsDim[currItem.name].sizX / 2, (windowHeight / 2) - (assetsDim[currItem.name].sizY / 2), currVehicle.name);
                monit.removeTarget(obj);
                GameTransitionAnimStep = 1;
                // resetAll();
            }
            _.remove(monit.fixationObjects, d => d.id == obj.id);
        }
    });
}
// SLIP DOWN CAR
function slipDownCar(){
    monit.fixationObjects.forEach(obj => {
        if(obj.counter > monit.fixationTime){
            console.log('over fixation time on car')
            if(monit.itemsNames[obj.id] == currVehicle.name){
                monit.sendEvent({ts: new Date().getTime(), type:"event", event_type: "fixation_time_reached", data: obj.id});
                var temp = _.cloneDeep(getTargetByName(currVehicle.name));
                let dis = monit.addDistractor(temp.posX, temp.posY, currVehicle.name);
                monit.removeTarget(getTargetByName(currVehicle.name));
                dis.rot = (PI / 4);
                // monit.removeDistractor(getDistractorByName('auto_landscape_01.png'))
                GameTransitionAnimStep = 3;
                // GameSceneStage = 'game-return-path';
                // resetAll();
            }
            _.remove(monit.fixationObjects, d => d.id == obj.id);
        }
    });
}
// CHARACTER PICK-UP SHELL 
function submarinePickUpShell(){
    monit.fixationObjects.forEach(obj => {
        if(obj.counter > monit.fixationTime){
            if( monit.itemsNames[obj.id] == currVehicle.name){
                monit.sendEvent({ts: new Date().getTime(), type:"event", event_type: "fixation_time_reached", data: obj.id});
                monit.sendEvent({ts: new Date().getTime(), type:"event", event_type: "end_fixation", data: obj.id});
                var currSub = _.find(monit.getTargets(), el =>  monit.itemsNames[el.id] == currVehicle.name);
                if(currSub !== undefined){
                    var temp = _.cloneDeep(currSub);
                    monit.addDistractor(temp.posX, temp.posY, currVehicle.name);
                    monit.removeTarget(currSub);
                }
                GameTransitionAnimStep = 0;
                GameSceneStage = 'game-return-path';
            }
            _.remove(monit.fixationObjects, d => d.id == obj.id);
        }
    });
}
var flFixationReach = 0,
// start rocket position 
startRocketPosX = 0,
startSubmPosX = 0;
// MOVING TARGET DURING FIXATION
function movingTarget(direction){
    if(monit.trackerActive){
        monit.fixationObjects.forEach(obj =>{
            // console.log('fixationObjects', monit.pState.targets[obj.id])
            if(obj.counter > monit.fixationTime){
                // console.log('fixation time reached')
                if(!flFixationReach){
                    monit.sendEvent({ts: new Date().getTime(), type:"event", event_type: "fixation_time_reached", data: obj.id});
                    flFixationReach = 1;
                }   
                var tgName =  monit.itemsNames[obj.id]; 
                var targ = getTargetByName(currVehicle.name);
                fixationMoved = 1;
                // types of fixation
                switch (direction){
                    case 'up':
                        var vehicles = ['razzo-A.png', 'razzo-B.png', 'razzo-C.png'];
                        if(vehicles.indexOf(monit.itemsNames[obj.id]) !== -1 ){
                            targ.posY -= currentTargetSpeed;
                            rocketFire.on();
                            if(monit.sounds['rocket-path.wav'].pl.state !== 'started'){
                                playLoopSound('rocket-path.wav');
                            }
                            // TOP REACHED
                            if(targ.posY < targ.sizY / 2){
                                flFixationReach = 0;
                                // go to return part 
                                // resetAll();
                                monit.stopSound('rocket-path.wav');
                                monit.playSound('applause.wav');
                                console.log('target', targ)
                                startRocketPosX = targ.posX;
                                GameSceneStage = 'game-transition-animation';
                                monit.sendEvent({ts: new Date().getTime(), type:"event", event_type: "end_fixation", data: obj.id});
                                return
                            }
                        }
                        break;
                    case 'down':
                        vehicles = ['sottomarino-A.png', 'sottomarino-B.png', 'sottomarino-C.png'];
                        if(vehicles.indexOf(monit.itemsNames[obj.id]) !== -1 ){
                            // BOTTOM REACHED
                            if(targ.posY > windowHeight - (targ.sizY / 2)){
                                flFixationReach = 0;
                                // go to return part 
                                monit.fixationObjects = []
                                animationsStack = [];
                                pulsatingObjs = [];
                                var bottomSea = {},
                                conchiglia = {};
                                if(getDistractorByName('fondale-marino.png') === undefined){
                                    monit.addDistractor(windowWidth / 2, windowHeight, 'fondale-marino.png');
                                    bottomSea = _.find(monit.getDistractors(), d => monit.itemsNames[d.id] == 'fondale-marino.png');
                                    bottomSea.sizX = windowWidth;
                                    bottomSea.posY = windowHeight + (bottomSea.sizY / 2);
                                }
                                // if(getTargetByName(currItem.name) === undefined){
                                //     conchiglia = monit.addTarget(windowWidth / 2, windowHeight, currItem.name);
                                //     conchiglia.posY = windowHeight + conchiglia.sizY;
                                // }
                                // if(getTargetByName(currVehicle.name) !== undefined){
                                //     monit.removeTarget(getTargetByName(currVehicle.name))
                                // }
                                monit.stopSound('submarine-path.wav');
                                monit.playSound('applause.wav');
                                startSubmPosX = monit.getTargetByName(currVehicle.name).posX;
                                startSubmPosY = monit.getTargetByName(currVehicle.name).posY;
                                monit.sendEvent({ts: new Date().getTime(), type:"event", event_type: "end_fixation", data: obj.id});
                                GameSceneStage = 'game-transition-animation';
                            }else{
                                targ.posY += currentTargetSpeed;
                                if(monit.sounds['submarine-path.wav'].pl.state !== 'started'){
                                    monit.playSound('submarine-path.wav');
                                    monit.sounds['submarine-path.wav'].pl.loop = true;
                                }
                            }
                        }
                        break;
                }
            }
        });
    }
}
// get specified distractor
function getDistractorByName(dis){
    return _.find(monit.getDistractors(), d => monit.itemsNames[d.id] === dis);
}
// get specified target
function getTargetByName(tg){
    return _.find(monit.getTargets(), d => monit.itemsNames[d.id] === tg);
}
// SET GAME VARIABLES TO GO TO A CERTAIN POINT OF THE GAME
function setGameState(gs, gss, gstf){
    monit.GameScene = gs;
    GameSceneStage = gss;
    GameSceneTypeFix = gstf;
    if(monit.eventconnection.readyState === 1){// if websocket open, send change scene to 
        if(monit.GameScene == 0){
            monit.eventconnection.send(JSON.stringify({ts: new Date().getTime(), type: "event", event_type: "change_scene", data: 0}));    
        }else{
            monit.eventconnection.send(JSON.stringify({ts: new Date().getTime(), type: "event", event_type: "change_scene", data: GameSceneTypeFix + 1}));
        }
        monit.sendEvent({ts: new Date().getTime(), type: "event", event_type: "buttons_state", data: {cooldown: false, back: monit.GameScene == 1, next: false}});
    }
    // add target is possible only at start of the fixation
    // redefining onAddTarget
    monit.onAddTarget = (x,y, type) => {
        if(GameSceneStage == 'game-fixation' && !animationsStack.filter(a => a.type == 'chooseGame').length && !positionToReach){

            var group = ['A', 'B', 'C'],
            currTg = getTargetByName(currVehicle.name),
            // get upper case letter of type 
            letter = group.filter(g => type.indexOf(g) !== -1)[0],
            oldLetter = currVehicle.name.split('-')[1].replace('.png', '');
            console.log(currTg.img.split('-'))
            // currVehicle.name = currVehicle.name.replace(oldLetter, letter);
            currVehicle.setName(currTg.img.split('-')[0], group.indexOf(letter), GameSceneTypeFix == 1 ? '-' + currTg.img.split('-')[2].replace('.png', '') : undefined)
            monit.addTarget(currTg.posX, currTg.posY, currVehicle.name);
            monit.removeTarget(currTg);
            if(GameSceneTypeFix == 1 && car != undefined) car.obj = monit.getTargetByName(currVehicle.name);
        }
    }
}
// 30 degrees rotation of parachute character
function rotateSide(obj) {
    var eyeT = monit.eyeTracker;
    obj.rot = indx;
    if (eyeT.posX < obj.posX - 2) {
        if (indx > -(PI / 6)) {
            indx -= PI / 16;
        }
    } else if (eyeT.posX > obj.posX + 2) {
        if (indx < PI / 6) {
            indx += PI / 16;
        }
    } else {
        if (indx > 0) {
            indx -= PI / 16;
        }
        if (indx < 0) {
            indx += PI / 16;
        }
    }
}
// car draw tail function and auto global object
var carTail = [], macchina = {};
function drawCarTail(macchinaT) {
    var maxLen = 30;
    if (pulsatingObjs[macchinaT.id] == undefined && carTail.length <= maxLen) {
        carTail.push({
            x: macchinaT.posX + (macchinaT.img.indexOf('right') !== -1 ? -macchinaT.sizX : macchinaT.sizX) / 4,
            y: macchinaT.posY + 10,
            indx: 0
        });
    }else{
        carTail.splice(carTail.length - 1, 1);
    }
    if (carTail.length == maxLen || carTail.filter(d => d.indx > maxLen).length > 0) {
        carTail.splice(0, 1);
    }
    for (var x = 1; x < carTail.length; x++){
        push();
        strokeWeight(map(carTail[x].indx, 0, maxLen, 10, 60));
        carTail[x].indx += 2;
        // console.log(carTail[x].indx)
        stroke('rgba(200, 200, 200,' + map(carTail[x].indx, 0, maxLen * 2, 0.9, 0.1) + ')')
        line(carTail[x].x, carTail[x].y, carTail[x - 1].x, carTail[x - 1].y)
        pop();
    }
}
// fill assets dimension object
function setAssetsSizes(){
    monit.json.details.ingame_assets.forEach(el => {
        assetsDim[el.name] = {sizX: el.sizX * (window.innerWidth / monit.idealRes.width), sizY: el.sizY * (window.innerHeight / monit.idealRes.height)}
    });
    console.log('assets dimension', assetsDim);
}
// set fixation settings
function loadTabletSettings(){
        let args = (new URL(location.href)).searchParams;
        tabletSettings['rocket'] = {
            ft: args.get('ftRocket') !== null ? parseInt(args.get('ftRocket')) : 500,
            speedTarget: args.get('speedTargetRocket') !== null ? parseInt(args.get('speedTargetRocket')) : 5
        };
        monit.fixationTime = args.get('ft') !== null ? parseInt(args.get('ft')) : 500;
        tabletSettings['car'] = {
            ft: args.get('ftCar') !== null ? parseInt(args.get('ftCar')) : 500,
            speedTarget: args.get('speedTargetCar') !== null ? parseInt(args.get('speedTargetCar')) : 5
        };
        tabletSettings['submarine'] = {
            ft: args.get('ftSubmarine') !== null ? parseInt(args.get('ftSubmarine')) : 500,
            speedTarget: args.get('speedTargetSubmarine') !== null ? parseInt(args.get('speedTargetSubmarine')) : 5
        };
}
// -------------- PRELOAD --------------
function preload() {
    monit.loadJsonItems();
    // load car path jsons
    loadPathsJsons();
}
// -------------- SETUP ----------------
function setup() {
    createCanvas(windowWidth,windowHeight);
    document.getElementById('defaultCanvas0').click();
    noStroke();
    frameRate(30);
    monit.onBackNext = dir => {
        console.log(monit.GameScene)
        if(dir == 'back' && monit.GameScene == 1){
            returnFixationChoose();
        }
    }
    monit.onChangeDynamics = data => {
        var type = data.id;
        switch(type){
            case 'ft':
                monit.fixationTime = parseFloat(data.value);
                break;
            // case 'ftRocket':
            //     tabletSettings['rocket'].ft = parseInt(data.value);
            //     monit.fixationTime = parseInt(data.value);
            //     break;
            // case 'ftCar':
            //     tabletSettings['car'].ft = parseInt(data.value);
            //     monit.fixationTime = parseInt(data.value);
            //     break;
            // case 'ftSubmarine':
            //     tabletSettings['submarine'].ft = parseInt(data.value);
            //     monit.fixationTime = parseInt(data.value);
            //     break;
            // case 'characterFollow':
            //     monit.eyeTracker.easing = 20 / parseFloat(data.value);
            //     break;
            case 'speedTarget':
                currentTargetSpeed = parseFloat(data.value);
                break;
            case 'carDirection':
                if(monit.GameScene == 1 && GameSceneTypeFix == 1 && GameSceneStage == 'game-fixation'){
                    if(!isEmpty(car)){
                        console.log(data.value)
                        switch(data.value){
                            case true:
                                if(car.dir == 'left') changeCarDir()
                                break;
                            case false:
                                if(car.dir == 'right') changeCarDir()
                                break;
                        }
                    }
                }else if(!monit.GameScene){
                    currCar = monit.getTargetByName('auto-A-' + currDirection + '.png');
                    if(currCar !== undefined){
                        monit.removeTarget(currCar);
                        currDirection = data.value ? 'right' : 'left';
                        monit.addTarget(animations['chooseGame'].chooseGamePositions['auto'].posX, animations['chooseGame'].chooseGamePositions['auto'].posY, 'auto-A-' + currDirection + '.png')   
                    }
                    currDirection = data.value ? 'right' : 'left';
                }
                break;
            // case 'speedTargetRocket':
            //     tabletSettings['rocket'].speedTarget = parseInt(data.value);
            //     currentTargetSpeed = data.value;
            //     break;
            // case 'speedTargetCar':
            //     tabletSettings['car'].speedTarget = parseInt(data.value);
            //     currentTargetSpeed = data.value;
            //     break;
            // case 'speedTargetSubmarine':
            //     tabletSettings['submarine'].speedTarget = parseInt(data.value);
            //     currentTargetSpeed = data.value;
            //     break; 
        }
    }
    monit.eventconnection.onmessage = (message) => {
        const msg = JSON.parse(message.data)
        switch(msg.type){
            case "event":
                console.log('messaggione', msg)
                switch(msg.event_type){
                    case "add_target":
                        console.log('addTarget', msg.data.type);
                        monit.onAddTarget(msg.data.x, msg.data.y, msg.data.type);
                        break;
                    case "remove_target":
                         monit.onRemoveTarget(msg.data.id);
                         break;
                    case "move_target":
                         monit.onMoveTarget(msg.data);
                         break;
                    case "toggle_tracker":
                        monit.onToggleTracker(msg.data);
                        break;
                    case "end_session":
                        monit.onEndSession();
                        break;
                    case "attention_sig":
                        monit.onAttentionSig();
                        break;
                    case "change_dynamics":
                        monit.onChangeDynamics(msg.data)
                        break;
                    case "back_scene":
                        monit.onBackNext("back");
                        break;
                    default:
                        break;
                }
                break;
            default:
                break
        }
    };
    // redefine function triggered by attention signal
    monit.onAttentionSig = function(){
        monit.playSound('richiamo.mp3');
        for(var p in pulsatingObjs){
            pulsatingObjs[p].amp = 60;
            pulsatingObjs[p].sInd = 0;
        }
        setTimeout(()=>{
            for(var p in pulsatingObjs){
            pulsatingObjs[p].amp = 20;
            pulsatingObjs[p].sInd = 0;
        }
        }, 2000);
    }
    // redefine move target
    monit.onMoveTarget = (ob) => {
        // move target only on game fixation
        if(GameSceneStage == 'game-fixation'){
            switch(GameSceneTypeFix){
                // ROCKET 
                case 0:
                    var tg = monit.getTargetByName(currVehicle.name);
                    tg.posX = ob.x * windowWidth;
                    constrainItem(tg, 
                        0, 
                        windowWidth - (assetsDim[currVehicle.name].sizX / 2), 
                        windowHeight, 
                        assetsDim[currVehicle.name].sizX / 2
                        );
                    break;
                // CAR 
                case 1:
                    if(!isEmpty(car)){

                        switch(car.dir){
                            case 'right': 
                                if(ob.x * windowWidth > car.line[car.ind + car.currentSpeed].x) {
                                    var next = car.line.filter((e, i) => e.x > ob.x * windowWidth && i % car.currentSpeed == 0)[0];
                                } 
                                break;
                            case 'left': 
                                if(ob.x * windowWidth < car.line[car.ind - car.currentSpeed].x){
                                    var temp = _.cloneDeep(car.line).reverse();
                                    next = temp.filter((e, i) => e.x < ob.x * windowWidth && i % car.currentSpeed == 0)[0];
                                }
                                break;
                        }
                        if(next !== undefined){
                            console.log(next)
                            var tempVal = 0;
                            car.line.forEach((el, i) => {
                                if(el.x == next.x) {
                                    tempVal = i;
                                    car.ind = tempVal;
                                    car.followLine();
                                }
                            });
                        }
                    }
                    break;
                // SUBMARINE
                case 2:
                    var tg = monit.getTargetByName(currVehicle.name);
                    tg.posX = ob.x * windowWidth;
                    constrainItem(tg, 
                        0, 
                        windowWidth - (assetsDim[currVehicle.name].sizX / 2), 
                        windowHeight, 
                        assetsDim[currVehicle.name].sizX / 2
                        )
                    break;
            }
        }
    };
    // deactive removing target
    monit.onRemoveTarget = id => {
        switch(monit.GameScene){
            case 0:
                monit.fixationObjects.push({id: id, counter: 10000});
                chooseGame();
                break;
            case 1:
                // ROCKET BEFORE RETURN PATH TRIGGER  
                if(GameSceneTypeFix == 0){
                    if(GameSceneStage == 'game-transition-animation' && rocketFire.ind <= 0){
                        monit.fixationObjects.push({id: id, counter: 10000});
                        jumpRocket();
                    }
                }
                // SUBMARINE BEFORE RETURN PATH TRIGGER
                if(GameSceneTypeFix == 2){
                    if(GameSceneStage == 'game-transition-animation' && isStaring){
                        monit.fixationObjects.push({id: id, counter: 10000});
                        submarinePickUpShell();
                    }
                }
                break;
        }
    }
    monit.onAddTarget = ()=>{
        console.log('can\'t add target');
    }
    // define play loop sound
    playLoopSound = name => {
        if(monit.sounds[name] !== undefined){
            monit.sounds[name].pl.fadeIn = 0.5;
            monit.playSound(name)
            monit.sounds[name].pl.loop = true;
            var me = monit;
            // send message to event socket when play sound
        }
    }
}
var incsStart = []
// -------------- DRAW -----------------
function draw() {
    if(monit.loaded && pathsJsonsFl){
        if(monit.pState.bg != backgroundImg && !monit.GameScene){monit.setBg(backgroundImg);} // set first bg      
        if(isEmpty(assetsDim)){setAssetsSizes()} // set assets dimension object 
        if(isEmpty(tabletSettings)){loadTabletSettings()} // set assets dimension object 
        if(!firstStateButtons){
            monit.sendEvent({ts: new Date().getTime(), type: "event", event_type: "buttons_state", data: {cooldown: false, back: false, next: false}})
            firstStateButtons = 1;
        }
        monit.drawBg();
        switch(monit.GameScene){
            // CHOOSE GAME
            case 0:
                var currTgs = ['razzo-A.png', 'auto-A-' + currDirection + '.png', 'sottomarino-A.png'];
                switch(GameSceneStage){
                    case 0:
                        var incs = [];
                        // SET AND SHOW ALL TARGETS TO CHOOSE TYPE OF OCULAR MOVEMENT PART 
                        // CHOOSE TYPE OF OCULAR MOVEMENT
                        currTgs.forEach((el, i) =>{
                            if(!monit.getTargets().filter(d => monit.itemsNames[d.id] == el).length){
                                // create and assign start targets
                                var temp = monit.addTarget(windowWidth / 3 * i + 200, (windowHeight / 4) * 3, el);
                                if(assetsDim[el].sizX > assetsDim[el].sizY){
                                    temp.sizY = 20;
                                    temp.sizX = 20 * (assetsDim[el].sizX / assetsDim[el].sizY);
                                }
                                if(assetsDim[el].sizX < assetsDim[el].sizY){
                                    temp.sizX = 20;
                                    temp.sizY = 20 * (assetsDim[el].sizY / assetsDim[el].sizX);
                                }
                                incsStart[el] = (assetsDim[el].sizX - temp.sizX) / (400 / 33);
                            }
                            if(chooseGameTargetsStartPositions[el] == undefined){
                                chooseGameTargetsStartPositions[el] = getTargetByName(el);
                            }
                        });
                        // make targets bigger
                        monit.getTargets().forEach( tg => {
                            if(tg.sizX < assetsDim[tg.img].sizX && tg.sizY < assetsDim[tg.img].sizY){
                                tg.sizX += incsStart[tg.img];
                                tg.sizY += incsStart[tg.img] * (assetsDim[tg.img].sizY / assetsDim[tg.img].sizX);
                            }else{ 
                                // GameSceneStage = 1;
                            }
                        });
                        if(monit.getTargets().filter(t => 
                            currTgs.indexOf(t.img) !== -1 && 
                            t.sizX >= assetsDim[t.img].sizX && 
                            t.sizY >= assetsDim[t.img].sizY).length === currTgs.length){
                            monit.getTargets().forEach( el =>{
                                el.sizX = assetsDim[el.img].sizX;
                                el.sizY = assetsDim[el.img].sizY;
                            });
                            GameSceneStage = 1;
                        }
                        break;
                        // start fixation to choose the type of fixation
                    case 1:
                        currTgs.forEach((el, i) =>{
                            if(!monit.getTargets().filter(d => monit.itemsNames[d.id] == el).length){
                                // create and assign start targets
                                monit.addTarget(windowWidth / 3 * i + 200, (windowHeight / 4) * 3, el);
                            }
                            if(chooseGameTargetsStartPositions[el] == undefined){
                                chooseGameTargetsStartPositions[el] = getTargetByName(el);
                            }
                        });
                        // check overlap
                        if(!animationsStack.filter(a => a.type == 'chooseGame').length){
                            checkInteractionTargets();
                        }
                        chooseGame(); 
                        break;
                }
                break;
            case 1: 
                // GAME
                switch(GameSceneTypeFix){
                    //---------------------------------------------------
                    // ------------------ ROCKET ------------------------
                    //---------------------------------------------------
                    case 0:
                        // choosing vehicle and game
                        switch(GameSceneStage){
                            case 'game-fixation':
                                // clouds positions
                                var cloudsPositions = [
                                    {posX:  assetsDim['nuvola.png'].sizX / 2, posY: windowHeight / 2 + 200},
                                    {posX: (windowWidth / 3) + (assetsDim['nuvola.png'].sizX / 2), posY: windowHeight / 2 - 100},
                                    {posX: ((windowWidth / 3) * 2) + (assetsDim['nuvola.png'].sizX / 2), posY: windowHeight / 2 + 100}
                                ],
                                startRocketPosY = windowHeight - assetsDim[currVehicle.name].sizY / 2;
                                startRocketPosX = windowWidth / 2;
                                var startFloorPosX =  windowWidth / 2,
                                startFloorPosY = windowHeight - assetsDim['floor.png'].sizY / 2;
                                //add ROCKET
                                if(getTargetByName(currVehicle.name) == undefined){
                                    rocket = monit.addTarget(startRocketPosX, startRocketPosY, currVehicle.name);
                                }else{
                                    rocket = getTargetByName(currVehicle.name);
                                }
                                // set rocket fire
                                rocketFire.posX = rocket.posX;
                                rocketFire.posY = rocket.posY + assetsDim[currVehicle.name].sizY / 2;
                                // add CLOUDS
                                var nuvole = monit.getDistractors().filter(d => monit.itemsNames[d.id] == 'nuvola.png');
                                if(nuvole.length < 3){
                                    cloudsPositions.forEach(cl => {
                                        monit.addDistractor(cl.posX, cl.posY, 'nuvola.png', -1);
                                    });
                                }
                                // add FLOOR 
                                if(getDistractorByName('floor.png') === undefined){
                                    var floor = monit.addDistractor(startFloorPosX, startFloorPosY, 'floor.png');
                                    // floor.sizX = windowWidth + 200;
                                }
                                // check if ther is fixation
                                if(!positionToReach){
                                    // handle target movement
                                    movingTarget('up');
                                    if(!monit.fixationObjects.filter(f => f.id == rocket.id).length){
                                        rocketFire.off();
                                    }else{
                                        rocketFire.on();
                                        if(monit.sounds['rocket-path.wav'].pl.state !== 'started'){
                                            playLoopSound('rocket-path.wav');
                                        }
                                    }
                                    // rocketFire.draw();
                                    checkInteractionTargets();
                                }else {
                                    isStaring = 0;
                                    if(!animationsStack.filter(a => a.type == 'chooseGame').length){      // if choose game animation is present, move rocket to his start position
                                        if(rocket.posX <= animations['chooseGame'].startPositions['razzo'].posX - 2000 / 33){
                                            let nIteration = 1000 / 33;
                                            rocket.posX += (animations['chooseGame'].startPositions['razzo'].posX - animations['chooseGame'].chooseGamePositions['razzo'].posX) / nIteration; 
                                            rocket.posY += (animations['chooseGame'].startPositions['razzo'].posY - animations['chooseGame'].chooseGamePositions['razzo'].posY) / nIteration; 
                                        }else{
                                            rocket.posX = windowWidth / 2;
                                            rocket.posY = animations['chooseGame'].startPositions['razzo'].posY;
                                            positionToReach = 0;
                                        }
                                    }   
                                }
                                break;
                            case 'game-transition-animation':
                                if(monit.sounds['rocket-path.wav'].pl.state == 'started'){
                                    monit.stopSound('rocket-path.wav');
                                }
                                // ANIMATION TO ASTEROID around GameTransitionAnimStep
                                switch(GameTransitionAnimStep){
                                    // rocket GOES UP, clouds disappear and planet appears
                                    case 0: 
                                        cloudsPositions = [
                                            {
                                                posX:  assetsDim['nuvola.png'].sizX / 2, 
                                                posY: windowHeight / 2 + 200
                                            },
                                            {
                                                posX: (windowWidth / 3) + (assetsDim['nuvola.png'].sizX / 2), 
                                                posY: windowHeight / 2 - 100
                                            },
                                            {
                                                posX: ((windowWidth / 3) * 2) + (assetsDim['nuvola.png'].sizX / 2), 
                                                posY: windowHeight / 2 + 100
                                            }
                                        ];
                                        var startPlanetPosY = -500,
                                        endPlanetPosY = windowHeight / 2,
                                        endCloudsPosY = -600;
                                        // rocket
                                        // startRocketPosX = windowWidth / 2;
                                        console.log(startRocketPosX)
                                        startRocketPosY = assetsDim[currVehicle.name].sizY / 2,
                                        endRocketPosX = assetsDim[currItem.name].sizX / 2,
                                        endRocketPosY = (windowHeight / 2) - (assetsDim[currItem.name].sizY / 2),
                                        // floor
                                        startFloorPosX =  windowWidth / 2,
                                        startFloorPosY = windowHeight - assetsDim['floor.png'].sizY / 2,
                                        endFloorPosY = windowHeight + assetsDim['floor.png'].sizY,

                                        partAnimDuration = 5000,
                                        animNumSteps = (partAnimDuration / 33),

                                        incPlanet = Math.abs(startPlanetPosY - endPlanetPosY) / animNumSteps,
                                        incCloud = Math.abs(cloudsPositions[0].posY - endCloudsPosY) / animNumSteps,
                                        incRocketX = Math.abs(startRocketPosX - endRocketPosX) / animNumSteps,
                                        incRocketY = Math.abs(startRocketPosY - endRocketPosY) / animNumSteps,
                                        incFloor = Math.abs(startFloorPosY - endFloorPosY) / animNumSteps;

                                        distractors = monit.getDistractors();
                                        // add PLANET 
                                        if(getDistractorByName(currItem.name) === undefined){
                                            console.log('aggiunta pianetone')
                                            monit.addDistractor(assetsDim[currItem.name].sizX / 2, startPlanetPosY, currItem.name);
                                        }
                                        // add ROCKET 
                                        if(getTargetByName(currVehicle.name) === undefined){
                                            console.log('aggiunta razzo')
                                            rocket = monit.addTarget(windowWidth / 2, startRocketPosY, currVehicle.name);
                                        }else{
                                            rocket = getTargetByName(currVehicle.name);
                                            // rocket.img = rocket.img.replace('-overcast', '');
                                        }
                                        // add CLOUDS
                                        var nuvole = monit.getDistractors().filter(d => monit.itemsNames[d.id] == 'nuvola.png');
                                        if(nuvole.length < 3){
                                            cloudsPositions.forEach(cl => {
                                                monit.addDistractor(cl.posX, cl.posY, 'nuvola.png', -1);
                                            });
                                        }
                                        // add FLOOR 
                                        if(getDistractorByName('floor.png') === undefined){
                                            console.log('add floor');
                                            var floor = monit.addDistractor(startFloorPosX, startFloorPosY, 'floor.png');
                                            // floor.sizX = windowWidth + 200;
                                        }
                                        // moving distractors (clouds, floor and planet)
                                        distractors.forEach(el =>{
                                            switch(monit.itemsNames[el.id]){
                                                case currItem.name:
                                                    if(el.posY > endPlanetPosY){
                                                        // resetAll();
                                                        // GameTransitionAnimStep = 1;
                                                        // remove clouds
                                                        monit.getDistractors().filter(cl => monit.itemsNames[cl.id] === 'nuvola.png').forEach(cl => monit.removeDistractor(cl));
                                                        // checkInteractionTargets();
                                                        // jumpRocket();
                                                    }else{
                                                        el.posY += incPlanet;
                                                    }
                                                    break;
                                                case 'nuvola.png':
                                                    el.posY += incCloud;
                                                    break;
                                                case 'floor.png':
                                                    // there's a reason why i'm using the same increment of the cloud: perspective
                                                    el.posY += incCloud;
                                                    break;
                                            }
                                        });
                                        // move rocket
                                        var rocket = getTargetByName(currVehicle.name);
                                        if(rocket.posX > endRocketPosX || rocket.posY < endRocketPosY){
                                            rocket.posY += incRocketY;
                                            rocket.posX -= incRocketX;
                                            // add fire to rocket 
                                            rocketFire.ind = rocketFire.maxS;
                                            rocketFire.posX = rocket.posX;
                                            rocketFire.posY = rocket.posY + assetsDim[currVehicle.name].sizY / 2;
                                            rocketFire.on();
                                            isStaring = 0;
                                            if(monit.fixationObjects.length){
                                                monit.fixationObjects = [];
                                            }
                                        }else{
                                            rocketFire.ind = 0;
                                            rocketFire.off();
                                            isStaring = 1;
                                            checkInteractionTargets();
                                            jumpRocket();
                                        }
                                        break;
                                        // rocket JUMPS from the planet to the void
                                    case 1:
                                        isStaring = 0;
                                        // planet
                                        startPlanetPosY = (windowHeight / 2) - assetsDim[currItem.name].sizY;
                                        startPlanetPosX = assetsDim[currItem.name].sizX / 2;
                                        
                                        startRocketPosX = assetsDim[currVehicle.name].sizY / 2;
                                        startRocketPosY = (windowHeight / 2) - (assetsDim[currItem.name].sizY / 2);
                                        endRocketPosX = windowWidth / 2;

                                        partAnimDuration = 1000;
                                        animNumSteps = (partAnimDuration / 33);   
                                        incRocket = Math.abs(startRocketPosX - endRocketPosX) / animNumSteps;     
                                        // console.log(incRocket) 

                                        // add PLANET 
                                        if(getDistractorByName(currItem.name) === undefined){
                                            var planet = monit.addDistractor(startPlanetPosX, startPlanetPosY, currItem.name);
                                        }else{
                                            planet = getDistractorByName(currItem.name);
                                        }                                        
                                        // add ROCKET 
                                        if(getDistractorByName(currVehicle.name) === undefined){
                                            rocket = monit.addDistractor(windowWidth / 2, startRocketPosY, currVehicle.name);
                                        }else{
                                            rocket = getDistractorByName(currVehicle.name);
                                        }
                                        if(jumpSinIndx < PI){
                                            // console.log('dentro increment')
                                            jumpSinIndx += 0.040;
                                        }
                                        if(rocket.posX < (windowWidth / 2)){
                                            // console.log(sin(map(rocket.posX, startRocketPosX, endRocketPosX, 0, PI)));
                                            rocket.posY = startRocketPosY - 60 * sin(map(rocket.posX, startRocketPosX, endRocketPosX, 0, PI));
                                            rocket.posX += incRocket;
                                        }else{
                                            // return part of the game, reset animation step conter and jump sin index
                                            GameTransitionAnimStep = 0;
                                            GameSceneStage = 'game-return-path';
                                            jumpSinIndx = 0;
                                        }
                                        break;
                                }

                                break;
                            case 'game-return-path':
                                playLoopSound('communication.wav');
                                isStaring = 0;
                                cloudsPositions = [
                                    {
                                        posX:  assetsDim['nuvola.png'].sizX / 2, 
                                        posY: windowHeight + 800
                                    },
                                    {
                                        posX: (windowWidth / 3) + (assetsDim['nuvola.png'].sizX / 2), 
                                        posY: windowHeight + 600
                                    },
                                    {
                                        posX: ((windowWidth / 3) * 2) + (assetsDim['nuvola.png'].sizX / 2),
                                        posY: windowHeight + 700
                                    }
                                ];
                                // planet
                                startPlanetPosY = windowHeight / 2;
                                startPlanetPosX = assetsDim[currItem.name].sizX / 2;
                                endPlanetPosY = -assetsDim[currItem.name].sizY - 600;
                                endPlanetPosX = assetsDim[currItem.name].sizX / 2;
                                // clouds
                                startCloudPosY = windowHeight + 200;
                                endCloudPosY = (windowHeight / 2) - 100;
                                // floor
                                startFloorPosY = windowHeight + assetsDim['floor.png'].sizY;
                                endFloorPosY = windowHeight - (assetsDim['floor.png'].sizY / 2) + 50;

                                partAnimDuration = 10000;
                                animNumSteps = (partAnimDuration / 33);   
                                speedStars = (windowHeight) / animNumSteps;
                                incPlanet = Math.abs(startPlanetPosY - endPlanetPosY) / animNumSteps;
                                incCloud = Math.abs(startCloudPosY - endCloudPosY) / animNumSteps;
                                // add PLANET 
                                if(getDistractorByName(currItem.name) === undefined){
                                    var planet = monit.addDistractor(startPlanetPosX, startPlanetPosY, currItem.name);
                                }else{
                                    planet = getDistractorByName(currItem.name);
                                }
                                // add CLOUDS
                                var nuvole = monit.getDistractors().filter(d => monit.itemsNames[d.id] == 'nuvola.png');
                                if(nuvole.length < 3){
                                    cloudsPositions.forEach(cl => {
                                        monit.addDistractor(cl.posX, cl.posY, 'nuvola.png', -1);
                                    });
                                }        
                                // add FLOOR
                                if(getDistractorByName('floor.png') === undefined){
                                    var floor = monit.addDistractor(startFloorPosX, startFloorPosY, 'floor.png');
                                    console.log(floor.posY);
                                    // floor.sizX = windowWidth;
                                }else{
                                    floor = getDistractorByName('floor.png');
                                    floor.posY = startFloorPosY;
                                } 
                                var asteroid = monit.getDistractors().filter(d => monit.itemsNames[d.id] == currItem.name)[0];
                                var clouds = monit.getDistractors().filter(d => monit.itemsNames[d.id] == 'nuvola.png');
                                // moving distractors (clouds, floor and planet)
                                monit.getDistractors().forEach(el =>{
                                    switch(monit.itemsNames[el.id]){
                                        case currItem.name:
                                            el.posY -= incPlanet;
                                            break;
                                        case 'nuvola.png':
                                            el.posY -= incCloud;
                                            break;
                                    }
                                });
                                // moving parachute character 
                                rocket = getDistractorByName(currVehicle.name);
                                var eyeT = monit.eyeTracker;
                                // rotate character 
                                rotateSide(rocket);
                                // moving with easing
                                // be careful of limits
                                rocket.posX += (eyeT.posX - rocket.posX) * 0.05;
                                if(rocket.posX > windowWidth - (assetsDim[currVehicle.name].sizX / 2)){
                                    rocket.posX = windowWidth - (assetsDim[currVehicle.name].sizX / 2)
                                }else if(rocket.posX < assetsDim[currVehicle.name].sizX / 2){
                                    rocket.posX = assetsDim[currVehicle.name].sizX / 2;
                                }
                                drawReturnItems(-speedStars, [currStar.name]);
                                checkInteractionReturnItems(rocket, 'stella');
                                checkReachTarget('stella');
                                if(clouds.filter(cl => cl.posY < (windowHeight / 2) - 60).length){
                                    // add next rocket
                                    // currVehicle.number = currVehicle.number == currVehicle.group.length - 1 ? 0 : currVehicle.number + 1;
                                    // monit.addTarget(windowWidth / 2, windowHeight + 200, currVehicle.name);
                                    monit.removeDistractor(getDistractorByName(currItem.name));
                                    rocket.rot = 0;
                                    // resetAll();
                                    drawOnceReturnItems = 0;
                                    GameSceneStage = 'game-landing-animation'
                                    // adding animation in stack
                                }
                                break;
                            case 'game-landing-animation':
                                cloudsPositions = [
                                    {posX:  assetsDim['nuvola.png'].sizX / 2, posY: windowHeight / 2 + 200},
                                    {posX: (windowWidth / 3) + (assetsDim['nuvola.png'].sizX / 2), posY: windowHeight / 2 - 100},
                                    {posX: ((windowWidth / 3) * 2) + (assetsDim['nuvola.png'].sizX / 2), posY: windowHeight / 2 + 100}
                                ];
                                // rocket
                                startRocketPosY = windowHeight + assetsDim[currVehicle.name].sizY;
                                endRocketPosY = windowHeight - assetsDim[currVehicle.name].sizY / 2;
                                startRocketPosX = windowWidth / 2;
                                // floor
                                startFloorPosY = windowHeight + assetsDim['floor.png'].sizY;
                                endFloorPosY = windowHeight - (assetsDim['floor.png'].sizY / 2) + 50;

                                partAnimDuration = 1000;
                                animNumSteps = (partAnimDuration / 33); 
                                incFloor = Math.abs(startFloorPosY - endFloorPosY) / animNumSteps;
                                incRocket = Math.abs(startRocketPosY - endRocketPosY) / animNumSteps;
                                // add CLOUDS
                                nuvole = monit.getDistractors().filter(d => monit.itemsNames[d.id] == 'nuvola.png');
                                if(nuvole.length < 3){
                                    cloudsPositions.forEach(cl => {
                                        monit.addDistractor(cl.posX, cl.posY, 'nuvola.png', -1);
                                    });
                                }  
                                //add ROCKET
                                if(getDistractorByName(currVehicle.name) == undefined){
                                    monit.addDistractor(startRocketPosX, startRocketPosY, currVehicle.name);
                                }
                                // add FLOOR
                                if(getDistractorByName('floor.png') === undefined){
                                    var floor = monit.addDistractor(startFloorPosX, startFloorPosY, 'floor.png');
                                    console.log(floor.posY);
                                    // floor.sizX = windowWidth;
                                }else{
                                    floor = getDistractorByName('floor.png');
                                }
                                // disappear stars
                                monit.getTargets().filter(tg =>  monit.itemsNames[tg.id] == currStar.name).forEach(star => {
                                    if(star.sizX > 10){
                                        star.sizX -= 10;
                                        star.sizY -= 10;
                                    }else{
                                        monit.removeTarget(star);
                                    }
                                });
                                // end animation
                                let distrs = monit.getDistractors();
                                distrs.forEach( dis => {
                                    if( monit.itemsNames[dis.id] == currVehicle.name){
                                        if(dis.posY < windowHeight - getDistractorByName(currVehicle.name).sizY / 2){
                                            dis.posY += 5;
                                            if(dis.posX < (windowWidth / 2) - 7){
                                                dis.posX += 7;
                                            }else if(dis.posX > (windowWidth / 2) + 7){
                                                dis.posX -= 7;
                                            }
                                        }else{
                                            monit.removeDistractor(dis); 
                                            monit.addTarget(windowWidth / 2, windowHeight - assetsDim[currVehicle.name].sizY / 2, currVehicle.name)                                 
                                            GameTransitionAnimStep = 0;   
                                            // change current item name
                                            currItem.setNext();
                                            // increment star
                                            currStar.setNext();
                                            monit.stopSound('communication.wav');
                                            GameSceneStage = 'game-fixation';
                                            fixationMoved = 0;
                                        }
                                    }
                                    // make reapper floor
                                    if( monit.itemsNames[dis.id] == 'floor.png'){
                                        if(dis.posY >= endFloorPosY){
                                            // there's a reason why using the same increment of the rocket: perspective
                                            dis.posY -= incRocket;
                                        }
                                    }
                                });
                                break;
                        }
                        break;
                    //---------------------------------------------------
                    //--------------------- CAR -------------------------
                    //---------------------------------------------------
                    case 1:                    
                    // choosing vehicle and game
                        // current car path to follow  
                        var startIndCar = currVehicle.name.indexOf('right') !== -1 ? 80 : pathsJsons[currentPath.replace('png', 'json')].length - 80,
                        cacti = ['cactus-A.png', 'cactus-B.png', 'cactus-C.png'];

                         switch(GameSceneStage){
                            case 'game-fixation': 
                                startCarPosX = parseInt(assetsDim[currVehicle.name].sizX / 2);
                                // change car name 
                                if(currVehicle.name.indexOf('right') == -1 && currVehicle.name.indexOf('left') == -1 ){
                                    currVehicle.name = currVehicle.name.replace('.png','') + '-' + currDirection + '.png';
                                    // precalculated car posY
                                    startCarPosY = pathsJsons[currentPath.replace('png', 'json')][startIndCar].y;
                                    startCarPosX = pathsJsons[currentPath.replace('png', 'json')][startIndCar].x;
                                    // console.log('assign path')
                                }
                                endCarPosX = windowWidth - assetsDim[currVehicle.name].sizX / 2;
                                // mount
                                startMountPosX = windowWidth;
                                startMountPosY = windowHeight / 2;
                                endMountPosX = windowWidth / 2;
                                endMountPosY = windowHeight / 2;
                                // endCarPosY = assetsDim[currVehicle.name].sizY / 2;
                                if(getDistractorByName(currentPath) == undefined){
                                    var ground = monit.addDistractor(windowWidth / 2, windowHeight / 2, currentPath);
                                }else{
                                    ground = getDistractorByName(currentPath);
                                }
                                if(getTargetByName(currVehicle.name) == undefined){
                                    // create and reduce 
                                    macchina = monit.addTarget(startCarPosX, startCarPosY, currVehicle.name);
                                }else{
                                    macchina = getTargetByName(currVehicle.name);
                                }
                                // analyse and move car after start position is reached
                                if(!positionToReach){// create car
                                    // monit.drawItems([ground]);
                                    if(isEmpty(car)){
                                        // creating car object and analyse grounds
                                        car = new Car(macchina);
                                        // assign 
                                        car.line = pathsJsons[currentPath.replace('png', 'json')];
                                        car.ind = startIndCar;
                                    }
                                    car.draw();
                                    // car.drawLine()
                                    // go to mountain animation
                                    var isCarReachEnd = car.dir == 'right' ? car.ind > car.line.length - 80 : car.ind < 80;
                                    if(isCarReachEnd){
                                        // stop car sound
                                        monit.stopSound('car-path.wav');
                                        monit.sounds['car-path.wav'].pl.loop = false;
                                        monit.playSound('applause.wav');
                                        monit.playSound('acceleration.wav');
                                        flFixationReach = 0;
                                        GameSceneStage = 'game-transition-animation';
                                        _.remove(monit.fixationObjects, d => d.id == getTargetByName(currVehicle.name).id);
                                        // replace the current car the other one with the opposite driving direction
                                        var replaceCar = currVehicle.name.indexOf('right') !== -1 ? ['right', 'left'] : ['left', 'right']
                                        currVehicle.name = currVehicle.name.replace(replaceCar[0], replaceCar[1]);
                                        let tgcar = getTargetByName(currVehicle.name.replace(replaceCar[1], replaceCar[0]))
                                        monit.sendEvent({ts: new Date().getTime(), type:"event", event_type: "end_fixation", data: tgcar.id});
                                        monit.addTarget(tgcar.posX, tgcar.posY, currVehicle.name)
                                        monit.removeTarget(tgcar);
                                        startCarPosX = monit.getTargetByName(currVehicle.name).posX
                                        break;
                                    }
                                    checkInteractionTargets();
                                    // monitorHoveredColor();
                                }else if(!animationsStack.filter(a => a.type == 'chooseGame').length){     // if choose game animation is present, move rocket to his start position
                                    var carRule = currDirection == 'right' ? macchina.posX >= animations['chooseGame'].startPositions['auto'].posX : macchina.posX <= animations['chooseGame'].startPositions['auto'].posX
                                    if(carRule){
                                        let nIteration = 1000 / 33;
                                        macchina.posX -= (animations['chooseGame'].chooseGamePositions['auto'].posX - animations['chooseGame'].startPositions['auto'].posX) / nIteration; 
                                        macchina.posY -= (animations['chooseGame'].chooseGamePositions['auto'].posY - animations['chooseGame'].startPositions['auto'].posY) / nIteration; 
                                    }else{
                                        macchina.posX = animations['chooseGame'].startPositions['auto'].posX;
                                        macchina.posY = animations['chooseGame'].startPositions['auto'].posY;
                                        positionToReach = 0;
                                    }
                                }
                                break;
                            case 'game-transition-animation':
                                isStaring = 0;
                                // empty car object
                                if(!isEmpty(car)) car = {};
                                // up to the mountain
                                startGroundPosX = windowWidth / 2;
                                startGroundPosY = windowHeight / 2;
                                endGroundPosY = -(windowHeight / 2);
                                startRetPathPosX = windowWidth / 2;
                                startRetPathPosY = windowHeight + (windowHeight / 2);
                                endRetPathPosY = windowHeight / 2;
                                // startCarPosX = windowWidth - assetsDim[currVehicle.name].sizX / 2;
                                startCarPosY = windowHeight / 2; 

                                partAnimDuration = 2000;
                                animNumSteps = partAnimDuration / 33;

                                incGround = Math.abs(startGroundPosY - endGroundPosY) / animNumSteps;
                                incRect = Math.abs(windowHeight + 10) / animNumSteps;
                                incBackGround = Math.abs(startRetPathPosY - endRetPathPosY) / animNumSteps;
                                // ground
                                if(getDistractorByName(currentPath) == undefined){
                                    var ground = monit.addDistractor(startGroundPosX, startGroundPosY, currentPath);
                                    ground.sizX = windowWidth;
                                }else{
                                    ground = getDistractorByName(currentPath);
                                }
                                // car
                                if(getTargetByName(currVehicle.name) == undefined){
                                    macchina = monit.addTarget(startCarPosX, startCarPosY, currVehicle.name);
                                }else{
                                    macchina = getTargetByName(currVehicle.name);
                                }
                                // background of return path
                                if(getDistractorByName('return-path.png') == undefined){
                                    var backgroundCar = monit.addDistractor(startRetPathPosX, startRetPathPosY, 'return-path.png');
                                }else{
                                    backgroundCar = getDistractorByName('return-path.png');
                                }
                                // move background png and dune
                                if(backgroundCar.posY > endRetPathPosY){
                                    backgroundCar.posY -= incBackGround;
                                    ground.posY -= incGround;
                                }else{
                                    startCarPosX = monit.getTargetByName(currVehicle.name).posX;
                                    GameSceneStage = 'game-return-path';
                                }
                                break;
                            case 'game-return-path':
                                isStaring = 0;
                                var cpath = pathsJsons[currentPath.replace('png', 'json')]
                                if(getDistractorByName(currentPath) !== undefined){
                                    monit.removeDistractor(getDistractorByName(currentPath))
                                }
                                if(monit.sounds['car-path.wav'].pl.state !== 'started'){
                                    playLoopSound('car-path.wav');
                                }
                                // car
                                // startCarPosX = windowWidth - assetsDim[currVehicle.name].sizX / 2;
                                startCarPosY = windowHeight / 2;
                                endCarPosX = currVehicle.name.indexOf('right') !== -1 ? cpath[cpath.length - 80].x : cpath[80].x;

                                partAnimDuration = 5000;
                                animNumSteps = partAnimDuration / 33;

                                incCar = Math.abs(startCarPosX - endCarPosX) / animNumSteps;
                                // create and draw cacti
                                drawReturnItems(0, cacti);
                                // car
                                if(getTargetByName(currVehicle.name) == undefined){
                                    macchina = monit.addTarget(startCarPosX, startCarPosY, currVehicle.name);
                                }else{
                                    macchina = getTargetByName(currVehicle.name);
                                }
                                // move y of car
                                // be careful 
                                macchina.posY += (monit.eyeTracker.posY - macchina.posY) * 0.05;
                                constrainItem(
                                    macchina,
                                    0,
                                    windowWidth * 2,
                                    windowHeight,
                                    -windowWidth
                                    )
                                var canStillRun = currVehicle.name.indexOf('right') !== -1 ? macchina.posX < endCarPosX : macchina.posX > endCarPosX;
                                if(canStillRun){
                                    macchina.posX = currVehicle.name.indexOf('right') !== -1 ? macchina.posX + incCar : macchina.posX - incCar;
                                }else{
                                    // go to finish tour animation
                                    replaceCar = currVehicle.name.indexOf('right') !== -1 ? ['right', 'left'] : ['left', 'right'];
                                    currVehicle.name = currVehicle.name.replace(replaceCar[0], replaceCar[1]);
                                    let cardis = getTargetByName(currVehicle.name.replace(replaceCar[1], replaceCar[0]));
                                    cr = monit.addTarget(cardis.posX, cardis.posY, currVehicle.name)
                                    startCarPosX = cr.posX;
                                    monit.removeTarget(cardis);
                                    GameSceneStage = 'game-finish-tour-animation';
                                    drawOnceReturnItems = 0;
                                }
                                checkInteractionReturnItems(macchina, 'cactus');
                                checkReachTarget('cactus');
                                break;
                            case 'game-finish-tour-animation':
                                isStaring = 0; 
                                // change background 
                                if(monit.pState.bg.startsWith('#')){
                                    monit.setBg('sfondo-auto.png');
                                    // set rect sizY
                                    floorCarPosY = windowHeight + 10;
                                }
                                var speedCacti = 20;
                                // clear cactus animation
                                _.remove(animationsStack, d => d.type == 'returnObjContact');
                                // make cactus disappear
                                monit.getTargets().filter(el => cacti.indexOf(monit.itemsNames[el.id]) !== -1).forEach(cactus => {
                                    if(cactus.posY < windowHeight + 400){
                                        cactus.posY += speedCacti;
                                    }else{
                                        monit.removeTarget(cactus);
                                    }
                                });
                                // dune
                                startGroundPosX = windowWidth / 2;
                                startGroundPosY = -windowHeight / 2;
                                endGroundPosY = windowHeight / 2;
                                // car
                                // startCarPosX = assetsDim[currVehicle.name].sizX / 2;
                                startCarPosY = windowWidth / 2;
                                // return path background
                                startRetPathPosX = windowWidth / 2;
                                startRetPathPosY = windowHeight / 2;
                                endRetPathPosY = windowHeight + (windowHeight / 2);
                                partAnimDuration = 2000;
                                animNumSteps = partAnimDuration / 33;

                                incRect = Math.abs(windowHeight - 10) / animNumSteps;
                                incGround = Math.abs(startGroundPosY - endGroundPosY) / animNumSteps;
                                incBackground = Math.abs(startRetPathPosY - endRetPathPosY) / animNumSteps;
                                incCar = Math.abs()
                                // dune
                                if(getDistractorByName(currentPath) == undefined){
                                    var ground = monit.addDistractor(startGroundPosX, startGroundPosY, currentPath);
                                    // dune.sizY = wind
                                }else{
                                    ground = getDistractorByName(currentPath);
                                }                                
                                // car
                                if(getTargetByName(currVehicle.name) == undefined){
                                    macchina = monit.addTarget(startCarPosX, startCarPosY, currVehicle.name);
                                }else{
                                    macchina = getTargetByName(currVehicle.name);
                                }
                                // return path background
                                if(getDistractorByName('return-path.png') == undefined){
                                    var backgroundCar = monit.addDistractor(startRetPathPosX, startRetPathPosY, 'return-path.png');
                                }else{
                                    backgroundCar = getDistractorByName('return-path.png');
                                }
                                // macchina.posY = macchina.posY !== windowHeight / 2 ? macchina.posY < windowHeight / 2  ? macchina.posY + 5 : macchina.posY - 5 : macchina.posY;
                                if(macchina.posY < pathsJsons[currentPath.replace('png', 'json')][80].y - 15){
                                    macchina.posY += 10;
                                }else if(macchina.posY > pathsJsons[currentPath.replace('png', 'json')][80].y + 15){
                                    macchina.posY -= 10;
                                }
                                if(ground.posY < endGroundPosY){
                                    ground.posY += incGround;
                                    backgroundCar.posY += incBackground;
                                    push(); 
                                    noStroke();
                                    fill(238, 211, 112); 
                                    rect(0, windowHeight - floorCarPosY, windowWidth, floorCarPosY)
                                    pop();
                                    floorCarPosY -= incRect;
                                }else{
                                    if(monit.sounds['car-path.wav'].pl.state === 'started'){
                                        monit.stopSound('car-path.wav');
                                        monit.sounds['car-path.wav'].pl.fadeIn = 0;
                                    }
                                    // remove background of return path
                                    monit.removeDistractor(getDistractorByName('return-path.png'));
                                    GameSceneStage = 'game-fixation';
                                    fixationMoved = 0;
                                }
                            // console.log('torno indietro', incDune, dune.posX, endDunePosX);
                                break;
                        }
                        break;
                    //---------------------------------------------------
                    //------------------ SUBMARINE ----------------------
                    //---------------------------------------------------
                    case 2:                    
                        // choosing vehicle and game
                        switch(GameSceneStage){
                            case 'game-fixation':      
                                //add surface
                                startSurfPosX = windowWidth / 2; 
                                startSurfPosY = assetsDim['superficie.png'].sizY / 2; 

                                startSubmPosX = windowWidth / 2; 
                                startSubmPosY = assetsDim['superficie.png'].sizY / 2; 

                                if(getDistractorByName('superficie.png') == undefined){
                                    var surface = monit.addDistractor(startSurfPosX, startSurfPosY, 'superficie.png');
                                    // surface.sizX = windowWidth + 300;
                                    surface.posY = surface.sizY / 2;
                                    console.log('creation surface');
                                }else{
                                    surface = getDistractorByName('superficie.png');
                                }
                                if(isEmpty(surfaceMov)){
                                    console.log('creation surface movement obj')
                                    surfaceMov = new HorizontalMove(surface)
                                }
                                surface.posX = surfaceMov.draw();
                                // add target
                                if(getTargetByName(currVehicle.name) === undefined){
                                    var subM = monit.addTarget(startSubmPosX, startSubmPosY, currVehicle.name);
                                    console.log('creation vehicle')
                                }else{
                                    subM = getTargetByName(currVehicle.name);
                                }
                                if(!positionToReach){
                                    checkInteractionTargets();
                                    movingTarget('down');
                                }else if(!animationsStack.filter(a => a.type == 'chooseGame').length){
                                    if(subM.posX >= animations['chooseGame'].startPositions['sottomarino'].posX ){
                                        let nIteration = 1000 / 33;
                                        subM.posX -= (animations['chooseGame'].chooseGamePositions['sottomarino'].posX - animations['chooseGame'].startPositions['sottomarino'].posX) / nIteration; 
                                        subM.posY -= (animations['chooseGame'].chooseGamePositions['sottomarino'].posY - animations['chooseGame'].startPositions['sottomarino'].posY) / nIteration; 
                                    }else{
                                        subM.posX = windowWidth / 2;
                                        subM.posY = animations['chooseGame'].startPositions['sottomarino'].posY;
                                        positionToReach = 0;
                                    }
                                }
                                break;
                            case 'game-transition-animation':
                                if(monit.sounds['bubble.wav'].pl.state !== 'started'){
                                    playLoopSound('bubble.wav');
                                }
                                switch(GameTransitionAnimStep){
                                    // reach the bottom of the sea with the submarine
                                    case 0:   
                                        isStaring = 0;   
                                        // submarine
                                        if(getTargetByName(currVehicle.name) !== undefined){
                                            monit.removeTarget(getTargetByName(currVehicle.name));
                                        }
                                        // startSubmPosX = windowWidth / 2; 
                                        // startSubmPosY = windowHeight - (assetsDim['superficie.png'].sizY / 2); 
                                        // endSubmPosX = assetsDim[currVehicle.name].sizX / 2; 
                                        endSubmPosY = windowHeight / 2;
                                        // surface
                                        startSurfPosY = assetsDim['superficie.png'].sizY / 2; 
                                        endSurfPosY = -assetsDim['superficie.png'].sizY; 

                                        partAnimDuration = 2000;
                                        animNumSteps = (partAnimDuration / 33); 
                                        // incSubmX = Math.abs(startSubmPosX - endSubmPosX) / animNumSteps;
                                        incSubmY = Math.abs(startSubmPosY - endSubmPosY) / animNumSteps;
                                        incSurf = Math.abs(startSurfPosY - endSurfPosY) / animNumSteps;
                                        // add SUBMARINE
                                        if(getDistractorByName(currVehicle.name) == undefined){
                                            subM = monit.addDistractor(startSubmPosX, startSubmPosY, currVehicle.name);    
                                        }else{
                                            subM = getDistractorByName(currVehicle.name);
                                        }
                                        // add surface
                                        if(getDistractorByName('superficie.png') == undefined){
                                            var surf = monit.addDistractor(startSubmPosX, startSubmPosY, 'superficie.png');    
                                        }else{
                                            surf = getDistractorByName('superficie.png');
                                        }
                                        if(isEmpty(surfaceMov)){
                                            console.log('creation surface movement obj')
                                            surfaceMov = new HorizontalMove(surf)
                                        }
                                        surf.posX = surfaceMov.draw();
                                        // let disappear the surface and moving submarine  
                                        if(subM.posY > windowHeight / 2){
                                            subM.posY -= incSubmY;
                                            // subM.posX -= incSubmX;
                                            surf.posY -= incSurf;
                                            // change bubble speed during descent
                                            Bubbles.speed = 12;
                                        }else{
                                            // remove surface
                                            monit.removeDistractor(surf);
                                            GameTransitionAnimStep = 1;
                                        }
                                        break;
                                        // reaching bottom of the sea
                                    case 1:
                                        isStaring = 0;
                                        surfaceMov = {}
                                         // submarine
                                        startSubmPosX = assetsDim[currVehicle.name].sizX / 2; 
                                        startSubmPosY = (windowHeight / 2) - (assetsDim['superficie.png'].sizY / 2); 
                                        endSubmPosY = windowHeight - (assetsDim[currVehicle.name].sizY / 2);
                                        // bottomsea
                                        startBottSeaPosY = windowHeight + assetsDim['fondale-marino.png'].sizY;
                                        endBottSeaPosY = windowHeight - (assetsDim['fondale-marino.png'].sizY / 2) + 30;
                                        // seashell
                                        // startShellPosY = windowHeight + (assetsDim[currItem.name].sizY);
                                        // startShellPosX = windowWidth / 2;
                                        // endShellPosY = windowHeight - (assetsDim[currItem.name].sizY / 2);

                                        partAnimDuration = 2000;
                                        animNumSteps = (partAnimDuration / 33); 
                                        incSubm = Math.abs(startSubmPosY - endSubmPosY) / animNumSteps;
                                        // incShell = Math.abs(startShellPosY - endShellPosY) / animNumSteps;
                                        incBottSea = Math.abs(startBottSeaPosY - endBottSeaPosY) / animNumSteps;
                                        // add SUBMARINE
                                        if(getDistractorByName(currVehicle.name) == undefined){
                                            var subM = monit.addDistractor(startSubmPosX, startSubmPosY, currVehicle.name);    
                                        }else{
                                            subM = getDistractorByName(currVehicle.name);
                                            // console.log('presente sottomarino')
                                        }
                                        // add BOTTOM SEA
                                        if(getDistractorByName('fondale-marino.png') == undefined){
                                            var seaBottom = monit.addDistractor(startBottSeaPosX, startBottSeaPosY + 20, 'fondale-marino.png');    
                                            seaBottom.sizY = windowWidth + 400;
                                        }else{
                                            seaBottom = getDistractorByName('fondale-marino.png');
                                        }
                                        Bubbles.speed = 2;
                                        if(subM.posY < endSubmPosY){
                                            subM.posY += incSubm;
                                        }else{
                                            GameTransitionAnimStep = 2;
                                            // monit.playSound('win-seashell.wav');
                                        }
                                        // seabottom movement 
                                        if(seaBottom.posY > endBottSeaPosY){
                                            seaBottom.posY -= incBottSea;
                                        }
                                        // if(seaShell.posY > endShellPosY){
                                        //     seaShell.posY -= incShell;
                                        // }
                                        break;
                                        // WAIT FIXATION CHARACTER to get the sea shell
                                    case 2:
                                        isStaring = 1
                                        // if(!animationsStack.filter(an => an.type == 'getShell').length){
                                            var currSub = getDistractorByName(currVehicle.name);
                                            if(currSub !== undefined){
                                                var temp = _.cloneDeep(currSub);
                                                monit.addTarget(temp.posX, temp.posY, monit.itemsNames[temp.id]);
                                                monit.removeDistractor(currSub);
                                            }
                                            // remove sea shell
                                            // if(getTargetByName(currItem.name) !== undefined){
                                            //     monit.removeTarget(getTargetByName(currItem.name));
                                            // }
                                            checkInteractionTargets();
                                            submarinePickUpShell();
                                        // }
                                        break;
                                }
                                break;
                                // character goes up catching fishes all around
                            case 'game-return-path':
                                isStaring = 0;
                                if(monit.sounds['bubble.wav'].pl.state !== 'started'){
                                    playLoopSound('bubble.wav');
                                }
                                // startCharSeaPosX = windowWidth / 2;
                                // startCharSeaPosY = assetsDim['character-sottomarino.png'];
                                // submarine
                                startSubmPosX = windowWidth / 2;
                                startSubmPosY = assetsDim['character-sottomarino.png'];
                                endSubmPosY = 
                                // surface
                                startSurfPosX = windowWidth / 2;
                                startSurfPosY = -assetsDim[currVehicle.name];
                                // add CHARACTER
                                // if(getDistractorByName('character-sottomarino.png') == undefined){
                                //     var subM = monit.addDistractor(startCharSeaPosX, startCharSeaPosY, 'character-sottomarino.png');    
                                // }else{
                                //     subM = getDistractorByName('character-sottomarino.png');
                                // }        
                                // add SUBMARINE
                                if(getDistractorByName(currVehicle.name) == undefined){
                                    subM = monit.addDistractor(startSubmPosX, startSubmPosY, currVehicle.name);    
                                }else{
                                    subM = getDistractorByName(currVehicle.name);
                                }                                
                                // create and draw fishes 
                                var pescioni = ['pesce-A.png', 'pesce-B.png', 'pesce-C.png'];
                                drawReturnItems(4, pescioni);
                                // animate fishes
                                monit.getTargets().forEach(tg => {
                                    if(pescioni.indexOf( monit.itemsNames[tg.id]) !== -1){
                                        // change frame amount
                                        if(monit.animationFramesAmount !== 3){monit.animationFramesAmount = 3;}
                                        monit.animateTarget(tg);
                                    }
                                });
                                // check interaction of character with pescioni
                                checkInteractionReturnItems(subM, 'pesce');
                                checkReachTarget('pesce');
                                var subM = getDistractorByName('character-sottomarino.png');
                                // console.log('charsub', subM)
                                var seaBottom = getDistractorByName('fondale-marino.png');
                                subM = getDistractorByName(currVehicle.name);
                                if(subM.posY > windowHeight / 2){
                                    subM.posY -= 1.5;
                                    subM.posX += (monit.eyeTracker.posX - subM.posX) * 0.05;
                                    if(subM.posX > windowWidth - (assetsDim[currVehicle.name].sizX / 2)){
                                        subM.posX = windowWidth - (assetsDim[currVehicle.name].sizX / 2)
                                    }else if(subM.posX < assetsDim[currVehicle.name].sizX / 2){
                                        subM.posX = assetsDim[currVehicle.name].sizX / 2;
                                    }
                                    // remove from the scene the sea bottom
                                    if(seaBottom !== undefined){
                                        if(seaBottom.posY < windowHeight + seaBottom.sizY){
                                            seaBottom.posY += 3;
                                        }else{
                                            monit.removeDistractor(seaBottom);
                                        }
                                    }
                                }else{
                                    // add next submarine
                                    if(getDistractorByName('superficie.png') == undefined){
                                        surface = monit.addDistractor(windowWidth / 2, -assetsDim['superficie.png'].sizY, 'superficie.png');
                                    }else{
                                        surface = getDistractorByName('superficie.png');
                                    }
                                    // changing next item
                                    // currItem.number = currItem.number == currItem.group.length - 1 ? 0 : currItem.number + 1;
                                    // currItem.setName(currItem.type, currItem.number);
                                    monit.addTarget(subM.posX, subM.posY, currVehicle.name);
                                    monit.removeDistractor(subM)
                                    GameSceneStage = 'game-surface-animation';
                                    drawOnceReturnItems = 0;
                                }
                                break;
                            case 'game-surface-animation':  
                                isStaring = 0;
                                monit.stopSound('bubble.wav');                            
                                // surface
                                startSurfPosX = windowWidth / 2;
                                startSurfPosY = -(assetsDim['superficie.png'].sizY / 2);
                                endSurfPosY = assetsDim['superficie.png'].sizY / 2 - 30;

                                // startSubmPosX = windowWidth / 2;
                                startSubmPosX = windowHeight / 2;
                                startSubmPosY = (windowHeight / 2) - assetsDim[currVehicle.name].sizY / 2;
                                endSubmPosY = assetsDim[currVehicle.name].sizY / 2;

                                partAnimDuration = 1000;
                                animNumSteps = partAnimDuration / 33; 
                                incSurf = Math.abs(startSurfPosY - endSurfPosY) / animNumSteps;
                                incSubm = Math.abs(startSubmPosY - endSubmPosY) / animNumSteps;
                                // add SUBMARINE
                                if(getTargetByName(currVehicle.name) == undefined){
                                    subM = monit.addTarget(startSubmPosX, startSubmPosY, currVehicle.name);    
                                }else{
                                    subM = getTargetByName(currVehicle.name);
                                } 
                                // add next submarine
                                if(getDistractorByName('superficie.png') == undefined){
                                    surface = monit.addDistractor(startSurfPosX, startSurfPosY, 'superficie.png');
                                }else{
                                    surface = getDistractorByName('superficie.png');
                                }
                                surface.sizX = windowWidth + 200;
                                // draw surface movement 
                                if(isEmpty(surfaceMov)){
                                    console.log('creation surface movement obj')
                                    surfaceMov = new HorizontalMove(surface)
                                }
                                surface.posX = surfaceMov.draw();
                                // console.log(subM)
                                // keep moving up vehicle
                                if(surface.posY < endSurfPosY){
                                    surface.posY += incSurf;
                                    subM.posY += 2;
                                    // GameSceneStage = 'game-fixation';
                                }
                                _.remove(animationsStack, d => d.type == 'returnObjContact');
                                // make fishes disappear
                                monit.getTargets().filter(el => monit.itemsNames[el.id].indexOf('pesce') !== -1).forEach(fish => {
                                    if(fish.sizX < 16 || fish.sizY < 16){
                                        monit.removeTarget(fish);
                                    }else{
                                        let imgFish = fish.img.split('-');
                                        imgFish.splice(imgFish.length - 1);
                                        imgFish = imgFish.join('-')
                                        imgFish += '.png';
                                        console.log(imgFish);
                                        fish.sizX -= 4;
                                        fish.sizY -= 4 * (assetsDim[imgFish].sizY / assetsDim[imgFish].sizX);
                                        fish.posY += 10;
                                    }
                                });
                                // horizontal movement
                                if(subM.posX < (windowWidth / 2) - 7){
                                    subM.posX += 7;
                                }else if(subM.posX > (windowWidth / 2) + 7){
                                    subM.posX -= 7;
                                }
                                // vertical movement
                                if(subM.posY > endSubmPosY){
                                    subM.posY -= 4;
                                }else{
                                    // monit.addTarget(windowWidth / 2, assetsDim[currVehicle.name].sizY / 2, currVehicle.name)
                                    // monit.removeDistractor(subM);
                                    GameSceneStage = 'game-fixation';
                                    fixationMoved = 0;
                                }
                                break;
                        }
                        // draw bubbles
                        var mineVehicle = {};
                        if(getTargetByName(currVehicle.name) !== undefined){
                            mineVehicle = getTargetByName(currVehicle.name);
                        }else{
                            if(getDistractorByName(currVehicle.name) !== undefined){
                                mineVehicle = getDistractorByName(currVehicle.name);
                            }
                        }                   
                        if(!isEmpty(mineVehicle)){
                            Bubbles.draw(mineVehicle.posX, mineVehicle.posY);
                        }      
                        break; 
                }
                break;
            case 2: 
                // GAME END 
                break;
        }
        monit.drawDistractors();
        // draw car tail
        if(GameSceneTypeFix == 1 && !animationsStack.filter(d => d.type == 'chooseGame').length){
            drawCarTail(macchina);
        }
        // if(monit.getTargets().length) console.log(monit.getTargets()[0].img)
        monit.drawTargets();
        // monit.monitoring();
        // monit.drawTracker();
        //draw animations if present
        drawAnimations();
        //draw target fire
        if(!GameSceneTypeFix && ( 
            (GameSceneStage == 'game-fixation' || GameSceneStage == 'game-transition-animation') && 
            !animationsStack.filter(a => a.type == 'chooseGame').length) ||
            animationsStack.filter(a => a.type == 'targetOverlap').length){
            rocketFire.draw();
        }
        // draw alfa on target if deactivated eyeTracker
        if(!monit.trackerActive && getTargetByName(currVehicle.name) && isStaring){
            monit.silence();
            push();
            imageMode(CENTER);
            image(monit.images['gradient.png'], getTargetByName(currVehicle.name).posX, getTargetByName(currVehicle.name).posY,);
            pop();
            if(monit.fixationObjects.length){
                monit.fixationObjects = [];
                animationsStack = []
            }
        }
    }else{
        background('black');
        textSize(30);
        stroke('white');
        fill('white')
        textAlign(CENTER, CENTER);
        text('Caricamento in corso...', windowWidth / 2, windowHeight / 2);
        textSize(12);
        noFill();
        noStroke();
    }
}
// check if an object is empty 
function isEmpty(obj) {
    for(var key in obj) {
        if(obj.hasOwnProperty(key))
            return false;
    }
    return true;
}
// conversion rgb to hex
function rgbToHex(r, g, b) {
    return "#" + ((1 << 24) + (r << 16) + (g << 8) + b).toString(16).slice(1);
}

function monitorHoveredColor(){
    push();
    fill('black');
    var pxs = get(mouseX - 20, mouseY - 20);
    text(pxs[0] + ' ' + pxs[1] + ' ' + pxs[2] + ' ', windowWidth - 50, 50);
    stroke('black');
    rectMode(CENTER);
    fill(pxs);
    rect(windowWidth - 100, 100, 100, 100);
    pop();
}
function duneFun(x){
    return (windowHeight / windowWidth) * x;
}

function loadPathsJsons(){
    var paths = ['top_hill.json', 'auto_landscape_01.json', 'auto_landscape_02.json', 'auto_landscape_left_mount.json'],
    counter = 0;
    paths.forEach(p => {
        fetch('pathsCar/' + p)
            .then(response => {
                return response.json();
            })
            .then(data => {         
                // console.log('caricato il json', data)
                // me.line = data.path;
                pathsJsons[p] = data.path;
                normalise(pathsJsons[p]);
                ++counter;
                // console.log('line', p);
                // set the third flag for play games
                // console.log('elementi path json', counter, paths.length)
                if(counter == paths.length){
                    pathsJsonsFl = 1;
                    animations['chooseGame'].chooseGamePositions = {
                        'razzo': {
                            posX: 200,
                            posY: (windowHeight / 4) * 3
                        },
                        'auto': {
                            posX: (windowWidth / 3) + 200,
                            posY: (windowHeight / 4) * 3
                        },
                        'sottomarino': {
                            posX: (windowWidth / 3) * 2 + 200,
                            posY: (windowHeight / 4) * 3
                        }
                    }
                }
            });
    })
}
function normalise(ar){
    ar.forEach(el =>{
        el.x *= (window.innerWidth / 1920);
        el.y *= (window.innerHeight / 1080);
    })
}

function returnFixationChoose(){
    // switch off sounds
    monit.silence();
    setGameState(0, 0, 0);
    GameTransitionAnimStep = 0;
    animationsStack = [];
    pulsatingObjs = [];
    monit.fixationObjects = [];
    car = {};
    positionToReach = 0;
    fixationMoved = 0;
    drawOnceReturnItems = 0;
    currVehicle = new RoutingItem(['A', 'B', 'C'])
    // currDirection = 'right'
    resetAll();
    if(monit.eventconnection.readyState === 1){
        monit.eventconnection.send(JSON.stringify({ts: new Date().getTime(), type: "event", event_type: "change_scene", data: parseInt(0)}));
    }
}
function keyPressed() {
    if (keyCode === LEFT_ARROW) {
        returnFixationChoose();
    }
    if (keyCode === UP_ARROW) {
        changeCarDir();
    }
}
// change direction of the car
function changeCarDir(){
    if(car !== undefined && GameSceneStage == 'game-fixation'){
        console.log('click up')
        car.changeDir();
    }
}
function constrainItem(item, top, right, bottom, left){
    // horizontal
    var refDim = assetsDim[item.img];
    if(item.posX - (refDim.sizX / 2) < left){
        item.posX = left + (refDim.sizX / 2);
    }else if(item.posX + (refDim.sizX / 2) > right){
        item.posX = right - (refDim.sizX / 2);
    }
    // vertical
    if(item.posY - (refDim.sizY / 2) < top){
        item.posY = top + (refDim.sizY / 2);
    }else if(item.posY + (refDim.sizY / 2) > bottom){
        item.posY = bottom - (refDim.sizY / 2);
    }
}
var currReachedTargets = [];
function checkReachTarget(){
    var eyet = monit.eyeTracker;
    monit.getTargets().forEach(tg => {
        var overlapF = checkOverlap({posX: eyet.posX + tg.sizX / 2, posY: eyet.posY + tg.sizY / 2, sizX: eyet.sizX, sizY: eyet.sizY}, tg)
        if(overlapF && currReachedTargets.indexOf(tg.id) == -1){
            monit.sendEvent({ts: new Date().getTime(), type: "event", event_type: "start_fixation", data: tg.id});
            currReachedTargets.push(tg.id)
        }
        if(!overlapF && currReachedTargets.indexOf(tg.id) != -1){
            monit.sendEvent({ts: new Date().getTime(), type: "event", event_type: "end_fixation", data: tg.id});
            _.remove(currReachedTargets, e => e == tg.id);
        }
    })
}