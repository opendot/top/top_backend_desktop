var animations = [],
// background for all game steps
backgroundGame = ['#000', '#c1e5ca','#c1e5ca', '#c1e5ca', '#bbb'],
bgSuggest = '#999',
animationsStack = [],
GameTransitionAnimStep = 0,
assetsDim = {},
catAndObjsJson = {},
catAndObjsJsonFl = 0,
correctObjectMove = 0,
rotatingObjs = [],
countDownIndx = 0,
suggestFl = 0,
// levels object
levels = {},
// three parameters of the game board
gameShuffle = false,
itemsOnBoard = 8,
noiseGrill = 20;
var tabletSettings = [];
var bgEyeTracker = {};
var firstStateButtons = 0;
var indexCoolDown = 0;
var charNameMagic = 'char-magic-stick.png'
var charName = 'char-magic.png'
// LEVELS CLASS in order to handle
var Levels = function(json){
    this.currCategory = parseInt(monit.GameLevel);
    this.currKind = 0;
    this.currKindName = catAndObjsJson.categories[this.currCategory].values[this.currKind];
    this.json = json;
    this.nlevels = 0;
    let me = this;
    this.json.categories.filter(c => c.criterion == catAndObjsJson.categories[this.currCategory].criterion).forEach( c => {
        c.values.forEach( l => {
            me.nlevels++;
        });
    });
    this.clearGame = function(){
        resetAll();
        board = {};
        highlight = {};
        countdown = {};
    }
    this.setNext = function(){
        // end reached
        if(this.currCategory == this.json.categories.length - 1 && this.currKind == this.json.categories[this.currCategory].values.length - 1){
            // monit.onEndSession();
        }else{
            if(this.currKind == this.json.categories[this.currCategory].values.length - 1){
                // this.currCategory = this.currCategory == this.json.categories.length - 1 ? 0 : this.currCategory + 1;
                // this.currKind = 0;
                // noLoop();
                // monit.onEndSession();
            }else{
                this.currKind++;
                this.currKindName = catAndObjsJson.categories[this.currCategory].values[this.currKind];
            }
            // clear everything in order to create a new board game
            this.clearGame()
        }
    }
    this.setLevelRound = function(level, round){
        this.currCategory = level;
        this.currKind = round;
        this.currKindName = catAndObjsJson.categories[this.currCategory].values[this.currKind];
        this.clearGame();
    }
    this.setPrevious = function(){
        if(this.currKind > 0) {
            this.currKind--;
            this.currKindName = catAndObjsJson.categories[this.currCategory].values[this.currKind];
        }
        // else if(this.currCategory > 0)
        else{
            this.currKind = 0;
            this.currKindName = catAndObjsJson.categories[this.currCategory].values[this.currKind];
        }
        this.clearGame()
    }
    var args = (new URL(location.href)).searchParams;
    var level = parseInt(args.get('level'));
    var round = parseInt(args.get('round'));
    if(level !== null && round !== null) this.setLevelRound(level, round);
}
// GAME BOARD   
var Board = function(shuffle, noiseGrill){
    let me = this;
    console.log('board creation');
    this.criterions = catAndObjsJson.categories.map(c => c.criterion);
    this.obj = {};
    // current kind inside a category
    this.selected = [];
    this.completed = [];
    this.currCriterion = this.criterions[levels.currCategory];
    this.currCategory = levels.currKind;
    // categories of classification (es. rosso, tondo)
    this.categories = catAndObjsJson.categories.filter(c => c.criterion == this.criterions[levels.currCategory])[0].values;
    this.elementsCorrect = catAndObjsJson.tuples.filter(t => t.kind == levels.currKindName)[0].tuple.filter(t => t.valid == true).map(t => t.name);
    console.log(this.elementsCorrect)
    this.numObjToGuess = catAndObjsJson.tuples.filter(js => js.category == this.currCriterion && js.kind == this.categories[levels.currKind])[0].tuple.filter(t => t.valid == true).length;
    this.nameBoard = 'board_' + this.numObjToGuess + '.png';
    // moving objects after selection
    this.movingObjs = [];
    // shuffle object array
    this.shuffleObjs = [];
    // easing of the selected object
    this.movementEase = 0.07;
    this.noiseGrill = noiseGrill;
    this.boardObjsInd = 0;
    // this.nPlacements = 5;
    this.objectsTgs = [];
    // flag used to create shuffle array
    this.shufflingFl = 0;
    this.enableShuffle = shuffle;
    // number items to show
    this.itemsOnBoard = catAndObjsJson.tuples.filter(js => js.category == this.currCriterion && js.kind == this.categories[levels.currKind])[0].to_show;
    this.elements = catAndObjsJson.tuples.filter(js => js.category == this.currCriterion && js.kind == this.categories[levels.currKind])[0].tuple.map(el => el.name);
    // lenght of a line in the grill of targets
    this.lineLength = 5;
    this.elements.splice(this.itemsOnBoard);
    // this.elToSuggest = this.elements[0];
    this.elements = _.shuffle(this.elements);
    // CREATE CLASSIFICATION BOARD GAME (only targets)
    this.createBoard = function(){
        // fill array of line in order to draw them in the board.
        drawGrillTargets(this.elements, this.lineLength, windowWidth - 140, windowHeight / 1.8, 70, 100, noiseGrill);
        // get all targets created and save inside the board
        // // create 'board' ditractor
        if(monit.getDistractorByName(this.nameBoard) == undefined){
            // this.obj = monit.addDistractor((windowWidth / 2) + (assetsDim[charName].sizX / 2), (windowHeight / 4) * 3, this.nameBoard);
            this.obj = {posX: (windowWidth / 2) + (assetsDim[charName].sizX / 2), posY: (windowHeight / 3) * 2, sizX: windowWidth - 200 - (assetsDim[charName].sizX / 2), sizY: assetsDim['board_6.png']};
            // set pentole
            if(!monit.getDistractors().filter(d => d.img == 'pentolone.png').length){
                for(var x = 0; x < this.numObjToGuess; x++){
                    monit.addDistractor(
                        (this.obj.posX - (this.obj.sizX / 2)) + ((this.obj.sizX / this.numObjToGuess) / 2) + ((this.obj.sizX / this.numObjToGuess) * x), 
                        this.obj.posY + assetsDim['pentolone.png'].sizY,
                        'pentolone.png'
                        );
                }
            }
            // this.obj.sizX = windowWidth - 200 - (assetsDim[charName].sizX / 2);
        }
    }
    // check choice of the board target
    this.checkChoice = function(){
        let me = this;
        let startBoard = this.obj.posX - (this.obj.sizX / 2),
        placeSize = this.obj.sizX / this.numObjToGuess;
        monit.fixationObjects.forEach( f => {
            if(f.counter > monit.fixationTime){
                monit.sendEvent({ts: new Date().getTime(), type: "event", event_type: "fixation_time_reached", data: f.id});
                var currTg = monit.pState.targets[f.id];
                // RIGHT CHOICE
                if(me.isCorrectObject(currTg.img)){
                    // add target to movingObjs object 
                    me.movingObjs.push({
                        obj: currTg,
                        rX: currTg.posX,
                        rY: currTg.posY,
                        tX: startBoard + (placeSize / 2) + (placeSize * me.boardObjsInd),
                        tY: me.obj.posY + 60
                    })
                    me.boardObjsInd++;
                    if(me.boardObjsInd % 2 == 0) monit.playSound('woo.wav');
                    monit.playSound('corretto.wav');
                    if(!animationsStack.filter(a => a.type == 'bouncing').length){
                        triggerBouncing(monit.getDistractorByName(charName));
                    }
                    if(me.enableShuffle){
                        me.triggerShuffle();
                    }
                    // WRONG CHOICE
                }else{
                    monit.playSound('error.wav');
                    temp = _.cloneDeep(animations['wrong'].obj);
                    temp.timestamp = getCurrTM();
                    temp.refX = currTg.posX;
                    temp.obj = currTg;
                    animationsStack.push(temp);
                    var ch = monit.getDistractorByName(charName); 
                    if(ch !== undefined){
                        ch.img = 'char-magic-sad.png';
                    }
                }
                _.remove(monit.fixationObjects, fr => f.id == fr.id);
                _.remove(animationsStack, d => d.obj.id == f.id && d.type == 'targetOverlap');
            }
        });
    }
    // draw moving object
    this.play = function(){
        let me = this;
        // draw objects on BOARD and PLAY 
        if(!animationsStack.filter(a => a.type == 'wrong').length){
            var movingImgs = [];
            for(var m in this.movingObjs){
                movingImgs.push(this.movingObjs[m].obj.img);
            }
            // avoid target overlap animation during shuffling
            if(!this.shufflingFl && this.completed.length + this.movingObjs.length !== this.numObjToGuess){
                checkInteractionTargets(t => this.completed.indexOf(t.img) === -1 && movingImgs.indexOf(t.img) === -1);
                this.checkChoice();
            }
            // SHUFFLE elements
            var shuffleToRemove = [];
            this.shuffleObjs.forEach( s => {
                if(Math.abs(s.tX - s.obj.posX) < 1 && Math.abs(s.tY - s.obj.posY) < 1){
                    shuffleToRemove.push(s);
                }else{
                    s.obj.posX += (s.tX - s.obj.posX) * 0.1;
                    s.obj.posY += (s.tY - s.obj.posY) * 0.1;
                }
            });
            // removing shuffle objects
            shuffleToRemove.forEach(sr => {
                _.remove(me.shuffleObjs, r => r.obj.id == sr.obj.id);
                if(!me.shuffleObjs.length){
                    me.shufflingFl = 0;
                }
            })
        }
        var movingToRemove = [];
        this.movingObjs.forEach( m => {
            if(Math.abs(m.tX - m.obj.posX) < 1 && Math.abs(m.tY - m.obj.posY) < 1){
                // remove from this array the object that finish to move
                movingToRemove.push(m)
                me.completed.push(m.obj.img);
                // console.log(me.completed, me.movingObjs);
            }else{
                m.obj.posX += (m.tX - m.obj.posX) * me.movementEase;
                m.obj.posY += (m.tY - m.obj.posY) * me.movementEase;
            }
        });
        movingToRemove.forEach(mr => {
            _.remove(me.movingObjs, r => r.obj.id == mr.obj.id);
            if(me.completed.length == me.numObjToGuess && !me.movingObjs.length){
                monit.playSound('completato.wav')
                setGameState(3);
            }
        })
        // reset character img 
        if(monit.getDistractorByName(charName) !== undefined){
            var ch = monit.getDistractorByName(charName); 
            if(ch.img !== charName && !animationsStack.filter(a => a.type == 'bouncing' || a.type == 'wrong').length){
                ch.img = charName;
            }
        }
    }
    this.drawPlacements = function(){
        let startBoard = this.obj.posX - (this.obj.sizX / 2),
        placeSize = this.obj.sizX / this.numObjToGuess;
        for(var x = 0; x < this.numObjToGuess; x++){
            let posX = startBoard + (placeSize / 2) + (placeSize * x),
            posY = this.obj.posY;
            push();
            fill('black');
            rectMode(CENTER);
            rect(posX, posY, 50, 50);
            pop();
        }
    }
    this.getCorrectEls = function(){
        return catAndObjsJson.tuples.filter(t => t.category == board.currCriterion && t.kind == levels.currKindName)[0].tuple.filter(t => t.valid).map(t => t.name);
    }
    this.triggerShuffle = function(){
        let me = this;
        if(!this.shufflingFl){
            var els = _.cloneDeep(this.elements), 
            currPositions = [],
            movingImgs = [];
            for(var m in this.movingObjs){
                movingImgs.push(this.movingObjs[m].obj.img);
            }
            els = els.filter(e => this.completed.indexOf(e) == -1 && movingImgs.indexOf(e) == -1);
            newPositions = getGrillArr(els, this.lineLength, windowWidth, windowHeight / 2, 0, 100, 10);
            // shuffle array of images 
            els = _.shuffle(els);
            els.forEach( (el, i) => {
                let currTg = monit.getTargetByName(el);
                me.shuffleObjs.push({
                    obj: currTg,
                    rX: currTg.posX,
                    rY: currTg.posY,
                    tX: newPositions[i].x,
                    tY: newPositions[i].y
                });
            });
            this.shufflingFl = 1;
        }
    }
    this.isCorrectObject = function(img){
        var verdict = false;
        catAndObjsJson.tuples.filter(t => t.category == this.currCriterion && t.kind == levels.currKindName).forEach(tpl => {
            tpl.tuple.forEach(image => {
                if(image.name == img && image.valid){
                    verdict = true;
                }
            })
        });
        return verdict;
    }
};
// circle to fixation
var Circle = function(x, y, dur){
    this.ind = 0;
    this.r = 100;
    this.x = x;
    this.y = y;
    this.done = 0;
    this.inc = (2 * PI) / (dur / 33);
    this.draw = function(){
        if(this.ind < 2 * PI){
            // push();
            // fill(255, 0, 95);
            // strokeWeight(4);
            // stroke('white');
            // arc(this.x, this.y, this.r, this.r, PI, this.ind + PI);
            // pop();
             push();
        // noStroke();
            stroke(200);
            fill(255,100)
            ellipse(this.x, this.y, this.r, this.r);
            fill(255,210);
            arc(this.x, this.y, this.r, this.r, -PI/2 + this.ind, (3/2)*PI, PIE); //
            pop();

            this.ind += this.inc;
        }else{
            this.done = 1;
        }
    }
};

var roulette = {};
var roulAss = {
    shapes: [
        'nuvola-tondi.png',
        'nuvola-quadrati.png',
        'nuvola-triangoli.png',
        'nuvola-rettangoli.png',
        'nuvola-cose-piccole-graficamente.png',
        'nuvola-cose-piccole-reali.png',
        'nuvola-cose-grandi-graficamente.png',
        'nuvola-cose-grandi-reali.png'
    ],
    colors: [
        'nuvola-roulette-nera.png', 
        'nuvola-roulette-rossa.png', 
        'nuvola-roulette-dettaglio-rosso.png', 
        'nuvola-roulette-verde.png', 
        'nuvola-roulette-dettaglio-blu.png', 
        'nuvola-roulette-gialla.png'
    ],
    colorsAndShapes: [
        'nuvola-tondi-rossi.png',
        'nuvola-tondi-gialli.png',
        'nuvola-cose-piccole-gialle.png',
        'nuvola-cose-grandi-nere.png',
        'nuvola-rettangoli-verdi.png',
        'nuvola-cose-piccole-blu.png'
    ],
    semantic: [
        'criterio-mangiare.png',
        'criterio-cielo.png',
        'criterio-spiaggia.png',
        'criterio-cucina.png',
        'criterio-bagno.png',
        'criterio-inverno.png'
    ],
    action: [
        'criterio-giocare.png',
        'criterio-lavare.png',
        'criterio-vestire.png',
        'criterio-viaggiare.png',
        'criterio-scrivere.png',
        'criterio-dormire.png',
    ]
}
var Roulette = function(){
    // add distractor high zIndex 
    this.obj = monit.addDistractor(windowWidth / 2, windowHeight / 2, 'nuvola-roulette.png', 1);
    this.witch = monit.addDistractor(windowWidth / 6, windowHeight / 2, charNameMagic);
    this.animCounter = 0;
    this.running = true;
    this.block = false;
    this.elementsDst = [];
    this.rotIndMax = (2 * PI) / 30;
    this.start = false;
    this.currRouletteItem = '';
    this.rouletteColorTimeRef = new Date().getTime();
    this.timer = new Date().getTime();
    this.singleAnimate = function(dist, nframes){
        if(this.animCounter < nframes * 2) {
            this.animCounter++;
            monit.animateDistractor(dist, undefined, nframes);
        }
    }
    this.rotateShape = function(){
        let me = this;
        this.elementsDst.forEach((el, i) => {
            el.rot += (me.rotIndMax / roulAss.shapes.length) * (i + 1);
            el.rot %= (2 * PI);
        })
    }
    this.drawShape = function(){
        let me = this;
        this.elementsToDraw = _.shuffle(roulAss.shapes);
        this.speed = 0;
        this.maxSpeed = 10;
        this.speeds = [];
        if(!this.start){
            this.elementsToDraw.forEach( (el, i) => {
                this.elementsDst.push(monit.addDistractor(windowWidth / 2, windowHeight / 2, el, 2));
                this.speeds[el] = (this.rotIndMax / roulAss.shapes.length) * (i + 1);
            });
            this.start = true;
        }
        if(this.running){
            // rotate elements
            this.rotateShape();
        }else if(!this.block){
            var currDs = monit.getDistractorByName(roulAss.shapes[levels.currKind]);
            if(currDs !== undefined){
                if(currDs.rot < (2*PI)){
                    this.elementsDst.forEach((el, i) => {
                        el.rot += (me.rotIndMax / roulAss.shapes.length) * (i + 1);
                    })
                }else{
                    currDs.rot = 0;
                    currDs.img = currDs.img.replace('-hide', '');
                    this.elementsDst.filter(d => d.img != currDs.img).forEach(el => {
                        monit.removeDistractor(el);
                    });
                    this.block = true;
                    pulsatingObjs.push(new Pulse(currDs))
                } 
            }
        }
        pulsatingObjs.forEach(el => {
            el.draw();
        })
    }   
    this.rouletteItems = function(rouletteElements){
        if(new Date().getTime() - this.rouletteColorTimeRef > 200 && !this.block){
            var ds = monit.getDistractors().filter(d => d.img == this.currRouletteItem)[0];
            if(ds !== undefined){
                var index = 0;
                if(rouletteElements.indexOf(this.currRouletteItem) + 1 == rouletteElements.length){
                    index = 0  
                }else{
                    index = rouletteElements.indexOf(this.currRouletteItem) + 1;
                }
                // ds.img = rouletteElements[index]
                monit.removeDistractor(monit.getDistractorByName(this.currRouletteItem))
                var ds = monit.addDistractor(windowWidth / 2, windowHeight / 2, rouletteElements[index], 2);
                this.currRouletteItem = ds.img;
                if(!this.running && rouletteElements.indexOf(this.currRouletteItem) == levels.currKind){
                    monit.playSound('stop_roulette_magic.mp3');
                    this.block = true;
                }
            }
            this.rouletteColorTimeRef = new Date().getTime();
        }else if(this.block){
            this.singleAnimate(this.witch, 4)
        }
    }
    this.drawColor = function(){
        if(!this.start) {
            monit.removeDistractor(monit.getDistractorByName('nuvola-roulette.png'));
            this.currRouletteItem = roulAss.colors[0];
            monit.addDistractor(windowWidth / 2, windowHeight / 2, this.currRouletteItem)
            this.start = true;
        }
        this.rouletteItems(roulAss.colors);
    }
    this.drawColorAndShape = function(){
        if(!this.start){
            this.currRouletteItem = roulAss.colorsAndShapes[0];
            monit.addDistractor(windowWidth / 2, windowHeight / 2, this.currRouletteItem, 2)
            this.start = true;
        }
        this.rouletteItems(roulAss.colorsAndShapes);
    }
    this.drawNewShapes = function(arr){
        if(!this.start){
            this.currRouletteItem = arr[0];
            monit.addDistractor(windowWidth / 2, windowHeight / 2, this.currRouletteItem, 2)
            this.start = true;
        }
        this.rouletteItems(arr);
    }
    this.drawAnimationSemanticAction = function(arr){
        if(!this.start){
            monit.removeDistractor(monit.getDistractorByName('nuvola-roulette.png'));
            this.currRouletteItem = arr[levels.currKind];
            monit.addDistractor(windowWidth / 2, windowHeight / 2, this.currRouletteItem)
            this.start = true;
        }
        // console.log(this.currRouletteItem.indexOf('viaggiare') , this.currRouletteItem )
        // if(this.currRouletteItem.indexOf('viaggiare') !== -1){
        //     monit.animationRate = 15
        // }else if(monit.animationRate != 2){
        //     monit.animationRate = 2;
        // }
        if(monit.animationRate != 15){monit.animationRate = 15;}
        monit.animateDistractor(monit.getDistractorByName(this.currRouletteItem), undefined, 6);
    }
    this.draw = function(){
        if(new Date().getTime() - this.timer > 2000) this.running = false;
        switch(monit.GameLevel){
            case 0:
                this.drawNewShapes(roulAss.shapes);
                break;
            case 1:
                this.drawColor()
                break;
            case 2:
                this.drawColorAndShape()
                break;
            case 3:
                this.drawAnimationSemanticAction(roulAss.semantic);
                break;
            case 4:
                this.drawAnimationSemanticAction(roulAss.action);
                break;
        }
    }
}
var timerSuggest = 0; 
var animationObj = undefined;
var image2Show = undefined;
var loadPulse = false;
function animateImage(localImg){
    if(monit.counter % monit.animationRate == 0){
        if(animationObj === undefined){
            animationObj = {inc: 1, val:1};
        }
        var counter = animationObj.val;
        if(counter == 5){
            animationObj.inc = -1;
        }else if(counter == 1){
            animationObj.inc = 1;
        } 
        animationObj.val += animationObj.inc;
        image2Show = localImg.replace('.png', '') + '-' + counter + '.png';
    }
}
// DRAW SUGGEST ANIMATION
function drawSuggest(){
    // init suggest
    if(timerSuggest == 0) {
        // monit.setBg(bgSuggest);
        timerSuggest = new Date().getTime();
    }
    background(bgSuggest);
    push();
    imageMode(CENTER);
    switch(monit.GameLevel){
        case 0:
            var currImg = roulAss.shapes[levels.currKind];
            image(monit.images['nuvola-roulette.png'], windowWidth / 2, windowHeight / 2, assetsDim['nuvola-roulette.png'].sizX, assetsDim['nuvola-roulette.png'].sizY);
            image(monit.images[currImg], windowWidth / 2, windowHeight / 2, assetsDim[currImg].sizX, assetsDim[currImg].sizY);
            break;
        case 1:
            currImg = roulAss.colors[levels.currKind];
            image(monit.images[currImg], windowWidth / 2, windowHeight / 2, assetsDim[currImg].sizX, assetsDim[currImg].sizY);
            console.log(currImg)
            break;
        case 2:
            currImg = roulAss.colorsAndShapes[levels.currKind];
            image(monit.images['nuvola-roulette.png'], windowWidth / 2, windowHeight / 2, assetsDim['nuvola-roulette.png'].sizX, assetsDim['nuvola-roulette.png'].sizY);
            image(monit.images[currImg], windowWidth / 2, windowHeight / 2, assetsDim[currImg].sizX, assetsDim[currImg].sizY);
            break;
        case 3:
            currImg = roulAss.semantic[levels.currKind];
            animateImage(currImg);
            if(image2Show !== undefined) image(monit.images[image2Show], windowWidth / 2, windowHeight / 2, assetsDim[image2Show].sizX, assetsDim[image2Show].sizY);
            break;
        case 4:
            currImg = roulAss.action[levels.currKind];
            animateImage(currImg);
            if(image2Show !== undefined) image(monit.images[image2Show], windowWidth / 2, windowHeight / 2, assetsDim[image2Show].sizX, assetsDim[image2Show].sizY);
            break;
    }
    // if([0, 1, 2].indexOf(monit.GameScene) !== -1 && !loadPulse) {
    //     pulsatingObjs.push(new Pulse({
    //         posX: windowWidth / 2, 
    //         posY: windowHeight / 2, 
    //         sizX: assetsDim[currImg].sizX, 
    //         sizY: assetsDim[currImg].sizY, 
    //         img: monit.images[currImg]
    //     }));
    //     console.log('create new pulse', )
    //     loadPulse = true;
    // }
    // pulsatingObjs.forEach(el => {
    //     el.draw();
    // })
    pop();

    if(new Date().getTime() - timerSuggest > 3000){
        timerSuggest = 0;
        suggestFl = 0;
        animationObj = undefined;
        image2Show = undefined;
        pulsatingObjs = [];
    }
}


// fridgeGame
var FridgeGame = function(){
    this.elements = catAndObjsJson.tuples.filter(t => 
        catAndObjsJson.cool_down[indexCoolDown].assets_location.category == t.category && 
        catAndObjsJson.cool_down[indexCoolDown].assets_location.kind == t.kind)[0]
        .tuple.filter(i => i.valid == true).map(i => i.name)
    console.log('elements', this.elements);
    // this.elements = ['camera.png', 'ombrello.png', 'cuore.png', 'cappello.png', 'infradito.png', 'palma.png'];
    this.cartolina = catAndObjsJson.cool_down[indexCoolDown].nome;
    // indexCoolDown++;
    indexCoolDown %= catAndObjsJson.cool_down.length;
    this.obj = {};
    this.currMagnet = {ind: 0, tg: {}, animInd: 0, ref: {}};
    // current kind inside a category
    this.completed = [];
    // moving objects after selection
    this.movingObjs = [];
    this.toChoosePos = [];
    this.chosenCircles = [];
    this.magnets = [];
    this.circlesIds = [];
    // lenght of a line in the grill of targets
    this.lineLength = 3;
    this.movementEase = 0.07;
    this.indexGrill = 0;
    this.timerExitCoolDown = 0;
    this.grillValues = getGrillArr(this.elements, 3, windowWidth / 2.5, windowHeight / 2, 300, windowHeight / 2.5, 20);
    this.elementsGrillValues = getGrillArr(this.elements, 1, windowWidth / 3, windowHeight, ((windowWidth / 3) * 2) + 30, windowHeight / 8, 0)
    // CREATE CLASSIFICATION BOARD GAME (only targets)
    this.createBoard = function(){
        // fill array of line in order to draw them in the board.
        // drawGrillTargets(this.elements, this.lineLength, windowWidth / 3, windowHeight / 2, windowWidth / 2, windowHeight / 6, 30);

        // // create sea photo ditractor
        if(monit.getDistractorByName(this.cartolina) == undefined){
            this.obj = monit.addDistractor((assetsDim[this.cartolina].sizX / 2), (windowHeight / 2) + 100, this.cartolina);
        }
    }
    this.moveFridgeMagnet = function(){
        let me = this;
        monit.fixationObjects.forEach( f => {
            if(f.counter > monit.fixationTime){
                monit.sendEvent({ts: new Date().getTime(), type: "event", event_type: "fixation_time_reached", data: f.id});
                // return to previous size
                me.currMagnet.tg.sizX = me.currMagnet.ref.sizX;
                me.currMagnet.tg.sizY = me.currMagnet.ref.sizY;
                me.currMagnet.ref = {};
                me.currMagnet.animInd = 0;
                var currTg = me.currMagnet.tg;
                // var currTg = monit.pState.targets[f.id];
                var target = monit.pState.targets[f.id];
                me.movingObjs.push({
                    obj: currTg,
                    rX: currTg.posX,
                    rY: currTg.posY,
                    // tX: me.grillValues[me.indexGrill].x,
                    // tY: me.grillValues[me.indexGrill].y
                    tX: target.posX,
                    tY: target.posY
                });
                monit.playSound('corretto.wav');
                // me.indexGrill++;
                me.currMagnet.ind++;
                _.remove(monit.fixationObjects, fr => f.id == fr.id);
                _.remove(animationsStack, d => d.obj.id == f.id && d.type == 'targetOverlap');
                // remove target after fixation
                monit.removeTarget(f);
            }
        });
    };
    // draw moving object
    this.play = function(){
        // draw objects on BOARD and PLAY 
        var movingImgs = [];
        for(var m in this.movingObjs){
            movingImgs.push(this.movingObjs[m].obj.img);
        }
        checkInteractionTargets(t => t.img == 'alpha-circle.png');
        // checkInteractionTargets(t => this.completed.indexOf(t.img) === -1 && movingImgs.indexOf(t.img) === -1);
        this.moveFridgeMagnet();
        let me = this;
        this.movingObjs.forEach( m => {
            if(Math.abs(m.tX - m.obj.posX) > 1 && Math.abs(m.tX - m.obj.posX) > 1){
                m.obj.posX += (m.tX - m.obj.posX) * me.movementEase;
                m.obj.posY += (m.tY - m.obj.posY) * me.movementEase;
            }else{
                // remove from this array the object that finish to move
                _.remove(me.movingObjs, r => r.obj.id == m.obj.id);
                me.completed.push(m.obj.img);
                if(me.completed.length == me.elements.length){
                    // setGameState(3);
                    monit.playSound('completato.wav');
                }
            }
        });
    }
    this.createChooseGrill = function(){
        let me = this;
        if(isEmpty(this.obj)){
            this.grillValues.forEach( g => {
                var currTg = monit.addTarget(g.x, g.y, 'alpha-circle.png');
                me.toChoosePos.push({tg: currTg, ind: 0});
                me.circlesIds.push(currTg.id);
            });
            this.elementsGrillValues.forEach( (g, i) => {
                me.magnets.push(monit.addTarget(g.x, g.y, me.elements[i]));
            });
        }
        if(this.currMagnet.tg.img !== this.elements[this.currMagnet.ind] && this.currMagnet.ind < this.elements.length){
        // if(monit.getTargetByName(this.elements[this.currMagnet.ind]) == undefined){
            this.currMagnet.tg = monit.getTargetByName(this.elements[this.currMagnet.ind]);
            this.currMagnet.ref = {sizX: this.currMagnet.tg.sizX, sizY: this.currMagnet.tg.sizY}
        }
        // make pulse targets
        if(!isEmpty(this.currMagnet.ref) && monit.trackerActive){
            this.currMagnet.animInd = this.currMagnet.animInd >= 2 * PI ? 0 + this.currMagnet.animInd % (2 * PI) : this.currMagnet.animInd + (2 * PI) / (1000 / 33);
            this.currMagnet.tg.sizX = this.currMagnet.ref.sizX + 10 * sin(this.currMagnet.animInd);
            this.currMagnet.tg.sizY = this.currMagnet.ref.sizY + ((10 * sin(this.currMagnet.animInd)) * (this.currMagnet.ref.sizY / this.currMagnet.ref.sizX));
        }

        if(monit.getDistractorByName(this.cartolina) == undefined){
            this.obj = monit.addDistractor((assetsDim[this.cartolina].sizX / 2), (windowHeight / 2) + 100, this.cartolina);
        }
    }
    this.playChoose = function(){
        var animDur = 1000;
        // animate circles
        if(monit.trackerActive){
            this.toChoosePos.forEach( c => {
                if(!monit.fixationObjects.filter(f => f.id == c.tg.id ).length){
                    c.tg.sizX = assetsDim['alpha-circle.png'].sizX + (10 * sin(c.ind));
                    c.tg.sizY = assetsDim['alpha-circle.png'].sizY + (10 * sin(c.ind));
                    c.ind = c.ind >= 2 * PI ? 0 + c.ind % (2 * PI) : c.ind + (2 * PI) / (animDur / 33);
                }
            });
        }
        checkInteractionTargets(t => t.img == 'alpha-circle.png');
        this.moveFridgeMagnet();
        let me = this;
        this.movingObjs.forEach( m => {
            if(Math.abs(m.tX - m.obj.posX) > 1 || Math.abs(m.tX - m.obj.posX) > 1){
                m.obj.posX += (m.tX - m.obj.posX) * me.movementEase;
                m.obj.posY += (m.tY - m.obj.posY) * me.movementEase;
            }else{
                // remove from this array the object that finish to move
                _.remove(me.movingObjs, r => r.obj.id == m.obj.id);
                me.completed.push(m.obj.img);
                // me.currMagnet.ind++;
                if(me.completed.length == me.elements.length){
                    // setGameState(3);
                    monit.playSound('completato.wav');
                }
            }
        });
        // exit from cool-down part
        if(this.completed.length == this.elements.length){
            if(me.timerExitCoolDown == 0) {
                me.timerExitCoolDown = new Date().getTime();
            }
            if(new Date().getTime() - me.timerExitCoolDown > 2000) {
                goNextScene();
            }
        }
    }
}
// BOARD OBJECT
var board = {},
// FRIDGE OBJECT
fridge = {};
var Pulse = function(obj){
    this.obj = obj;
    this.refX = obj.sizX;
    this.refY = obj.sizY;
    this.sInd = 0;
    this.amp = 40;
    this.period = 1000;
    this.draw = function(obj){
        this.obj.sizX = this.refX + this.amp * sin(this.sInd);
        this.obj.sizY = this.refY + this.amp * sin(this.sInd) * 
        (assetsDim[this.obj.img].sizY / assetsDim[this.obj.img].sizX);
        this.sInd = this.sInd >= 2 * PI ? 0 + this.sInd % (2 * PI) : this.sInd + (2 * PI) / (this.period / 33);
    }
};
var countdown = {};
var CountDown = function(to){
    this.dur = 3000;
    this.index = 0;
    this.count = function(){
        var currInd = this.index % (this.dur / 3),
        currImg = 3;
        push();
        imageMode(CENTER);
        var sizX = map(currInd, 0, (this.dur / 3), assetsDim['1.png'].sizX, assetsDim['1.png'].sizX + 200),
        sizY = sizX * (assetsDim['1.png'].sizY / assetsDim['1.png'].sizX)
        if(this.index > 0 && this.index < this.dur / 3){
            currImg = 3;
        }else if(this.index < (this.dur / 3) * 2){
            currImg = 2;
        }else if(this.index < this.dur){
            currImg = 1;
        }else{ // end timeout
            pop();
            setGameState(to);
            return;
        }
        image(monit.images[currImg + '.png'], windowWidth / 2, windowHeight / 2, sizX, sizY);
        fill('rgba(0,0,0,' + map(currInd, 0, (this.dur / 3), 0, 1) + ')');
        rect(0, 0, windowWidth, windowHeight);
        pop();
        this.index += this.dur / (this.dur / 33);
    }
}

// array of pulsating objects
var pulsatingObjs = [],
surfaceMov = {},
currInstr = {};
animations['targetOverlap'] = { 
    cb: function(el) {
        el.circle.draw();
        el.index += el.end / (el.end / 33);
    },
    obj: {
        type: 'targetOverlap', 
        id: '', 
        index: 0,
        obj: {}, 
        timestamp: 0, 
        end: 500
    }
};
// bouncing animation on character
animations['bouncing'] = {
    cb: function(el){
        el.obj.posY = el.refY + -40 * cos(map(el.index, 0, el.end, 0, PI / 2)) * Math.abs(sin(map(el.index, 0, el.end, 10, 20)));
        el.index += (el.end / 33);
    },
    obj:{
        type: 'bouncing',
        timestamp: getCurrTM(),
        index: 0,
        obj: {},
        refY: 0,
        end: 2000
    }
}
// suggest animation
animations['suggest'] = {
    cb: function(el){
        el.index += el.end / (el.end / 33)
    },
    obj: {
        type: 'suggest',
        index: 0,
        timestamp: 0,
        obj: {},
        end: 3000
    }
}
// completed animation
animations['completed'] = {
    cb: function(el){
        el.index += el.end / (el.end / 33);
    },
    obj: {
        type: 'completed',
        index: 0,
        timestamp: 0,
        obj: {},
        name: '',
        end: 3000
    }
}
animations['wrong'] = {
    cb: function(el){
        el.obj.posX = sin(map(el.index, 0, el.end, 0, PI)) * 10 * sin(map(el.index, 0, el.end, 0, 5 * (2 * PI))) + el.refX;
        el.index += (el.end / 33);
    },
    obj: {
        type: 'wrong',
        index: 0,
        refX: 0,
        timestamp: 0,
        obj: {},
        end: 1000
    }
}
function getDimensionsEls(el){
    if(levels.currKindName.indexOf('grafica') !== -1){
        if(levels.currKindName.indexOf('piccola') !== -1){
            if(board.elementsCorrect.indexOf(el) == -1){
                return [assetsDim[el].sizX * 1.4, assetsDim[el].sizY * 1.4 * (assetsDim[el].sizY / assetsDim[el].sizX)];
            }else{
                return [assetsDim[el].sizX, assetsDim[el].sizY];
            }
        }else if(levels.currKindName.indexOf('grande') !== -1){
            if(board.elementsCorrect.indexOf(el) == -1){
                return [assetsDim[el].sizX * 0.5, assetsDim[el].sizY * 0.5 * (assetsDim[el].sizY / assetsDim[el].sizX)];
            }else{
                return [assetsDim[el].sizX, assetsDim[el].sizY];
            }
        }
    }else{
        return [assetsDim[el].sizX, assetsDim[el].sizY];
    }
}
// draw a grill of element
// element: targets to draw in 'img.png'
// lineLength: how many items per line
// grillW: width of the grill in px
// grillH: height of the grill in px
// offX: offset of the grill along x axis
// offY: offset of the grill along y axis
// noise: randomize position in % of box size (width and height)
function drawGrillTargets(elements, lineLength, grillW, grillH, offX, offY, noise){
    var positions = getGrillArr(elements, lineLength, grillW, grillH, offX, offY, noise);
    positions.forEach( (p, i) => {
        if(monit.getTargetByName(elements[i]) == undefined){
            var dims = getDimensionsEls(elements[i]);
            var crTg = monit.addTarget(
                p.x, 
                p.y, 
                elements[i], 
                (board.elementsCorrect.indexOf(elements[i]) !== -1 ? 1 : 0)
            );
            crTg.sizX = dims[0];
            crTg.sizY = dims[1];
            if(!isEmpty(board)){
                if(board.getCorrectEls().indexOf(elements[i]) == -1) crTg.correct = false;
            }
        }
    });
}
// like draw grill, but return only values, it doesn't draw anything
function getGrillArr(elements, lineLength, grillW, grillH, offX, offY, noise){
    var groupObjects = [];
    var temp = [],
    array = [];
    elements.forEach( (el, i) => {
        temp.push(el);
        if((i + 1) % lineLength == 0){
            groupObjects.push(temp);
            temp = [];
        }
    });
    groupObjects.push(temp);
    // set objects disposition
    nLines = groupObjects.length,
    boxH = grillH / nLines;
    groupObjects.forEach( (line, x) => {
        line.forEach( (el, y) => {
            // if(monit.getTargetByName(el) == undefined){
                boxW = grillW / line.length;
                posX = (boxW / 2) + (boxW * y) + offX + random(-(boxW * noise) / 100, (boxW * noise) / 100);
                posY = (boxH / 2) + (boxH * x) + offY + random(-(boxH * noise) / 100, (boxH * noise) / 100);
                array.push({x: posX, y: posY});
            // }
        });                 
    });
    return array;
}
// CHECK INTERACTION WITH TARGETS (add/remove elements to/from fixation object array)
function checkInteractionTargets(ruleFilter){
    if(monit.trackerActive){
        // check only for elements still to choose
        monit.getTargets().filter(ruleFilter).forEach( Tg => {
            // var Tg = tgs[x];
            var eyeT = monit.eyeTracker,
            currTarget = {
                posX: Tg.posX,
                posY: Tg.posY,
                sizX: Tg.sizX,
                sizY: Tg.sizY
            };
            // compensate offset for overlap
            if(checkOverlap({posX: eyeT.posX + Tg.sizX / 2, posY: eyeT.posY + Tg.sizY / 2, sizX: eyeT.sizX, sizY: eyeT.sizY}, currTarget)){
                if(_.find(monit.fixationObjects, d => d.id === Tg.id) == undefined){
                    // monit.sendEvent({ts: new Date().getTime(), type: "event", event_type: "start_fixation", data: Tg.id})
                    // add target to the interacted list
                    monit.fixationObjects.push({id: Tg.id, counter: 0});
                    var temp = _.cloneDeep(animations['targetOverlap'].obj);
                    temp.timestamp = getCurrTM();
                    temp.obj = Tg;
                    temp.end = monit.fixationTime;
                    temp.circle = new Circle(Tg.posX, Tg.posY, temp.end);
                    animationsStack.push(temp)
                }else{
                    var temp = _.find(monit.fixationObjects, d => d.id === Tg.id);
                    temp.counter += 33.33;
                }
            }else{
                if(monit.fixationObjects.filter(f => f.id == Tg.id).length > 0){
                    _.remove(monit.fixationObjects, d => d.id === Tg.id);
                    // monit.sendEvent({ts: new Date().getTime(), type: "event", event_type: "end_fixation", data: Tg.id});
                }
                _.remove(animationsStack, d => d.obj.id == Tg.id && d.type == 'targetOverlap');
            }
        });
    }
}
// set fixation settings
function loadTabletSettings(){
        let args = (new URL(location.href)).searchParams;
        tabletSettings['game'] = {
            ft: args.get('ftGame') !== null ? parseInt(args.get('ftGame')) : 500,
            shuffle: args.get('shuffle') !== null ? (args.get('shuffle') == 'true') : false
        };
        monit.fixationTime = args.get('ft') !== null ? parseInt(args.get('ft')) : 500;
        gameShuffle = tabletSettings['game'].shuffle;
        console.log('shuffle', gameShuffle)
        tabletSettings['cooling'] = {
            ft: args.get('ftCooling') !== null ? parseInt(args.get('ftCooling')) : 500,
        };
}
// reset targets distractor and fixation objects
function resetAll(){
    // monit.fixationObjects = [];
    monit.clearTargets();
    monit.clearDistractors();
}
function drawAnimations(){
    animationsStack.forEach(animObj =>{
        // console.log(animObj);
        if(animObj.index > animObj.end){
            _.remove(animationsStack, d => d.obj.id == animObj.obj.id && d.type == animObj.type);
        }else{
            animations[animObj.type].cb(animObj);
        }
    })
}
// trigger all overcast
function triggerBouncing(obj){
    var temp = _.cloneDeep(animations['bouncing'].obj);
    temp.timestamp = getCurrTM();
    temp.obj = obj;
    temp.refY = obj.posY;
    animationsStack.push(temp);
    monit.getDistractorByName(charName).img = 'char-magic-4.png';
}
// fill assets dimension object
function setAssetsSizes(){
    monit.json.details.ingame_assets.forEach(el => {
        assetsDim[el.name] = {sizX: el.sizX * (window.innerWidth / monit.idealRes.width), sizY: el.sizY * (window.innerHeight / monit.idealRes.height)}
    });
    console.log('assets dimension', assetsDim);
}
// sin objects
var tgsHurra = [],
hurraInd = 0;
function drawSinHurraObjects(){
    var tgs = monit.getTargets();
    if(!tgsHurra.length){
        tgs.forEach( t => {
            tgsHurra.push({refY: t.posY, tg: t});
        });
    }
    // console.log('here', tgsHurra);
    tgsHurra.forEach( (t, i) => {
        t.tg.posY = t.refY + -40 * Math.abs(sin((hurraInd + map(i, 0, tgsHurra.length, 0, PI / 2)) % (2 * PI)))
    });
    hurraInd = hurraInd > 2 * PI ? hurraInd % (2 * PI) : hurraInd + (PI / (500 / 33))
}
// draw FINISH COLLECTED objects
function drawFinishCollect(){
    drawGrillTargets(board.completed, board.completed.length, windowWidth - (assetsDim[charName].sizX), 600, assetsDim[charName].sizX, (windowHeight / 2) - 200, 0);
    drawSinHurraObjects();
}
// -------------- PRELOAD --------------
function preload() {
    monit.loadJsonItems();
}
// -------------- SETUP ----------------
function setup() {
    createCanvas(windowWidth,windowHeight);
    frameRate(30);
    setGameState(0);
    monit.onAttentionSig = () => {
        if(monit.GameScene == 2 && !suggestFl){
            suggestFl = 1;
            monit.playSound('richiamo.mp3');
            // var temp = _.cloneDeep(animations['suggest'].obj);
            // temp.obj = monit.getDistractorByName(charName);
            // temp.timestamp = getCurrTM();
            // animationsStack.push(temp);
        }
    }
    fetch('./classificazione.json')
        .then(response => {
            return response.json();
        })
        // get categories and items json 
        .then(data =>{
            catAndObjsJson = data;            
            catAndObjsJsonFl = 1
        });
    // redefine change dynamic
    monit.onChangeDynamics = function(data){
        var type = data.id;
        switch(type){
            case 'ft':
                // tabletSettings['game'].ft = data.value;
                monit.fixationTime = parseInt(data.value);
                break;
            case 'shuffle':
                // tabletSettings['game'].shuffle = data.value;
                gameShuffle = data.value;
                if(monit.GameScene == 2 && !isEmpty(board)){
                    board.enableShuffle = gameShuffle;
                }
                break;
        }
    }
    // redefine add target
    monit.onAddTarget = () => {
        // if(monit.GameScene !== 2){
        //     goNextScene();
        // }
    }
    monit.eventconnection.onmessage = (message) => {
        const msg = JSON.parse(message.data)
       if(monit.loaded && catAndObjsJsonFl){
            switch(msg.type){
               case "event":
                   console.log('messaggione', msg)
                   switch(msg.event_type){
                       case "add_target":
                           console.log('addTarget', msg.data.type);
                           monit.onAddTarget(msg.data.x, msg.data.y, msg.data.type);
                           break;
                       case "remove_target":
                            monit.onRemoveTarget(msg.data.id);
                            break;
                       case "move_target":
                            monit.onMoveTarget(msg.data);
                            break;
                       case "toggle_tracker":
                           monit.onToggleTracker(msg.data);
                           break;
                       case "end_session":
                           monit.onEndSession();
                           break;
                       case "attention_sig":
                           monit.onAttentionSig();
                           break;
                       case "change_dynamics":
                           monit.onChangeDynamics(msg.data)
                           break;
                       case "next_scene":  
                            if(monit.GameScene !== 4){
                                goNextScene();
                            }else{
                                indexCoolDown++;
                                indexCoolDown %= catAndObjsJson.cool_down.length;
                                setGameState(4);
                            }
                           break;
                       case "back_scene":
                            if(monit.GameScene == 0 || monit.GameScene == 1 || monit.GameScene == 2) {
                               levels.setPrevious();
                               setGameState(0, false);
                            }else if(monit.GameScene == 4){
                                // indexCoolDown = 0
                                indexCoolDown = indexCoolDown == 0 ? catAndObjsJson.cool_down.length - 1 : indexCoolDown - 1;
                                setGameState(4);
                            }
                           break;
                       case "toggle_fatigue":
                           coolDownToggle()
                           break;
                       default:
                           break;
                   }
                   break;
               default:
                   break
            }
        }
    };
    monit.onMoveTarget = () =>{};
    // redefine remove target: select target to go make choice from the tablet 
    monit.onRemoveTarget = id => {
        if(monit.GameScene == 2 && !isEmpty(board)){
            if(!board.movingObjs.length){
                let startBoard = board.obj.posX - (board.obj.sizX / 2),
                placeSize = board.obj.sizX / board.numObjToGuess;
                var currTg = monit.pState.targets[id];
                if(!board.completed.filter(img => img == currTg.img).length){
                // RIGHT CHOICE
                    if(board.isCorrectObject(currTg.img)){
                        // add target to movingObjs object 
                        board.movingObjs.push({
                            obj: currTg,
                            rX: currTg.posX,
                            rY: currTg.posY,
                            tX: startBoard + (placeSize / 2) + (placeSize * board.boardObjsInd),
                            tY: board.obj.posY
                        })
                        board.boardObjsInd++;
                        monit.playSound('woo.wav');
                        triggerBouncing(monit.getDistractorByName(charName));
                        if(board.enableShuffle){
                            board.triggerShuffle();
                        }
                        // WRONG CHOICE
                    }else{
                        monit.playSound('error.wav');
                        temp = _.cloneDeep(animations['wrong'].obj);
                        temp.timestamp = getCurrTM();
                        temp.refX = currTg.posX;
                        temp.obj = currTg;
                        animationsStack.push(temp);
                        var ch = monit.getDistractorByName(charName); 
                        if(ch !== undefined){
                            ch.img = 'char-magic-sad.png';
                        }
                    }
                }
            }
        }else if(monit.GameScene == 4 && !isEmpty(fridge)) {
            if(!fridge.movingObjs.length && monit.pState.targets[id].img == 'alpha-circle.png'){
                // return to previous size
                // fridge.currMagnet.tg.sizX = fridge.currMagnet.ref.sizX;
                // fridge.currMagnet.tg.sizY = fridge.currMagnet.ref.sizY;
                // fridge.currMagnet.ref = {};
                // fridge.currMagnet.animInd = 0;
                if(fridge.currMagnet.tg.img !== fridge.elements[fridge.currMagnet.ind] && fridge.currMagnet.ind < fridge.elements.length){
                // if(monit.getTargetByName(fridge.elements[fridge.currMagnet.ind]) == undefined){
                    fridge.currMagnet.tg = monit.getTargetByName(fridge.elements[fridge.currMagnet.ind]);
                    fridge.currMagnet.ref = {sizX: fridge.currMagnet.tg.sizX, sizY: fridge.currMagnet.tg.sizY}
                }
                var currTg = fridge.currMagnet.tg;
                var target = monit.pState.targets[id];
                fridge.movingObjs.push({
                    obj: currTg,
                    rX: currTg.posX,
                    rY: currTg.posY,
                    tX: target.posX,
                    tY: target.posY
                });
                monit.playSound('corretto.wav');
                fridge.currMagnet.ind++;
                // me.indexGrill++;
                _.remove(monit.fixationObjects, fr => f.id == fr.id);
                _.remove(animationsStack, d => d.obj.id == f.id && d.type == 'targetOverlap');
                // remove target after fixation
                monit.removeTargetById(id);
            }
        }else{
            // goNextScene();
        }
    }
    monit.skipIntro = 0;
}
// -------------- DRAW -----------------
function draw() {
    if(monit.loaded && catAndObjsJsonFl){
        if(isEmpty(assetsDim)){setAssetsSizes()} // set assets dimension object 
        if(isEmpty(tabletSettings)){loadTabletSettings()} // set dynamics from tablet
        if(isEmpty(levels)){levels = new Levels(catAndObjsJson)};    // create levels class
        if(isEmpty(board)){board = new Board(gameShuffle, noiseGrill)};    // create board game
        if(!firstStateButtons){
            monit.sendEvent({
                ts: new Date().getTime(), 
                type: "event", 
                event_type: "buttons_state", 
                data: {
                    cooldown: true, 
                    back: levels.currKind == 0 ? false : true, 
                    next: levels.currKind == levels.nlevels - 1 ? false : true
                }
            });
            firstStateButtons = 1;
        }
        if(isEmpty(bgEyeTracker)) {bgEyeTracker = new Bg(frameRate())}
        monit.drawBg();
        bgEyeTracker.run();
        if(!suggestFl){
            switch(monit.GameScene){
                case 0:
                    if(monit.skipIntro){
                        setGameState(1);
                    }else{
                        if(isEmpty(countdown)){
                            countdown = new CountDown(1);
                        }
                        countdown.count();
                    }
                    break;
                // SHOW DOMINION CLASSIFICATION
                case 1:
                    if(!isEmpty(countdown)) countdown = {};
                    if(isEmpty(roulette) && !isEmpty(board)) {roulette = new Roulette()} 
                    roulette.draw();
                    break;
                    // PLAY WITCH GAME
                case 2:
                    // create character
                    if(monit.getDistractorByName(charName) == undefined){
                        monit.addDistractor(assetsDim[charName].sizX / 2, windowHeight - assetsDim[charName].sizY / 2, charName);
                    }
                    board.createBoard();
                    board.play();
                    checkReachTarget();
                    break;
                // draw finish collect animation
                case 3:
                    drawFinishCollect();
                    if(monit.getDistractorByName(charName) == undefined){
                        var char = monit.addDistractor(assetsDim[charName].sizX / 2, windowHeight / 2, charName);
                    }else{
                        char = monit.getDistractorByName(charName)
                    }
                    monit.animateDistractor(char, '', undefined, 4)
                    break;
                // FRIDGE DEFATICATION PART
                case 4: 
                    if(isEmpty(fridge)) fridge = new FridgeGame();     // fridge game
                    fridge.createChooseGrill();
                    fridge.playChoose();
                    checkReachTarget();
                    break;
            }
            monit.drawDistractors();
            monit.drawTargets();
        }else{
            drawSuggest();
        }
        if(monit.trackerActive){
            push();
            noStroke();
            // monit.drawTracker();
            pop();
            bgEyeTracker.fade();
        }else{
            bgEyeTracker.fade();
            if(monit.fixationObjects.length) {monit.fixationObjects = [];} // empty fixation objects 
        }
        // monit.monitoring();
        //draw animations if present
        drawAnimations();
    }else{
        loading();
    }
}
// restart game part
function gameAgain(){
    levels.currCategory = 0;
    board = {};
    setGameState(1);
    tgsHurra = [];
    highlight = {};
}
function loading(){
    push();
    background('black');
    textSize(30);
    stroke('white');
    fill('white');
    textAlign(CENTER, CENTER);
    text('Caricamento in corso...', windowWidth / 2, windowHeight / 2);
    pop();
}

// KEY PRESSED FUNCTION
function keyPressed(){
    // console.log(keyCode)
    switch(keyCode){
        case UP_ARROW:
            coolDownToggle();
            // monit.onAttentionSig();
            break;
        case RIGHT_ARROW:
            goNextScene();
            break;
        case DOWN_ARROW:
            if(!isEmpty(roulette)){
                monit.playSound('stop_roulette_magic.mp3');
                roulette.running = !roulette.running;
            } 
            break;
    }
}
// SET GAME STATE 
function setGameState(gs, next){
    var nextLev = next == undefined ? true : next;
    noLoop();
    monit.sendEvent({ts: new Date().getTime(), type: "event", event_type: "change_scene", data: parseInt(gs)});
    // console.log(gs);
    var oldGameScene = monit.GameScene;
    monit.GameScene = gs;
    monit.setBg(backgroundGame[monit.GameScene]);
    monit.fixationObjects = [];
    animationsStack = [];
    pulsatingObjs = [];
    switch(monit.GameScene){
        case 0: 
            if(!isEmpty(levels) && oldGameScene !== 4 && nextLev){
                levels.setNext();
            }
            var backB = levels.currKind != 0;
            monit.sendEvent({ts: new Date().getTime(), type: "event", event_type: "buttons_state", data: {cooldown: true, back: backB, next:true}});
            break;
        case 1:
            monit.sendEvent({ts: new Date().getTime(), type: "event", event_type: "buttons_state", data: {cooldown: true, back: true, next: true}});
            break;
        case 2:
            board = {};
            // monit.fixationTime = tabletSettings['game'].ft;
            if(levels.currKind == levels.nlevels - 1){
                monit.sendEvent({ts: new Date().getTime(), type: "event", event_type: "buttons_state", data: {cooldown:true, back: true, next: false}});
            }else{
                monit.sendEvent({ts: new Date().getTime(), type: "event", event_type: "buttons_state", data: {cooldown: true, back: true, next: true}});
            }
            break;
        case 3:
            monit.sendEvent({ts: new Date().getTime(), type: "event", event_type: "buttons_state", data: {cooldown: true, back: true, next: true}});
            break;
        case 4:
            // if(oldGameScene == 4){
            monit.sendEvent({ts: new Date().getTime(), type: "event", event_type: "buttons_state", data: {cooldown:true, back: true, next: true}});
            // }else{
            //     monit.sendEvent({ts: new Date().getTime(), type: "event", event_type: "buttons_state", data: {cooldown:true, back: false, next: false}});
            // }
            fridge = {};
            // monit.fixationTime = tabletSettings['cooling'].ft;
            break;
    }
    if(!isEmpty(bgEyeTracker))bgEyeTracker.update();
    roulette = {};
    tgsHurra = [];
    resetAll();
    loop();
}
function coolDownToggle(){
    if(monit.GameScene == 4){
        setGameState(0);
    }else{
        setGameState(4);
    }
}

function goNextScene(){
    var next = 0;
    switch(monit.GameScene){
        case 2:
            next = 1;
            levels.setNext();
            break;
        case 3:
            next = 0
            break;
        case 4:
            setGameState(4);
            break;
        default:
            next = monit.GameScene + 1
            break;
    }
    setGameState(next);
}
var currReachedTargets = [];
function checkReachTarget(){
    var eyet = monit.eyeTracker;
    monit.getTargets().forEach(tg => {
        var overlapF = checkOverlap({posX: eyet.posX + tg.sizX / 2, posY: eyet.posY + tg.sizY / 2, sizX: eyet.sizX, sizY: eyet.sizY}, tg)
        if(overlapF && currReachedTargets.indexOf(tg.id) == -1){
            monit.sendEvent({ts: new Date().getTime(), type: "event", event_type: "start_fixation", data: tg.id});
            currReachedTargets.push(tg.id)
        }
        if(!overlapF && currReachedTargets.indexOf(tg.id) != -1){
            monit.sendEvent({ts: new Date().getTime(), type: "event", event_type: "end_fixation", data: tg.id});
            _.remove(currReachedTargets, e => e == tg.id);
        }
    })
}