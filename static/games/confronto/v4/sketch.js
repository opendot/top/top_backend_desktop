//state machine
const stateMachine = [
    init,               
    startScene,       
    cutScene,       
    beforeComparison,  
    countdown,     
    play,    
    endScene,      
    endGame,
    cooldown   
];

const STARTSCENE = 1;
const CUTSCENE = 2;
const BEFORE = 3;
const COUNTDOWN = 4;
const PLAY = 5;
const ENDSCENE = 6;
const ENDGAME = 7;
const COOLDOWN = 8;

var currentState = 0;

//cooldown state machine
const stateMachineCD = [
    initCD,
    playCD,
    shootCD,
    seePictureCD,
    exitCD
];
var currentStateCD = 0;

var timer = {};
var timerValue = 5000;
var inhibitNext = false;

var isHint = false;
var isTap = false;
var isTimeout = false;
var isAlreadyHint = 0;
var gazeTime = 0;
var isFirstEndScene = true;
var countdownImages = ["1.png","2.png","3.png","stella.png"];
var countdownCounter = 0;
var currentCharacter = "";
var character = {};

var collisionWithColor = true;
var pixelColor = {before:[],after:[]}; 
var gazeX = 0;
var gazeY = 0;

var zoomCounter = 0;

var hintToggle = 0;
var positionToggle = 0;

//
var numberOfRounds = 0;
var gameRound = 0;

// animations
var animations = [];
var animationsStack = [];
var animationsToSplice = [];
var movements = [];
var movementsStack = [];
var myFrameRate = 30;

var fEngine = {};
var levelSpecs = {};
var currentRound = {};
var separatorStroke = 8;
var charName = 'char.png';

//temp
var buttons = {
    back:false,
    next:false,
    cooldown: false
}

animations['move'] = {
    cb: function(el){

        el.distX = el.endPosX - el.beginPosX; //unefficient
        el.distY = el.endPosY - el.beginPosY; //unefficient

        el.prog = map(el.index,0,el.end,0,PI);

        el.item.posX = el.beginPosX + ((1-cos(el.prog))/2) * el.distX;
        el.item.posY = el.beginPosY + ((1-cos(el.prog))/2) * el.distY;

        el.index += 1000/myFrameRate;//frameRate();
        
    },
    obj:{
        type: 'move',
        timestamp: getCurrTM(),
        item:{},
        index: 0,
        end: 1500,
        beginPosX:0,
        beginPosY:0,
        endPosX:0,
        endPosY:0,
        distX:0,
        distY:0,
        prog:0
    }
}

animations['countdown'] = {
    cb: function(el){

        var dimX = monit.images[el.image].width*0.7;
        var dimY = monit.images[el.image].height*0.7; 
        var alpha = 255;

        if (el.isGrow) {
            dimX = map(el.index,0,el.end,el.beginSizX,el.endSizX);
            dimY = map(el.index,0,el.end,el.beginSizY,el.endSizY);
        }
        if (el.isFade) {
            alpha = (1-cos(map(el.index,0,el.end,0,2*PI)))/2;
        }

        push();
        imageMode(CENTER);
        tint(255,alpha*255)
        image(monit.images[el.image],currentRound.position == 'left' ? windowWidth*3/4 : windowWidth/4,windowHeight/2,dimX,dimY);
        pop();

        el.index += 1000/myFrameRate;//frameRate();
        
    },
    obj:{
        type: 'countdown',
        timestamp: getCurrTM(),
        image: "",
        isFade: 0,
        isGrow: 0,
        index: 0,
        end: 1000,
        beginSizX:0,
        beginSizY:0,
        endSizX:0,
        endSizY:0
    }
}

animations['attention'] = {
    cb: function(el){

        var progress = map(el.index,0,el.end,0,10*PI);
        el.item.rot = radians(20)*sin(progress*10)*exp(-progress/3);

        el.index += 1000/myFrameRate;//frameRate();
    },
    obj:{
        type: 'attention',
        timestamp: getCurrTM(),
        item: {},
        index: 0,
        end: 2000
    }
}

animations['win'] = {
    cb: function(el){

        var dim = map(el.index,0,el.end,0,windowWidth*10);

        // gameRound > 4 ? fill('#D9858B') : fill('#ffeea7');
        // ellipse(el.beginPosX,el.beginPosY,dim,dim);

        // ellipse(el.item.posX,el.item.posY,dim,dim);
        // monit.drawTargets();

        el.item.sizX = dim;
        el.item.sizY = dim;
        // console.log("dim",dim)

        el.index += 1000/myFrameRate;//frameRate();
    },
    obj:{
        type: 'win',
        item: {},
        timestamp: getCurrTM(),
        index: 0,
        end: 6000
    }
}

animations['fade'] = {
    cb: function(el){

        el.prog = 255*(1+sin(2*PI*map(el.index,0,el.end,0,1)-PI/2))/2;
        push();
        fill(0,el.prog)
        rect(-windowWidth*2,-windowHeight*2,windowWidth*4,windowHeight*4);
        pop();
        el.index += 1000/frameRate();
        
    },
    obj:{
        type: 'fade',
        timestamp: getCurrTM(),
        index: 0,
        end: 1000,
        prog:0
    }
}

animations['selected'] = {
    cb: function(el){

        el.prog = map(el.index,0,el.end,0,1);

        var x = (1+sin(15*2*PI*el.prog-PI/2))/2;//+atan(50*el.prog)/(PI/2))/2;

        var dim = 10;

        el.item.sizX = el.beginSizX + dim*x;
        el.item.sizY = el.beginSizY + dim*x;        

        el.index += 1000/myFrameRate;//frameRate();

    },
    obj:{
        type: 'selected',
        item:{},
        beginSizX:0,
        beginSizY:0,
        timestamp: getCurrTM(),
        index: 0,
        end: 6000
    }
}

function preload() {
    monit.loadJsonItems();
}

function setup() {
    
    createCanvas(windowWidth,windowHeight);
    noStroke();
    frameRate(myFrameRate);
    loadMyJson("./confronto.json");
    loadTabletSettings();

    monit.setBg('#b9cce1');

}

//do nothing until the flag monit.started is false
function checkFlag() {

    if(!monit.started || !monit.loaded) {
       setTimeout(checkFlag, 100); /* this checks the flag every 100 milliseconds*/   
    }
}

function draw() {

    if(!monit.started || !monit.loaded) {
        setTimeout(checkFlag, 100); /* this checks the flag every 100 milliseconds*/
        background('black');
        textSize(30);
        stroke('white');
        fill('white')
        textAlign(CENTER, CENTER);
        text('Caricamento in corso...', windowWidth / 2, windowHeight / 2); 
    } else {
        
        // console.log("stato: ",currentState);  	
        //draw the background
        monit.drawBg();// if (currentState < COOLDOWN) monit.drawBg();
        if(levelSpecs && levelSpecs.bg) levelSpecs.bg.run();


        if (currentState < stateMachine.length) {
            stateMachine[currentState]();
        }
        if(currentRound && currentRound.separator) currentRound.separator.col = monit.pState.bg;
        // console.log(stateMachine[currentState].name);
        // console.log("next: ",buttons.next,"back: ",buttons.back,"cooldown: ",buttons.cooldown);

        //real fps on top left corner
        // fill(0); //black text
        // text(frameRate(), 10, 10);
    }
}

function gaze() {
    if (monit.trackerActive) {
        gazeX = monit.mode == 'eye' ? monit.gaze[0] * window.innerWidth : mouseX;
        gazeY = monit.mode == 'eye' ? monit.gaze[1] * window.innerHeight : mouseY;
    }
}

function colorCollide(tgt) {

    if (gazeX > tgt.posX - tgt.sizX / 2 && 
        gazeX < tgt.posX + tgt.sizX / 2 && 
        gazeY > tgt.posY - tgt.sizY / 2 && 
        gazeY < tgt.posY + tgt.sizY / 2 &&
        pixelColor.before.toString() != pixelColor.after.toString()) {
        fEngine.tgtGazed(tgt.id);
    } else {
        fEngine.tgtNotGazed(tgt.id);
    }
}

function stdCollide(tgt) {
    //console.log(tgt.posX, tgt.posY, monit.gaze);
    if (gazeX > tgt.posX - tgt.sizX / 2 && 
        gazeX < tgt.posX + tgt.sizX / 2 && 
        gazeY > tgt.posY - tgt.sizY / 2 && 
        gazeY < tgt.posY + tgt.sizY / 2) {
        fEngine.tgtGazed(tgt.id);
    } else {
        fEngine.tgtNotGazed(tgt.id);
    }
}

function getCurrTM(){

    return (new Date()).getTime();

}

function init() {
    
    //slow down default animation rate
    monit.animationRate = 3;
    // monit.init();
    overrideMonit();
    if (monit.GameLevel == null) monit.GameLevel = 1; //temp
    if(!monit.fixationTime) monit.fixationTime = 200; //3000
    // gameRound = 4;

    numberOfRounds = levelSpecs.levels[monit.GameLevel].rounds.length;

    // monit.fixationTime = 1000;
    // monit.GameLevel = 0;
    
    fEngine = new Fixation(myFrameRate,monit.fixationTime,targetGazed);
    levelSpecs.screenShots = [];

    //update the state
    currentState++;

}

function startScene() {

    animationsStack = [];
    monit.pState.targets = {};
    monit.pState.distractors = {};
    isFirstEndScene = true;
    hintToggle = 0;
    zoomCounter = 0;
    inhibitNext = false;
    gazeTime = 0;
    state = 0;
    isTap = false;
    currentRound.flip = false;

    currentRound = levelSpecs.levels[monit.GameLevel].rounds[gameRound];
    

    monit.setBg(currentRound.background);
    levelSpecs.bg = new Bg(myFrameRate);

    levelSpecs.bg.rect.zIndex = 5;

    //place the character in the middle of the screen before the comparison
    // distractor 0 is the "before" character
    // monit.addDistractor(windowWidth/2,windowHeight/2,currentCharacter);
    addBeforeElement(windowWidth/2,windowHeight/2);

    
    //update the state
    setButtonsState({next:true,back:true,cooldown:true});
    currentState++;
    // currentState = 4; //temp

}
var tempTempo = 0;
function cutScene() {

    // background('black');
    // textSize(30);
    // stroke('white');
    // fill('white')
    // textAlign(CENTER, CENTER);
    // text("ROUND "+gameRound, windowWidth/2, windowHeight/2);

    // tempTempo+=1000/myFrameRate;
    // if(tempTempo>1500) {
        currentState++;
    //     tempTempo = 0;
    // }
}

function beforeComparison() {

    var distr = monit.getDistractors();
    
    if (isTap) {
        setButtonsState({next:false,back:false,cooldown:false});
        flip();
        setTimeout(()=>{
            flip();
        },800);

        setTimeout(()=>{
            moveBeforeElement({distPosX: currentRound.position == 'left' ? -windowWidth/4 : +windowWidth/4,type:"sin",duration:1000});
            currentRound.curtain = monit.addDistractor(currentRound.position == 'left' ? windowWidth*3/2+separatorStroke*3 : -windowWidth/2-separatorStroke*3,windowHeight/2,"rect",15,currentRound.curtainColor,windowWidth+separatorStroke*3,windowHeight);
            currentRound.hintCurtain = monit.addDistractor(currentRound.position == 'left' ? -windowWidth/2-separatorStroke*3 : windowWidth*3/2+separatorStroke*3,windowHeight/2,"rect",15,currentRound.curtainColor,windowWidth+separatorStroke*3,windowHeight);
            initMovement(currentRound.curtain,{endPosX: currentRound.position == 'left' ? windowWidth : 0,type:"sin",duration:1000});
            timer = setTimeout(()=>{
                currentState++;
                isTimeout = true;
                inhibitNext = false;
                countdownCounter = 0;
                if(currentRound.separator) {
                    currentRound.separator.posX = currentRound.overflow=='left' ? windowWidth/4-separatorStroke/2 : windowWidth*3/4+separatorStroke/2;
                }
                if (currentRound.position == "left") {
                    currentRound.difference.ds = monit.addDistractor(windowWidth*3/4,windowHeight/2,currentRound.difference.base,10);
                } else if (currentRound.position == "right") {
                    currentRound.difference.ds = monit.addDistractor(windowWidth/4,windowHeight/2,currentRound.difference.base,10);
                }
                addTargets();
            },1000);
        },1600);

        inhibitNext = true;
        isTap = false;
    }
    
    // monit.drawDistractors();
    drawDistrButOne(currentRound.original.ds);
    drawOneDistractor(currentRound.original.ds,currentRound.flip)
    animateElements();
    moveElements();

}

function countdown() {

    if (!animationsStack.length && countdownCounter < 4){
        var obj = {};
        Object.assign(obj, animations['countdown'].obj);
        obj.timestamp = getCurrTM();
        obj.image = countdownImages[countdownCounter];
        obj.beginSizX = monit.images[countdownImages[countdownCounter]].width * 0.8;
        obj.beginSizY = monit.images[countdownImages[countdownCounter]].height * 0.8;
        obj.endSizX = monit.images[countdownImages[countdownCounter]].width * 1.2;
        obj.endSizY = monit.images[countdownImages[countdownCounter]].height * 1.2;
        obj.isFade = countdownCounter < 3;
        obj.isGrow = false; //countdownCounter < 3;

        animationsStack.push(obj);
        countdownCounter++;
    } else if (!animationsStack.length && countdownCounter == 4) {
        initMovement(currentRound.curtain,{endPosX: currentRound.position == 'left' ? windowWidth*3/2+separatorStroke*3 : -windowWidth/2-separatorStroke*3,type:"sin",duration:1000});
        if(currentRound.hide) hideBeforeElement();
        countdownCounter++;
    } else if (!movementsStack.length && countdownCounter == 5) {
        // initScene();
        animationsStack = [];  
        isHint = false;
        setButtonsState({next:gameRound<numberOfRounds,back:true,cooldown:true});
        currentState++;
    }

    drawDistrButList([currentRound.curtain,currentRound.hintCurtain]);
    monit.drawTargets();
    drawSeparator();
    drawOneDistractor(currentRound.curtain);
    drawOneDistractor(currentRound.hintCurtain);
    animateElements();
    moveElements();
}

function play() {

    // if(monit.getDistractors().filter(el=>el.id == currentRound.curtain.id).length) monit.removeDistractor(currentRound.curtain);

    gaze();

    
    drawDistrButList([currentRound.hintCurtain]);
    //pick the color before drawing the target
    pixelColor.before = get(gazeX,gazeY);
    monit.drawTargets();
    //pick the color after drawing the target
    pixelColor.after = get(gazeX,gazeY);
    drawSeparator();
    drawOneDistractor(currentRound.hintCurtain);
    animateElements();
    moveElements();
    hint();

    if(currentState == PLAY) {
        //check if the pointer is on any target (collision)
        currentRound.difference.elements.filter(el=> !el.notTarget && !el.gazed).forEach(d => {
            d.shapeCollide ? stdCollide(d.tgt) : colorCollide(d.tgt);
        })
    }
}

function endScene() {
    // console.log("endScene")

    if (isFirstEndScene) {
        
        monit.removeDistractor(currentRound.curtain);
        currentRound.difference.ds.zIndex = 20;
        currentRound.difference.elements.forEach((el)=>{
            el.tgt.zIndex=21;
        })
        var circle = monit.addDistractor(currentRound.difference.ds.posX,currentRound.difference.ds.posY,'ellipse',currentRound.difference.ds.zIndex-1,currentRound.hurraColor,0,0);

        var obj = {};
        Object.assign(obj, animations['win'].obj);
        obj.item = circle;
        obj.timestamp = getCurrTM();
        animationsStack.push(obj);

        timer = setTimeout(function(){goToNextScene()},10000);

        isFirstEndScene = false;
        // // changePath = 1;
    }

    if(zoomCounter<30) zoomCounter++;
    var zoom = map(zoomCounter,0,30,1,1.5);
    var shift = map(zoomCounter,0,30,0,windowWidth/4);

    // push();
    translate(windowWidth/2,windowHeight/2);
    scale(zoom);
    translate(-windowWidth/2,-windowHeight/2);
    if (currentRound.position == "left") {
        translate(-shift,0);
    } else if (currentRound.position == "right") {
        translate(shift,0);
    }
    drawSeparator();
    
    animateElmentsByType('win');
    monit.drawDistractors();
    monit.drawTargets();
    animateElmentsByType('selected');
    animateElmentsByType('fade');

    // pop();
}

function endGame() {
    monit.animateDistractor(character,"-hurra");
    currentRound.friends.forEach((friend)=>{
        monit.animateDistractor(friend,"_hurra");
    })
    monit.drawDistractors();
    moveElements();
    animateElements();
    // background('black');
    // textSize(30);
    // stroke('white');
    // fill('white')
    // textAlign(CENTER,CENTER);
    // text('EVVIVA!', windowWidth/2, windowHeight/2);    
}

function cooldown() {
    if (currentStateCD < stateMachineCD.length) {
        stateMachineCD[currentStateCD]();
    }
    // console.log(stateMachineCD[currentStateCD].name);
}

var state = 0;
function initCD() {

    switch(state) {
        case 0:
            initMovement(currentRound.curtain,{endPosX: currentRound.position == 'left'? windowWidth : 0,type:'sin',duration:1000});
            if(currentRound.hide) {
                initMovement(currentRound.hintCurtain,{endPosX: currentRound.position == 'left' ? -windowWidth/2-separatorStroke*3 : +windowWidth*3/2+separatorStroke*3,type:'sin',duration:1000});
            }
            drawDistrButList([currentRound.hintCurtain,currentRound.curtain]);
            // monit.drawDistractors();
            monit.drawTargets();
            drawSeparator();
            drawOneDistractor(currentRound.hintCurtain)
            drawOneDistractor(currentRound.curtain)
            initializeCD();
            // currentRound.cellBorders = {
            //     posX:levelSpecs.cooldown.cell.posX,
            //     posY:levelSpecs.cooldown.cell.posY,
            //     sizX:levelSpecs.cooldown.cell.sizX,
            //     sizY:levelSpecs.cooldown.cell.sizY
            // }
            // if(levelSpecs.cooldown.jumps.length == 5) levelSpecs.cooldown.jumps.unshift({posX:windowWidth/2,posY:windowHeight/2,sizX:700,sizY:700,rot:0});
            // currentRound.screenShotTimer = levelSpecs.cooldown.screenShotTimer;
            // currentRound.showTimer = true;
            // currentRound.counter = 0;
            // currentRound.filter =  new movingAverageVect(15);
            // currentRound.flashCounter = 0;
            // currentRound.cell = {};
            state++;
            break;
        case 1:
            drawDistrButList([currentRound.hintCurtain,currentRound.curtain]);
            // monit.drawDistractors();
            monit.drawTargets();
            drawSeparator();
            drawOneDistractor(currentRound.hintCurtain)
            drawOneDistractor(currentRound.curtain)
            if(!movementsStack.length) state++;
            break;
        case 2:
            setTimeout(()=>{
                initMovement(currentRound.curtain,{endPosX: currentRound.position == 'left' ? windowWidth*3/2+separatorStroke*3 : -windowWidth*3/2-separatorStroke*3,type:'sin',duration:1000});
            },50);
            var before = getOriginalElements();
            var after = getDifferenceElements();
            before.forEach((el)=>{
                initMovement(el,{distPosX:currentRound.position == 'left' ? windowWidth/4 : -windowWidth/4,type:'sin',duration:1000});
            });
            after.forEach((el)=>{
                initMovement(el,{distPosX:currentRound.position == 'left' ? windowWidth : -windowWidth,type:'sin',duration:1000});
            });
            initMovement(currentRound.separator,{distPosX:currentRound.position == 'left' ? windowWidth : -windowWidth,type:'sin',duration:1000});
            monit.drawDistractors();
            state++
            break;
        case 3:
            monit.drawDistractors();
            if(!movementsStack.length) state++;
            break;
        case 4:
            if(currentRound.cooldownFromBefore) initializeCD();
            monit.drawDistractors();
            currentRound.cell = monit.addDistractor(windowWidth/2,windowHeight*2,"defaticamento_cellulare.png",20);
            initMovement(currentRound.cell,{endPosY:windowHeight,type:"sin",duration:1500});
            state++;
            break;
        case 5:
            monit.drawDistractors();
            currentRound.filter.getNextValue(createVector(currentRound.cell.posX,currentRound.cell.posY));
            if(!movementsStack.length) {
                setButtonsState({cooldown:true});
                currentStateCD++;
            };
            break;
        default:
            monit.drawDistractors();
            break;
    }
    moveElements();
}

function initializeCD() {
    currentRound.cellBorders = {
        posX:levelSpecs.cooldown.cell.posX,
        posY:levelSpecs.cooldown.cell.posY,
        sizX:levelSpecs.cooldown.cell.sizX,
        sizY:levelSpecs.cooldown.cell.sizY
    }
    if(levelSpecs.cooldown.jumps.length == 5) levelSpecs.cooldown.jumps.unshift({posX:windowWidth/2,posY:windowHeight/2,sizX:700*monit.screenScaleFactor,sizY:700*monit.screenScaleFactor,rot:0});
    currentRound.screenShotTimer = levelSpecs.cooldown.screenShotTimer;
    currentRound.showTimer = true;
    currentRound.counter = 0;
    currentRound.filter =  new movingAverageVect(15);
    currentRound.flashCounter = 0;
    currentRound.cell = {};
}

function playCD() {

    moveElements();
    // monit.drawBg();

    charJump();

    gaze();
    if(!movementsStack.filter(el=>el.type=='sin').length){ //if the cell movement is over
        var newPos = currentRound.filter.getNextValue(createVector(gazeX+levelSpecs.cooldown.offsets.x,gazeY+levelSpecs.cooldown.offsets.y));
        currentRound.cell.posX = newPos.x;
        currentRound.cell.posY = newPos.y
        constraintToWindow(currentRound.cell);
    } else {
        var newPos = currentRound.filter.getNextValue(createVector(windowWidth/2,windowHeight/2));
    }
    drawDistrButOne(currentRound.cell);
    if(currentRound.clicked) {

        setButtonsState({cooldown:false});

        currentRound.clicked = false;
        levelSpecs.screenShots.forEach((pic)=>{
            pic.alpha = 256;
        });
        if(levelSpecs.screenShots.length == levelSpecs.cooldown.picPos.length) levelSpecs.screenShots = [];
        var nextPicPos = levelSpecs.cooldown.picPos[levelSpecs.screenShots.length];
        levelSpecs.screenShots.push({img:get( currentRound.cell.posX-currentRound.cell.sizX/2 + currentRound.cellBorders.posX,
                                                currentRound.cell.posY-currentRound.cell.sizY/2 + currentRound.cellBorders.posY,
                                                currentRound.cellBorders.sizX,
                                                currentRound.cellBorders.sizY),
                                        posX:nextPicPos.posX,
                                        posY:nextPicPos.posY,
                                        rot:nextPicPos.rot,
                                        alpha:0});
        monit.playSound("shoot.wav");
        currentStateCD++;
        currentRound.counter = 0;
        setTimeout(()=>{
            setButtonsState({cooldown:true,back:true});
            currentStateCD++;
            // monit.setBg("#ffffff");
            if(!currentRound.cooldownCurtain) {
                currentRound.cooldownCurtain = monit.addDistractor(windowWidth/2,windowHeight/2,'rect',30,'#ffffff',windowWidth,windowHeight);
            }
            moveBoard("down");

            monit.removeDistractor(currentRound.cell);
            movementsStack = [];
            currentRound.original.ds.posX = windowWidth/2;
            currentRound.original.ds.posY = windowHeight/2;
            currentRound.original.ds.sizX = 700*monit.screenScaleFactor;
            currentRound.original.ds.sizY = 700*monit.screenScaleFactor;
            currentRound.original.ds.rot = 0;
        },100);
        setTimeout(()=>{
            monit.playSound("magic.mp3");
        },800);
    }
    drawOneDistractor(currentRound.cell);


    if(!movementsStack.filter(el=>el.type=='sin').length) {
        currentRound.counter += 1000/myFrameRate;
        var posX = currentRound.cell.posX-currentRound.cell.sizX/2 + levelSpecs.cooldown.button.posX;
        var posY = currentRound.cell.posY-currentRound.cell.sizY/2 + levelSpecs.cooldown.button.posY;

        push();
        stroke(200);
        fill(255,100)
        ellipseMode(CENTER);
        ellipse(posX,posY,100,100);
        fill(255,210);
        arc(posX,posY,100,100,-PI/2+map(currentRound.counter,0,currentRound.screenShotTimer,0,2*PI),3/2*PI,PIE); //
        pop();
    }

    if (currentRound.counter >= currentRound.screenShotTimer) {
        currentRound.clicked = true;
        currentRound.showTimer = false;
    }
}

function shootCD() {
    push();
    tint(255,currentRound.flashCounter);
    if(currentRound.flashCounter<=255)currentRound.flashCounter+=5;
    rect(0,0,windowWidth,windowHeight);
    pop();
}

function seePictureCD() {
    monit.drawDistractors();
    drawPictures(levelSpecs.screenShots);
    moveElements();
}

function exitCD() {

    if(!movementsStack.length) {
        setButtonsState({next:true,back:true,cooldown:true});
        currentState = STARTSCENE;
        state=0;
    };
    monit.drawDistractors();
    drawPictures(levelSpecs.screenShots);
    moveElements();
}

function drawPictures(pics) {
    pics.forEach((pic,i)=>{
        if(pic.alpha<255) pic.alpha+=2;
        drawPicture(pic);
    });
}

function drawPicture(pic) {
    push();
    translate(pic.posX,pic.posY);
    scale(0.7,0.7);
    rotate(pic.rot);
    tint(255,pic.alpha);
    imageMode(CENTER);
    image(pic.img,0,0);
    pop();
}

function moveBoard(dir) {
    switch(dir) {
        case 'up':
            levelSpecs.screenShots.forEach((pic,i)=>{
                var obj = _.cloneDeep(movements['sinImgY'].obj);
                obj.id = i+'img';
                obj.item = pic;
                obj.movDescr = {
                    step: 1000/(myFrameRate * 1000),
                    startPosY: levelSpecs.cooldown.picPos[i].posY,
                    distPosY: -windowHeight-10
                }
                movementsStack.push(obj);
            });
            initMovement(currentRound.cooldownCurtain,{distPosY:-windowHeight-10,type:'sin',duration:1000});
            break;
        case 'down':
            levelSpecs.screenShots.forEach((pic,i)=>{
                pic.posX = levelSpecs.cooldown.picPos[i].posX;
                pic.posY = levelSpecs.cooldown.picPos[i].posY;
            });
            currentRound.cooldownCurtain.posY = windowHeight/2;
            break;
        default:
            break;
    }
}

function constraintToWindow(item) {

    if (item.posX < levelSpecs.cooldown.contraints.minX) {
        item.posX = levelSpecs.cooldown.contraints.minX;
    } else if (item.posX > levelSpecs.cooldown.contraints.maxX) {
        item.posX = levelSpecs.cooldown.contraints.maxX;
    }

    if (item.posY < levelSpecs.cooldown.contraints.minY) {
        item.posY = levelSpecs.cooldown.contraints.minY;
    } else if (item.posY > levelSpecs.cooldown.contraints.maxY) {
        item.posY = levelSpecs.cooldown.contraints.maxY;
    }
}

function charJump() {
    if(!movementsStack.filter(el=>el.type=='jump').length){
        var item = levelSpecs.cooldown.jumps[Math.floor(Math.random()*levelSpecs.cooldown.jumps.length)];
        initMovement(currentRound.original.ds,{endPosX:item.posX,endPosY:item.posY,endSizX:item.sizX,endSizY:item.sizY,type:'jump',duration:2000,endRot:item.rot});
        monit.playSound("jump.wav",true);
    }
}

function mouseClicked() {
    // console.log("scatta!");
    if(currentStateCD == 1) currentRound.clicked = true;
}


function goToNextScene() {

    if (gameRound < numberOfRounds-1) {

        currentState = STARTSCENE;
        //skip to the next scene
        gameRound++;
        // zoomCounter = 0;
    } else {
        console.log("inizio l'animazione fade")
        //end game
        var obj = _.cloneDeep(animations['fade'].obj);
        obj.end = 1000;
        obj.timestamp = getCurrTM();
        animationsStack.push(obj);
        setTimeout(()=>{
            console.log("aggiungo personaggio al centro e inizializzo movimenti personaggi")
            monit.clearTargets();
            monit.clearDistractors();
            currentRound.friendsPosition = [
                {
                    img:"amici_leone.png",
                    startX:-windowWidth/4,
                    startY:windowHeight/3,
                    endX:windowWidth/4,
                    endY:windowHeight/3
                },
                {
                    img:"amici_porcello.png",
                    startX:-windowWidth/4,
                    startY:windowHeight*2/3,
                    endX:windowWidth/4-100,
                    endY:windowHeight*2/3
                },
                {
                    img:"amici_tricheco.png",
                    startX:windowWidth*3/8,
                    startY:windowHeight*5/4,
                    endX:windowWidth*3/8,
                    endY:windowHeight*2/3-100
                },
                {
                    img:"amici_gatto.png",
                    startX:windowWidth*5/8,
                    startY:windowHeight*5/4,
                    endX:windowWidth*5/8,
                    endY:windowHeight*2/3
                },
                {
                    img:"amici_elefante.png",
                    startX:windowWidth*5/4,
                    startY:windowHeight*2/3,
                    endX:windowWidth*3/4+100,
                    endY:windowHeight*2/3
                },
                {
                    img:"amici_cane.png",
                    startX:windowWidth*5/4,
                    startY:windowHeight/3,
                    endX:windowWidth*3/4,
                    endY:windowHeight/3
                }
            ]
            setButtonsState({next:false,back:false,cooldown:false});
            character = monit.addDistractor(windowWidth/2,windowHeight/2,charName);
            currentRound.friends = [];
            currentRound.friendsPosition.forEach((friend,i)=>{
                currentRound.friends.push(monit.addDistractor(friend.startX,friend.startY,friend.img));
                initMovement(currentRound.friends[i],{type:'sin',duration:3000,endPosX:friend.endX,endPosY:friend.endY});
            });
            setTimeout(()=>{
                monit.playSound('applause.wav');
                monit.playSound('magic.mp3');
            },500);
            // currentRound.friends.forEach((friend)=>{
            //     initMovement(friend,{type:'sin',duration:3000,endPosX:currentRound.friendsPosition[i].endX,endPosY:currentRound.friendsPosition[i].endY});
            // })
            currentState = ENDGAME;
        },500);
    }
}

function drawSeparator() {

    push();
    // fill("#b1d8a2");
    fill(0,0);
    strokeWeight(separatorStroke);
    stroke(0);
    rect(windowWidth/2,-10,windowWidth,windowHeight*2);
    pop();

}

//to be called after addDistractors
function addTargets() {
    currentRound.difference.elements.forEach((el)=>{
        if(el.notTarget) {
            el.tgt = monit.addDistractor(   currentRound.difference.ds.posX - currentRound.difference.ds.sizX/2 + el.posX,
                                            currentRound.difference.ds.posY - currentRound.difference.ds.sizY/2 + el.posY,
                                            el.img,10);
        } else {
            el.tgt = monit.addTarget(   currentRound.difference.ds.posX - currentRound.difference.ds.sizX/2 + el.posX,
                                        currentRound.difference.ds.posY - currentRound.difference.ds.sizY/2 + el.posY,
                                        el.img,
                                        11);
        }
    });

    currentRound.difference.backgroundElements = [];

    var zB =0;
    var zA =0;
    if((currentRound.overflow == 'left' && currentRound.position == 'left')||(currentRound.overflow == 'right' && currentRound.position == 'right')) {
        zB = 3;
        zA = 1;
    } else if ((currentRound.overflow == 'right' && currentRound.position == 'left')||(currentRound.overflow == 'left' && currentRound.position == 'right')) {
        zB = 1;
        zA = 3;
    }

    currentRound.original.elements.forEach((el)=>{
        el.ds.zIndex = zB;
        currentRound.difference.backgroundElements.push(
            monit.addDistractor(currentRound.difference.ds.posX - currentRound.difference.ds.sizX/2 + el.posX,
                                currentRound.difference.ds.posY - currentRound.difference.ds.sizY/2 + el.posY,
                                el.img,zA)
        );
    });
}

function getDiffByTgtId(id) {
    return currentRound.difference.elements.find(el=>el.tgt.id == id);
}

function animateElements() {
    
    animationsStack.forEach((animObj, i)=>{
        if(getCurrTM() - animObj.timestamp > animObj.end && !animObj.loop){
            animationsToSplice.push(i);
        } else {
            animations[animObj.type].cb(animObj);
        }
    })
    animationsToSplice.forEach((i)=>{animationsStack.splice(i,1)});
    animationsToSplice = [];
}

function animateElmentsByType(type) {

    animationsStack.forEach((animObj, i)=>{
        if(animObj.type == type) {
            if(getCurrTM() - animObj.timestamp > animObj.end && !animObj.loop){
                animationsToSplice.push(i);
            } else {
                animations[animObj.type].cb(animObj);
            }
        }
    })
    animationsToSplice.forEach((i)=>{animationsStack.splice(i,1)});
    animationsToSplice = [];
}

function targetGazed(tgtId) {
    var tgt = monit.getTargets().find(el=>el.id==tgtId);
    var obj = {};
    Object.assign(obj, animations['selected'].obj);
    obj.item = tgt;
    obj.beginSizX = tgt.sizX;
    obj.beginSizY = tgt.sizY;
    obj.loop = true;
    obj.timestamp = getCurrTM();
    animationsStack.push(obj);

    gazeTime = 0;
    
    monit.playSound('magic.mp3',true);

    currentRound.difference.elements[currentRound.difference.elements.findIndex(el=> !el.notTarget && el.tgt.id == tgtId)].gazed = true;

    if(currentRound.difference.elements.filter(el=>!el.notTarget).every((el)=>{return el.gazed})) {
        currentState++;
    }
}

function hint(){

    // this if should check the condition of the hint triggered (isHint is temporary)
    if(isHint && !currentRound.hide) {

        monit.playSound('richiamo.mp3');
        
        currentRound.difference.elements.forEach((el)=>{

            if(!el.notTarget && !el.gazed) {

                var obj = {};
                Object.assign(obj, animations['attention'].obj);
                obj.item = el.tgt;
                obj.timestamp = getCurrTM();
                animationsStack.push(obj);  
            }    
        });
        
        isHint = false;

    } else if (isHint && currentRound.hide) { // && !isAlreadyHint) {

        monit.playSound('richiamo.mp3');
        
        if (!hintToggle) {
            showBeforeElement();
            hintToggle = 1;
        } else {
            hideBeforeElement();
            hintToggle = 0;
        }

        isHint = false;
    }
}

function hello(){
    
    var isAlready = animationsStack.filter(function(obj){return obj.type == 'hello'}).length

    if (!isAlready) {
               
    }
}


//test purpose
function keyPressed() {

    if(keyCode == RIGHT_ARROW) {
    // console.log("here")
        monit.onAttentionSig();
    } else if(keyCode == DOWN_ARROW) {
        moveBoard('down');
        // flip();
        // hello();
    } else if(keyCode == UP_ARROW) {
        moveBoard('up');
        // var tgts = monit.getTargets();
        // monit.onRemoveTarget(tgts[0].id);
    } else if ((key == "n" || key == "N") && !inhibitNext) {
        monit.onBackNext('next');
    } else if (key == "b" || key == "B") {
        monit.onBackNext('back');
    } else if (key == "d" || key == "D") {
        monit.onToggleFatigue();
    }
}

function addBeforeElement(posX,posY) {
    
    currentRound.original.ds = monit.addDistractor(posX,posY,currentRound.original.base,10);
    currentRound.separator = monit.addDistractor(currentRound.overflow=='left' ? windowWidth/4-separatorStroke/2 : windowWidth*3/4+separatorStroke/2,windowHeight/2,'rect',2,currentRound.background,windowWidth/2,windowHeight);
    currentRound.original.elements.forEach(el=>{
        el.ds = monit.addDistractor(currentRound.original.ds.posX - currentRound.original.ds.sizX/2 + el.posX,
                                    currentRound.original.ds.posY - currentRound.original.ds.sizY/2 + el.posY,
                                    el.img,3);
    });
    monit.removeDistractor(currentRound.original.ds);
    currentRound.original.ds = monit.addDistractor(posX,posY,currentRound.original.base,10);
}

function moveBeforeElement(movDescr) {
    var md = _.cloneDeep(movDescr);
    initMovement(currentRound.original.ds,md);
    currentRound.original.elements.forEach(el=>{
        md = _.cloneDeep(movDescr);
        initMovement(el.ds,md);
    });
}

function hideBeforeElement() {
    initMovement(currentRound.hintCurtain,{endPosX: currentRound.position == "left" ? 0 : windowWidth,type:"sin",duration:1000})
}

function showBeforeElement() {
    initMovement(currentRound.hintCurtain,{endPosX: currentRound.position == "left" ? -windowWidth/2-separatorStroke*3 : +windowWidth*3/2+separatorStroke*3,type:"sin",duration:1000})
}

function drawDistrButOne(d) {
    var distr = monit.getDistractors();
    distr.splice(distr.findIndex((el)=>{return el.id == d.id}),1);
    monit.drawItems(distr.sort(orderElementsCB));
}

function drawDistrButList(arr) {
    var distr = monit.getDistractors();
    var nameList = [];
    arr.forEach((el)=>{nameList.push(el.id)});
    distr = distr.filter(el => !nameList.includes(el.id));
    monit.drawItems(distr.sort(orderElementsCB));
}

function getDistrButList(arr) {
    var distr = monit.getDistractors();
    var nameList = [];
    arr.forEach((el)=>{nameList.push(el.id)});
    return distr.filter(el => !nameList.includes(el.id));
}

function drawOneDistractor(d,flip) {
    monit.drawItems([d],flip==undefined ? false : flip);
}

function flip() {
    monit.playSound("whoosh.wav");
    currentRound.flip = !currentRound.flip;
    // currentRound.original.elements.forEach(el=>{
    //     el.ds.posX = currentRound.original.ds.posX - currentRound.original.ds.sizX/2 + (currentRound.flip ? currentRound.original.ds.sizX-el.posX : el.posX);
    // })
}
function orderElementsCB(a, b){
    if(a.zIndex < b.zIndex){
        return -1;
    }else if(a.zIndex > b.zIndex){
        return 1;
    }
}
function getOriginalElements() {
    var rtn = [];
    rtn.push(currentRound.original.ds);
    currentRound.original.elements.forEach((el)=>{
        rtn.push(el.ds);
    });
    return rtn;
}
function getDifferenceElements(returnBackground) {
    var rtn = [];
    rtn.push(currentRound.difference.ds);
    if(returnBackground !== false) {
        currentRound.difference.backgroundElements.forEach((el)=>{
        rtn.push(el);
        });
    }
    currentRound.difference.elements.forEach((el)=>{
        rtn.push(el.tgt);
    });
    return rtn;
}

// ===================================================================================

function overrideMonit() {

    monit.onRemoveTarget = (idT) => {

        if(currentState == PLAY) {
            targetGazed(idT);
        }
    }

    monit.onAttentionSig = () => {

        if(currentState == PLAY) isHint = true;
    } 

    monit.onAddTarget = () => {
        // isTap = true
    } //I could use monit.attentionSig instead

    monit.onMoveTarget = () => {} //I could use monit.attentionSig instead

    monit.onChangeDynamics = (data) => {
        console.log('data', data);
        var type = data.id;
        switch(type){
            case 'ft':
                monit.fixationTime = parseInt(data.value);
                fEngine.setFixationTime(monit.fixationTime);
                break;
        }
    }
    monit.drawDistractors = function() {
        this.drawItems(_.values(this.pState.distractors).sort(orderElementsCB));
    }
    monit.drawItems = function(items,flip){
        let me = this;
        items.forEach(function(t,i){

            switch(t.img){

                case "rect":
                    push();
                    translate(t.posX,t.posY);
                    rotate(t.rot);
                    rectMode(CENTER);
                    noStroke();
                    fill(t.col);
                    rect(0,0,t.sizX,t.sizY);
                    pop();
                    break;

                case "ellipse":
                    push();
                    translate(t.posX,t.posY);
                    rotate(t.rot);
                    ellipseMode(CENTER);
                    noStroke();
                    fill(t.col);
                    ellipse(0,0,t.sizX,t.sizY);
                    pop();
                    break;

                default:
                    if(!me.images[t.img] || me.images[t.img] == null) {
                        console.error(t.img +" not loaded");
                        return;
                    }
                    if(t.sizX && t.sizY) {
                        push();
                        translate(t.posX,t.posY);
                        rotate(t.rot);
                        imageMode(CENTER);
                        if(flip) scale(-1,1);
                        image(me.images[t.img], 0, 0, t.sizX, t.sizY);
                        pop();                        
                    }
                    else {
                        push();
                        translate(t.posX,t.posY);
                        rotate(t.rot);
                        imageMode(CENTER);
                        image(me.images[t.img], 0, 0);
                        pop();  
                    }
            }
        })
    }
    monit.onBackNext = function(button) {
        switch(button) {
            case "next":
                if(buttons.next) {
                    console.log("NEXT button pressed");
                    tempTempo = 0;
                    switch(currentState) {
                        case CUTSCENE: 
                            currentState++; //to complete when the cnutscene is ready
                            break;
                        case BEFORE:
                            isTap = true;
                            break;
                        case PLAY: 
                            // goToNextScene();
                            if(gameRound < numberOfRounds-1){
                                clearTimeout(timer);
                                goToNextScene();
                            }
                            break;
                        case ENDSCENE:
                            clearTimeout(timer);
                            goToNextScene();
                            break;
                        default:
                            break;
                    }
                }
                break;
            case "back":
                if(buttons.back) {
                    console.log("back button pressed");
                    tempTempo=0;
                    switch(currentState) {
                        case BEFORE: 
                            clearTimeout(timer);
                            if (gameRound > 0) {
                                gameRound--;
                            }
                            currentState = STARTSCENE;
                            break;
                        case PLAY: 
                            clearTimeout(timer);
                            if (gameRound > 0) {
                                gameRound--;
                            }
                            currentState = STARTSCENE;
                            break;
                        case ENDSCENE:
                            // clearTimeout(timer);
                            // if (pathID < numberOfPaths) {
                            //     currentState = STARTSCENE;
                            // } else {
                            //     currentState = ENDGAME;
                            // }
                            break;
                        case COOLDOWN:
                            moveBoard('up');
                            currentStateCD = 0;
                            state = 4;
                            setButtonsState({back:false})
                            break;
                        default:
                            break;
                    }
                }
                break;
        }
    }
    monit.onToggleFatigue = function() {
        if(buttons.cooldown) {
            if(currentState == PLAY || currentState == BEFORE || (currentState == COOLDOWN && currentStateCD > 0)) {
                clearTimeout(timer);
                setButtonsState({next:false,back:false,cooldown:false});
                if(currentState == COOLDOWN) {
                    switch(currentStateCD){
                        case 1:
                            movementsStack = [];
                            initMovement(currentRound.original.ds,{endPosX:windowWidth/2,endPosY:windowHeight/2,endRot:0,type:'sin',duration:1000,endSizX:700*monit.screenScaleFactor,endSizY:700*monit.screenScaleFactor});
                            initMovement(currentRound.cell,{endPosX:windowWidth/2,endPosY:windowHeight*2,type:'sin',duration:1000});
                            break;
                        case 3:
                            setButtonsState({cooldown:false})
                            moveBoard('up');
                            break;
                        default:
                            break;
                    }
                    currentStateCD = 4;
                } else {
                    if(currentState == BEFORE) {
                        currentRound.cooldownFromBefore = true;
                        state = 4;
                    } else {
                        currentRound.cooldownFromBefore = false;
                        state = 0;
                    }
                    currentStateCD = 0;
                    currentState = COOLDOWN;
                }
                // currentState = currentState == COOLDOWN ? STARTSCENE : COOLDOWN;
            }
        }
    }
    monit.onToggleTracker = function (val) {
        if(val == 1) this.trackerActive = true;
        else this.trackerActive = false;
        if(levelSpecs && levelSpecs.bg) {
            levelSpecs.bg.fade();
        }
    }
}

function setButtonsState(state) {
    if(state.next !== undefined) buttons.next = state.next;
    if(state.back !== undefined) buttons.back = state.back;
    if(state.cooldown !== undefined) buttons.cooldown = state.cooldown;
    monit.sendEvent({ts: new Date().getTime(), type: "event", event_type: "buttons_state", data:{cooldown:buttons.cooldown,next:buttons.next,back:buttons.back}});
}

function loadMyJson(url) {
    fetch(url)
        .then(response => {
            return response.json()
        })
        .then(data => {
            let string = JSON.stringify(data, replacer);
            let nData = JSON.parse(string);
            levelSpecs = _.cloneDeep(nData);
        });
}

function replacer(key, val) {
    const keys = [
      "posX",
      "posY",
      "minX",
      "maxX",
      "minY",
      "maxY",
      "x",
      "y"
    ];
    let rtn = val;
    if (keys.indexOf(key) >= 0) {
        rtn *= monit.screenScaleFactor; 
    }
    return rtn; 
}

function loadTabletSettings(){
    let args = (new URL(location.href)).searchParams;
    let round = args.get('round');
    if (round != null) {
        gameRound = round;
    }
}

//==============================================================

movements['sin'] = {
    cb: function(el){

        el.prog += el.movDescr.step;

        var index = (1+sin(PI*el.prog-PI/2))/2;

        el.item.posX = el.movDescr.startPosX +  index * el.movDescr.distPosX;
        el.item.posY = el.movDescr.startPosY +  index * el.movDescr.distPosY;

        el.item.sizX = el.movDescr.startSizX +  index * el.movDescr.distSizX;
        el.item.sizY = el.movDescr.startSizY +  index * el.movDescr.distSizY;

        el.item.rot = el.movDescr.startRot +  index * el.movDescr.distRot;
        
    },
    obj:{
        type: 'sin',
        item:{},
        movDescr:{},
        prog:0
    }
}

movements['sinImgY'] = {
    cb: function(el){
        el.prog += el.movDescr.step;
        var index = (1+sin(PI*el.prog-PI/2))/2;
        el.item.posY = el.movDescr.startPosY +  index * el.movDescr.distPosY;
    },
    obj:{
        type: 'sinImgY',
        item:{},
        movDescr:{},
        prog:0
    }
}

movements['jump'] = {
    cb: function(el){

        el.prog += el.movDescr.step;

        var index = (1+sin(PI*el.prog-PI/2))/2;

        // (1-sq(el.prog-1)) * el.distY - 100*2*(cos(2*PI*el.prog+PI)+1)/2;;

        el.item.posX = el.movDescr.startPosX +  index * el.movDescr.distPosX;
        el.item.posY = el.movDescr.startPosY +  index * el.movDescr.distPosY - 100*(cos(2*PI*el.prog+PI)+1)/2;

        el.item.sizX = el.movDescr.startSizX +  index * el.movDescr.distSizX + 50*(cos(2*PI*el.prog+PI)+1)/2;
        el.item.sizY = el.movDescr.startSizY +  index * el.movDescr.distSizY + 50*(cos(2*PI*el.prog+PI)+1)/2;

        el.item.rot = el.movDescr.startRot +  index * el.movDescr.distRot;
        
    },
    obj:{
        type: 'jump',
        item:{},
        movDescr:{},
        prog:0
    }
}

function initMovement(item,movDescr) {
    //prog spans between 0 and 1
    movDescr.prog = 0;

    movDescr.step = 1000/(myFrameRate * movDescr.duration);
    // console.log("inizio: ",getCurrTM());
    // console.log("frameRate: ",movDescr.frameRate,"movDescr.duration: ",movDescr.duration,"step: ", movDescr.step);

    //POSITION
    if(movDescr.startPosX == null) movDescr.startPosX = item.posX;
    if(movDescr.endPosX == null) movDescr.endPosX = item.posX;
    if(movDescr.startPosY == null) movDescr.startPosY = item.posY;
    if(movDescr.endPosY == null) movDescr.endPosY = item.posY;

    if(movDescr.distPosX == null) movDescr.distPosX = movDescr.endPosX - movDescr.startPosX;
    if(movDescr.distPosY == null) movDescr.distPosY = movDescr.endPosY - movDescr.startPosY;

    //SIZE
    if(movDescr.startSizX == null) movDescr.startSizX = item.sizX;
    if(movDescr.endSizX == null) movDescr.endSizX = item.sizX;
    if(movDescr.startSizY == null) movDescr.startSizY = item.sizY;
    if(movDescr.endSizY == null) movDescr.endSizY = item.sizY;

    movDescr.distSizX = movDescr.endSizX - movDescr.startSizX;
    movDescr.distSizY = movDescr.endSizY - movDescr.startSizY;

    //ROTATION
    if(movDescr.startRot == null) movDescr.startRot = item.rot;
    if(movDescr.endRot == null) movDescr.endRot = item.rot; 

    movDescr.distRot = movDescr.endRot - movDescr.startRot;

    // var obj = {};
    // Object.assign(obj, movements[movDescr.type].obj);
    var obj = _.cloneDeep(movements[movDescr.type].obj);
    obj.id = item.id;
    obj.item = item;
    obj.movDescr = movDescr;
    movementsStack.push(obj);


}

function moveElements() {

    movementsStack.forEach((movObj, i)=>{
        if(movObj.prog < 1){
            movements[movObj.type].cb(movObj);
        } else {
            _.remove(movementsStack, d => d.id == movObj.id && d.type == movObj.type)   
        }
    })
}



