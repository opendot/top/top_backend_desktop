class Path{

	constructor(table,level,round,stepSize) {

		this.table = table;
		this.level = level;
		this.round = round;
		this.stepSize = stepSize;
		this.segments = [];

		this.counterPointId = 0;
		this.gaitAngle = 5;
		this.repeatWrongSeg = true;

		//filter the table for the specific path
		this.getPathTable();
		//load the segment objects
		this.readSegments();
		//compute the footprint rotation
		this.setPointsRot();
		

	}

	getPathTable() {

		var levelTableArr = this.table.findRows(this.level.toString(),'level');
		var levelTable = new p5.Table();
		levelTableArr.forEach((el)=>{
			levelTable.addRow(el);
		});
		levelTable.columns = this.table.columns;

		var roundTableArr = levelTable.findRows(this.round.toString(),'path');
		var roundTable = new p5.Table();
		roundTableArr.forEach((el)=>{
			roundTable.addRow(el);
		});
		roundTable.columns = this.table.columns;
		
		this.table = roundTable;		
	}

	getSegmentsCount() {

		var segmentsCount = 0;

	    for (let i = 0; i < this.table.getRowCount(); i++) {
	        if (this.table.getNum(i,"segID") > segmentsCount) {
	            segmentsCount = this.table.getNum(i,"segID");
	        }
	    }		

	    return segmentsCount;
	}

	readSegments() {

		//for each segment, read the bezier points
	    for (let s = 1; s <= this.getSegmentsCount(); s++) {
	    	this.readSegment(s);
	    	this.setPointsSeq(this.segments[s]);
		}
	}

	readSegment(segID) {

    	var segment = {};

    	segment.id = segID;
    	segment.coords = [];
    	//rely on the first row in the table (the table must be correct)
    	segment.idParentSeg = Number(this.table.findRow(segID.toString(),'segID').obj.parentID);
    	segment.type = this.table.findRow(segID.toString(),'segID').obj.type;

    	var bezierCoord = {a:[],c:[]};

		//get the coordinates of the current path
	    for (let i = 0; i < this.table.getRowCount(); i++) {

	        if (this.table.getNum(i,"segID") == segID) {

	            switch (this.table.getString(i,"ac")) {

	                case "a": 
	                    bezierCoord.a.push({x:this.table.getNum(i,"x") * monit.screenScaleFactor,
	                                        y:this.table.getNum(i,"y") * monit.screenScaleFactor});
	                    break;
	                case "c": 
	                    bezierCoord.c.push({x:this.table.getNum(i,"x") * monit.screenScaleFactor,
	                                        y:this.table.getNum(i,"y") * monit.screenScaleFactor});
	                    break;
	                default:
	                    console.log("error while reading the bezier curve");
	                    break;

	            }
	        }
	    }	    	

    	let bezierCount = bezierCoord.c.length/2;

	    for (let s = 0; s < bezierCount; s++) {

	        var a1 = createVector(bezierCoord.a[s].x,bezierCoord.a[s].y);
	        var c1 = createVector(bezierCoord.c[s*2].x,bezierCoord.c[s*2].y);
	        var c2 = createVector(bezierCoord.c[s*2+1].x,bezierCoord.c[s*2+1].y);
	        var a2 = createVector(bezierCoord.a[s+1].x,bezierCoord.a[s+1].y);
	        //if it is the last bezier segment return also the last point
	        var curve = new BezierCurve(a1,c1,c2,a2,s==bezierCount-1); 

	        var stepDensity = ceil(curve.length() / this.stepSize); // dense: 100, sparse: 200
	        var pointsOnBezier = curve.equidistantPoints(stepDensity);
	        //if it is the last bezier segment -> discard the second last point (to separate last footprint from the hiding place)
	        if(s == bezierCount-1 && segment.type != 'through') pointsOnBezier.splice(pointsOnBezier.length-2,1);
	        pointsOnBezier.forEach((el)=>{
	        	segment.coords.push({posX:el.x/(monit.idealRes.width * monit.screenScaleFactor), posY:el.y/(monit.idealRes.height * monit.screenScaleFactor),id:this.setPointId()});
	        });
	    }		

		this.segments[segment.id] = segment;
	}

	setPointsSeq(segment) {

		for(let i = 0; i<segment.coords.length;i++) {

			// if it's the starting point of the entire path
			if (i == 0 && segment.idParentSeg == 0) {
				segment.coords[i].idParentPoint = null;
			// if it's the starting point of a segment but not the first segment
			} else if (i == 0 && segment.idParentSeg != 0) {
				var parentSegment = this.segments[segment.idParentSeg];
				segment.coords[i].idParentPoint = parentSegment.coords[parentSegment.coords.length-1].id;
			// for regular points the parent point is the previous one	
			} else {
				segment.coords[i].idParentPoint = segment.coords[i-1].id;
			}
		}
	}

	setPointsRot() {

		this.segments.forEach((seg,index)=>{
			seg.coords.forEach((point,i)=>{

				var next = {};
				var nexts = this.getNextPointsById(point.id);

				// console.log("index: ",index,"i: ",i,"nexts: ",nexts);
				//if there is more that one next point (fork) average the positions
				if (nexts.length>1) {
					next.posX = 0;
					nexts.forEach((el)=>{next.posX += el.posX});
					next.posX /= nexts.length;
					next.posY = 0;
					nexts.forEach((el)=>{next.posY += el.posY});
					next.posY /= nexts.length;
				//if it is the last point rotation and offset has no importance since it is not a footprint
				//arbitrarily assign next point equal to the current point
				} else if (nexts.length == 0) {
					next.posX = point.posX;
					next.posY = point.posY;					
				} else {
					next.posX = nexts[0].posX;
					next.posY = nexts[0].posY;
				}

				//managment of cases PI/2 PI3/2 (undefined tan)
		        if (next.posX == point.posX) next.posX+=1;

        	        var footprintsAngle = atan((next.posY-point.posY)/(next.posX-point.posX));
			        // 1° and 4° quarters
			        if (next.posX > point.posX) {        
			            //if the slope is too high shift the character also on horizontal axis
			            (abs(footprintsAngle) > PI/3) ? point.offsetX = -1 : point.offsetX = 0;
			            //left footprints
			            if (i % 2 == 0) {
			                point.rot = HALF_PI + footprintsAngle - radians(this.gaitAngle);
			            } else { //right footprints
			                point.rot = HALF_PI + footprintsAngle + radians(this.gaitAngle);
			            }          
			        } else {    //2° and 3° quarters
			            //if the slope is too high shift the character also on horizontal axis
			            (abs(footprintsAngle) > PI/3) ? point.offsetX = +1 : point.offsetX = 0;
			            //left footprints
			            if (i % 2 == 0) {                
			                point.rot = -HALF_PI + footprintsAngle - radians(this.gaitAngle);
			            } else { //right footprints
			                point.rot = -HALF_PI + footprintsAngle + radians(this.gaitAngle);
			            } 
			        }
			});
		});
	}

	// UTILITIES =====================================================

	getPointById(id) {

		var returnPoint;
		this.segments.forEach((seg)=>{
			seg.coords.forEach((point)=>{
				if(point.id == id) returnPoint = point; 
			})
		})
		return returnPoint;
	}

	getSegById(id) {

		var returnSeg;
		this.segments.forEach((seg)=>{
			seg.coords.forEach((point)=>{
				if(point.id == id) returnSeg = seg; 
			})
		})
		return returnSeg;
	}

	getPointByTgtId(tgtId) {

		var returnPoint;
		this.segments.forEach((seg)=>{
			seg.coords.forEach((point)=>{
				if(point.tgt && point.tgt.id == tgtId) {
					returnPoint = point;
				}
			})
		})
		return returnPoint;
	}

	getSegByTgtId(tgtId) {

		var returnSeg;
		this.segments.forEach((seg)=>{
			seg.coords.forEach((point)=>{
				if(point.tgt && point.tgt.id == tgtId) returnSeg = seg;
			})
		})
		return returnSeg;
	}

	getNextPointsById(id) {

		var returnPoints = [];
		this.segments.forEach((seg)=>{
			seg.coords.forEach((point)=>{
				if(point.idParentPoint == id) returnPoints.push(point); 
			})
		})
		return returnPoints;		

	}

	getSegsByType(type) {

		var returnSeg = [];
		this.segments.forEach((seg)=>{
			if(seg.type == type) returnSeg.push(seg);
		})
		return returnSeg;		

	}

	getNextPathById(id,howMany) {

		if (!howMany) howMany = 5;
		var path = [];
		var currPoint = this.getPointById(id);
		path.push(currPoint.tgt.id);

		for (let i = 0; i < howMany-1; i++) {

			var next = this.getNextPointsById(currPoint.id);
			
			//assume that the right path is one, made of "through" and "final" segments
			if (next.length == 2) {
				
				next = next.find((el)=>{return this.getSegById(el.id).type != 'dead'});
				var seg = this.getSegById(next.id);


				if(seg.type != 'final' || next.tgt.id != seg.coords[seg.coords.length-1].tgt.id) {
					currPoint = this.getPointById(next.id);
					path.push(currPoint.tgt.id);
				}

			} else if (next.length == 1) {

				next = next[0];
				var seg = this.getSegById(next.id);
				if(seg.type != 'final' || next.tgt.id != seg.coords[seg.coords.length-1].tgt.id) {
					currPoint = this.getPointById(next.id);
					path.push(currPoint.tgt.id);
				}

			} else {
				path.pop();
				break;
			}
		}
		return path;
	}

	setPointId() {
    	this.counterPointId++;
    	return this.counterPointId.toString() + 'p'; 
	}
}