//state machine
const esplorazioneSM = [
    
    init,      
    startScene, 
    showRound, //temporary state to uniform to the other games
    cutScene,  
    play,  
    endScene, 
    endGame,
    coolDown    

];

const STARTSCENE = 1;
const SHOWROUND = 2;
const CUTSCENE = 3;
const PLAY = 4;
const ENDSCENE = 5;
const ENDGAME = 6;
const COOLDOWN = 7;

var currentState = 0;
var myFrameRate = 30;

const coolDownSM = [
    
    setupCoolDown,
    enterCoolDown,
    playCoolDown,
    exitCoolDown

];
var currCoolDownState;

//test purpose -> temporary
var isHint = false;

//offset angle to add to the angle of the vector joining 2 footprints
var gaitAngle = 5;
// vertical and horizontal offsets of character position wrt the footprint position
var charOffsets = [];

//coordinates of the path - values B[0;1]
var table;
//ID of the path in the csv file
var pathID = 0;
var numberOfPaths = 0;
var tgOnTheLeft = true;
var placeHolder;

//array with the coordinates of the current path
let pathCoord = [];
var stepSize = 110 * window.innerWidth / 1920;
var stepSizeCm = null;//2;
var screenInches = null;//14;
var gazeTime = 0;
var pathObj = {};
var repeatWrongSeg = 0;

var isFirstEndScene = true;
var isStart = false;

//======================================
var character = {};
var friend = {};
var helloLR = 1;
var timer = [];
var timerValue = 5000;
//======================================

// animations
var animations = [];
var animationsStack = [];
var animationsToSplice = [];
// movements
var movements = [];
var movementsStack = [];

//======================================
var fEngine = {};
var levelSpecs = {};
var currentRound = {};
var charName = 'char.png';

//temp
var buttons = {
    back:false,
    next:false,
    cooldown: false
}

var gazeX = 0;
var gazeY = 0;

animations['hint'] = {
    cb: function(el){

        el.index += 1000/frameRate();

        let alphaF = map(el.index,0,el.end,0,2*PI);       

        var num = el.tgtList.length;

        for (let i = 0 ; i < num ; i++) {
        	noStroke();
            fill(234,117,66,(255 - i*(255/num)) *(1-cos(alphaF))/2);
            ellipse(monit.pState.targets[el.tgtList[i]].posX,
                    monit.pState.targets[el.tgtList[i]].posY,
                    stepSize*1.4,stepSize*1.4); //(80 - i*(80/num)));
        }
    },
    obj:{
        type: 'hint',
        timestamp: getCurrTM(),
        tgtList: [],
        index: 0,
        end: 1000,
    }
}

animations['star'] = {
    cb: function(el){

        el.index += 1000/frameRate();

    },
    obj:{
        type: 'star',
        timestamp: getCurrTM(),
        index: 0,
        end: 1000,
    }
}

animations['selected'] = {
    cb: function(el){

        el.prog = map(el.index,0,el.end,0,1);

        var x = (1+sin(PI*el.prog-PI/2))/2;
        var y = (1+sin(2*PI*el.prog-PI/2))/2;
        push();
        ellipseMode(CENTER);
        noStroke();
        fill(el.color.r,el.color.g,el.color.b,map(y,0,1,0,150));
        ellipse(el.item.posX,el.item.posY,el.item.sizX+el.dimFactor*x,el.item.sizY+el.dimFactor*x);
        pop();

        el.index += 1000/frameRate();
        
    },
    obj:{
        type: 'selected',
        timestamp: getCurrTM(),
        item:{},
        color:{},
        index: 0,
        end: 1000,
        dimFactor: 100,
        prog:0
    }
}

animations['fade'] = {
    cb: function(el){

        el.prog = 255*(1+sin(2*PI*map(el.index,0,el.end,0,1)-PI/2))/2;
        push();
        fill(0,el.prog)
        rect(0,0,windowWidth,windowHeight);
        pop();
        el.index += 1000/frameRate();
        
    },
    obj:{
        type: 'fade',
        timestamp: getCurrTM(),
        index: 0,
        end: 1000,
        prog:0
    }
}


function preload() {

    monit.loadJsonItems();
    table = loadTable('pathsBF.csv','csv','header');

}

function setup() {
    
    createCanvas(windowWidth,windowHeight);
    noStroke();
    frameRate(myFrameRate);

    loadMyJson("./es.json");
    loadTabletSettings();
    
    overrideMonit();

}

//do nothing until the flag monit.started is false
function checkFlag() {

    if(!monit.started || !monit.loaded) {
       setTimeout(checkFlag, 100); /* this checks the flag every 100 milliseconds*/   
    }
}

function draw() {


    if(!monit.started || !monit.loaded) {
        setTimeout(checkFlag, 100); /* this checks the flag every 100 milliseconds*/ 
        background('black');
        textSize(30);
        stroke('white');
        fill('white')
        textAlign(CENTER, CENTER);
        text('Caricamento in corso...', windowWidth / 2, windowHeight / 2);
        textSize(12);
        noFill();
        noStroke();

    } else {
    	//draw the background
    	// background('#b9cce1');
        monit.drawBg();
        if(levelSpecs && levelSpecs.bg) levelSpecs.bg.run();
        // drawTrackerDisabled();

        if (currentState < esplorazioneSM.length) {
            esplorazioneSM[currentState]();
        }
        // console.log("next",buttons.next,"back",buttons.back,"cooldown",buttons.cooldown);
        // console.log(esplorazioneSM[currentState].name);

        // //real fps on top left corner
        // fill(0); //black text
        // text(frameRate(), 10, 10);

        // drawTrackerDisabled();
    }
}

function drawTrackerDisabled() {
    push();
    rectMode(CENTER);
    if(monit.trackerActive){
        if(currentRound.trackerDisabledAlpha>0){
            currentRound.trackerDisabledAlpha-=10;
        } else {
            currentRound.trackerDisabledAlpha = 0;
        }
    } else {
        if(currentRound.trackerDisabledAlpha<150){
            currentRound.trackerDisabledAlpha+=10;
        } else {
            currentRound.trackerDisabledAlpha = 150;
        }
    }
    fill(0,0,0,currentRound.trackerDisabledAlpha);
    rect(windowWidth/2,windowHeight/2,windowWidth*2,windowHeight*2);
    pop();
}

function gaze() {
    if (monit.trackerActive) {
        gazeX = monit.mode == 'eye' ? monit.gaze[0] * window.innerWidth : mouseX;
        gazeY = monit.mode == 'eye' ? monit.gaze[1] * window.innerHeight : mouseY;
    }
}

// to assess if the pointer is on a target and what to do if so
function collide(tgt) {

    if(tgt.id == friend.id || (nextTgtList && nextTgtList.filter(el=>el == tgt.id).length)) {
        if (gazeX > tgt.posX - tgt.sizX / 2 && 
            gazeX < tgt.posX + tgt.sizX / 2 && 
            gazeY > tgt.posY - tgt.sizY / 2 && 
            gazeY < tgt.posY + tgt.sizY / 2) {     

            // targetGazed(tgt);
            fEngine.tgtGazed(tgt.id);
        } else {
            fEngine.tgtNotGazed(tgt.id);
        }
    }
}

function getCurrTM(){

    return (new Date()).getTime();

}

function init() {

    //slow down default animation rate
    monit.animationRate = 3;
    // pathID = 0;

    if (screenInches && stepSizeCm) setStepSize(screenInches,stepSizeCm);
    if(!monit.fixationTime) monit.fixationTime = 100; //3000

    // monit.GameLevel = 2; //TEST
    // monit.fixationTime = 20; //TEST

    for (let i = 0; i < table.getRowCount(); i++) {

        if (table.getNum(i,"level") == monit.GameLevel && table.getNum(i,"path") > numberOfPaths) {
            numberOfPaths = table.getNum(i,"path");
        }
    }

    fEngine = new Fixation(myFrameRate,monit.fixationTime,targetGazed);
    //update the state
    currentState++;
}

function startScene() {

    //index of the correct footprint to point
    isFirstEndScene = true;
    isStart = false;
    // monit.trackerActive = false;
    monit.pState.targets = {};
    monit.pState.distractors = {};
    currCoolDownState = 0;

    pathID++;

    //TEST
    pathObj = new Path(table,monit.GameLevel,pathID,stepSize);
    pathObj.repeatWrongSeg = repeatWrongSeg;

    currentRound = levelSpecs.levels[monit.GameLevel].rounds[pathID-1];
    currentRound.friendImg = "amici_" + currentRound.friend + ".png";
    currentRound.footprintImg = "ES_target_" + currentRound.friend + ".png";
    currentRound.refuge = currentRound.sceneElements.find(el=>el.type == "final");
    currentRound.trackerDisabledAlpha = 0;

    monit.setBg(currentRound.background);
    levelSpecs.bg = new Bg(myFrameRate);

    //read the coordinates of the current path from table
    readPath(pathID);


    tgOnTheLeft = pathCoord[pathCoord.length-1].posX < pathCoord[pathCoord.length-2].posX;

    if(tgOnTheLeft) {
        friend = monit.addDistractor(windowWidth*3/4,windowHeight/2+currentRound.refuge.offsetY*1.5,currentRound.friendImg,5);
        var refuge = monit.addDistractor(windowWidth/4,windowHeight/2,currentRound.refuge.img,10);
    } else {
        friend = monit.addDistractor(windowWidth/4,windowHeight/2+currentRound.refuge.offsetY*1.5,currentRound.friendImg,5);
        var refuge = monit.addDistractor(windowWidth*3/4,windowHeight/2,currentRound.refuge.img,10);            
    }
    //draw the elements bigger 
    var distrs = monit.getDistractors();
    distrs.forEach((el,i)=>{
        el.sizX *= 1.5;
        el.sizY *= 1.5;
    });

    //initialize the friend's movement
    initMovement(friend,{   type:"sin",
                            endPosX:refuge.posX,
                            duration:3000});

    setTimeout(()=>{monit.playSound(currentRound.friendSound)},500);
    placeHolder = 1;
    setButtonsState({next:true,back:true,cooldown:false});
    currentState++;
}

function initScene() {

    //read the coordinates of the current path from table
    // readPath(pathID);
    monit.pState.targets = {};
    monit.pState.distractors = {};
    gazeTime = 0;
    movementsStack = [];
    //add targets and distractors
    addElements();
    //initialize the elements positions
    initPositions();
    
    nextTgtList = [monit.getTargets()[0].id];

    setButtonsState({next:pathID<numberOfPaths,cooldown:true});
    //update the state
    currentState++;

}

function play() {

    var tgts = monit.getTargets();
    // var distr = monit.getDistractors();
    gaze();

    
    moveElements();
    constraintToWindow(character);
    // //draw all the targets
    // monit.drawTargets();
    //  //draw the distractors (character)
    // monit.drawDistractors();
    monit.drawAllItems();
    //move the character
    moveCharacter();
    animateElements();
    //if the final target isn't reached yet (nextTgtList is null on the final target)
    if (nextTgtList && !animationsStack.filter((el)=>{return el.type == 'selected'}).length){

        //check if the pointer is on any target (collision)
        tgts.forEach(function (d,i) {
            collide(d);
            // if(d.img == "footprint1.png" || d.img == currentRound.friendImg) collide(d);
        })        

    } else if (!nextTgtList){ //if the final target is reached

        //if the character isn't moving
        if (character.img == charName) {
            setButtonsState({cooldown:false});
            currentState++;
        }
    }

    if(!movementsStack.length && pathObj.coolDown) {
        setButtonsState({next:false,back:false});
        pathObj.coolDown = false;
        currentState = COOLDOWN;
    }
    //if the event "hint" is triggered light up the next footprint
    hint();
}

function endScene() {

    var distr = monit.getDistractors();

    if(isFirstEndScene) {

        //clear animations
        animationsStack = [];

        monit.pState.targets = {};
        monit.pState.distractors = {};

        if(tgOnTheLeft) {
            character = monit.addDistractor(windowWidth*3/4,windowHeight/2,charName,10);
            friend = monit.addDistractor(windowWidth/4,windowHeight/2+currentRound.refuge.offsetY*1.5,currentRound.friendImg);
            monit.addDistractor(windowWidth/4,windowHeight/2,currentRound.refuge.img);
        } else {
            character = monit.addDistractor(windowWidth/4,windowHeight/2,charName,10);
            friend = monit.addDistractor(windowWidth*3/4,windowHeight/2+currentRound.refuge.offsetY*1.5,currentRound.friendImg);
            monit.addDistractor(windowWidth*3/4,windowHeight/2,currentRound.refuge.img);            
        }
        //draw the elements bigger 
        distr = monit.getDistractors();
        distr.forEach((el,i)=>{
            el.sizX *= 1.5;
            el.sizY *= 1.5;
        });
        //shift the tree
        // distrs[2].posY += distrs[1].sizY - distrs[2].sizY; 

        //initialize the friend's movement
        initMovement(friend,{   type:"sin",
                                endPosX:windowWidth/2,
                                duration:1000});

        monit.playSound('magic.mp3');
        monit.playSound(currentRound.friendSound);

        if (pathID < numberOfPaths) {
                timer.push(setTimeout(function(){currentState = STARTSCENE},timerValue));
        } else {
                timer.push(setTimeout(()=>{
                    var obj = _.cloneDeep(animations['fade'].obj);
                    obj.end = timerValue;
                    obj.timestamp = getCurrTM();
                    animationsStack.push(obj);
                },timerValue/2))
                timer.push(setTimeout(function(){
                    monit.clearDistractors();
                    character = monit.addDistractor(windowWidth/2,windowHeight/2,charName);
                    currentRound.friends = [];
                    currentRound.friendsPosition = [
                        {
                            startX:-windowWidth/4,
                            startY:windowHeight/3,
                            endX:windowWidth/4,
                            endY:windowHeight/3
                        },
                        {
                            startX:-windowWidth/4,
                            startY:windowHeight*2/3,
                            endX:windowWidth/4-100,
                            endY:windowHeight*2/3
                        },
                        {
                            startX:windowWidth*3/8,
                            startY:windowHeight*5/4,
                            endX:windowWidth*3/8,
                            endY:windowHeight*2/3-100
                        },
                        {
                            startX:windowWidth*5/8,
                            startY:windowHeight*5/4,
                            endX:windowWidth*5/8,
                            endY:windowHeight*2/3
                        },
                        {
                            startX:windowWidth*5/4,
                            startY:windowHeight*2/3,
                            endX:windowWidth*3/4+100,
                            endY:windowHeight*2/3
                        },
                        {
                            startX:windowWidth*5/4,
                            startY:windowHeight/3,
                            endX:windowWidth*3/4,
                            endY:windowHeight/3
                        }
                    ]
                    levelSpecs.levels[monit.GameLevel].rounds.forEach((round,i)=>{
                        // console.log("i",i,"round",round)
                        currentRound.friends.push(monit.addDistractor(currentRound.friendsPosition[i].startX,currentRound.friendsPosition[i].startY,"amici_" + round.friend + ".png"));
                        initMovement(currentRound.friends[i],{type:'sin',duration:3000,endPosX:currentRound.friendsPosition[i].endX,endPosY:currentRound.friendsPosition[i].endY});
                    })
                    setButtonsState({next:false,back:false,cooldown:false});
                    currentState = ENDGAME;
                },timerValue));
                timer.push(setTimeout(()=>{
                    monit.playSound('applause.wav');
                    monit.playSound('magic.mp3');
                },timerValue*1.2))
        }

        isFirstEndScene = false;
    }

    monit.drawDistractors();
    monit.animateDistractor(character,"-hurra");
    monit.animateDistractor(friend,"-hurra");
    animateElements();
    moveElements();
    // monit.animateDistractor(distr[2],"");
}

function endGame() {

    
    monit.animateDistractor(character,"-hurra");
    currentRound.friends.forEach((friend)=>{
        monit.animateDistractor(friend,"-hurra");
    })
    monit.drawDistractors();
    moveElements();
    animateElements();
    // background('black');
    // textSize(30);
    // stroke('white');
    // fill('white')
    // textAlign(CENTER,CENTER);
    // text('EVVIVA!', windowWidth/2, windowHeight/2);

}

function coolDown() {

    if (currCoolDownState < coolDownSM.length) {
        coolDownSM[currCoolDownState]();
    }

    // console.log(coolDownSM[currCoolDownState].name);

}

var gameState = {};
function setupCoolDown() {
    monit.drawTargets();
    monit.drawDistractors();
    gameState = {};
    gameState.frameCount = frameCount;
    gameState.filter =  new movingAverageVect(30);
    gameState.exit = false;
    gameState.timers = [];
    gameState.flys = [];
    gameState.time = 0;
    // gameState.curtain = monit.addDistractor(windowWidth/2,windowHeight/2,'rect',10,'#151539')
    gameState.counter = 0;
    setTimeout(()=>{
        currCoolDownState++;
        gameState.counter = 0;
        hideDayElements();
        gameState.flys.push({   ds:monit.addDistractor(windowWidth/2,windowHeight/2,"asset_lucciola.png"),
                                periodX:15000});
        // gameState.flys.push({   ds:monit.addDistractor(windowWidth/2,windowHeight/2,"asset_lucciola.png"),
        //                         periodX:25000});
        // gameState.flys.push({   ds:monit.addDistractor(windowWidth/2,windowHeight/2,"asset_lucciola.png"),
        //                         periodX:5000});
    },1000)
    currCoolDownState++;

    gameState.savedCharPos = {posX:character.posX,posY:character.posY};

}

function enterCoolDown() {
    monit.drawTargets();
    fill(21,21,57,gameState.counter*3);
    rect(0,0,windowWidth,windowHeight);
    drawDistrButCharacter();
    fill(21,21,57,gameState.counter);
    rect(0,0,windowWidth,windowHeight);

    drawCharacter();
    if(gameState.counter < 100) gameState.counter += 4;

    gameState.filter.getNextValue(createVector(character.posX,character.posY));
}

function exitCoolDown() {
    
    gameState.prevCharPos = createVector(character.posX,character.posY);
    monit.drawTargets();
    fill(21,21,57,255-gameState.counter*3);
    rect(0,0,windowWidth,windowHeight);
    drawDistrButCharacter();
    fill(21,21,57,100-gameState.counter);
    rect(0,0,windowWidth,windowHeight);

    if(gameState.counter < 100) gameState.counter += 4;

    drawCharacter();
    moveElements();

    if (gameState.prevCharPos.x < character.posX-1) {
        monit.animateDistractor(character,"-right-spostamento");
    } else if (gameState.prevCharPos.x > character.posX+1) {
        monit.animateDistractor(character,"-left-spostamento");
    } else {
        character.img = charName;
    }

    if(!movementsStack.filter(el=>el.type=='sin').length) {
        setButtonsState({next:true,back:true});
        currentState = PLAY;
        movementsStack = [];
        monit.getDistractors().forEach((el)=>{
            if(el.img == "star.png" || el.img == "asset_lucciola.png") monit.removeDistractorById(el.id)
        });
        gameState.timers.forEach((t)=>{
            clearTimeout(t);
        })
    }
}

function playCoolDown() {
    fill(21,21,57,255);
    rect(0,0,windowWidth,windowHeight);
    drawDistrButCharacter();
    fill(21,21,57,100);
    rect(0,0,windowWidth,windowHeight);
    drawCharacter();

    gameState.prevCharPos = createVector(character.posX,character.posY);
    gaze();

    var newPos = gameState.filter.getNextValue(createVector(gazeX,gazeY));

    character.posX = newPos.x;
    character.posY = newPos.y;

    constraintToWindow(character);

    if (gameState.prevCharPos.x < character.posX-1) {
        monit.animateDistractor(character,"-right-spostamento");
    } else if (gameState.prevCharPos.x > character.posX+1) {
        monit.animateDistractor(character,"-left-spostamento");
    } else {
        character.img = charName;
    }

    animateFlys();

    createTail();
    moveElements();
    if(gameState.exit) {
        gameState.exit = false;
        showDayElements();
        initMovement(character,{endPosX:gameState.savedCharPos.posX,endPosY:gameState.savedCharPos.posY,type:'sin',duration:2000});
        gameState.flys.forEach((fly)=>{initMovement(fly.ds,{endPosY:-2*windowHeight,type:'sin',duration:1000})})
        currCoolDownState++;
    }
}

function createTail() {
    var distr = {};
    var life = 7000;
    // if (dist(character.posX,character.posY,gameState.prevCharPos.x,gameState.prevCharPos.y) < gameState.gap) {
    if ((frameCount - gameState.frameCount)*1000/myFrameRate > 200) {
        distr = monit.addDistractor(character.posX,//+randomGaussian(0,10),
                                    character.posY,//+randomGaussian(0,10),
                                    'star.png');//,'#ffffff',50,50);

        initMovement(distr,{type:'pulsate',
                            duration:life,
                            endSizX:distr.sizX*1.2,
                            endSizY:distr.sizY*1.2,
                            endPosX:10,
                            endPosY:10,});

        gameState.timers.push(setTimeout(()=>{monit.removeDistractorById(distr.id)},life));
        gameState.frameCount = frameCount;
    }
}

function animateFlys() {
    gameState.time += 1000/myFrameRate;
    gameState.flys.forEach((fly,i)=>{
        if(i == 0) {
            fly.ds.posX = windowWidth/2 + sin(2*PI*gameState.time/(fly.periodX))*(windowWidth/2-200);
            fly.ds.posY = windowHeight/2 + sin(2*PI*gameState.time/(fly.periodX/2))*(windowHeight/2-200); 
        } else if (i == 1) {
            fly.ds.posX = windowWidth/2 + sin(2*PI*gameState.time/(fly.periodX/4))*(windowWidth/2-200);
            fly.ds.posY = windowHeight/2 + sin(2*PI*gameState.time/(fly.periodX))*(windowHeight/2-200); 
        }
    })
}

function drawDistrButCharacter() {
    var distr = monit.getDistractors();
    distr.splice(distr.findIndex((el)=>{return el.id == character.id}),1);
    monit.drawItems(distr);
}

function drawDistrButImgNames(imgNames) {
    var distr = monit.getDistractors();
    distr = distr.filter(el => !imgNames.includes(el.img));
    monit.drawItems(distr);
}

function drawDistrByNames(imgNames) {
    var distr = monit.getDistractors();
    distr = distr.filter(el => imgNames.includes(el.img));
    monit.drawItems(distr);
}

function drawCharacter() {
    monit.drawItems([character]);
}

function hideDayElements() {
    pathObj.segments.forEach((seg)=>{
        seg.coords.forEach((point)=>{
            point.tgt.posY += windowHeight;
        })
    })
}
function showDayElements() {
    pathObj.segments.forEach((seg)=>{
        seg.coords.forEach((point)=>{
            point.tgt.posY -= windowHeight;
        })
    })
}


function readPath(path) {

    //empty the coordinates of the previous path
    pathCoord = [];
    charOffsets = [];

    var bezierCoord = {a:[],c:[]};

    //get the coordinates of the current path
    for (let i = 0; i < table.getRowCount(); i++) {

        if (table.getNum(i,"level") == monit.GameLevel && table.getNum(i,"path") == path) {

            switch (table.getString(i,"ac")) {

                case "a": 
                    bezierCoord.a.push({x:table.getNum(i,"x"),
                                        y:table.getNum(i,"y")});
                    break;
                case "c": 
                    bezierCoord.c.push({x:table.getNum(i,"x"),
                                        y:table.getNum(i,"y")});
                    break;
                default:
                    console.log("error while reading the bezier curve");
                    break;

            }
        }
    }

    let segNumber = bezierCoord.c.length/2;

    // //temp: chose the step density for each path
    // if (pathID == 2 || pathID == 4 || pathID == 6) {
    //     stepSize = 125;
    // } else {
    //     stepSize = 125;
    // }

    for (s = 0; s < segNumber; s++) {

        var a1 = createVector(bezierCoord.a[s].x,bezierCoord.a[s].y);
        var c1 = createVector(bezierCoord.c[s*2].x,bezierCoord.c[s*2].y);
        var c2 = createVector(bezierCoord.c[s*2+1].x,bezierCoord.c[s*2+1].y);
        var a2 = createVector(bezierCoord.a[s+1].x,bezierCoord.a[s+1].y);
        var curve = new BezierCurve(a1,c1,c2,a2,s==segNumber-1); //if it is the last segment return also the last point
// console.log("lunghezza curva: ", curve.length());

        stepDensity = ceil(curve.length() / stepSize); // dense: 100, sparse: 200
        var pointsOnBezier = curve.equidistantPoints(stepDensity);
        //if it is the last segment -> discard the second last point
        if(s == segNumber-1) pointsOnBezier.splice(pointsOnBezier.length-2,1);
        pointsOnBezier.forEach((el)=>{pathCoord.push({posX:el.x/monit.idealRes.width,posY:el.y/monit.idealRes.height})});
    }
}

var tempTempo = 0;
function showRound() {  //TEMPORARY!!

    // background('black');
    // textSize(30);
    // stroke('white');
    // fill('white')
    // textAlign(CENTER, CENTER);
    // text("ROUND "+pathID, windowWidth/2, windowHeight/2);

    // tempTempo+=1000/myFrameRate;
    // if(tempTempo>1500) {
        currentState++;
    //     tempTempo = 0;
    // }
}


function cutScene() {

    var distr = monit.getDistractors();
    monit.drawTargets();
    monit.drawDistractors();
    animateElements();
    moveElements();

    if (tgOnTheLeft) {

        if (friend.posX > windowWidth/4 + 50){
            monit.animateDistractor(friend,"-hurra");
        } else {
            friend.img = currentRound.friendImg;
        }
        if (friend.posX < windowWidth*(1-placeHolder/4)) {
            
            var newDistr = monit.addDistractor(windowWidth*(1-placeHolder/4),friend.posY+friend.sizY/2,currentRound.footprintImg);
            monit.playSound("pop.mp3");
            newDistr.sizX *=1.5;
            newDistr.sizY *=1.5;
            newDistr.rot = -PI/2;
            placeHolder++;
        }

    } else {
        
        if (friend.posX < windowWidth*3/4 - 50){
            monit.animateDistractor(friend,"-hurra");
        } else {
            friend.img = currentRound.friendImg;
        }
        if (friend.posX > windowWidth*placeHolder/4) {

            var newDistr = monit.addDistractor(windowWidth*placeHolder/4,friend.posY+friend.sizY/2,currentRound.footprintImg);
            monit.playSound("pop.mp3");
            newDistr.sizX *=1.5;
            newDistr.sizY *=1.5;
            newDistr.rot = PI/2;           
            placeHolder++;
        }
    }


    if (isStart) {

        initScene();
        // currentState++;
        isStart = false;
    }
}

function addElements() {

    addFootprints();
    //add the friend
    addFriend();
    //add the character
    addCharacter();
    //add the scene elements
    addSceneElements();

}

//function to add the footprints as ordered targets 
function addFootprints() {

    pathObj.segments.forEach((seg)=>{
        seg.coords.forEach((point,i)=>{
            
            if (seg.type != 'through' && i == seg.coords.length-1) {
            } else {
                var tgt = monit.addTarget(  point.posX*windowWidth,
                                            point.posY*windowHeight,
                                            currentRound.footprintImg,i);
                tgt.rot = point.rot;
                tgt.sizX = stepSize;
                tgt.sizY = stepSize;
                point.tgt = tgt;
            }
        });
    });    
}

function addFriend() {
    
    var seg = pathObj.getSegsByType('final')[0];

    friend = monit.addTarget(   seg.coords[seg.coords.length-1].posX*windowWidth,
                                seg.coords[seg.coords.length-1].posY*windowHeight + currentRound.refuge.offsetY,
                                currentRound.friendImg);

    seg.coords[seg.coords.length-1].tgt = friend;
}

function addCharacter() {

    //add the character
    character = monit.addDistractor(0,0,charName,10);

}

function addSceneElements() {

    var finalSegment = pathObj.getSegsByType('final')[0];
    monit.addDistractor(finalSegment.coords[finalSegment.coords.length-1].posX*windowWidth,
                        finalSegment.coords[finalSegment.coords.length-1].posY*windowHeight,
                        currentRound.refuge.img);

    var deadSegments = pathObj.getSegsByType('dead');
    var deadSegmentsImg = currentRound.sceneElements.filter(el=>el.type == "dead");
    if(deadSegmentsImg.length) {
        deadSegments.forEach((s,index)=>{
            var element = monit.addTarget(  s.coords[s.coords.length-1].posX*windowWidth,
                                            s.coords[s.coords.length-1].posY*windowHeight,
                                            deadSegmentsImg[index].img);
            s.coords[s.coords.length-1].tgt = element;
        });
    }
    
    var decSegmentsImg = currentRound.sceneElements.filter(el=>el.type == "decoration");
    if(decSegmentsImg.length) decSegmentsImg.forEach((s,index)=>{
        var d = monit.addDistractor(s.posX,s.posY,s.img);
        if(s.scale) {
            d.sizX *= s.scale;
            d.sizY *= s.scale;
        }
    });
}

function initPositions() {

    var charOffsetY = -stepSize/2-(character.sizY)/2+(character.sizY)*0.08;

    pathObj.segments.forEach((seg,si)=>{
        seg.coords.forEach((point,i)=>{
            if(point.offsetX == 0) {
                if(point.tgt.posY + charOffsetY < 0) {
                    if (i!=seg.coords.length-1) point.offsetX = -(seg.coords[i+1].tgt.posX-seg.coords[i].tgt.posX)/abs(seg.coords[i+1].tgt.posX-seg.coords[i].tgt.posX)*stepSize*1.2;
                    point.offsetY = 0;
                } else {
                    point.offsetY = charOffsetY;
                }
            } else {
                point.offsetX *= 150;
                point.offsetY = 0;
            }
        });
    });             

    var firstTarget = monit.getTargets()[0];
    var firstPoint = pathObj.getPointByTgtId(firstTarget.id);
    //starting coordinates of the character
    character.posX = firstTarget.posX + firstPoint.offsetX;
    character.posY = firstTarget.posY + firstPoint.offsetY;
}

function moveCharacter(){

    //if the character is moving
    var charMove = movementsStack.find(function(obj){return obj.item.id == character.id});
    if(charMove) {
        //do not animate if it is on the first footprint
        if (monit.getTargets().filter((el)=>{return el.img == currentRound.footprintImg.replace(".png","-pestata.png")}).length != 1) {
            (charMove.movDescr.endPosX > charMove.movDescr.startPosX) ? monit.animateDistractor(character,"-right-spostamento") : 
                                                                        monit.animateDistractor(character,"-left-spostamento");
        }
    } else {
        character.img = charName;
    }
}

function animateElements() {

    animationsStack.forEach((animObj, i)=>{
        // console.log("valuto l'animazione numero: ",i);
        if(getCurrTM() - animObj.timestamp > animObj.end){
          
            animationsToSplice.push(i);
        // console.log("tolgo l'animazione numero: ",i);
        } else {
            
            animations[animObj.type].cb(animObj);
        // console.log("eseguo la cb dell'animazione numero: ",i);

        }
    })
    animationsToSplice.forEach((i)=>{animationsStack.splice(i,1)});
    animationsToSplice = [];
}
function targetGazed (tgtId) {
    var tgt = monit.getTargets().find(el=>el.id==tgtId);
    var point = pathObj.getPointByTgtId(tgt.id);
        // for each possible next target
    nextTgtList.forEach((nextid)=>{

        switch (tgt.img) {

            case currentRound.footprintImg:
                // if the gazed footprint is the correct one
                if (tgt.id == nextid && point.isWrong != true) {

                    pathObj.currentPointId = pathObj.getPointByTgtId(tgt.id).id;

                    nextTgtList = [];
                    pathObj.getNextPointsById(pathObj.currentPointId).forEach((point)=>{
                        nextTgtList.push(point.tgt.id);
                    });

                    tgt.img = currentRound.footprintImg.replace(".png","-pestata.png"); //change footprint property to mark it as done
                    // monit.convertItem(tgt.id,"distractor");
                    monit.playSound('pop.mp3');

                    //remove the previous character's movement from the stack
                    _.remove(movementsStack, d => d.item.id == character.id);
                    //initialize the new movement
                    initMovement(character,{type:"sin",
                                            endPosX:tgt.posX + point.offsetX,
                                            endPosY:tgt.posY + point.offsetY,
                                            duration:800});
                // if the gazed footprint is the next one but on a wrong (dead) segment                       
                } else if (tgt.id == nextid && point.isWrong == true) {
                    monit.playSound("wrong.wav");
                    var obj = {};
                    Object.assign(obj, animations['selected'].obj);
                    obj.timestamp = getCurrTM();
                    obj.color = {r:230,g:41,b:0};
                    obj.item = tgt;
                    // obj.end = 1000;
                    // obj.dimFactor = 3;
                    animationsStack.push(obj);
                }
                break;
            case currentRound.friendImg:
                if (tgt.id == nextid) {
                    nextTgtList = null;
                } else {
                    hello();
                }
                break;
            default:
                if(currentRound.sceneElements.findIndex(el=>el.type=="dead" && el.img == tgt.img) != -1 && tgt.id == nextid) {
                    wrongBranch(point);
                }
                break;
        }
    });
}

function wrongBranch(point) {

    var segment = pathObj.getSegById(point.id);
    var parentSegment = pathObj.segments[segment.idParentSeg];
    var forkPoint =  parentSegment.coords[parentSegment.coords.length-1];

    //revert footprint image on the dead segment
    segment.coords.forEach((p,i)=>{
        if (i != segment.coords.length-1) p.tgt.img = currentRound.footprintImg;
    });

    nextTgtList = []
    // get the next valid targets from the fork point
    pathObj.getNextPointsById(pathObj.getPointById(forkPoint.id).id).forEach((point)=>{
        
        if (pathObj.getSegById(point.id).type == "dead" && pathObj.repeatWrongSeg) {
            nextTgtList.push(point.tgt.id);
        } else if (pathObj.getSegById(point.id).type == "dead" && !pathObj.repeatWrongSeg) {
            nextTgtList.push(point.tgt.id);
            point.isWrong = true;
        } else if (pathObj.getSegById(point.id).type != "dead") {
            nextTgtList.push(point.tgt.id);
        }
    });

    initMovement(character,{endPosX:forkPoint.tgt.posX+forkPoint.offsetX,
                            endPosY:forkPoint.tgt.posY+forkPoint.offsetY,
                            type:'jump',
                            duration:1000})
    
    monit.playSound("wrong.wav");
    var obj = {};
    Object.assign(obj, animations['selected'].obj);
    obj.timestamp = getCurrTM();
    obj.color = {r:230,g:41,b:0};
    obj.item = point.tgt;
    // obj.end = 1000;
    // obj.dimFactor = 3;
    animationsStack.push(obj);

}

function getCurrForkPoint() {

    var forkPoint = null;

    if(nextTgtList.length > 1) {

        forkPoint = pathObj.getPointById(pathObj.getPointByTgtId(nextTgtList[0]).idParentPoint);

    } else {

        var seg = pathObj.getSegByTgtId(nextTgtList[0]);
        if (seg.type == 'dead') {
            var parSeg = pathObj.segments[seg.idParentSeg]; 
            forkPoint = parSeg.coords[parSeg.coords.length-1];
        }
    }

    return forkPoint;
}

function hint(){

    // this if should check the condition of the hint triggered (isHint is temporary)
    if(isHint) {

        var idNextTgt = nextTgtList.length == 1 ? 
                        nextTgtList[0] :
                        nextTgtList.find((el)=>{return pathObj.getSegByTgtId(el).type != 'dead'});


        var nextTargets = pathObj.getNextPathById(pathObj.getPointByTgtId(idNextTgt).id);
                
        monit.playSound('richiamo.mp3');
        var obj = {};
        Object.assign(obj, animations['hint'].obj);
        obj.tgtList = nextTargets;
        obj.timestamp = getCurrTM();
        animationsStack.push(obj);
        isHint = false;

    }
}

function hello(){
    
    var isAlready = movementsStack.filter(function(obj){return obj.item.id == friend.id}).length;

    if (!isAlready) {

        helloLR ? helloLR = 0 : helloLR = 1;

        initMovement(friend,{   type:"fullSin",
                                endRot:(PI/8)*pow(-1,helloLR),
                                endPosX:friend.posX + 60*pow(-1,helloLR),
                                duration:1000});

        monit.playSound(currentRound.friendSound);       
    }
}

//test purpose
function keyPressed() {

    if(keyCode == RIGHT_ARROW) {
    
        // isHint = 1; //I could use monit.attentionSig instead
        monit.onAttentionSig();

    } else if(keyCode == LEFT_ARROW) {
    
        if(currentState == PLAY) hello();

    } else if(keyCode == DOWN_ARROW) {
        
        // let toggleVal;
        // monit.trackerActive ? toggleVal = 0 : toggleVal = 1;
        monit.onToggleTracker(monit.trackerActive ? 0 : 1);
        // levelSpecs.bg.fade();
        // console.log("toggle tracker, trackerActive: ",monit.trackerActive);

    } else if (keyCode == UP_ARROW) {
    
        currentRound.trackerDisabledAlpha = currentRound.trackerDisabledAlpha ? 0 : 50;
        console.log("currentRound.trackerDisabledAlpha",currentRound.trackerDisabledAlpha);
    } else if (key == "n" || key == "N") {
        monit.onBackNext('next');
    } else if (key == "b" || key == "B") {
        monit.onBackNext('back');
    } else if (key == "d" || key == "D") {
        monit.onToggleFatigue();
    }
}


function getRandomInt(max) {
  return Math.floor(Math.random() * Math.floor(max));
}

function getPathCoordById(id) { 

    var index = pathCoord.findIndex((coords)=>{
        return coords.tgt.id == id; 
    });

    return pathCoord[index]; 
}

function setStepSize(inches,cm) {

    var diagPx = sqrt(sq(windowWidth)+sq(windowHeight));
    var dpiEst = diagPx/inches;
    var dpcmEst = dpiEst/2.54;

    console.log("estimated dots per cm: ", dpcmEst);

    stepSize = dpcmEst*cm;

}

// ===================================================================================

function overrideMonit() {

    monit.onRemoveTarget = (idTs) => {

        // gazeTime = monit.fixationTime +1;
        // targetGazed(monit.getTargets().find((el)=>{return el.id == idTs}));
        targetGazed(idTs);

    }

    monit.onAttentionSig = () => {

        if (currentState == PLAY) isHint = true;
    
    }

    monit.onAddTarget = () => {
        // isStart = true
    } //I could use monit.attentionSig instead

    monit.onMoveTarget = () => {} //I could use monit.attentionSig instead

    monit.drawItems = function(items){
        let me = this;
        items.forEach(function(t,i){

            switch(t.img){

                case "rect":
                    push();
                    translate(t.posX,t.posY);
                    rotate(t.rot);
                    rectMode(CENTER);
                    noStroke();
                    fill(t.col);
                    rect(0,0,t.sizX,t.sizY);
                    pop();
                    break;

                case "ellipse":
                    push();
                    translate(t.posX,t.posY);
                    rotate(t.rot);
                    ellipseMode(CENTER);
                    noStroke();
                    fill(t.col);
                    ellipse(0,0,t.sizX,t.sizY);
                    pop();
                    break;

                default:
                    if(!me.images[t.img] || me.images[t.img] == null) {
                        console.error(t.img +" not loaded");
                        return;
                    }
                    if(t.sizX && t.sizY) {
                        push();
                        translate(t.posX,t.posY);
                        rotate(t.rot);
                        imageMode(CENTER);
                        image(me.images[t.img], 0, 0, t.sizX, t.sizY);
                        pop();                        
                    }
                    else {
                        push();
                        translate(t.posX,t.posY);
                        rotate(t.rot);
                        imageMode(CENTER);
                        image(me.images[t.img], 0, 0);
                        pop();  
                    }
            }
        })
    }

    monit.onChangeDynamics = function(data){
        var type = data.id;
        console.log(data.value);
        switch(type){
            case 'ft':
                monit.fixationTime = parseInt(data.value);
                fEngine.setFixationTime(monit.fixationTime);
                break;
            case 'repeatWrongSeg':

                if(data.value)  {
                    pathObj.repeatWrongSeg = true;
                    repeatWrongSeg = true;
                } else {
                    pathObj.repeatWrongSeg = false;
                    repeatWrongSeg = false;
                }

                if(currentState == PLAY) {
                    var currForkPoint = getCurrForkPoint();
                
                    if(currForkPoint) {
    
                        var nextPoints = pathObj.getNextPointsById(currForkPoint.id);
                        var deadSeg = pathObj.getSegById(nextPoints.find((el)=>{return pathObj.getSegById(el.id).type == 'dead'}).id);
                        deadSeg.coords[0].isWrong = !data.value;
                    }
                }

                break;
            default:
                break;
        }
    }
    monit.onBackNext = function(button) {
        switch(button){
            case "back":
                if(buttons.back) {
                    console.log("back button pressed");
                    tempTempo=0;
                    switch(currentState) {
                        case SHOWROUND:
                            clearTimers(); 
                            currentState = STARTSCENE;
                            pathID > 1 ? pathID -= 2 : pathID--;
                            break;
                        case CUTSCENE:
                            clearTimers(); 
                            currentState = STARTSCENE;
                            pathID > 1 ? pathID -= 2 : pathID--;
                            break;
                        case PLAY: 
                            currentState = STARTSCENE;
                            pathID--;
                            break;
                        case ENDSCENE:
                            clearTimers(); 
                            currentState = STARTSCENE;
                            pathID--;                
                            break;
                        default:
                            break;
                    }
                }
                break;
            case "next":
                if(buttons.next) {
                    console.log("next button pressed");
                    tempTempo=0;
                    switch(currentState) {
                        case SHOWROUND: 
                            clearTimers();
                            currentState++;
                            break;
                        case CUTSCENE: 
                            clearTimers();
                            isStart = true;
                            break;
                        case PLAY: 
                            clearTimers();
                            if (pathID < numberOfPaths) {
                                currentState = STARTSCENE;
                            }
                            break;
                        case ENDSCENE:
                            if (pathID < numberOfPaths) {
                                clearTimers();
                                currentState = STARTSCENE;
                            }
                            break;
                        default:
                            break;
                    }
                }
                break;
            default:
                break;
        }
    }
    monit.onToggleFatigue = function(){
        if(buttons.cooldown) {
            if (currentState == PLAY) {
                pathObj.coolDown = true;
                currCoolDownState = 0;
            } else if (currentState == COOLDOWN && currCoolDownState ==2) {
                gameState.exit = true;
            }
        }
    }
    monit.onToggleTracker = function (val) {
        if(val == 1) this.trackerActive = true;
        else this.trackerActive = false;
        if(levelSpecs && levelSpecs.bg) {
            levelSpecs.bg.fade();
        }
    }
}

function loadTabletSettings(){
    let args = (new URL(location.href)).searchParams;
    let rws = args.get('repeatWrongSeg');
    let round = args.get('round');
    if (rws === null || rws == 'true') {
        repeatWrongSeg = true;
    } else {
        repeatWrongSeg = false;
    }
    if (round != null) {
        // console.log("round",round)
        pathID = round;
    }
}

function loadMyJson(url) {
    fetch(url)
        .then(response => {
            return response.json()
        })
        .then(data => {         
            let string = JSON.stringify(data, replacer);
            let nData = JSON.parse(string);
            levelSpecs = _.cloneDeep(nData);
        });
}

function replacer(key, val) {
    const keys = [
      "posX",
      "posY"
    ];
    let rtn = val;
    if (keys.indexOf(key) >= 0) {
        rtn *= monit.screenScaleFactor; 
    }
    return rtn; 
}

function constraintToWindow(item) {

    if (item.posX < item.sizX/3) {
        item.posX = item.sizX/3;
    } else if (item.posX > windowWidth-item.sizX/3) {
        item.posX = windowWidth-item.sizX/3;
    }

    if (item.posY < item.sizY/3) {
        item.posY = item.sizY/3;
    } else if (item.posY > windowHeight-item.sizY/3) {
        item.posY = windowHeight-item.sizY/3;
    }
}

function clearTimers() {
    timer.forEach((t)=>{
        clearTimeout(t);
    })
}

function setButtonsState(state) {
    if(state.next !== undefined) buttons.next = state.next;
    if(state.back !== undefined) buttons.back = state.back;
    if(state.cooldown !== undefined) buttons.cooldown = state.cooldown;
    monit.sendEvent({ts: new Date().getTime(), type: "event", event_type: "buttons_state", data:{cooldown:buttons.cooldown,next:buttons.next,back:buttons.back}});
}

//=====================================================================================

movements['sin'] = {
    cb: function(el){

        el.prog += el.movDescr.step;

        var index = (1+sin(PI*el.prog-PI/2))/2;

        el.item.posX = el.movDescr.startPosX +  index * el.movDescr.distPosX;
        el.item.posY = el.movDescr.startPosY +  index * el.movDescr.distPosY;

        el.item.sizX = el.movDescr.startSizX +  index * el.movDescr.distSizX;
        el.item.sizY = el.movDescr.startSizY +  index * el.movDescr.distSizY;

        el.item.rot = el.movDescr.startRot +  index * el.movDescr.distRot;
        
    },
    obj:{
        type: 'sin',
        item:{},
        movDescr:{},
        prog:0
    }
}

movements['fullSin'] = {
    cb: function(el){

        el.prog += el.movDescr.step;

        var index = (1+sin(2*PI*el.prog-PI/2))/2;

        el.item.posX = el.movDescr.startPosX +  index * el.movDescr.distPosX;
        el.item.posY = el.movDescr.startPosY +  index * el.movDescr.distPosY;

        el.item.sizX = el.movDescr.startSizX +  index * el.movDescr.distSizX;
        el.item.sizY = el.movDescr.startSizY +  index * el.movDescr.distSizY;

        el.item.rot = el.movDescr.startRot +  index * el.movDescr.distRot;
        
    },
    obj:{
        type: 'fullSin',
        item:{},
        movDescr:{},
        prog:0
    }
}

movements['pulsate'] = {
    cb: function(el){

        el.prog += el.movDescr.step;

        var sizIndex1 = (1-sin(10*PI*el.prog+PI/2))/2;
        var sizIndex2 = sq(el.prog-1);
        var posIndex1 = sin(4*PI*el.prog);
        var posIndex2 = cos(4*PI*el.prog);

        el.item.posX = el.movDescr.startPosX + posIndex1 * 10;//el.movDescr.distPosX;
        el.item.posY = el.movDescr.startPosY + posIndex2 * 10;//el.movDescr.distPosY;

        el.item.sizX = sizIndex2*el.movDescr.startSizX +  sizIndex1 * el.movDescr.distSizX;
        el.item.sizY = sizIndex2*el.movDescr.startSizY +  sizIndex1 * el.movDescr.distSizY;

        // el.item.rot = el.movDescr.startRot +  index * el.movDescr.distRot;
        
    },
    obj:{
        type: 'pulsate',
        item:{},
        movDescr:{},
        prog:0
    }
}

movements['jump'] = {
    cb: function(el){

        el.prog += el.movDescr.step;

        var index = (1+sin(PI*el.prog-PI/2))/2;

        // (1-sq(el.prog-1)) * el.distY - 100*2*(cos(2*PI*el.prog+PI)+1)/2;;

        el.item.posX = el.movDescr.startPosX +  index * el.movDescr.distPosX;
        el.item.posY = el.movDescr.startPosY +  index * el.movDescr.distPosY - 100*(cos(2*PI*el.prog+PI)+1)/2;

        el.item.sizX = el.movDescr.startSizX +  index * el.movDescr.distSizX + 50*(cos(2*PI*el.prog+PI)+1)/2;
        el.item.sizY = el.movDescr.startSizY +  index * el.movDescr.distSizY + 50*(cos(2*PI*el.prog+PI)+1)/2;

        el.item.rot = el.movDescr.startRot +  index * el.movDescr.distRot;
        
    },
    obj:{
        type: 'jump',
        item:{},
        movDescr:{},
        prog:0
    }
}

function initMovement(item,movDescr) {

    //prog spans between 0 and 1
    movDescr.prog = 0;
    // movDescr.frameRate = myFrameRate;

    movDescr.step = 1000/(myFrameRate * movDescr.duration);
    // console.log("inizio: ",getCurrTM());
    // console.log("frameRate: ",movDescr.frameRate,"movDescr.duration: ",movDescr.duration,"step: ", movDescr.step);

    //POSITION
    if(movDescr.startPosX == null) movDescr.startPosX = item.posX;
    if(movDescr.endPosX == null) movDescr.endPosX = item.posX;
    if(movDescr.startPosY == null) movDescr.startPosY = item.posY;
    if(movDescr.endPosY == null) movDescr.endPosY = item.posY;

    movDescr.distPosX = movDescr.endPosX - movDescr.startPosX;
    movDescr.distPosY = movDescr.endPosY - movDescr.startPosY;

    //SIZE
    if(movDescr.startSizX == null) movDescr.startSizX = item.sizX;
    if(movDescr.endSizX == null) movDescr.endSizX = item.sizX;
    if(movDescr.startSizY == null) movDescr.startSizY = item.sizY;
    if(movDescr.endSizY == null) movDescr.endSizY = item.sizY;

    movDescr.distSizX = movDescr.endSizX - movDescr.startSizX;
    movDescr.distSizY = movDescr.endSizY - movDescr.startSizY;

    //ROTATION
    if(movDescr.startRot == null) movDescr.startRot = item.rot;
    if(movDescr.endRot == null) movDescr.endRot = item.rot; 

    movDescr.distRot = movDescr.endRot - movDescr.startRot;

    var obj = _.cloneDeep(movements[movDescr.type].obj);
    obj.id = item.id;
    obj.item = item;
    obj.movDescr = movDescr;
    movementsStack.push(obj);

    return obj;
}

function moveElements() {

    movementsStack.forEach((movObj, i)=>{
        // console.log("valuto l'animazione numero: ",i);
        if(movObj.prog < 1){
            movements[movObj.type].cb(movObj);
        // console.log("eseguo la cb dell'animazione numero: ",i);           
        } else {
            _.remove(movementsStack, d => d.id == movObj.id && d.type == movObj.type)
         }
    })
}



