//state machine
const stateMachine = [
    init,               //
    startScene,         //
    play,               //
    endScene,           //
    endGame,
    gameOver
];
var currentState = 0;

const STARTSCENE = 1;
const PLAY = 2;
const ENDSCENE = 3;
const ENDGAME = 4;
const GAMEOVER = 5;

var currentSceneID = 0;
var currentScene = null;

var trackerActive = false;
var gazeX = 0;
var gazeY = 0;
var gazeTime = 0;

var cuscinateJson = {};
var isHint = 0;
var pixelColor = {before:[],after:[]}; 
var hitTime = 2000;
var pillowSounds = ["pillow-1.wav","pillow-2.wav","pillow-3.wav","pillow-4.wav"];

var doCheckCollisions = true;

var pillowQueue = [];
var tgtToRemoveQueue = [];
var handIsBusy = false;

var playTimer = {duration:60000};
var myFrameRate = 30;
var showFixation = true;
var disableMalus = false;
var tempSettings = {vel:3};

//monit clone
var myCounterUuid = 0;

// animations
var animations = [];
var animationsStack = [];
var animationsToSplice = [];

// movements
var movements = [];
var movementsStack = [];
var movementsToSplice = [];
//temp
var buttons = {
    back:false,
    next:false,
    cooldown: false
}

animations['selected'] = {
    cb: function(el){

        el.prog = map(el.index,0,el.end,0,20*PI);

        push();
        noStroke();
        fill(146,208,23,180);
        ellipse(el.item.posX,el.item.posY,el.item.sizY*(1+0.1*cos(el.prog)),el.item.sizY*(1+0.1*cos(el.prog)));
        pop();

        el.index += 1000/myFrameRate;
        
    },
    obj:{
        type: 'selected',
        timestamp: getCurrTM(),
        item:{},
        index: 0,
        end: 1500,
        prog:0
    }
}

animations['selectedBis'] = {
    cb: function(el){

        el.prog = map(el.index,0,el.end,0,1);

        var x = (1+sin(PI*el.prog-PI/2))/2;

        push();
        rectMode(CENTER);
        noStroke();
        fill(el.color.r,el.color.g,el.color.b,map(x,0,1,150,0));
        ellipse(el.item.posX,el.item.posY,el.item.sizY+el.dimFactor*x,el.item.sizY+el.dimFactor*x);
        pop();
        el.index += 1000/myFrameRate;
        
    },
    obj:{
        type: 'selectedBis',
        timestamp: getCurrTM(),
        item:{},
        color:{r:230,g:41,b:0},
        index: 0,
        end: 800,
        dimFactor: 50,
        prog:0
    }
}

animations['hint'] = {
    cb: function(el){

        el.prog = map(el.index,0,el.end,el.item.sizX,el.item.sizX*2);
        var fillAlpha = map(el.index,0,el.end,255,0);
        push();
        // stroke(255);
        // strokeWeight(5);
        noStroke();
        fill(255,0,0,fillAlpha);
        ellipse(el.item.posX,el.item.posY,el.prog,el.prog);
        pop();

        el.index += 1000/myFrameRate;
        
    },
    obj:{
        type: 'hint',
        timestamp: getCurrTM(),
        item:{},
        index: 0,
        end: 800,
        prog:0
    }
}

animations['blood'] = {
    cb: function(el){
        el.prog = map(el.index,0,el.end,0,1);
        var fillAlpha = exp(-el.prog*6)*150;
        push();
        noStroke();
        fill(255,255,255,fillAlpha);
        rect(0,0,windowWidth,windowHeight);
        pop();
        el.index += 1000/myFrameRate;
    },
    obj:{
        type: 'blood',
        timestamp: getCurrTM(),
        item:{},
        index: 0,
        end: 1000,
        prog:0,
        x:1
    }
}

function preload() {

    monit.loadJsonItems();
    loadMyJson("./cuscinate.json");
    // table = loadTable('data.csv','csv','header');

}

function setup() {
    
    // monit.loadJsonItems();

    createCanvas(windowWidth,windowHeight);
    noStroke();
    frameRate(myFrameRate);
    loadTabletSettings();
    // monit.setBg('#b9cce1');
    


}

//do nothing until the flag monit.started is false
function checkFlag() {

    if(!monit.started || !monit.loaded) {
       setTimeout(checkFlag, 100); /* this checks the flag every 100 milliseconds*/   
    }
}

function draw() {

    if(!monit.started || !monit.loaded) {
        setTimeout(checkFlag, 100); /* this checks the flag every 100 milliseconds*/ 
        background('black');
        textSize(30);
        stroke('white');
        fill('white');
        textAlign(CENTER, CENTER);
        text('Caricamento in corso...', windowWidth / 2, windowHeight / 2);
        textSize(12);
    } else {
        // monit.setBg('background.png');
        // console.log("stato: ",currentState);  	
        //draw the background
        monit.drawBg();
        if(currentScene && currentScene.bg) currentScene.bg.run();

        if (currentState < stateMachine.length) {
            stateMachine[currentState]();
        }
        // monit.monitoring();
        // // // real fps on top left corner
        // fill(0); //black text
        // text(frameRate(), 10, 10);
    }
}

function gaze() {
    if (monit.trackerActive && trackerActive) {
        gazeX = monit.mode == 'eye' ? monit.gaze[0] * window.innerWidth : mouseX;
        gazeY = monit.mode == 'eye' ? monit.gaze[1] * window.innerHeight : mouseY;
    }
}

// to assess if the pointer is on a target and what to do if so
function colorCollide(tgt) {

    // console.log("pixelColor.before.toString()",pixelColor.before.toString(),"pixelColor.after.toString()",pixelColor.after.toString());
    var friend = getFriendFromTgt(tgt);
    if (gazeX > tgt.posX - tgt.sizX / 2 && 
        gazeX < tgt.posX + tgt.sizX / 2 && 
        gazeY > tgt.posY - tgt.sizY / 2 && 
        gazeY < tgt.posY + tgt.sizY / 2 &&
        pixelColor.before.toString() != pixelColor.after.toString()) {
        // console.log("before",pixelColor.before.toString(),"after",pixelColor.after.toString())
        // var hotspot = getHotspotFromTgt(tgt);
        friend.gazeTime += 1000/myFrameRate;
        friend.pauseTimers();
        if(friend.gazeTime > (tgt.correct ? monit.fixationTime : monit.fixationTime*2)) {
            friend.gazeTime = 0;
            targetGazed(tgt);
        } else {
            if(showFixation && !friend.hotspot.isGazed) {
                push();
                stroke(200);
                fill(255,100)
                ellipse(tgt.posX,tgt.posY,100,100);
                fill(255,210);
                arc(tgt.posX,tgt.posY,100,100,-PI/2+map(friend.gazeTime,50,tgt.correct ? monit.fixationTime : monit.fixationTime*2,0,2*PI),3/2*PI,PIE); //
                pop();
            }
        }
    } else {
        friend.gazeTime = 0;
        friend.resumeTimers();
    }
}

function init() {
   
    // monit.init();
    overrideMonit();
    currentSceneID = 0;

    // myFrameRate = frameRate();

    // monit.fixationTime = 2000; //temp
    // monit.GameLevel = 4; //temp
    
    //update the state
    currentState++;
}

function startScene() {
    // console.log("startScene()");
    setButtonsState({next:false,back:false,cooldown:false});

    monit.clearDistractors();
    monit.clearTargets();

    //to test tutorial
    if(!currentScene) {
        currentScene = cuscinateJson.tutorials[monit.GameLevel].scenes[currentSceneID];
        setButtonsState({next:true,back:false,cooldown:false});
    } else {
        if(currentScene.activeFriends) {
            for(let i = 0;i<10;i++) {
                if(currentScene.activeFriends[0]) {
                    currentScene.activeFriends[0].destroy();
                }
            }
        }
        currentScene = cuscinateJson.levels[monit.GameLevel].scenes[currentSceneID];
        setButtonsState({next:false,back:false,cooldown:false});
    }

    monit.setBg(currentScene.background);

    //resize data in cuscinate.json to the current windowWidth windowHeight
    resizeElementsToWindow();
    //init array of active friends
    currentScene.activeFriends = [];
    //add the distractors (refuges and score)
    addElements();

    //remove refuges without hotspot
    currentScene.refuge = currentScene.refuge.filter((refuge)=>{
        return refuge.hotspots.length;
    })

    currentScene.bg = new Bg(myFrameRate);
    //set the absolute positions of the target in all the hotspots (call after addElements)
    setTargetPositions();

    trackerActive = true;

    if(!currentScene.tutorial) {
        //save the timestamp
        currentScene.startTime = getCurrTM();
    }

    // let args = (new URL(location.href)).searchParams;
    // if(args.get('vel') !== null && !currentScene.tutorial) {
    //     currentScene.exposureTime = currentScene.velocities[parseInt(args.get('vel'))-1].exposureTime;
    //     currentScene.generationRate = currentScene.velocities[parseInt(args.get('vel'))-1].generationRate;
    // }
    currentScene.exposureTime = cuscinateJson.levels[monit.GameLevel].scenes[currentSceneID].velocities[tempSettings.vel].exposureTime;
    currentScene.generationRate = currentScene.tutorial ? currentScene.generationRate : cuscinateJson.levels[monit.GameLevel].scenes[currentSceneID].velocities[tempSettings.vel].generationRate;

    doCheckCollisions = true;

    currentScene.arms = [];
    currentScene.friends.forEach((friend)=>{
        currentScene.arms.push(friend.img.replace(".png","-braccio.png"));
    });

    currentState++;
}

function play() {

    gaze();
    moveElements();
    runFriends();

    drawDistrButImg(currentScene.arms.concat(["cuscino.png"]));
    pixelColor.before = get(gazeX,gazeY);
    monit.drawAllItems();
    pixelColor.after = get(gazeX,gazeY);

    if(doCheckCollisions && !currentScene.paused) {
        generateTargets();
        checkCollisions();
    }

    throwPillows();
    removeHitTargets();

    if(isHint) {
        giveHint();
        isHint = false;
    }
    animateElements();
}

function endScene() {
    // console.log("endScene()");
    monit.clearTargets();
    let allButScore = monit.getDistractors().filter(el=>!el.img.startsWith("cuscino_"));
    clearDistractorsByArray(allButScore);

    let charNum = 0;
    charNum += currentScene.score.currentScore;
    charNum += currentScene.score.malusCounters.wrong;
    charNum += currentScene.score.malusCounters.catched;
    let scaleFactor = 1/(charNum * 0.4);
    let pos = 0;
    for(let i = 0; i < currentScene.score.currentScore; i++) {
        var char = monit.addDistractor((0.5 + pos)*windowWidth/charNum,windowHeight*(1/4*(1.5*random()-1) + 3/4),currentScene.friends.find(el=>el.correct).img.replace(".png","_colpito.png"));
        char.sizX = 700*scaleFactor;
        char.sizY = 500*scaleFactor;
        pos++;
    }
    for(let i = 0; i < currentScene.score.malusCounters.wrong; i++) {
        var char = monit.addDistractor((0.5 + pos)*windowWidth/charNum,windowHeight*(1/4*(1.5*random()-1) + 3/4),currentScene.friends.find(el=>!el.correct).img.replace(".png","_colpito.png"));
        char.sizX = 700*scaleFactor;
        char.sizY = 500*scaleFactor;
        pos++;
    }
    for(let i = 0; i < currentScene.score.malusCounters.catched; i++) {
        var char = monit.addDistractor((0.5 + pos)*windowWidth/charNum,windowHeight*(1/4*(1.5*random()-1) + 5/8),currentScene.friends.find(el=>el.correct).img.replace(".png","_vincitore.png"));
        char.sizX = 500*scaleFactor;
        char.sizY = 700*scaleFactor;
        pos++;
    }

    if(currentScene.score.win) {
        monit.playSound("applause.wav");
        monit.playSound("magic.mp3");
    } else {
        monit.playSound("fail.wav");
    }
    movementsStack = [];
    currentState++;
}

function endGame() {
    // background('black');
    // textSize(30);
    // stroke('white');
    // fill('white')
    // text('EVVIVA!', windowWidth / 2 - 50, windowHeight / 2 - 50);
    // textSize(12);
    monit.drawAllItems();
    moveElements();
    if(random() > 0.95) {
        var friends = monit.getDistractors().filter(el=>el.img.endsWith("_vincitore.png"));
        var friend = friends[getRandomInt(friends.length)];
        if(friend && movementsStack.findIndex(el=>el.id == friend.id)<0) initMovement(friend,{type:'jump',duration:1000,distPosY:-100},true);
    }
}

function gameOver() {
    background('black');
    textSize(30);
    stroke('white');
    fill('white')
    text('GAME OVER!', windowWidth / 2 - 50, windowHeight / 2 - 50);
    textSize(12);
}

function clearDistractorsByArray(array){
    array.forEach( el => {
        monit.removeDistractor(el);
    });
}

function resizeElementsToWindow() {

    currentScene.refuge.forEach((refuge,index)=>{

        refuge.position.x *= (window.innerWidth / monit.idealRes.width);
        refuge.position.y *= (window.innerHeight / monit.idealRes.height);

        refuge.hotspots.forEach((hotspot,i)=>{

            hotspot.startPosition.x *= refuge.scaleFactor*(window.innerWidth / monit.idealRes.width);
            hotspot.startPosition.y *= refuge.scaleFactor*(window.innerHeight / monit.idealRes.height);
            hotspot.endPosition.x *= refuge.scaleFactor*(window.innerWidth / monit.idealRes.width);
            hotspot.endPosition.y *= refuge.scaleFactor*(window.innerHeight / monit.idealRes.height);

            hotspot.startPosition.r = radians(hotspot.startPosition.r);
            hotspot.endPosition.r = radians(hotspot.endPosition.r);

            hotspot.scaleFactor = refuge.scaleFactor;
        });
    });
}

function addElements() {

    //add all the refuges of the current scene
    currentScene.refuge.forEach((refuge,index)=>{

        refuge.distractor = monit.addDistractor(refuge.position.x,refuge.position.y,refuge.name,refuge.zIndex ? refuge.zIndex : 15);
        //resize the distractor according to depth
        refuge.distractor.sizX *= refuge.scaleFactor;
        refuge.distractor.sizY *= refuge.scaleFactor;

    });

    if (!currentScene.tutorial) {
        currentScene.score = new Score(currentScene.bullets,currentScene.scoreToWin,"cuscino.png");
    }
}

function setTargetPositions() {

    currentScene.refuge.forEach((refuge,index)=>{
        refuge.hotspots.forEach((hotspot,i)=>{
            hotspot.startPosition.x += refuge.distractor.posX - refuge.distractor.sizX/2; 
            hotspot.startPosition.y += refuge.distractor.posY - refuge.distractor.sizY/2; 
            hotspot.endPosition.x += refuge.distractor.posX - refuge.distractor.sizX/2;   
            hotspot.endPosition.y += refuge.distractor.posY - refuge.distractor.sizY/2; 
        });
    });
}

function generateTargets() {

    var tgts = monit.getTargets();

    if (random() > currentScene.generationRate && tgts.length < currentScene.maxTargets) {

        //pick a random refuge
        var refuge = currentScene.refuge[getRandomInt(currentScene.refuge.length)];
        //pick a random hotspot
        var hotspot = refuge.hotspots[getRandomInt(refuge.hotspots.length)];
        //pick a random friend
        var friend = hotspot.character != null  ? currentScene.friends[hotspot.character] : currentScene.friends[getRandomInt(currentScene.friends.length)];
        
        if (!hotspot.isActive) activateHotspot(refuge,hotspot,friend);
    }
}

function checkCollisions(){

    var friends = currentScene.activeFriends.filter(el=>!el.hasAttacked);

    friends.forEach((friend,index)=>{
        colorCollide(friend.tgt);
    });
}

function throwPillows() {
// console.log("coda: ",pillowQueue.length,"handIsBusy:",handIsBusy);
    if (pillowQueue.length && !handIsBusy) {
        //throw the first element
        throwOnePillow(pillowQueue[0]);

        handIsBusy = true;

        //add the hotspot to the queue of targets to remove
        tgtToRemoveQueue.push(pillowQueue[0]);

        //remove the first element from the queue
        pillowQueue.shift();
    }
}

function throwOnePillow(hotspot) {

    // var distr = monit.getDistractors();

    //IF THE PILLOW MUST BE VISIBLE ON THE TABLET
    var pillow = monit.addDistractor(0,windowHeight-837/2+478/2,"cuscino.png",20);

    //add the hand as distractor
    var hand = monit.addDistractor(0,windowHeight+100,"mano.png",25);
    hand.rot = -PI/4;

    var friend = getFriendFromHotspot(hotspot);

    //pillow flight
    initMovement(pillow,{   endPosX:hotspot.endPosition.x,
                            endPosY:hotspot.endPosition.y,
                            endRot:4*PI,
                            duration:hitTime/3,
                            type:"pillow",
                            endSizX:friend.tgt.sizY/2,
                            endSizY:friend.tgt.sizY/2,});

    initMovement(hand, {endRot:PI*2/3,            //distr[0]
                        duration:hitTime/8,
                        type:"linear"});

    //timeout for the moment when the pillow hits the target
    setTimeout(()=>{
        friend.hit();
    },hitTime/3);

    //timeout for the removal of the pillow
    setTimeout(()=>{
        hotspot.selAnimation.end = 1;
        hotspot.isHit = true;
        monit.removeDistractorById(pillow.id);
    },hitTime/2);

    //timeout to free the hand
    setTimeout(()=>{
        handIsBusy = false;
        monit.removeDistractorById(hand.id);
    },hitTime/8);
}

function removeHitTargets() {
    tgtToRemoveQueue.forEach((hotspot,index)=>{

        var friend = getFriendFromHotspot(hotspot);
        if(hotspot.isHit) {
            hotspot.isHit = false;
            tgtToRemoveQueue.shift();

            setTimeout(()=>{
                friend.hideUp();
            },currentScene.exposureTime*2/8);
        }
    }); 
}

function activateHotspot(refuge,hotspot,friend) {
    //add the target
    var newFriend = new Friend( friend,
                                refuge.scaleFactor,
                                hotspot,
                                refuge.zIndex ? refuge.zIndex : undefined);
    currentScene.activeFriends.push(newFriend);
}

function shakeThemAll() {
    var items = monit.getTargets().concat(monit.getDistractors()).filter(el=>el.zIndex<20);
    items.forEach((item)=>{
        initMovement(item,{
            duration:1000,
            distPosX:50,
            type:'shake'
        })
    });
}

function animateElements() {
    animationsStack.forEach((animObj, i)=>{
        // console.log("valuto l'animazione numero: ",i);
        if(getCurrTM() - animObj.timestamp > animObj.end){
          
            animationsToSplice.push(i);
        // console.log("tolgo l'animazione numero: ",i);
        } else {
            
            animations[animObj.type].cb(animObj);
        // console.log("eseguo la cb dell'animazione numero: ",i);

        }
    })
    animationsToSplice.forEach((i)=>{animationsStack.splice(i,1)});
    animationsToSplice = [];
}

function animateElementsByType(type) {
    animationsStack.filter(el=>el.type == type).forEach((animObj, i)=>{
        // console.log("valuto l'animazione numero: ",i);
        if(getCurrTM() - animObj.timestamp > animObj.end){
          
            animationsToSplice.push(i);
        // console.log("tolgo l'animazione numero: ",i);
        } else {
            
            animations[animObj.type].cb(animObj);
        // console.log("eseguo la cb dell'animazione numero: ",i);

        }
    })
    animationsToSplice.forEach((i)=>{animationsStack.splice(i,1)});
    animationsToSplice = [];
}

function targetGazed(tgt) {
    var friend = getFriendFromTgt(tgt);

    if (!friend.hotspot.isGazed) {
        friend.hotspot.isGazed = true;
        friend.catched();
        if(friend.tgt.correct) {
            pillowQueue.push(friend.hotspot);
            monit.playSound("catched.wav");
            var obj = {};
            Object.assign(obj, animations['selectedBis'].obj);
            obj.timestamp = getCurrTM();
            obj.color = {r:146,g:208,b:23};
            obj.end = 800;
            obj.item = friend.tgt;
            friend.hotspot.selAnimation = obj;
            animationsStack.push(obj);
        } else {
            friend.clearTimers();
            friend.hideUp();
            var obj = {};
            Object.assign(obj, animations['selectedBis'].obj);
            obj.timestamp = getCurrTM();
            obj.color = {r:230,g:41,b:0};
            obj.end = 800;
            obj.item = friend.tgt;
            animationsStack.push(obj);
            monit.playSound("wrong.wav");
            //penalty
            if(currentScene.score) currentScene.score.decrement('wrong');
        }
    }
}

function getHotspotFromTgt(tgt) {
    var rtn = {};
    currentScene.refuge.forEach((refuge,index)=>{
        refuge.hotspots.forEach((hs,i)=>{
            if (hs.target != null && hs.target.id == tgt.id) rtn = hs;
        });
    });
    return rtn;
}

function getFriendFromTgt(tgt) {
    return currentScene.activeFriends.find(fr=>fr.tgt.id == tgt.id);
}

function getFriendFromHotspot(hotspot) {
    return currentScene.activeFriends.find(fr=>fr.tgt.id == hotspot.id);
}

function giveHint() {

    currentScene.refuge.forEach((refuge,index)=>{
        refuge.hotspots.forEach((hs,i)=>{
            var friend = getFriendFromHotspot(hs);
            if (hs.isActive && !hs.isGazed && friend.tgt.correct) {
                var obj = {};
                Object.assign(obj, animations['selectedBis'].obj);
                obj.timestamp = getCurrTM();
                obj.color = {r:43,g:104,b:156};
                obj.end = 800;
                obj.item = friend.tgt;
                animationsStack.push(obj);

                monit.playSound("richiamo.wav");
            }
        });
    });    
}

function loadMyJson(url) {
    fetch(url)
        .then(response => {
            return response.json()
        })
        .then(data => {
            cuscinateJson = _.cloneDeep(data);
        });
}

function getCurrTM() {return (new Date()).getTime()}

//return a number between 0 and max-1
function getRandomInt(max) {return Math.floor(Math.random() * Math.floor(max))}

//test purpose
function keyPressed() {

    if(keyCode == RIGHT_ARROW) {

        monit.onAttentionSig();

    } else if(keyCode == LEFT_ARROW) {

        var tgts = monit.getTargets();
        if(tgts.length) monit.onRemoveTarget(tgts[tgts.length-1].id);

    } else if(keyCode == UP_ARROW) {

        // var tgts = monit.getTargets();
        // monit.onRemoveTarget(tgts[0].id);
    } else if (key == "n" || key == "N") {
        monit.onBackNext('next');
    } else if (key == "b" || key == "B") {
        monit.onBackNext('back');
    } else if (key == "d" || key == "D") {
        monit.onToggleFatigue();
    } else if (key == "p" || key == "P") {
        if(monit.trackerActive) {
            monit.onToggleTracker(0);
        } else {
            monit.onToggleTracker(1);
        }
    }
}

function drawDistrButImg(imgs) {
    var distr = monit.getDistractors().filter(function(el) {return this.indexOf(el.img) < 0},imgs);
    monit.drawItems(distr);
}

function pauseAllTimers() {
    currentScene.paused = true;
    currentScene.activeFriends.forEach((fr)=>{
        fr.pauseTimers();
    });
}

function resumeAllTimers() {
    currentScene.paused = false;
    currentScene.activeFriends.forEach((fr)=>{
        fr.resumeTimers();
    });
}

// ===================================================================================

function overrideMonit() {

    monit.onRemoveTarget = (idT) => {
        var tgts = monit.getTargets();
        targetGazed(_.find(tgts,d => d.id == idT));
    }

    monit.onAttentionSig = () => {

        if(currentState == 2) {
            isHint = true;
        }
    } //I could use monit.attentionSig instead

    monit.onAddTarget = () => {} //I could use monit.attentionSig instead

    monit.onMoveTarget = () => {} //I could use monit.attentionSig instead

    monit.onChangeDynamics = (data) => {
        console.log('data', data);
        var type = data.id;
        switch(type){
            case 'ft':
                monit.fixationTime = parseInt(data.value);
                break;
            case 'vel':
                var vel = parseInt(data.value);
                tempSettings.vel = vel-1;
                currentScene.exposureTime = cuscinateJson.levels[monit.GameLevel].scenes[currentSceneID].velocities[tempSettings.vel].exposureTime;
                currentScene.generationRate = currentScene.tutorial ? currentScene.generationRate : cuscinateJson.levels[monit.GameLevel].scenes[currentSceneID].velocities[tempSettings.vel].generationRate;
                break;
            case 'dm':
                disableMalus = data.value;
                break;
            default:
                break;
        }
    }
    monit.onBackNext = function(button) {
        switch(button) {
            case "next":
                if(buttons.next) {
                    console.log("NEXT button pressed");
                    currentState = 1;
                }
                break;
            case "back":
                if(buttons.back) {
                    console.log("back button pressed");
                }
                break;
            default:
                break;
        }
    }
    monit.onToggleFatigue = function () {
        if(buttons.cooldown) {
            console.log("cooldown button pressed");
        }
    }
    monit.onToggleTracker = (val) => {
        if(val == 1) monit.trackerActive = true;
        else monit.trackerActive = false;
        if(currentScene.paused) {
            resumeAllTimers();
        } else {
            pauseAllTimers();
        }
        if(currentScene && currentScene.bg) {
            currentScene.bg.fade();
        }
    };
}

function loadTabletSettings(){
    let args = (new URL(location.href)).searchParams;
    let dm = JSON.parse(args.get('dm'));
    if (dm != null) {
        disableMalus = dm;
    }
    if(args.get('vel') !== null) {
        tempSettings.vel = parseInt(args.get('vel'))-1;
        // tempSettings.exposureTime = cuscinateJson.levels[monit.GameLevel].scenes[currentSceneID].velocities[parseInt(args.get('vel'))-1].exposureTime;
        // tempSettings.generationRate = cuscinateJson.levels[monit.GameLevel].scenes[currentSceneID].velocities[parseInt(args.get('vel'))-1].generationRate;
    }

}

function myUuid() {
    myCounterUuid++;
    return myCounterUuid.toString() + 'm'; 
}

function setButtonsState(state) {
    if(state.next !== undefined) buttons.next = state.next;
    if(state.back !== undefined) buttons.back = state.back;
    if(state.cooldown !== undefined) buttons.cooldown = state.cooldown;
    monit.sendEvent({ts: new Date().getTime(), type: "event", event_type: "buttons_state", data:{cooldown:buttons.cooldown,next:buttons.next,back:buttons.back}});
}

//===========================================================================

//MOVEMENTS
movements['linear'] = {
    cb: function(el){

        el.prog += el.movDescr.step;

        el.item.posX = el.movDescr.startPosX +  el.prog * el.movDescr.distPosX;
        el.item.posY = el.movDescr.startPosY +  el.prog * el.movDescr.distPosY;

        el.item.sizX = el.movDescr.startSizX +  el.prog * el.movDescr.distSizX;
        el.item.sizY = el.movDescr.startSizY +  el.prog * el.movDescr.distSizY;

        el.item.rot = el.movDescr.startRot +  el.prog * el.movDescr.distRot;
        
    },
    obj:{
        type: 'linear',
        item:{},
        movDescr:{},
        prog:0
    }
}

movements['sin'] = {
    cb: function(el){

        el.prog += el.movDescr.step;

        var index = (1+sin(PI*el.prog-PI/2))/2;

        el.item.posX = el.movDescr.startPosX +  index * el.movDescr.distPosX;
        el.item.posY = el.movDescr.startPosY +  index * el.movDescr.distPosY;

        el.item.sizX = el.movDescr.startSizX +  index * el.movDescr.distSizX;
        el.item.sizY = el.movDescr.startSizY +  index * el.movDescr.distSizY;

        el.item.rot = el.movDescr.startRot +  index * el.movDescr.distRot;
        
    },
    obj:{
        type: 'sin',
        item:{},
        movDescr:{},
        prog:0
    }
}

movements['pillow'] = {
    cb: function(el){

        el.prog += el.movDescr.step;

        var index = (1-sq(el.prog-1));

        // (1-sq(el.prog-1)) * el.distY - 100*2*(cos(2*PI*el.prog+PI)+1)/2;;

        el.item.posX = el.movDescr.startPosX +  index * el.movDescr.distPosX;
        el.item.posY = el.movDescr.startPosY +  index * el.movDescr.distPosY - 100*2*(cos(2*PI*el.prog+PI)+1)/2;

        el.item.sizX = el.movDescr.startSizX +  index * el.movDescr.distSizX;
        el.item.sizY = el.movDescr.startSizY +  index * el.movDescr.distSizY;

        el.item.rot = el.movDescr.startRot +  index * el.movDescr.distRot;
        
    },
    obj:{
        type: 'pillow',
        item:{},
        movDescr:{},
        prog:0
    }
}

movements['shake'] = {
    cb: function(el){

        el.prog += el.movDescr.step;
        var index = 0.5*(sin(2*PI*el.prog*5)*(exp(-6*el.prog)));
        el.item.posX = el.movDescr.startPosX +  index * el.movDescr.distPosX;
        if (el.prog >= 1) el.item.posX = el.movDescr.startPosX;

    },
    obj:{
        type: 'shake',
        item:{},
        movDescr:{},
        prog:0
    }
}

movements['rotateArm'] = {
    cb: function(el){

        el.prog += el.movDescr.step;
        var index = (1+sin(PI*el.prog-PI/2))/2;
        el.item.armRot = el.movDescr.startRot + index * el.movDescr.distRot;
        
    },
    obj:{
        type: 'rotateArm',
        item:{},
        movDescr:{},
        prog:0
    }
}

movements['jump'] = {
    cb: function(el){

        el.prog += el.movDescr.step;

        var index = 1-pow(el.prog*2-1,2);

        el.item.posY = el.movDescr.startPosY + index * el.movDescr.distPosY;
    },
    obj:{
        type: 'jump',
        item:{},
        movDescr:{},
        prog:0
    }
}

function initMovement(item,movDescr,singleId) {

    //prog spans between 0 and 1
    movDescr.prog = 0;
    movDescr.frameRate = myFrameRate;

    movDescr.step = 1000/(movDescr.frameRate * movDescr.duration);
    // console.log("inizio: ",getCurrTM());
    // console.log("frameRate: ",movDescr.frameRate,"movDescr.duration: ",movDescr.duration,"step: ", movDescr.step);

    //POSITION
    if(movDescr.startPosX == null) movDescr.startPosX = item.posX;
    if(movDescr.endPosX == null) movDescr.endPosX = item.posX;
    if(movDescr.startPosY == null) movDescr.startPosY = item.posY;
    if(movDescr.endPosY == null) movDescr.endPosY = item.posY;

    if(movDescr.distPosX == null) movDescr.distPosX = movDescr.endPosX - movDescr.startPosX;
    if(movDescr.distPosY == null) movDescr.distPosY = movDescr.endPosY - movDescr.startPosY;

    //SIZE
    if(movDescr.startSizX == null) movDescr.startSizX = item.sizX;
    if(movDescr.endSizX == null) movDescr.endSizX = item.sizX;
    if(movDescr.startSizY == null) movDescr.startSizY = item.sizY;
    if(movDescr.endSizY == null) movDescr.endSizY = item.sizY;

    movDescr.distSizX = movDescr.endSizX - movDescr.startSizX;
    movDescr.distSizY = movDescr.endSizY - movDescr.startSizY;

    //ROTATION
    if(movDescr.startRot == null) movDescr.startRot = item.rot;
    if(movDescr.endRot == null) movDescr.endRot = item.rot; 

    movDescr.distRot = movDescr.endRot - movDescr.startRot;

    // var obj = {};
    // Object.assign(obj, movements[movDescr.type].obj);
    var obj = _.cloneDeep(movements[movDescr.type].obj);
    obj.id = singleId ? item.id : item.id + myUuid();
    obj.item = item;
    obj.movDescr = movDescr;
    movementsStack.push(obj);
}

function moveElements() {
    // if(!currentScene.paused){
        // var movementsToSplice = [];
        movementsStack.filter(el=>!el.pause).forEach((movObj, i)=>{
            if(movObj.prog < 1){
                movements[movObj.type].cb(movObj);
            } else {
                movementsToSplice.push(movObj.id);
                _.remove(movementsStack, d => d.id == movObj.id && d.type == movObj.type)
            }
        })
        // movementsToSplice.forEach((id)=>{
        //     movementsStack.splice(movementsStack.findIndex(el=>el.id==id),1);
        // });
        // movementsToSplice = [];
    // }
}

Friend = function(friend,scale,hotspot,zIndex) {
    this.zIndex = zIndex ? zIndex-3 : 5;
    this.tgt = {};
    this.arm = {};
    this.bullet = null;
    this.armRot = PI/4;
    this.armOffset = {d:0,gamma:0,bd:0}; //d, gamma: polar coordinates of the shoulder joint wrt center of the body; bd: distance along the arm axis between shoulder joint and bullet center
    this.armPivot = _.cloneDeep(friend.armPivot);
    this.hotspot = hotspot;
    this.timers = [];
    this.exposureTime = currentScene.exposureTime;
    this.hitTime = 2000;
    this.doTrackBullet = true;
    this.hasAttacked = false;
    this.paused = false;
    this.hasAttacked = false;
    this.gazeTime = 0;

    this.init = function() {
        this.tgt = monit.addTarget(this.hotspot.startPosition.x,this.hotspot.startPosition.y,friend.img,this.zIndex);
        this.arm = monit.addDistractor(this.hotspot.startPosition.x,this.hotspot.startPosition.y,friend.img.replace('.png','-braccio.png'),this.armPivot.armBehindBody ? this.zIndex-1 : this.zIndex+1);
        this.tgt.rot = this.hotspot.startPosition.r;
        this.tgt.correct = friend.correct;
        
        this.tgt.sizX *= scale*this.hotspot.friendScaleFactor;
        this.tgt.sizY *= scale*this.hotspot.friendScaleFactor;
        this.arm.sizX *= scale*this.hotspot.friendScaleFactor;
        this.arm.sizY *= scale*this.hotspot.friendScaleFactor;

        this.armPivot.bodyRelative.x *= scale*this.hotspot.friendScaleFactor*monit.screenScaleFactor;
        this.armPivot.bodyRelative.y *= scale*this.hotspot.friendScaleFactor*monit.screenScaleFactor;
        this.armPivot.armRelative.x *= scale*this.hotspot.friendScaleFactor*monit.screenScaleFactor;
        this.armPivot.armRelative.y *= scale*this.hotspot.friendScaleFactor*monit.screenScaleFactor;
        this.armPivot.bulletArmRelative *= scale*this.hotspot.friendScaleFactor;

        var dx = this.armPivot.bodyRelative.x - this.tgt.sizX/2;
        var dy = this.armPivot.bodyRelative.y - this.tgt.sizY/2;
        this.armOffset.d = sqrt(dx*dx+dy*dy);
        this.armOffset.gamma = atan(dy/dx);
        this.trackArm();

        this.activateHotspot();
        
        this.showUp(this.exposureTime/8);
        if(currentScene.bidirectional && this.tgt.correct) {
            this.throwOnePillow(this.exposureTime*2/3);
        }
        if(!currentScene.tutorial || (this.tgt.correct && currentScene.bidirectional)) this.timers.push(new MyTimer(this.hideUp,this.exposureTime*7/8,'hideUp'));
    }

    this.run = function() {
        this.trackArm();
    }

    this.activateHotspot = function() {
        this.hotspot.isActive = true;
        this.hotspot.isHit = false;
        this.hotspot.id = this.tgt.id;
    }

    this.showUp = function(time) {
        //let the target exit from the refuge
        initMovement(this.tgt,{
            endPosX:this.hotspot.endPosition.x,
            endPosY:this.hotspot.endPosition.y,
            endRot:this.hotspot.endPosition.r,
            duration:time,
            type:'sin'
        });
    }

    this.hideUp = () => {
        //start hiding the target
        initMovement(this.tgt,{
            endPosX:this.hotspot.startPosition.x,
            endPosY:this.hotspot.startPosition.y,
            endRot:this.hotspot.startPosition.r,
            duration:this.exposureTime/8,
            type:'sin'
        });
        //remove the target after some time
        this.timers.push(new MyTimer(()=>{
            this.destroy();
        },this.exposureTime/8,'destroy friend'));
    }

    this.trackArm = function() {
        var alpha = this.tgt.rot;
        var beta = this.armOffset.gamma + alpha;
        this.arm.posX = this.tgt.posX - (this.armPivot.armRelative.x - this.arm.sizX/2) + this.armOffset.d * cos(beta);
        this.arm.posY = this.tgt.posY - (this.armPivot.armRelative.y - this.arm.sizY/2) + this.armOffset.d * sin(beta);

        this.arm.rot = alpha + this.armRot;
        this.arm.posX += (this.armPivot.armRelative.x - this.arm.sizX/2)*(1 - cos(this.arm.rot));
        this.arm.posY += (this.armPivot.armRelative.x - this.arm.sizX/2)*sin(-this.arm.rot);

        if(this.bullet && this.doTrackBullet) this.trackBullet();
    }

    this.trackBullet = function() {
        var teta = this.arm.rot;
        this.bullet.posX = this.arm.posX + this.armOffset.bd*cos(teta);
        this.bullet.posY = this.arm.posY + this.armOffset.bd*sin(teta);;
        this.bullet.rot =  teta + PI/2;
    }

    this.rotateArm = function(movDescr) {
        movDescr.step = 1000/(myFrameRate * movDescr.duration);
        //ROTATION
        if(movDescr.startRot == null) movDescr.startRot = this.armRot;
        if(movDescr.endRot == null) movDescr.endRot = this.armRot; 
        if(movDescr.distRot == null) movDescr.distRot = movDescr.endRot - movDescr.startRot;
        var obj = _.cloneDeep(movements['rotateArm'].obj);
        obj.id = this.arm.id+myUuid();
        obj.item = this;
        obj.movDescr = movDescr;
        movementsStack.push(obj);
    }

    this.stopArm = function() {
        movementsStack.splice(movementsStack.findIndex(el=>el.id.startsWith(this.arm.id) && el.type == 'rotateArm'),1);
    }

    this.load = function(time) {
        this.rotateArm({
            duration: time,
            startRot: PI*3/4,
            endRot: PI*7/4
        });
        this.bullet = monit.addDistractor(this.arm.posX,this.arm.posY,"cuscino.png",this.zIndex+2);
        this.bullet.sizX = this.tgt.sizY/3;
        this.bullet.sizY = this.tgt.sizY/3;
        var dx = this.armPivot.bulletArmRelative - this.arm.sizX/2;
        this.armOffset.bd = abs(dx);
        this.doTrackBullet = true;
        this.trackBullet();
    }

    this.throw = function(time) {
        this.rotateArm({
            duration: time,
            startRot: PI*7/4,
            endRot: PI/2
        })
        if(this.bullet){
            this.timers.push(new MyTimer(()=>{
                initMovement(this.bullet,{
                    duration:this.hitTime/3,
                    endPosX:windowWidth/2,
                    endPosY:windowHeight/2,
                    endRot:4*PI,
                    type:"pillow",
                    endSizX:windowHeight,
                    endSizY:windowHeight
                });
                this.doTrackBullet = false;
                this.bullet.zIndex = 20;
                this.hasAttacked = true;
                this.timers.push(new MyTimer(()=>{
                    monit.removeDistractor(this.bullet);
                    monit.playSound(pillowSounds[getRandomInt(4)]);
                    monit.playSound("wrong.wav");
                    shakeThemAll();
                    var obj = {};
                    Object.assign(obj, animations['blood'].obj);
                    obj.timestamp = getCurrTM();
                    animationsStack.push(obj);
                    // hotspot.pillow = null;
                    this.bullet = null;
                    if(currentScene.score) currentScene.score.decrement('catched');
                },this.hitTime/4,'remove pillow'));
            },time/4,'detach pillow'));
        }
    }

    this.throwOnePillow = function(loadTime) {
        this.load(loadTime);
        this.timers.push(new MyTimer(()=>{
            this.throw(200);
        },loadTime,'throw one pillow'));
    }

    this.catched = function() {
        if(this.tgt.correct) {
            if(currentScene.score) currentScene.score.increment();
            this.clearTimers();
            if(this.bullet) {
                this.stopArm();
                this.removeBullet();
            }
        }
    }

    this.hit = function() {
        //animate the shock
        this.tgt.img = this.tgt.img.replace(".png","-stordito.png");
        monit.playSound(pillowSounds[getRandomInt(4)]);
    }

    this.removeBullet = function() {
        monit.removeDistractor(this.bullet);
        this.bullet = null;
    }

    this.clearTimers = function() {
        this.timers.forEach((timer)=>{
            timer.destroy();
        });
        this.timers = [];
    }

    this.pauseTimers = function() {
        if(!this.paused) {
            // console.log("pause friend",this.tgt.id," timers",this.timers);
            this.paused = true;
            this.timers.forEach((timer)=>{
                timer.pause();
            })
        }
    }

    this.resumeTimers = function() {
        if(this.paused) {
            // console.log("RESUME friend",this.tgt.id," timers",this.timers);
            this.paused = false;
            this.timers.forEach((timer)=>{
                timer.resume();
            })
        }
    }

    this.destroy = function() {
        var index = currentScene.activeFriends.findIndex(el=>el.tgt.id==this.tgt.id);
        if(index>=0) {
            currentScene.activeFriends.splice(index,1);
            monit.removeTarget(this.tgt);
            monit.removeDistractor(this.arm);
            this.clearTimers();
            this.hotspot.isActive = false;
            this.hotspot.isGazed = false;
            if(this.bullet) monit.removeDistractor(this.bullet);
        }
    }

    this.init();

    return this;
}

function runFriends() {
    currentScene.activeFriends.forEach((friend)=>{
        friend.run();
    })
}

MyTimer = function(cb,duration,label) {
    this.cb = cb;
    this.duration = duration;
    this.id = null;
    this.timeToFinish = null;
    this.startTime = null;
    this.elapsedTime = 0;
    this.label = label;

    this.init = function() {
        // console.log(this.label," timer created");
        this.id = setTimeout(this.cb,this.duration);
        this.startTime = getCurrTM();
    }

    this.pause = function() {
        this.elapsedTime += getCurrTM() - this.startTime;
        this.timeToFinish = this.duration - this.elapsedTime;
        clearTimeout(this.id);
    }

    this.resume = function() {
        if(this.timeToFinish > 0){
            this.id = setTimeout(this.cb,this.timeToFinish);
            this.startTime = getCurrTM();
        }
    }

    this.destroy = function() {
        // console.log(this.label," timer destroyed");
        this.pause();
        delete this;
    }

    this.init();
}

Score = function(bullets,scoreToWin,bulletImg) {
    this.bullets = bullets;
    this.scoreToWin = scoreToWin;
    this.img = bulletImg;
    this.scoreMarks = [];
    this.currentScore = 0;
    this.gameOver = false;
    this.malusCounters= {wrong:0,catched:0};
    this.win = false;

    this.init = function() {
        for(let i = 0;i<this.bullets;i++) {
            var scoreMark = {};
            scoreMark = monit.addDistractor(100+i*120,100,this.img.replace('.png','_bianco.png'),30);
            scoreMark.sizX = 100;
            scoreMark.sizY = 100;
            this.scoreMarks.push(scoreMark)
        }
    }

    this.increment = function() {
        if(!this.gameOver) {
            this.currentScore++;
            var index = this.scoreMarks.findIndex((el)=>{return el.img == this.img.replace('.png','_bianco.png')}); 
            this.scoreMarks[index].img = this.img.replace('.png','_verde.png');
            this.checkGameOver();
        }
    }

    this.decrement = function(type) { //type is "wrong" or "catched"
        if(!disableMalus && !this.gameOver) {
            this.malusCounters[type]++;
            this.bullets--;
            // //to remove the pillow
            // var ds = this.scoreMarks.pop();
            //to make the pillow red
            var ds = this.scoreMarks[this.bullets];
            var obj = {};
            Object.assign(obj, animations['selectedBis'].obj);
            obj.timestamp = getCurrTM();
            obj.color = {r:230,g:41,b:0};
            obj.end = 800;
            Object.assign(obj.item,ds);
            animationsStack.push(obj);
            // //to remove the pillow
            // monit.removeDistractor(ds);
            //to make the pillow red
            this.scoreMarks[this.bullets].img = this.img.replace('.png','_rosso.png');
            this.checkGameOver();
        }
    }

    this.checkGameOver = function() {
        if(this.bullets < this.scoreToWin) {
            this.endGame(false);
        } else if (this.currentScore >= this.scoreToWin) {
            this.endGame(true);
        }
    }

    this.endGame = function(win) {
        this.gameOver = true;
        this.win = win;
        pauseAllTimers();
        currentScene.scoredTime = (getCurrTM() - currentScene.startTime)/1000;
        doCheckCollisions = false;
        setTimeout(()=>{
            currentState=ENDSCENE//win ? ENDGAME : GAMEOVER;
        },2000)
    }

    this.init();
} 


