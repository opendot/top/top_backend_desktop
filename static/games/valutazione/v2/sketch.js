const states = [
    fixationCenter,
    fixationLeft,
    fixationRight,
    fixationTop,
    fixationBottom,
    callAttention,
    pursuitRight,
    pursuitLeft,
    callAttention,
    pursuitTop,
    pursuitBottom,
    callAttention,
    saccadeLeft,
    saccadeRight,
    saccadeTop,
    saccadeBottom,
    callAttention,
    complexSaccadeLeft,
    complexSaccadeRight,
    complexSaccadeTop,
    complexSaccadeBottom,
    callAttention,
    drawFirstImage,
    drawSecondImage,
    drawFirstVideo,
    callAttention,
    drawFirstVideo,
]

let curr = 0;
let aniParam, aniStartTime, aniEndTime, aniStartVal, aniEndVal, aniCb;
let aniBusy = false;
let dim = {x: 10, y: 10};
let tgt, tgt2;
let attGrow = true;
let attMin = 100;
let attMax = 200;
let timeout, timeout2;
let timeunit = 5000;
let vid;
let playVid = false;
let fishMin = 250;
let fishMax = 300;
let fishGrow = false;
let opacity = 255;
let startcomplex = false;

function preload() {
    monit.loadJsonItems()
}

function setup() {
    monit.setProcBg("#000000")
    createCanvas(windowWidth, windowHeight);
    noStroke();
    vid = createVideo("img/peppa.mp4");
    vid.hide();
    vid.onended(()=>{
        vid.hide();
        vid.time(0);
        playVid = false;
        curr++});
    frameRate(30);

    monit.onAddTarget = (x,y,type) => {
        console.log("add taget")
    };
    monit.onRemoveTarget = (id) => {
        console.log("remove taget")
    };
    monit.onMoveTarget = (ob) => {
        console.log("move taget")
    };
    monit.onToggleTracker = (val) => {
        if(val == 1) this.trackerActive = true;
        else this.trackerActive = false;
    };
    monit.onAttentionSig= () => {
        monit.playSound('bell.wav');
    };
}

function draw() {

    if(monit.loaded){
        monit.drawPrBg();
        if (curr < states.length) {
            states[curr]()
            // console.log(monit.pState.targets);
        }
        if(curr <17 || curr > 20) monit.drawTargets()
    }
}


function fixationCenter() {
    if (!tgt) tgt = monit.addTarget(window.innerWidth / 2, window.innerHeight / 2, "pescedx.png")
    if (!timeout) timeout = setTimeout(function () {
        timeout = null;
        curr++;
        monit.clearTargets();
        tgt = null;
        fishGrow = false;
    }, timeunit * 2)
    growFish();
}

function fixationLeft() {
    if (!tgt) tgt = monit.addTarget(window.innerWidth / 2, window.innerHeight / 2, "pescesx.png")
    tgt.posX = window.innerWidth * 0.1;
    if (!timeout) timeout = setTimeout(function () {
        timeout = null;
        curr++;
        monit.clearTargets();
        tgt = null;
        fishGrow = false;
    }, timeunit * 2)
    growFish();
}

function fixationRight() {
    if (!tgt) tgt = monit.addTarget(window.innerWidth / 2, window.innerHeight / 2, "pescedx.png")
    tgt.posX = window.innerWidth * 0.9;
    if (!timeout) timeout = setTimeout(function () {
        timeout = null;
        curr++;
        monit.clearTargets();
        tgt = null;
        fishGrow = false;
    }, timeunit * 2)
    growFish();
}

function fixationTop() {
    if (!tgt) tgt = monit.addTarget(window.innerWidth / 2, window.innerHeight / 2, "pescesx.png")
    tgt.posX = window.innerWidth / 2;
    tgt.posY = window.innerHeight * 0.1;

    if (!timeout) timeout = setTimeout(function () {
        timeout = null;
        curr++;
        monit.clearTargets();
        tgt = null;
        fishGrow = false;
    }, timeunit * 2)
    growFish();
}

function fixationBottom() {
    if (!tgt) tgt = monit.addTarget(window.innerWidth / 2, window.innerHeight / 2, "pescedx.png")
    tgt.posY = window.innerHeight * 0.9;

    if (!timeout) timeout = setTimeout(function () {
        timeout = null;
        monit.clearTargets();
        tgt = null;
        fishGrow = false;
        curr++;
    }, timeunit * 2)
    growFish();
}

function callAttention() {
    tint(255,255)
    if (!tgt) tgt = monit.addTarget(window.innerWidth / 2, window.innerHeight / 2, "ellipse", "#ffffff", attMin, attMin);
    if (attGrow) {
        tgt.sizX += 5;
        tgt.sizY += 5;
        if (tgt.sizX >= attMax) attGrow = false;
    } else {
        tgt.sizX -= 5;
        tgt.sizY -= 5;
        if (tgt.sizX <= attMin) attGrow = true;
    }

    if (!timeout) timeout = setTimeout(function () {
        timeout = null;
        tgt = null;
        monit.clearTargets();
        if(curr == 11 || curr == 16) {tint(255,0); opacity = 0}
        curr++;
    }, timeunit)
}

function pursuitRight() {
    if (!tgt) tgt = monit.addTarget(window.innerWidth / 2, window.innerHeight / 2, "pescedx.png")
    tgt.posX = window.innerWidth / 2;
    tgt.posY = window.innerHeight / 2;
    animateVariable(tgt.posX, timeunit * 3, window.innerWidth / 2, window.innerWidth - tgt.sizX / 2,
        function () {
            tgt.img = "pescesx.png";
            animateVariable(tgt.posX, timeunit * 3, window.innerWidth - tgt.sizX / 2, window.innerWidth / 2,
                function () {
                    curr++;
                })
        })
    tgt.posX = aniParam;
}

function pursuitLeft() {
    if (!tgt) tgt = monit.addTarget(window.innerWidth / 2, window.innerHeight / 2, "pescesx.png")
    animateVariable(tgt.posX, timeunit * 3, window.innerWidth / 2, tgt.sizX / 2,
        function () {
            tgt.img = "pescedx.png";
            animateVariable(tgt.posX, timeunit * 3, tgt.sizX / 2, window.innerWidth / 2,
                function () {
                    tgt = null;
                    monit.clearTargets();
                    curr++;
                })
        })
    tgt.posX = aniParam;
}

function pursuitTop() {
    if (!tgt) tgt = monit.addTarget(window.innerWidth / 2, window.innerHeight / 2, "pescesx.png")
    animateVariable(tgt.posY, timeunit * 3, window.innerHeight / 2, tgt.sizY / 2,
        function () {
            //tgt.img = "pescedx.png";
            animateVariable(tgt.posY, timeunit * 3, tgt.sizY / 2, window.innerHeight / 2,
                function () {
                    curr++;
                })
        })
    tgt.posY = aniParam;
}

function pursuitBottom() {
    if (!tgt) tgt = monit.addTarget(window.innerWidth / 2, window.innerHeight / 2, "pescesx.png")
    animateVariable(tgt.posY, timeunit * 3, window.innerHeight / 2, window.innerHeight - tgt.sizY / 2,
        function () {
            //tgt.img = "pescedx.png";
            animateVariable(tgt.posY, timeunit * 3, window.innerHeight - tgt.sizY / 2, window.innerHeight / 2,
                function () {
                    tgt = null;
                    monit.clearTargets();
                    curr++;
                })
        })
    tgt.posY = aniParam;
}

function saccadeLeft() {
    if(opacity < 255) {opacity+=40; tint(255,opacity)}
    if (!tgt) tgt = monit.addTarget(window.innerWidth / 2, window.innerHeight / 2, "pescesx.png")
    tgt.posX = window.innerWidth * 0.1;
    if (!timeout) timeout = setTimeout(function () {
        timeout = null;
        tint(255,0)
        opacity = 0;
        curr++;
    }, timeunit)

}

function saccadeRight() {
    //if(!tgt) tgt = monit.addTarget(window.innerWidth/2, window.innerHeight/2, "pescesx.png")
    if(opacity < 255) {opacity+=40; tint(255,opacity)}
    tgt.img = "pescedx.png"
    tgt.posX = window.innerWidth * 0.9;
    if (!timeout) timeout = setTimeout(function () {
        timeout = null;
        curr++;
        tint(255,0)
        opacity = 0;
    }, timeunit)
}

function saccadeTop() {
    if(opacity < 255) {opacity+=40; tint(255,opacity)}
    tgt.img = "pescesx.png"
    tgt.posX = window.innerWidth / 2;
    tgt.posY = window.innerHeight * 0.1;
    if (!timeout) timeout = setTimeout(function () {
        timeout = null;
        curr++;
        tint(255,0)
        opacity = 0;
    }, timeunit)
}

function saccadeBottom() {
    if(opacity < 255) {opacity+=40; tint(255,opacity)}
    tgt.posX = window.innerWidth / 2;
    tgt.posY = window.innerHeight * 0.9;
    if (!timeout) timeout = setTimeout(function () {
        timeout = null;
        tgt = null;
        tgt2 = null;
        monit.clearTargets();
        curr++
    }, timeunit)
}

function complexSaccadeLeft() {
    if (!tgt2) tgt2 = monit.addTarget(window.innerWidth / 2, window.innerHeight / 2, "pescesx.png");
    push()
    tint(255,255)
    monit.drawItems([tgt2])
    pop()


    if(!timeout2) {
        timeout2 = setTimeout(() => {
            startcomplex = true
        }, 2000)
    }

    if(startcomplex){
        saccadeLeft()
        monit.drawItems([tgt])
    }
}

function complexSaccadeRight() {
    saccadeRight()
    push()
    tint(255,255)
    monit.drawItems([tgt2])
    pop()
    saccadeRight()
    monit.drawItems([tgt])
}

function complexSaccadeTop() {
    saccadeTop()
    push()
    tint(255,255)
    monit.drawItems([tgt2])
    pop()
    saccadeTop()
    monit.drawItems([tgt])
}

function complexSaccadeBottom() {
    saccadeBottom()
    push()
    tint(255,255)
    monit.drawItems([tgt2])
    pop()
    saccadeBottom()
    monit.drawItems([tgt])
}

function drawFirstImage() {
    drawStaticImage("pimpa1.png")
    if (!timeout) timeout = setTimeout(function () {
        timeout = null;
        tgt = null;
        monit.clearTargets();
        curr++;
    }, timeunit*2)
}

function drawSecondImage() {
    drawStaticImage("pimpa2.jpg")
    if (!timeout) timeout = setTimeout(function () {
        timeout = null;
        tgt = null;
        monit.clearTargets();
        curr++;
    }, timeunit*2)
}

function drawFirstVideo() {
    if(!playVid){
        vid.show();
        vid.speed(1);
        vid.play();
        playVid = true;
        monit.eventconnection.send(JSON.stringify({ts:new Date().getTime(), type: "event", event_type: "play_video", data: "peppa.mp4"}));
    }
}

function drawStaticImage(img) {
    if(!tgt) tgt = monit.addTarget(window.innerWidth/2, window.innerHeight/2, img)
}

function drawVideo(vid) {
}

function animateVariable(param, tgtTime, startVal, endVal, cb) {
    if (!aniBusy) {
        aniBusy = true
        aniParam = param
        aniStartTime = millis()
        aniEndTime = millis() + tgtTime
        aniStartVal = startVal
        aniEndVal = endVal
        aniCb = cb
        execAnimation()
    }
}

function execAnimation() {
    if (millis() < aniEndTime) {
        let curTime = norm(millis(), aniStartTime, aniEndTime)
        aniParam = lerp(aniStartVal, aniEndVal, curTime)
        setTimeout(execAnimation)
    } else {
        aniBusy = false
        aniCb()
    }
}

function growFish(){

    if (fishGrow) {
        tgt.sizX += 4;
        tgt.sizY += 3;
        if (tgt.sizX >= fishMax) fishGrow = false;
    } else {
        tgt.sizX -= 4;
        tgt.sizY -= 3;
        if (tgt.sizX <= fishMin) fishGrow = true;
    }
}