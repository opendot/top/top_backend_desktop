//state machine
const stateMachine = [
    init,    
    startScene,
    cutScene,
    play,     
    endScene,
    endGame,
    showHint,
    coolDown
];
var currentState = 0;

const STARTSCENE = 1;
const CUTSCENE = 2;
const PLAY = 3;
const ENDSCENE = 4;
const ENDGAME = 5;
const SHOWHINT = 6;
const COOLDOWN = 7;

const coolDownSM = [
    setupCD,
    transitionCD,
    playCD,
    idleCD,
    exitCD
];
var currCoolDownState = 0;

// const playSMCD = [
//     drawingCD,
//     endSceneCD,
//     endGameCD
// ];
// var currPlayStateCD = 0;

var coolDownGrid = {};
var wImg = null;
var wImgs = [];
var br = {};
var shirt = {};
var pen = {};
var filters = {};


//play state machine
const playSM = [
    chooseCell,        
    guessCell,  
    correctCell,        
    wrongCell,    
    moveForward,
    finalZoomout     
];
var currPlayState = 0;

var campanaJson = {};
var gazedTarget = {};
var currentBlock = 0;
var currentPath = {};
var introPath = {};
var gazeX = 0;
var gazeY = 0;
var unveilTime = 4000;
var gazeTime = 0;
var trackerActive = false;
var okGo = false;
var currentPathID = 0;
var isHurra = false;
var isHint = 0;
var firstShowPath = false;
var isWaitForUnveil = 0;
var hintDistractorID = "";
var isTap = false;

// animations
var animations = [];
var animationsStack = [];
var animationsToSplice = [];
// movements
var movements = [];
var movementsStack = [];
var myFrameRate = 30;
var character = {};
var myCounterId = 0;

var timers = [];
var unveiling = false;
var charName = "char.png";

var canvasAnimations = {
    transX:0,
    transY:0,
    scaleX:1,
    scaleY:1,
    counter:0,
    pathLength:0,
    scaleFactor:1,
    transAmount:0
};

//temp
var buttons = {
    back:false,
    next:false,
    cooldown: false
}

animations['selected'] = {
    cb: function(el){

        el.prog = map(el.index,0,el.end,0,1);

        var x = (1+sin(PI*el.prog-PI/2))/2;

        push();
        rectMode(CENTER);
        strokeWeight(el.dimFactor*x);
        strokeCap(ROUND);
        strokeJoin(ROUND);
        stroke(el.color.r,el.color.g,el.color.b,map(x,0,1,255,0));
        noFill();
        rect(el.item.posX,el.item.posY,el.item.sizX+el.dimFactor*x,el.item.sizY+el.dimFactor*x);
        pop();
        el.index += 1000/frameRate();
        
    },
    obj:{
        type: 'selected',
        timestamp: getCurrTM(),
        item:{},
        color:{},
        index: 0,
        end: 1000,
        dimFactor: 50,
        prog:0
    }
}

animations['selectedBis'] = {
    cb: function(el){

        el.prog = map(el.index,0,el.end,0,1);

        var x = ((1+sin(2*PI*el.prog-PI/2))/2+(1+sin(4*PI*el.prog-PI/2))/2)/(PI/2);

        push();
        rectMode(CENTER);
        strokeWeight(el.dimFactor*x);
        strokeCap(ROUND);
        strokeJoin(ROUND);
        stroke(el.color.r,el.color.g,el.color.b,map(x,0,1,0,180));
        noFill();
        rect(el.item.posX,el.item.posY,el.item.sizX+el.dimFactor*x,el.item.sizY+el.dimFactor*x);
        pop();
        el.index += 1000/frameRate();
        
    },
    obj:{
        type: 'selectedBis',
        timestamp: getCurrTM(),
        item:{},
        color:{},
        index: 0,
        end: 800,
        dimFactor: 50,
        prog:0
    }
}

animations['unveil'] = {
    cb: function(el){
    
        var alpha;
            
        el.isUnveil ? alpha = floor(map(el.index,0,el.end,el.opacity,0)) : alpha = floor(map(el.index,0,el.end,0,el.opacity));

        el.item.veil.col = "#" + el.item.veil.col.substr(1,6) + hex(alpha,2);

        el.index += 1000/frameRate();
        
    },
    obj:{
        type: 'unveil',
        timestamp: getCurrTM(),
        item:{},
        isUnveil:1,
        opacity:255,
        index: 0,
        end: 1000
    }
}


function preload() {

    // monit.loadJsonItems();
    // loadMyJson("./campana.json");
    // wImg = loadImage("./img/bianco.png");
    // br = loadImage("./img/brush.png");

}

function setup() {

    monit.loadJsonItems();
    loadMyJson("./campana.json");
    // wImg = loadImage("./img/bianco.png");
    br = loadImage("./img/brush.png");
    
    loadTabletSettings();

    createCanvas(windowWidth,windowHeight);
    noStroke();
    frameRate(myFrameRate);

    monit.setBg('#b9cce1');

}

//do nothing until the flag monit.started is false
function checkFlag() {

    if(!monit.started || !monit.loaded) {
       setTimeout(checkFlag, 100); /* this checks the flag every 100 milliseconds*/   
    }
}

function draw() {

    // var tempone = getCurrTM();

    if(!monit.started || !monit.loaded) {
        setTimeout(checkFlag, 100); /* this checks the flag every 100 milliseconds*/ 
        background('black');
        textSize(30);
        stroke('white');
        fill('white');
        textAlign(CENTER, CENTER);
        text('Caricamento in corso...', windowWidth / 2, windowHeight / 2);
        textSize(12);
    } else {
        
        // console.log("stato: ",currentState);  	
        //draw the background
        monit.drawBg();
        if(campanaJson && campanaJson.bg) campanaJson.bg.run();

        if(!unveiling && !movementsStack.length && campanaJson.cooldown.isCoolDown) {
            setButtonsState({next:false,back:false});
            campanaJson.cooldown.isCoolDown = false;
            currentState = COOLDOWN;
        }

        if (currentState < stateMachine.length) {
            stateMachine[currentState]();
        }
        // console.log(stateMachine[currentState].name);
        // real fps on top left corner
        // fill(255); //black text
        // text(frameRate(), 50, 50);
        // console.log("next: ",buttons.next,"back: ",buttons.back,"cooldown: ",buttons.cooldown);
    }
    // console.log("frame duration [ms]",getCurrTM() - tempone);

}

function gaze() {
    if (monit.trackerActive && (trackerActive || currentState == COOLDOWN)) {
        gazeX = monit.mode == 'eye' ? monit.gaze[0] * window.innerWidth : mouseX;
        gazeY = monit.mode == 'eye' ? monit.gaze[1] * window.innerHeight : mouseY;
    }
}

// to assess if the pointer is on a target and what to do if so
function collide(tgt) {

    var cells = currentPath.blocks[currentBlock].cells;
    // console.log("currentBlock",currentBlock);
    var nextCell = {};
    if(currentBlock < currentPath.blocks.length-1) nextCell = currentPath.blocks[currentBlock+1].cells[0];

    //the target var stores the cell corresponding to the gazed target
    var target = getCellByTgtID(tgt.id); // cells.find(cell=>{return cell.tgt.id == tgt.id});

    target.wasGazed = target.isGazed;

    if (gazeX > tgt.posX - tgt.sizX / 2 && 
        gazeX < tgt.posX + tgt.sizX / 2 && 
        gazeY > tgt.posY - tgt.sizY / 2 && 
        gazeY < tgt.posY + tgt.sizY / 2) {     
        
        target.isGazed = true;
    } else {
        target.isGazed = false;
    }
    
    if (target.isGazed && !target.wasGazed) { //first gaze
        // console.log("start gazing the target ",tgt.id);
        monit.sendEvent({ts: new Date().getTime(), type: "event", event_type: "start_fixation", data: tgt.id});
        // console.log("first gaze");
        gazeTime = 0;
        target.strokeColor = "#000000";
        if(!target.isForced) {
            
            target.tgtAnim = initMovement(tgt,{endSizX:nextCell.sizX*9/10,endSizY:nextCell.sizY*9/10,type:"sin",duration:250});
            target.veilAnim = initMovement(target.veil,{endSizX:nextCell.sizX*9/10,endSizY:nextCell.sizY*9/10,type:"sin",duration:250});
            
            target.tgtAnim.doCollide = true;
            target.veilAnim.doCollide = true;
        }
    } else if (target.isGazed && target.wasGazed) { //continue gazing
        // console.log("gazing");
        target.wasGazed = false;
        targetGazed(tgt);

    } else if (!target.isGazed && target.wasGazed) { //stop gazing
        // console.log("stop gazing the target ",tgt.id);
        monit.sendEvent({ts: new Date().getTime(), type: "event", event_type: "end_fixation", data: tgt.id});

        gazeTime = 0;
        target.strokeColor = "#000000";

        if(!target.isForced) {
            _.remove(movementsStack, d => d.id == target.tgtAnim.id && d.type == target.tgtAnim.type);
            _.remove(movementsStack, d => d.id == target.veilAnim.id && d.type == target.veilAnim.type);

            target.tgtAnim = initMovement(tgt,{endSizX:target.sizX,endSizY:target.sizY,type:"sin",duration:300});
            target.veilAnim = initMovement(target.veil,{endSizX:target.sizX,endSizY:target.sizY,type:"sin",duration:300});

            target.tgtAnim.doCollide = true;
            target.veilAnim.doCollide = true;
        }
    }
}

function getCurrTM(){

    return (new Date()).getTime();

}

//test purpose
function keyPressed() {

    if(keyCode == RIGHT_ARROW) {

        monit.onAttentionSig();
        // console.log(isHint);

    } else if(keyCode == LEFT_ARROW) {

        isTap = true;
        // hello();
    } else if(keyCode == UP_ARROW) {
        monit.onToggleTracker(monit.trackerActive?0:1);
        // var tgts = monit.getTargets();
        // monit.onRemoveTarget(tgts[0].id);
    } else if (key == "n" || key == "N") {
        monit.onBackNext('next');
    } else if (key == "b" || key == "B") {
        monit.onBackNext('back');
    } else if (key == "d" || key == "D") {
        monit.onToggleFatigue();
    }
}

function init() {
   
    // monit.init();
    overrideMonit();

    monit.animationRate = 3;

    if(!monit.fixationTime) monit.fixationTime = 1000; //3000


    // if (monit.GameLevel == null) monit.GameLevel = 1; //temp
    // monit.GameLevel = 2; //TEMP

    //update the state
    currentState++;
}

function startScene() {
    tempCell = 0;
    currentBlock = 0;
    currPlayState = 0;
    monit.clearDistractors();
    monit.clearTargets();
    animationsStack = [];
    movementsStack = [];
    firstShowPath = false;
    isHurra = false;
    okGo = false;
    canvasAnimations.transX = 0; 
    canvasAnimations.transY = 0;
    canvasAnimations.scaleX = 1; 
    canvasAnimations.scaleY = 1;
    campanaJson.pavements = [];

    timers.forEach((el)=>{
       clearTimeout(el); 
    });
    timers = []; 

    monit.setBg(campanaJson.levels[monit.GameLevel].paths[currentPathID].backgroundCol);
    campanaJson.bg = new Bg(myFrameRate);
    campanaJson.bg.rect.zIndex = -10;

    for(let i = 0; i < 3 ; i++) {
        for(let j = 0; j < 3 ; j++) {
            campanaJson.pavements.push(monit.addDistractor( -windowWidth/2+i*windowWidth,
                                                            -windowHeight/2+j*windowHeight,
                                                            campanaJson.levels[monit.GameLevel].paths[currentPathID].backgroundImg,
                                                            -50,
                                                            null,
                                                            windowWidth,
                                                            windowHeight) 
            )
        }
    }

    createGrid(monit.GameLevel,currentPathID);
    addTargets();

    shiftAll(windowWidth*7/8);
    createIntroGrid(campanaJson.levels.length-1,0);

    //add the character
    // monit.addDistractor(-100,windowHeight/2,"ellipse", [255,0,0], 50, 50);
    character = monit.addDistractor(-100,windowHeight/2,charName,100);
    // var distr = monit.getDistractors();

    initMovement(character,{endPosX:windowWidth/10,
                            endPosY:windowHeight/2,
                            duration:1500,
                            type:"sin"});

    if(wImg) {
        resetAlpha(wImg);
    }

    // monit.playSound("yuppi.mp3");
    
    setButtonsState({next:false,back:false,cooldown:false});
    currentState++;

}

var tempTempo = 0;
var introColors = ['#fe6d5a','#fdf461','#96eefe'];
var tempCell = 0;
function cutScene() {
    var oldPosX = character.posX;
    moveElements();
    animateElements();
    campanaJson.pavements.forEach((pavement)=>{
        drawOneItem(pavement);
    })
    drawEmptyGrid();
    monit.drawTargets();
    if (oldPosX < character.posX) {
        monit.animateDistractor(character,"-right-spostamento");
    } else if (oldPosX > character.posX) {
        monit.animateDistractor(character,"-left-spostamento");
    } else {
        isHurra ? monit.animateDistractor(character,"-hurra") : character.img = charName;
    }
    drawDistrButItems(campanaJson.pavements);

    if(!movementsStack.length && tempCell < 3) {
        if(tempCell>0) {
            introPath.blocks[tempCell-1].cells[0].veil.col = introColors[tempCell-1];
        }
        monit.playSound('boing.mp3',1);
        initMovement(character,{
            duration:1000,
            endPosX: introPath.blocks[tempCell].cells[0].veil.posX,
            endPosY: introPath.blocks[tempCell].cells[0].veil.posY,
            type:'jump'
        })
        tempCell++;
    } else if (!movementsStack.length && tempCell == 3) {
        if(tempCell>0) {
            introPath.blocks[tempCell-1].cells[0].veil.col = introColors[tempCell-1];
        }
        monit.playSound('boing.mp3',1);
        initMovement(character,{
            duration:1000,
            distPosX:windowWidth/4,
            type:'jump'
        })
        tempCell++;
    } else if (!movementsStack.length && tempCell == 4) {
        monit.getTargets().forEach((el,index)=>{
            initMovement(el,{distPosX:-windowWidth*7/8,duration: 2000,type: "sin"}).stopChar = true;
        });
        monit.getDistractors().forEach((el,index)=>{
            initMovement(el,{distPosX:-windowWidth*7/8,duration: 2000,type: "sin"}).stopChar = true;
        });
        currentState++;
        // setButtonsState({next:true,back:true,cooldown:true});
    }
}

function play() {
// console.log(playSM[currPlayState].name);
    // var distr = monit.getDistractors();
    if(!movementsStack.length && tempCell == 4) {
        introPath.blocks.forEach((block)=>{
            block.cells.forEach((cell)=>{
                monit.removeDistractor(cell.veil);
            });
        });
        okGo = true;
        trackerActive = true;
        setButtonsState({next:currentPathID<campanaJson.levels[monit.GameLevel].paths.length-1,back:currentPathID!=0,cooldown:true});
        tempCell = 0;
    } 
    var oldPosX = character.posX;
    moveElements();
    animateCanvas();

    campanaJson.pavements.forEach((pavement)=>{
        drawOneItem(pavement);
    })
    
    gaze();
    drawEmptyGrid();
    drawAllButItems(campanaJson.pavements.concat([character]));
    // monit.drawTargets();

    //play state machine
    playSM[currPlayState]();

    //check if the canvas is moving
    var canvasMov = movementsStack.filter((el)=>{return el.stopChar});
    if(!canvasMov.length) {
        if (oldPosX < character.posX) {
            monit.animateDistractor(character,"-right-spostamento");
        } else if (oldPosX > character.posX) {
            monit.animateDistractor(character,"-left-spostamento");
        } else {
            isHurra ? monit.animateDistractor(character,"-hurra") : character.img = charName;
        }
    }

    // drawDistrButItems(campanaJson.pavements.concat([character]));

    animateElements();
    drawOneItem(character);
}

function drawDistrButItems(items) {
    var distr = monit.getDistractors();
    items.forEach((elem)=>{
        if (monit.pState.distractors[elem.id]) distr.splice(distr.findIndex((el)=>{return el.id == elem.id}),1);
    });
    monit.drawItems(distr.sort(orderElementsCB));
}

function drawAllButItems(items) {
    var all = monit.getDistractors().concat(monit.getTargets());
    items.forEach((elem)=>{
        if (monit.pState.distractors[elem.id] || monit.pState.targets[elem.id]) all.splice(all.findIndex((el)=>{return el.id == elem.id}),1);
    });
    monit.drawItems(all.sort(orderElementsCB));
}

function drawOneItem(item) {

    if (monit.pState.distractors[item.id]) monit.drawItems([item]);

}


function chooseCell() {
    
    var currentCells = currentPath.blocks[currentBlock].cells;

    if (isHint) hint();

    if (currentBlock < currentPath.blocks.length-1 && currentCells.length > 1 && okGo) {
        unveilCards(currentBlock);
        unveiling = true;
        okGo = false;
    } else if (currentCells.length == 1 && okGo) {
        currentCells[0].tgt = monit.convertItem(currentCells[0].tgt.id,"target");
        okGo = false;
    }

    var sceneMovements = movementsStack.filter((el)=>{return !el.doCollide});

    if (!animationsStack.length && !sceneMovements.length && (!currentCells[0].veil || currentCells[0].veil.posX > windowWidth)) {
        // for each target in the current block check if the pointer is on it
        if(!buttons.cooldown) {
            setButtonsState({cooldown:true})};
        currentCells.forEach((cell)=>{collide(cell.tgt)});
    }
}

function centeringCheck() {
// console.log("centeringCheck()");

    var tgts = monit.getTargets();
    var distr = monit.getDistractors(); 

    var offset = 0;
    currentBlock == currentPath.blocks.length - 1 ? offset = 5/2 : offset = 7/2;
    var excessX = character.posX + offset*tgts[0].sizX - windowWidth; //assume that the targets have all the same size!!
    // console.log("eccesso",excessX,"= distr[0].posX",distr[0].posX,"3*tgts[0].sizX",3*tgts[0].sizX,"-","windowWidth",windowWidth);

    if (excessX > 0) {

        currentPath.allElemAnim = [];

        tgts.forEach((el,index)=>{

            initMovement(el,{endPosX:el.posX-(excessX),duration: 1000,type: "sin"}).stopChar = true;
            // currentPath.allElemAnim[el.id].stopChar = true;
            // console.log("target numero: ",index,"posX: ",elem.posX);
        });
        distr.filter(el=>el.zIndex!=-10).forEach((el,index)=>{

            initMovement(el,{endPosX:el.posX-(excessX),duration: 1000,type: "sin"}).stopChar = true;;
            // currentPath.allElemAnim[el.id].stopChar = true;
            // console.log("distrattore numero: ",index,"posX: ",elem.posX);
        });

        okGo = false;
        timers.push(setTimeout(()=>{okGo = true},1000));
    } else {
        okGo = true;
    }
}

function guessCell(){
    //disable eyetracker
    trackerActive = false;
    if(buttons.cooldown) setButtonsState({cooldown:false})
    var cells = currentPath.blocks[currentBlock].cells;
    var target = getCellByTgtID(gazedTarget.id); //cells.find(cell=>{return cell.tgt.id == gazedTarget.id});

    //wait for no animation present
    if (!animationsStack.length && !movementsStack.length) {
        //if the block has 1 cell start the sequence of showing the cards
        if (target.isForced) {

            //switch to correctCell()
            currPlayState++;

        } else {    //if the block has more than one cell check if the cell is right

            //go to correctCell() if right to wrongCell if wrong
            target.isRight ? currPlayState++ : currPlayState += 2;
        }
    }
}

function correctCell(){

    //animation of the stroke
    monit.playSound("magic.mp3");
    var obj = {};
    Object.assign(obj, animations['selected'].obj);
    obj.timestamp = getCurrTM();
    obj.color = {r:146,g:208,b:23};
    obj.item = gazedTarget;
    // obj.end = 1000;
    // obj.dimFactor = 3;
    animationsStack.push(obj);
    //change the stroke
    var corrCell = getCellByTgtID(gazedTarget.id); //currentPath.blocks[currentBlock].cells.find((el)=>{return el.tgt.id == gazedTarget.id});
    var corrCellIndex = currentPath.blocks[currentBlock].cells.findIndex((el)=>{return el.tgt.id == gazedTarget.id});
    corrCell.strokeColor = "#92d017";
    corrCell.strokeWeight = 10;

    var nextCell = {};
    if(!corrCell.isForced) nextCell = currentPath.blocks[currentBlock+1].cells[0];

    currentPath.blocks[currentBlock].cells.forEach((elem,index)=>{
        // console.log("cella: ",elem);

        if (elem.tgt.id != gazedTarget.id) { //the wrog cell/cells

            elem.veil.col = "#00000000";

            //bring the veil distractor back
            elem.veil.posX -= 2*windowWidth;

            var obj = {};
            Object.assign(obj, animations['unveil'].obj);
            obj.item = elem; 
            obj.timestamp = getCurrTM();
            obj.isUnveil = 0;
            obj.opacity = 100;
            obj.end = 500;
            animationsStack.push(obj);   
            var dist = 3;
            setTimeout(()=>{

                if(index < corrCellIndex) {//if the cell is above the correct one
                    initMovement(elem.tgt,{endPosY:elem.tgt.posY-dist*windowHeight,duration:1500,type:"sin"});
                    initMovement(elem.veil,{endPosY:elem.veil.posY-dist*windowHeight,duration:1500,type:"sin"});                    
                } else { //if the cell is below the correct one
                    initMovement(elem.tgt,{endPosY:elem.tgt.posY+dist*windowHeight,duration:1500,type:"sin"});
                    initMovement(elem.veil,{endPosY:elem.veil.posY+dist*windowHeight,duration:1500,type:"sin"});
                }

            },500);

        } else { //the correct cell

            if(!elem.isForced) {
                // console.log("currentBlock",currentBlock,"currentPath.blocks[currentBlock+1].cells.length",currentPath.blocks[currentBlock+1].cells.length)
                setTimeout(()=>{

                    initMovement(elem.tgt,{endPosY:nextCell.tgt.posY,duration:1000,type:"sin"});
                    initMovement(elem.veil,{endPosY:nextCell.tgt.posY,duration:1000,type:"sin"});
                    initMovement(character,{endPosY:nextCell.tgt.posY,duration:1000,type:"sin"});

                },500);
            }
        }
    });

    isHurra = true;
    setTimeout(()=>{isHurra = false},900);  

    //next block
    currentBlock++;
    currPlayState +=2;

}        
function wrongCell() {

    //animation of the stroke
    var obj = {};
    Object.assign(obj, animations['selected'].obj);
    obj.timestamp = getCurrTM();
    obj.color = {r:230,g:41,b:0};
    obj.item = gazedTarget;
    animationsStack.push(obj);

    var tgt = currentPath.blocks[currentBlock-1].cells[0].tgt;

    initMovement(character,{endPosX:tgt.posX - tgt.sizX/2,
                            endPosY:tgt.posY,
                            duration:1000,
                            type:"jump"});

    monit.playSound("boing.mp3");
    monit.playSound("wrong.mp3");

    // currentPath.blocks[currentBlock].cells.find((el)=>{return el.tgt.id == gazedTarget.id}).strokeColor = "#000000";
    getCellByTgtID(gazedTarget.id).strokeColor = "#000000";
    //go to moveForward()
    currPlayState++;
}

function moveForward(){

    if(isWaitForUnveil) {
        // monit.playSound("tactac.mp3");
    }
    //wait for no animation present 
    if (!animationsStack.length && !movementsStack.length) {
        
        //if it wasn't the last block of the path
        if (currentBlock < currentPath.blocks.length) {
            
            var gazedCell = getCellByTgtID(gazedTarget.id);
            if (gazedCell.isRight) {
                centeringCheck();
            }
            trackerActive = true;

            var currBlock = currentPath.blocks[currentBlock];
            currPlayState = 0;

        } else {
            firstShowPath = true;
            currentState++;
        }
    }
}

function finalZoomout() {

    var distr = monit.getDistractors();
    isHurra = true;
    var zoomoutDuration = 2000;

    if(firstShowPath) {
        
        monit.playSound("applause.mp3");
        monit.playSound("yuppi.mp3");
        canvasAnimations.pathLength = currentPath.blocks.length * currentPath.blocks[0].cells[0].sizX;
        canvasAnimations.scaleFactor = 1;
        if (canvasAnimations.pathLength > windowWidth) canvasAnimations.scaleFactor = windowWidth/(1.1*canvasAnimations.pathLength);
        canvasAnimations.transAmount = canvasAnimations.scaleFactor*(windowWidth/2 - (character.posX + currentPath.blocks[0].cells[0].sizX -canvasAnimations.pathLength/2));
        canvasAnimations.counter = 0;

        setFinalTimer();

        isTap = false;
        firstShowPath = false;
    }

    if(isTap) {
        //if it was the last block of the path and there are more paths in the level
        isTap = false;
        clearTimeout(campanaJson.skipTimeout);
        // monit.silence();

        if (currentBlock >= currentPath.blocks.length && currentPathID < campanaJson.levels[monit.GameLevel].paths.length -1) { 
            // console.log("era l'ultimo blocco del path")
            currentPathID++;
            // currentState = 4; //end scene
            currentState = STARTSCENE; //start scene

        } else {//if it was the last block of the path and it was the last path of the level
            // console.log("era l'ultimo blocco del livello!!")           
            currentPathID = 0;
            currentState = ENDGAME; //end game
        }
    }

    if (canvasAnimations.counter < zoomoutDuration) {

        canvasAnimations.counter += 1000/frameRate();

        canvasAnimations.transX = map(canvasAnimations.counter,0,zoomoutDuration,0,canvasAnimations.transAmount); 
        canvasAnimations.scaleX = map(canvasAnimations.counter,0,zoomoutDuration,1,canvasAnimations.scaleFactor); 
        canvasAnimations.scaleY = map(canvasAnimations.counter,0,zoomoutDuration,1,canvasAnimations.scaleFactor);   

    }
}

function shiftAll(distance) {
    monit.getTargets().concat(monit.getDistractors().filter(el=>el.zIndex!=-10)).forEach((item)=>{
        item.posX += distance;
    })
}

function setFinalTimer() {
    //if it was the last block of the path and there are more paths in the level
    campanaJson.skipTimeout = setTimeout(()=>{
        // if (currentBlock >= currentPath.blocks.length && currentPathID < campanaJson.levels[monit.GameLevel].paths.length -1) { 
        if (currentPathID < campanaJson.levels[monit.GameLevel].paths.length -1) { 
            currentPathID++;
            // currentState = 4; //end scene
            currentState = STARTSCENE; //start scene

        } else {//if it was the last block of the path and it was the last path of the level
            // currentPathID = 0;
            // currentState = ENDGAME; //end game
            // setButtonsState({next:false,back:false,cooldown:false});
        }
    },10000);
}

function animateCanvas() {

    translate(canvasAnimations.transX,canvasAnimations.transY);
    translate(windowWidth/2,windowHeight/2);
    scale(canvasAnimations.scaleX,canvasAnimations.scaleX);
    translate(-windowWidth/2,-windowHeight/2);

}
//block passed as argument is actually the next block
function unveilCards(block) {
    // console.log("unveilCards");
    isWaitForUnveil = 1;
    var obj = {};
    Object.assign(obj, animations['unveil'].obj);
    obj.timestamp = getCurrTM();
    obj.item = currentPath.blocks[block+1].cells[0]; //block +1 is 2 blocks far
    animationsStack.push(obj);

    //at the end of the animation remove the distractor
    var cell = currentPath.blocks[block+1].cells[0];
    timers.push(setTimeout(()=>{monit.removeDistractorById(cell.veil.id)},obj.end));
    timers.push(setTimeout(()=>{cell.veil.posX += 2*windowWidth},obj.end));
    currentPath.blocks[block].cells.forEach((el,index,array)=>{

        // console.log("imposto il timeout per la cella ",index," del bloccco", block)
        timers.push(setTimeout(()=>{
            monit.playSound("ding.mp3");
            isWaitForUnveil = 0;
            var obj = {};
            Object.assign(obj, animations['unveil'].obj);
            obj.timestamp = getCurrTM();
            obj.isUnveil = 1;
            obj.item = el; 
            animationsStack.push(obj);

            //at the end of the animation hide the distractors
            timers.push(setTimeout(()=>{el.veil.posX += 2*windowWidth},obj.end));
            el.tgt = monit.convertItem(el.tgt.id,"target");
            if(index == array.length - 1) unveiling = false;

        },unveilTime+index*200));
    });

}
function endScene() {

    currPlayState = 5;
    stateMachine[PLAY]();
}

function endGame() {

    background('black');
    textSize(30);
    stroke('white');
    fill('white')
    textAlign(CENTER,CENTER);
    text('EVVIVA!', windowWidth/2, windowHeight/2);

    // tempTempo+=1000/myFrameRate;
    // if(tempTempo>3000) {
    //     currentState = COOLDOWN;
    //     tempTempo = 0;
    // }

    // noStroke();
    // textSize(20);
    // text('premere NEXT per passare alla modalità defaticamento',windowWidth/2, windowHeight/2+100);
}
function showHint() {

    monit.setBg('#000000');

    var block = currentPath.blocks[currentBlock];

    block.animCycles += myAnimateDistractor(monit.pState.distractors[hintDistractorID],{framesAmount:6,
                                                                                        duration:block.duration,
                                                                                        loopType:block.loopType,
                                                                                        sound:block.hintSound});
    if(block.animCycles>3) isHint = 0;
    // console.log(block.animCycles);

    monit.drawItems([monit.pState.distractors[hintDistractorID]]);

    if(!isHint) {

        monit.silence();
        //draw the elements on the tablet
        monit.getDistractors().forEach((el)=>{el.hidden = false});
        currentPath.blocks.forEach((block)=>{
            block.cells.forEach((cell)=>{
                if (cell.veil) cell.veil.hidden = false;
            });
        });
        character.hidden = false;

        monit.setBg('#b9cce1');
        monit.removeDistractorById(hintDistractorID);
        currentState = PLAY;
    }
}

function createGrid(level,path) {


    var maxNumOfCells = 0;
    var startX = windowWidth/3;
    var startY = 0; //updated later
    var cellSpace = 0; //updated later

    currentPath = campanaJson.levels[level].paths[path];

    var blockHeight = currentPath.height;

    //check what is the maximum number of cells in all the blocks of the current path
    currentPath.blocks.forEach((block,index)=>{
        if (block.cells.length > maxNumOfCells) maxNumOfCells = block.cells.length;
    });
    //compute the dimension of each cell
    cellSpace = floor(blockHeight/maxNumOfCells);

    //cycle each block
    currentPath.blocks.forEach((block,index)=>{

        //for each block compute the starting coordinate on Y axis
        if (block.cells.length%2) { //odd number of cells
            startY = windowHeight/2 - floor(block.cells.length/2)*cellSpace;
        } else { //even number of cells
            startY = windowHeight/2 - (block.cells.length/2)*cellSpace + cellSpace/2;
        }

        //cycle each cell
        block.cells.forEach((cell,i)=>{        

            cell.posX = startX + cellSpace*index;
            cell.posY = startY + cellSpace*i;
            
            if (block.cells.length == 1) {
                cell.sizX = cellSpace-6; //smaller for the stroke
                cell.sizY = cellSpace-6; //smaller for the stroke
            } else {
                
                cell.sizX = cellSpace*2/3-6; //smaller for the stroke
                cell.sizY = cellSpace*2/3-6; //smaller for the stroke
            }

            
            cell.isStroke = 1;
            cell.strokeWeight = 3;
            cell.strokeColor = "#000000";

            if(index) cell.veil = monit.addDistractor(cell.posX,cell.posY,"rect",10,"#ffffff",cell.sizX-cell.strokeWeight,cell.sizY-cell.strokeWeight);
        })        
    });
}
function createIntroGrid(level,path) {
    var maxNumOfCells = 0;
    var startX = windowWidth/4;
    var startY = 0; //updated later
    var cellSpace = 0; //updated later

    introPath = campanaJson.levels[level].paths[path];

    var blockHeight = introPath.height;

    //check what is the maximum number of cells in all the blocks of the current path
    introPath.blocks.forEach((block,index)=>{
        if (block.cells.length > maxNumOfCells) maxNumOfCells = block.cells.length;
    });
    //compute the dimension of each cell
    cellSpace = floor(blockHeight/maxNumOfCells);

    //cycle each block
    introPath.blocks.forEach((block,index)=>{

        //for each block compute the starting coordinate on Y axis
        if (block.cells.length%2) { //odd number of cells
            startY = windowHeight/2 - floor(block.cells.length/2)*cellSpace;
        } else { //even number of cells
            startY = windowHeight/2 - (block.cells.length/2)*cellSpace + cellSpace/2;
        }

        //cycle each cell
        block.cells.forEach((cell,i)=>{        

            cell.posX = startX + cellSpace*index;
            cell.posY = startY + cellSpace*i;
            
            if (block.cells.length == 1) {
                cell.sizX = cellSpace-6; //smaller for the stroke
                cell.sizY = cellSpace-6; //smaller for the stroke
            } else {
                
                cell.sizX = cellSpace*2/3-6; //smaller for the stroke
                cell.sizY = cellSpace*2/3-6; //smaller for the stroke
            }

            cell.isStroke = 1;
            cell.strokeWeight = 3;
            cell.strokeColor = "#000000";

            cell.veil = monit.addDistractor(cell.posX,cell.posY,"rect",10,"#ffffff",cell.sizX-cell.strokeWeight,cell.sizY-cell.strokeWeight);
        })        
    });
}

function drawEmptyGrid() {

    currentPath.blocks.forEach((block)=>{
        block.cells.forEach((cell)=>{
            // console.log(cell);
            // var tgt = monit.pState.distractors[cell.tgt.id];
            push();
            strokeWeight(cell.strokeWeight);
            cell.isStroke ? stroke(cell.strokeColor) : noStroke();
            noFill();
            rectMode(CENTER);
            rect(cell.tgt.posX,cell.tgt.posY,cell.tgt.sizX,cell.tgt.sizY);  
            pop();
        });
    });

    if (currentState < PLAY) {
        introPath.blocks.forEach((block)=>{
            block.cells.forEach((cell)=>{
                // console.log(cell);
                // var tgt = monit.pState.distractors[cell.tgt.id];
                push();
                strokeWeight(cell.strokeWeight);
                cell.isStroke ? stroke(cell.strokeColor) : noStroke();
                noFill();
                rectMode(CENTER);
                rect(cell.veil.posX,cell.veil.posY,cell.veil.sizX,cell.veil.sizY);  
                pop();
            });
        });
    }
}

function addTargets() {

    currentPath.blocks.forEach((block,index)=>{
        
        var isForced = 0;
        // if the block has only one cell the passage is forced
        if (block.cells.length == 1) isForced = 1;

        block.cells.forEach((cell,i)=>{

            // cell.tgt = index ? monit.addDistractor(cell.posX,cell.posY,cell.img) : monit.addTarget(cell.posX,cell.posY,cell.img);
            cell.tgt = monit.addDistractor(cell.posX,cell.posY,cell.img,5);

            //resize the target to the dimensions of the cell
            cell.tgt.sizX = cell.sizX;
            cell.tgt.sizY = cell.sizY;

            cell.isForced = isForced;
            cell.isGazed = false;
            cell.wasGazed = false;
        });
    });
}

function animateElements() {

    animationsStack.forEach((animObj, i)=>{
        // console.log("valuto l'animazione numero: ",i);
        if(getCurrTM() - animObj.timestamp > animObj.end){
          
            animationsToSplice.push(i);
        // console.log("tolgo l'animazione numero: ",i);
        } else {
            
            animations[animObj.type].cb(animObj);
        // console.log("eseguo la cb dell'animazione numero: ",i);

        }
    })
    animationsToSplice.forEach((i)=>{animationsStack.splice(i,1)});
    animationsToSplice = [];
}

function targetGazed(tgt) {

    // var distr = monit.getDistractors();

    gazeTime += 1000/frameRate();
    // console.log("tempo di fissazione: ",gazeTime);

    if (gazeTime > monit.fixationTime) {

        // console.log("target ",tgt.id," triggered");
        monit.sendEvent({ts: new Date().getTime(), type: "event", event_type: "reach_fixation_time", data: tgt.id});
        //selection animation
        gazeTime = 0;
        gazedTarget = tgt;
        
        initMovement(character,{endPosX:tgt.posX - tgt.sizX/2,
                                endPosY:tgt.posY,
                                duration:1000,
                                type: "jump"});

        monit.playSound("boing.mp3");
        currPlayState++;

    } else {
        push();
        // noStroke();
        stroke(200);
        fill(255,100)
        ellipse(tgt.posX,tgt.posY,100,100);
        fill(255,210);
        arc(tgt.posX,tgt.posY,100,100,-PI/2+map(gazeTime,50,monit.fixationTime,0,2*PI),3/2*PI,PIE); //
        strokeWeight(4);
        stroke(255);
        noFill();
        rectMode(CENTER);
        rect(tgt.posX,tgt.posY,tgt.sizX,tgt.sizY);
        pop();
        // currentPath.blocks[currentBlock].cells.find((el)=>{return el.tgt.id == tgt.id}).strokeColor = "#ffffff";
        getCellByTgtID(tgt.id).strokeColor = "#ffffff";
    }
}

function hint() {
// console.log("hint()");

    var tgts = monit.getTargets();
    // var distr = monit.getDistractors();

    //if the player has to choose among different cells
    if(currentPath.blocks[currentBlock].cells.length-1) {

        hintDistractorID = monit.addDistractor(windowWidth/2,windowHeight/2,currentPath.blocks[currentBlock].hintAnimation,50).id;

        //hide the elements on the tablet
        monit.getDistractors().forEach((el)=>{el.hidden = true});
        currentPath.blocks.forEach((block)=>{
            block.cells.forEach((cell)=>{
                if (cell.veil) cell.veil.hidden = true;
            });
        });
        character.hidden = true;
        currentPath.blocks[currentBlock].animCycles = 0;
        currentState = SHOWHINT;
        // isHint = 0;

    } else { //if the next cell is mandatory (one-cell block)

        if(!animationsStack.filter((el)=>{return el.type == 'selectedBis'}).length) {

            var nextTarget = tgts.filter((el)=>{return el.id == currentPath.blocks[currentBlock].cells[0].tgt.id});
            
            var obj = {};
            Object.assign(obj, animations['selectedBis'].obj);
            obj.timestamp = getCurrTM();
            obj.color = {r:146,g:208,b:23};//{r:255,g:255,b:255};
            obj.item = nextTarget[0];
            // obj.end = 1000;
            // obj.dimFactor = 5;
            animationsStack.push(obj); 
            isHint = 0;
        }
    }
}

function coolDown() {
    // console.log(coolDownSM[currCoolDownState].name);
    if (currCoolDownState < coolDownSM.length) {
        coolDownSM[currCoolDownState]();
    }
}

function setupCD() {
    // setButtonsState({next:true,back:false});
    timers.forEach((t)=>{
        clearInterval(t);
    });
    timers = [];

    if(campanaJson.cooldown.base) {
        monit.removeDistractor(campanaJson.cooldown.base);
        campanaJson.cooldown.base = null;
    }
    if(campanaJson.cooldown.shirt) {
        monit.removeDistractor(campanaJson.cooldown.shirt);
        campanaJson.cooldown.shirt = null;
    }
    if(campanaJson.cooldown.pen) {
        monit.removeDistractor(campanaJson.cooldown.pen);
        campanaJson.cooldown.pen = null;
    }

    var randomIndex = Math.floor(Math.random()*campanaJson.cooldown.images.length);

    campanaJson.cooldown.shirt = monit.addDistractor(   windowWidth/2,
                                                        campanaJson.cooldown.nextPressed ? windowHeight/2 : -windowHeight/2,
                                                        "maglietta.png",
                                                        30);
    campanaJson.cooldown.base = monit.addDistractor(windowWidth/2,
                                                    (campanaJson.cooldown.nextPressed ? (windowHeight/2) + 100 : -(windowHeight/2) + 100),
                                                    campanaJson.cooldown.images[randomIndex].base,
                                                    31);
    wImg = wImgs[randomIndex];
    campanaJson.cooldown.isEntering = true;

    if(!campanaJson.cooldown.nextPressed) {
        monit.getTargets().forEach((el,index)=>{
            initMovement(el,{distPosY:windowHeight,duration: 2000,type: "sin"});
        });
        monit.getDistractors().forEach((el,index)=>{
            initMovement(el,{distPosY:windowHeight,duration: 2000,type: "sin"});
        });
    }

    filters =  new movingAverageVect(5);
    drawAll();
    campanaJson.cooldown.nextPressed = false;
    currCoolDownState=1;
}

function drawAll() {
    moveElements();
    animateCanvas();
    campanaJson.pavements.forEach((pavement)=>{
        drawOneItem(pavement);
    })
    drawEmptyGrid();
    monit.drawTargets();
    // isHurra ? monit.animateDistractor(character,"-hurra") : character.img = charName;
    drawDistrButItems(campanaJson.pavements.concat([character]));
    animateElements();
    drawOneItem(character);

    if(wImg) {
        imageMode(CENTER);
        image(wImg,campanaJson.cooldown.base.posX,campanaJson.cooldown.base.posY,campanaJson.cooldown.base.sizX,campanaJson.cooldown.base.sizY);
    }
}

function transitionCD() {
    drawAll();
    if(!movementsStack.length) {
        if(campanaJson.cooldown.isEntering) {
            var obj = {};
            Object.assign(obj, animations['selectedBis'].obj);
            obj.timestamp = getCurrTM();
            obj.color = {r:146,g:208,b:23};
            obj.item = campanaJson.cooldown.base;
            animationsStack.push(obj);

            campanaJson.cooldown.pen = monit.addDistractor(gazeX,gazeY,"matita.png",35);
            wImg.posX = campanaJson.cooldown.base.posX - campanaJson.cooldown.base.sizX/2;
            wImg.posY = campanaJson.cooldown.base.posY - campanaJson.cooldown.base.sizY/2;
            br.resize(150,0);
            wImg.loadPixels();
            br.loadPixels();
            timers = [];
            timers.push(setInterval(function(){checkAlpha(wImg)},1000));
            setButtonsState({next:true,back:false,cooldown:true});

            currCoolDownState++;
        } else {
            setButtonsState({next:currentPathID < campanaJson.levels[monit.GameLevel].paths.length -1,back:true,cooldown:true});
            currentState = campanaJson.cooldown.currentState;
        }
    }
}

function playCD() {
    gaze();
    monit.drawDistractors();
    animateElements();
    var newPos = filters.getNextValue(createVector(gazeX,gazeY));

    campanaJson.cooldown.pen.posX = newPos.x;
    campanaJson.cooldown.pen.posY = newPos.y;

    alphaBrush(wImg,newPos.x,newPos.y,br);
    wImg.updatePixels();
    imageMode(CENTER);
    image(wImg,campanaJson.cooldown.base.posX,campanaJson.cooldown.base.posY,campanaJson.cooldown.base.sizX,campanaJson.cooldown.base.sizY);
}

function completedCD() {

    timers.forEach((t)=>{
        clearInterval(t);
    });
    timers = [];

    monit.playSound("magic.mp3");
    monit.playSound("applause.mp3");
    var obj = {};
    Object.assign(obj, animations['selected'].obj);
    obj.timestamp = getCurrTM();
    obj.color = {r:146,g:208,b:23};
    obj.item = campanaJson.cooldown.base;
    animationsStack.push(obj);
    monit.removeDistractorById(campanaJson.cooldown.pen.id);
    wImg = null;
}

function idleCD() {
    drawAll();
}

function exitCD() {

    timers.forEach((t)=>{
        clearInterval(t);
    });
    timers = [];
    monit.removeDistractorById(campanaJson.cooldown.pen.id);

    monit.getTargets().forEach((el,index)=>{
        initMovement(el,{distPosY:-windowHeight,duration: 2000,type: "sin"});
    });
    monit.getDistractors().forEach((el,index)=>{
        initMovement(el,{distPosY:-windowHeight,duration: 2000,type: "sin"});
    });

    drawAll();
    campanaJson.cooldown.isEntering = false;
    currCoolDownState = 1; //transitionCD
}


function writeAlpha(imm,x,y,alpha) {
    let index = (x + y * imm.width) * 4;
    if (alpha < imm.pixels[index + 3]) {
        imm.pixels[index + 3] = alpha;
    }
}

function alphaBrush(data,x,y,brush) {

    for (let i = 0;i<brush.width;i++) {
        for (let j = 0;j<brush.height;j++) {
            let index = (i + j * brush.width) *4;
            let xd = floor(x-data.posX-brush.width/2+i);
            let yd = floor(y-data.posY-brush.height/2+j);

            if(xd >= 0 && xd < data.width && yd >= 0 && yd < data.height) {
                writeAlpha(data, xd, yd ,brush.pixels[index]);  
            }
        }
    }
}

function checkAlpha(img) {

    // console.log("checking % of transparency");

    var trPixels = 0;

    for (let i = 0;i<img.width;i++) {
        for (let j = 0;j<img.height;j++) {
            let index = (i + j * img.width) * 4 + 3;
            if(!img.pixels[index]) trPixels++;
        }
    }

    var perc = trPixels*100/(img.width*img.height);
    // console.log("%: ",perc)

    if(perc > 40) {
        completedCD();
        currCoolDownState = 3;//currPlayStateCD++; 
    }
}

function resetAlpha(img) {
    for (let i = 0;i<img.width;i++) {
        for (let j = 0;j<img.height;j++) {
            let index = (i + j * img.width) * 4 + 3;
            img.pixels[index] = 255;
        }
    }
}

function loadMyJson(url) {

    fetch(url)
        .then(response => {
            return response.json()
        })
        .then(data => {
            let string = JSON.stringify(data, replacer);
            let nData = JSON.parse(string);     
            campanaJson = _.cloneDeep(nData);
            campanaJson.cooldown.images.forEach((im)=>{
                wImgs.push(loadImage("./img/"+im.overlay));
            });
        });
}

function replacer(key, val) {
    const keys = [
      "height"
    ];
    let rtn = val;
    if (keys.indexOf(key) >= 0) {
        rtn *= monit.screenScaleFactor; 
    }
    return rtn; 
}


// ===================================================================================

function overrideMonit() {

    monit.onRemoveTarget = (idT) => {
        
        var tgts = monit.getTargets();

        if(currentState == PLAY && currPlayState == 0) {
            
            var currentCells = currentPath.blocks[currentBlock].cells;
            
            currentCells.forEach((cell)=>{ 
                if(idT == cell.tgt.id) {

                    gazeTime = monit.fixationTime+1;

                    var tgt = tgts.filter((elem)=>{return elem.id == idT})[0];
                    // var nextCell = currentPath.blocks[currentBlock+1].cells[0];

                    targetGazed(tgt);  
                    
                    // if (currentPath.blocks[currentBlock].cells.length > 1) {
                        
                    //     var target = getCellByTgtID(tgt.id);

                    //     initMovement(tgt,{endSizX:nextCell.sizX*9/10,endSizY:nextCell.sizY*9/10,type:"sin",duration:250});
                    //     initMovement(target.veil,{endSizX:nextCell.sizX*9/10,endSizY:nextCell.sizY*9/10,type:"sin",duration:250});

                    //     target.isGazed = true;

                    // }

                    
                    // console.log("target toccato: ",[monit.pState.targets[idT]]);                  
                }

            });            
        } else if (currentState == COOLDOWN) {

        }
    }

    monit.onAttentionSig = () => {

        if(currPlayState == 0) {

            isHint ? isHint = 0 : isHint = 1;
            monit.playSound("richiamo.mp3");
        }
    } //I could use monit.attentionSig instead

    monit.onAddTarget = () => {

        if(currPlayState == 0 || currPlayState == 5) {
            isTap = true 
        }

    } //I could use monit.attentionSig instead

    monit.onMoveTarget = () => {} //I could use monit.attentionSig instead

    monit.onBackNext = function(button) {
        switch(button) {
            case "next":
                if(buttons.next) {
                    console.log("NEXT button pressed");
                    tempTempo = 0;
                    switch(currentState) {
                        case CUTSCENE: 
                            currentState = PLAY;
                            break;
                        case PLAY:
                            clearTimeout(campanaJson.skipTimeout);
                            if(currentPathID < campanaJson.levels[monit.GameLevel].paths.length-1) {
                                currentState = STARTSCENE;
                                currentPathID++;
                            }
                            break;
                        case ENDSCENE:
                            clearTimeout(campanaJson.skipTimeout);
                            if(currentPathID < campanaJson.levels[monit.GameLevel].paths.length-1) {
                                currentState = STARTSCENE;
                                currentPathID++;
                            }
                            break;
                        case COOLDOWN:
                            if(wImg != null) {
                                resetAlpha(wImg)
                            }
                            campanaJson.cooldown.nextPressed = true;
                            currCoolDownState = 0;
                            break;
                        default:
                            break;
                    }
                }
                break;
            case "back":
                if(buttons.back) {
                    console.log("back button pressed");
                    tempTempo = 0;
                    switch(currentState) {
                        case CUTSCENE: 
                            currentState = STARTSCENE;
                            if (currentPathID) currentPathID--;
                        break;
                        case PLAY:
                            if(currPlayState == 0 && character.posX<windowWidth/8) {
                                currentState = STARTSCENE;
                                if (currentPathID) currentPathID--;
                            } else {
                                clearTimeout(campanaJson.skipTimeout);
                                currentState = STARTSCENE;
                            }
                        break;
                        case ENDSCENE:
                            clearTimeout(campanaJson.skipTimeout);
                            currentState = STARTSCENE; //TO BE FIXED
                        break;
                        case COOLDOWN:
                            currCoolDownState = 0;
                            break;
                        default:
                            break;
                    }
                }
                break;
        }
    }
    monit.onToggleFatigue = function() {
        if(buttons.cooldown) {
            if (currentState == PLAY) { // || currentState == ENDSCENE) {
                currCoolDownState = 0;
                // currentState = COOLDOWN;
                clearTimeout(campanaJson.skipTimeout);
                campanaJson.cooldown.currentState = currentState;
                campanaJson.cooldown.isCoolDown = true;
                setButtonsState({next:false,back:false,cooldown:false});
            } else if (currentState == COOLDOWN) {
                // currentState = STARTSCENE;
                // if(campanaJson.cooldown.currentState == ENDSCENE) setFinalTimer();
                setButtonsState({next:false,back:false,cooldown:false});
                currCoolDownState = 4;
            }
        }
    }
    monit.onToggleTracker = function (val) {
        if(val == 1) this.trackerActive = true;
        else this.trackerActive = false;
        if(campanaJson && campanaJson.bg) {
            campanaJson.bg.fade();
        }
    }
}

function loadTabletSettings(){
    let args = (new URL(location.href)).searchParams;
    let round = args.get('round');
    if (round != null) {
        currentPathID = round;
    }
}

function orderElementsCB(a, b){
    if(a.zIndex < b.zIndex){
        return -1;
    }else if(a.zIndex > b.zIndex){
        return 1;
    }
}

function getCellByTgtID (id) {

    var corrCell = {}; 

    currentPath.blocks.forEach((block)=>{
        block.cells.forEach((cell)=>{
            if (cell.tgt.id == id) corrCell = cell; 
        });
    });

    return corrCell;
}

function setButtonsState(state) {
    if(state.next !== undefined) buttons.next = state.next;
    if(state.back !== undefined) buttons.back = state.back;
    if(state.cooldown !== undefined) buttons.cooldown = state.cooldown;
    monit.sendEvent({ts: new Date().getTime(), type: "event", event_type: "buttons_state", data:{cooldown:buttons.cooldown,next:buttons.next,back:buttons.back}});
}

function myAnimateDistractor(obj, animDescr) { //animDescr = {suff:"",framesAmount:7,duration:1000,loopType:"loop",sound:"suono.mp3"){
    var id = obj.id;
    var cycleComplete = 0;
    
    var framesAmount = animDescr.framesAmount === undefined ? monit.animationFramesAmount : animDescr.framesAmount;
    var duration = animDescr.duration === undefined ? 2000 : animDescr.duration;
    var loopType = animDescr.loopType === undefined ? "loop" : animDescr.loopType;
    var suff = animDescr.suff === undefined ? "" : animDescr.suff;

    var animationRate = round(myFrameRate*duration/(1000*framesAmount));;//round(frameRate()*duration/(1000*framesAmount));
    if(monit.counter % animationRate == 0 && monit.counter){
        if(monit.distractorsAnimIndx[id] === undefined){
            monit.distractorsAnimIndx[id] = {id: obj.id, inc: 1, val:1};
        }

        if(monit.distractorsAnimIndx[id].val == framesAmount) cycleComplete = 1;

        switch(loopType) {

            case "loop":
                if(monit.distractorsAnimIndx[id].val > framesAmount){
                    monit.distractorsAnimIndx[id].val = 1;
                    if(animDescr.sound) {
                        // monit.stopSound(animDescr.sound);
                        monit.sounds[animDescr.sound].pl.stop();
                        // monit.distractorsAnimIndx[id].inc = 0;
                    }
                }
                if(monit.distractorsAnimIndx[id].val == 1){
                    if(animDescr.sound) monit.playSound(animDescr.sound);
                }
                break;

            case "noLoopSound":
                if(monit.distractorsAnimIndx[id].val > framesAmount){
                    monit.distractorsAnimIndx[id].val = 1;
                    if(animDescr.sound) {
                        monit.stopSound(animDescr.sound);
                        monit.distractorsAnimIndx[id].noLoop = true;
                        // monit.sounds[animDescr.sound].pl.stop();
                        // monit.distractorsAnimIndx[id].inc = 0;
                    }
                }
                if(monit.distractorsAnimIndx[id].val == 1 && monit.distractorsAnimIndx[id].noLoop != true){
                    if(animDescr.sound) monit.playSound(animDescr.sound);
                }
                break;

            case "loopLast":
                if(monit.distractorsAnimIndx[id].val > framesAmount){
                    monit.distractorsAnimIndx[id].val -= 2;
                    if(animDescr.sound) {
                        monit.stopSound(animDescr.sound);
                        monit.distractorsAnimIndx[id].noLoop = true;
                        // monit.sounds[animDescr.sound].pl.stop();
                        // monit.distractorsAnimIndx[id].inc = 0;
                    }
                }
                if(monit.distractorsAnimIndx[id].val == 1 && monit.distractorsAnimIndx[id].noLoop != true){
                    if(animDescr.sound) monit.playSound(animDescr.sound);
                }
                break;

            default:
                console.log("internal error: animation loop type not specified")
                break;                
        }

        monit.pState.distractors[id].img = monit.itemsNames[id].replace('-1.png', '') + suff + '-' + monit.distractorsAnimIndx[id].val + '.png';

        monit.distractorsAnimIndx[id].val += monit.distractorsAnimIndx[id].inc;

    }
    // console.log("monit.pState.distractors[id].img",monit.pState.distractors[id].img);
    return cycleComplete;
}

//=====================================================================================
//MOVEMENTS
movements['linear'] = {
    cb: function(el){

        el.prog += el.movDescr.step;

        el.item.posX = el.movDescr.startPosX +  el.prog * el.movDescr.distPosX;
        el.item.posY = el.movDescr.startPosY +  el.prog * el.movDescr.distPosY;

        el.item.sizX = el.movDescr.startSizX +  el.prog * el.movDescr.distSizX;
        el.item.sizY = el.movDescr.startSizY +  el.prog * el.movDescr.distSizY;

        el.item.rot = el.movDescr.startRot +  el.prog * el.movDescr.distRot;
        
    },
    obj:{
        type: 'linear',
        item:{},
        movDescr:{},
        prog:0
    }
}

movements['sin'] = {
    cb: function(el){

        el.prog += el.movDescr.step;

        var index = (1+sin(PI*el.prog-PI/2))/2;

        el.item.posX = el.movDescr.startPosX +  index * el.movDescr.distPosX;
        el.item.posY = el.movDescr.startPosY +  index * el.movDescr.distPosY;

        el.item.sizX = el.movDescr.startSizX +  index * el.movDescr.distSizX;
        el.item.sizY = el.movDescr.startSizY +  index * el.movDescr.distSizY;

        el.item.rot = el.movDescr.startRot +  index * el.movDescr.distRot;
        
    },
    obj:{
        type: 'sin',
        item:{},
        movDescr:{},
        prog:0
    }
}

movements['jump'] = {
    cb: function(el){

        el.prog += el.movDescr.step;

        var index = (1+sin(PI*el.prog-PI/2))/2;

        // (1-sq(el.prog-1)) * el.distY - 100*2*(cos(2*PI*el.prog+PI)+1)/2;;

        el.item.posX = el.movDescr.startPosX +  index * el.movDescr.distPosX;
        el.item.posY = el.movDescr.startPosY +  index * el.movDescr.distPosY - 100*(cos(2*PI*el.prog+PI)+1)/2;

        el.item.sizX = el.movDescr.startSizX +  index * el.movDescr.distSizX + 50*(cos(2*PI*el.prog+PI)+1)/2;
        el.item.sizY = el.movDescr.startSizY +  index * el.movDescr.distSizY + 50*(cos(2*PI*el.prog+PI)+1)/2;

        el.item.rot = el.movDescr.startRot +  index * el.movDescr.distRot;
        
    },
    obj:{
        type: 'jump',
        item:{},
        movDescr:{},
        prog:0
    }
}

function initMovement(item,movDescr) {

    //prog spans between 0 and 1
    movDescr.prog = 0;

    movDescr.step = 1000/(myFrameRate * movDescr.duration);
    // console.log("inizio: ",getCurrTM());
    // console.log("frameRate: ",movDescr.frameRate,"movDescr.duration: ",movDescr.duration,"step: ", movDescr.step);

    //POSITION
    if(movDescr.startPosX == null) movDescr.startPosX = item.posX;
    if(movDescr.endPosX == null) movDescr.endPosX = item.posX;
    if(movDescr.startPosY == null) movDescr.startPosY = item.posY;
    if(movDescr.endPosY == null) movDescr.endPosY = item.posY;

    if(movDescr.distPosX == null) movDescr.distPosX = movDescr.endPosX - movDescr.startPosX;
    if(movDescr.distPosY == null) movDescr.distPosY = movDescr.endPosY - movDescr.startPosY;

    //SIZE
    if(movDescr.startSizX == null) movDescr.startSizX = item.sizX;
    if(movDescr.endSizX == null) movDescr.endSizX = item.sizX;
    if(movDescr.startSizY == null) movDescr.startSizY = item.sizY;
    if(movDescr.endSizY == null) movDescr.endSizY = item.sizY;

    movDescr.distSizX = movDescr.endSizX - movDescr.startSizX;
    movDescr.distSizY = movDescr.endSizY - movDescr.startSizY;

    //ROTATION
    if(movDescr.startRot == null) movDescr.startRot = item.rot;
    if(movDescr.endRot == null) movDescr.endRot = item.rot; 

    movDescr.distRot = movDescr.endRot - movDescr.startRot;

    // var obj = {};
    // Object.assign(obj, movements[movDescr.type].obj);
    var obj = _.cloneDeep(movements[movDescr.type].obj);
    obj.id = getID();
    obj.item = item;
    obj.movDescr = movDescr;
    movementsStack.push(obj);

    return obj;
}

function moveElements() {

    movementsStack.forEach((movObj, i)=>{
        if(movObj.prog < 1){
            movements[movObj.type].cb(movObj);
        } else {
            _.remove(movementsStack, d => d.id == movObj.id && d.type == movObj.type)   
        }
    })
}

function getID() {
    myCounterId++;
    return myCounterId.toString() + 'a'; 
}




