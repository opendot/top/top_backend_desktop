//state machine
const stateMachine = [
    init,
    startScene,
    play,
    endGame
];
var currentState = 0;

const INIT = 0;
const STARTSCENE = 1;
const PLAY = 2;
const ENDGAME = 3;

//temp
var buttons = {
    back:false,
    next:false,
    cooldown: false
}
var sizeCharacter = {
    x: 200,
    y: 240
},
charName = "char-sacco.png",
specs = [],
levelSpecs = {},
myFrameRate = 30,
animations = [],
backgroundImg = '#fff',
// backgroundImg = 'sfondo-intro.png',
animationsStack = [],
// totalSteps = 48,/////////////////////////
players = [],
itemsEngine = {},
backgroundElements = [],
step = 0,//window.innerWidth/6,
gameIsOver = false,
// iconDisplayTime = 2000,////////////////
//hidePeriod = 1000,//////////////////////
// friendJumpPeriod = 2500,//////////////
tracksY = [window.innerHeight*5/8, window.innerHeight*7/8],
fixedObjectsID = [],
items = [
    {
        type:"collect",
        collection:"robot",
        img:"robot_1_c.png",
        cb: function(itemObj) {
            if(!players[itemObj.player].mute)monit.playSound("robot_1.wav");
            console.log("players[itemObj.player]",players[itemObj.player])
            players[itemObj.player].score.collected.push(itemObj.itemDef.img)
            players[itemObj.player].score.ds[0].img = itemObj.itemDef.img;

            var obj = _.cloneDeep(animations["catched"].obj);
            obj.obj = players[itemObj.player].score.ds[0];
            obj.startSizX = obj.obj.sizX;
            obj.startSizY = obj.obj.sizY;
            animationsStack.push(obj);

            itemsEngine.removeItem(itemObj.pairId,itemObj.player);
        }
    },
    {
        type:"collect",
        collection:"robot",
        img:"robot_2_c.png",
        cb: function(itemObj) {
            if(!players[itemObj.player].mute)monit.playSound("robot_2.wav");
            players[itemObj.player].score.collected.push(itemObj.itemDef.img)
            players[itemObj.player].score.ds[1].img = itemObj.itemDef.img;
            
            var obj = _.cloneDeep(animations["catched"].obj);
            obj.obj = players[itemObj.player].score.ds[1];
            obj.startSizX = obj.obj.sizX;
            obj.startSizY = obj.obj.sizY;
            animationsStack.push(obj);

            itemsEngine.removeItem(itemObj.pairId,itemObj.player);
        }
    },
    {
        type:"collect",
        collection:"robot",
        img:"robot_3_c.png",
        cb: function(itemObj) {
            if(!players[itemObj.player].mute)monit.playSound("robot_1.wav");
            players[itemObj.player].score.collected.push(itemObj.itemDef.img)
            players[itemObj.player].score.ds[2].img = itemObj.itemDef.img;
            
            var obj = _.cloneDeep(animations["catched"].obj);
            obj.obj = players[itemObj.player].score.ds[2];
            obj.startSizX = obj.obj.sizX;
            obj.startSizY = obj.obj.sizY;
            animationsStack.push(obj);

            itemsEngine.removeItem(itemObj.pairId,itemObj.player);
        }
    },
    {
        type:"collect",
        collection:"robot",
        img:"robot_4_c.png",
        cb: function(itemObj) {
            if(!players[itemObj.player].mute)monit.playSound("robot_2.wav");
            players[itemObj.player].score.collected.push(itemObj.itemDef.img)
            players[itemObj.player].score.ds[3].img = itemObj.itemDef.img;
            
            var obj = _.cloneDeep(animations["catched"].obj);
            obj.obj = players[itemObj.player].score.ds[3];
            obj.startSizX = obj.obj.sizX;
            obj.startSizY = obj.obj.sizY;
            animationsStack.push(obj);

            itemsEngine.removeItem(itemObj.pairId,itemObj.player);
        }
    },
    {
        type:"bonus",
        img:"powerup.png",
        cb: function(itemObj) {
            if(!players[itemObj.player].mute)monit.playSound("catched.wav");
            if(!itemObj.player) { //char
                players[itemObj.player].stopJumpSequence();
            } else { //friend
                players[itemObj.player].stopJumpTimer();
                players[itemObj.player].startJumpTimer();
            }
            if(players[itemObj.player].nSteps < 1) players[itemObj.player].nSteps = 1;
            players[itemObj.player].nextJumpLength = 3;
            // monit.removeDistractor(itemObj.ds);
            itemsEngine.removeItem(itemObj.pairId,itemObj.player);

            //cheat
            if(!itemObj.player) {
                itemsEngine.removeItem(itemObj.pairId,1);
            }
        }
    },
    // {
    //     type:"bonus",
    //     img:"lightning_1.png",  
    //     cb: function(itemObj) {
    //         if(!itemObj.player) { //char
    //             players[itemObj.player].stopJumpSequence();
    //         } else { //friend
    //             players[itemObj.player].stopJumpTimer();
    //             players[itemObj.player].startJumpTimer();
    //         }
    //         if(players[itemObj.player].nSteps < 1) players[itemObj.player].nSteps = 1;
    //         players[itemObj.player].nextJumpLength = 3;

    //         itemsEngine.currentItems.slice().reverse().forEach((item,index)=>{
    //             if(item.pairId == itemObj.pairId) {
    //                 monit.removeDistractor(item.ds);
    //                 itemsEngine.currentItems.splice(index,1);
    //             }
    //         })
    //     }
    // },
    // {
    //     type:"bonus",
    //     img:"semaforo.png",
    //     cb: function(itemObj) {

    //     }
    // }
];

var Player = function(playerObj,intro){
    this.intro = intro == undefined ? false : true;
    this.position = 0;
    this.name = playerObj.name ? playerObj.name : charName;
    // this.isMoving = false;
    this.isChar = this.name == charName ? true : false;
    this.offX = this.isChar ? 0 : -50;
    this.ds = monit.addDistractor((windowWidth/2) - step/2 + this.offX, this.isChar ? tracksY[0] -324/4 : tracksY[1] -324/4, this.name,50); //324 is the sizY of the players (temp)
    this.jumpDuration = 1000;
    this.stepCounter = 0;
    this.nSteps = 0;
    this.innerStepCounter = 0;
    this.nextJumpLength = 1;
    this.jumpPeriod = playerObj.jumpPeriod ? playerObj.jumpPeriod : 2500;
    this.timer = 0;
    if(!this.intro) {
        this.score = new Score(this,levelSpecs.collection);
    } else if (this.intro && levelSpecs.tutorial.collection) {
        this.score = new Score(this,levelSpecs.tutorial.collection);
    }
    this.wasMoving = false;
    this.mute = false;
    let me = this;
    this.jump = function(length){
        // animation
        if(!me.isMoving()) {

            // me.isMoving = true;
            me.stepCounter += length;
            var obj = _.cloneDeep(animations["jump"].obj);
            obj.obj = me.ds;
            obj.startPosY = me.ds.posY;
            obj.distX = step*length;
            obj.end = me.jumpDuration;
            animationsStack.push(obj);

            if(!this.mute) monit.playSound(this.isChar ? 'boing-1.wav' : 'boing-2.wav');
            if(length>1) monit.playSound(this.isChar ? "woo-1.wav" : "woo.wav");

            // if(this.isChar)console.log("char - nSteps:",me.nSteps,"innerStepCounter: ", me.innerStepCounter, "nextJumpLength: ",me.nextJumpLength)

            if (me.isChar) {
                var distrButChar = monit.getDistractors().filter((distr)=>{
                    return fixedObjectsID.indexOf(distr.id) === -1;
                    // return distr.img != 'char.png' && distr.img !== 'pista.png' && distr.img != 'avv_face.png' && distr.img != 'play.png'; //to be completed with other scene elements
                })
                distrButChar.forEach((el)=>{
                    var obj = _.cloneDeep(animations["slideBw"].obj);
                    obj.obj = el;
                    obj.distX = step*length;
                    obj.end = me.jumpDuration;
                    animationsStack.push(obj);
                });
            } else {
                var obj = _.cloneDeep(animations["slideFw"].obj);
                obj.obj = me.ds;
                obj.distX = step*length;
                obj.end = me.jumpDuration;
                animationsStack.push(obj);
            }
        }
        this.nextJumpLength = 1;
        // console.log("char: ",name, "number of steps:",me.stepCounter);
    }
    this.isMoving = function(){
        if(this.isChar){
            return animationsStack.filter(a => a.type == 'slideBw').length > 0 ? true : false;
        }else{
            return animationsStack.filter(a => a.type == 'slideFw').length > 0 ? true : false;   
        }
    }

    this.isFalling = function() {
        return this.cade;
    }

    this.fall = function() {
        this.cade = true;
        monit.playSound("pillow-2.wav");
        setTimeout(()=>{this.cade = false},2000);
    }

    this.draw = function(){
        //player is moving
        if(this.isMoving()) {
            monit.animateDistractor(this.ds,"-salto");
        } else if (!this.isMoving() && !this.isFalling()){ // player is NOT moving
            this.ds.img = levelSpecs.playerObj[(this.isChar ? 0 : 1).toString()].name;
            //the number of jumps to be done is not reached yet
            if(this.innerStepCounter < this.nSteps){
                this.jump(this.nextJumpLength);
                this.innerStepCounter++;
            }else {
                this.stopJumpSequence();
            }
        } else if (this.isFalling) {
            monit.animateDistractor(this.ds,"-inciampo")
        }

        if(!this.intro && !this.score.completed) this.checkCollection();

        // //////////////////////////
        // if(!this.isChar && this.ds.posX >= windowWidth + 100) this.ds.posX = windowWidth + 100 
    }
    this.checkCollection = function () {
        if(this.score.collected.length == this.score.collectionSize) {
            this.score.completed = true;
            console.log("COLLEZIONE COMPLETATA!");
        }
    }
    this.jumpSequence = function(howMany) {
        // this.innerStepCounter = 0;
        this.nSteps += howMany; //the jump sequences gets summed
    }
    this.stopJumpSequence = function() {
        this.innerStepCounter = 0;
        this.nSteps = 0;
        this.nextJumpLength = 1;
    }
    this.startJumpTimer = function() {
        this.timer = setInterval(()=>{
            this.jump(1);
        },this.jumpPeriod+100)
    }
    this.stopJumpTimer = function() {
        clearInterval(this.timer);
        this.timer = 0;
    }
};

Score = function(player,collection) {
    this.player = player;
    this.collection = collection;
    this.collectionSize = 0;
    this.collected = [];
    this.ds = [];
    this.face = {};
    if(this.collection != null) this.init()
};

Score.prototype.init = function() {
    var playerIndex = !this.player.isChar;

    var posX = playerIndex*windowWidth + pow(-1,playerIndex) * (windowWidth/15);
    var posY = windowHeight/13;

    // var circle = monit.addDistractor(posX,posY,"ellipse", "#0EA3B7", 120, 120)
    this.face = monit.addDistractor(posX,posY,this.player.name.replace("sacco.png","avatar.png"),100);

    fixedObjectsID.push(this.face.id)
    // fixedObjectsID.push(circle.id)

    this.collectionSize = items.filter(item=>item.collection==this.collection).length;
    // console.log("la dimensione della collezione ",this.collection, "è",collectionSize);
    for (let i = 0;i<this.collectionSize;i++) {
        var img = this.collection + "_" + (i+1).toString() + "_bw.png";
        // console.log(img)
        var ds = monit.addDistractor(playerIndex*windowWidth + pow(-1,playerIndex) * (windowWidth*2/15 + i *windowWidth/15),windowHeight/13,img,100);
        ds.sizX /= 2;
        ds.sizY /= 2;
        fixedObjectsID.push(ds.id);
        this.ds.push(ds);
    }
};

Icon = function(iconObj,type){
    this.iconName = 'freccia.png';
    this.type = type ? type : 'random';
    this.icon = undefined;
    this.nSteps = 3;
    this.innerStepCounter = 0;
    this.showWhenStill = iconObj.showWhenStill != undefined ? iconObj.showWhenStill : true;
    this.displayPeriod = iconObj.displayPeriod ? iconObj.displayPeriod : {'freccia':1000,'stop':1000};
    this.hidePeriod = iconObj.hidePeriod ? iconObj.hidePeriod : 1000;
    this.display = false;
    this.displayCounter = 0;
    this.hideCounter = 0;
    this.hide = function(){
        if(monit.getTargetByName(this.iconName)){
            monit.removeTarget(monit.getTargetByName(this.iconName));
            this.icon = undefined;
        }
    }
    this.show = function(type){
        if(type == 'random') {// odds 60 to find stop image
            this.iconName = random() > 0.5 ? 'stop.png' : 'freccia.png';
            this.icon = monit.addTarget(windowWidth / 2, windowHeight / 6, this.iconName,100);
        } else if (type == 'freccia') {
            this.iconName = 'freccia.png';
            this.icon = monit.addTarget(windowWidth / 4, windowHeight / 6, this.iconName,100);
        } else if (type == 'stop') {
            this.iconName = 'stop.png'
            this.icon = monit.addTarget(windowWidth * 3 / 4, windowHeight / 6, this.iconName,100);
        }
        fixedObjectsID.push(this.icon.id);
    }
    this.checkOverlap = function(){
        if(monit.getTargetByName(this.iconName) == undefined){
            this.show(this.type);
        }
        var Tg = monit.getTargetByName(this.iconName);
        if(monit.trackerActive && Tg !== undefined){
            var eyeT = monit.eyeTracker,
            currTarget = {
                posX: Tg.posX,
                posY: Tg.posY,
                sizX: Tg.sizX,
                sizY: Tg.sizY
            };
            // compensate offset for overlap
            if(checkOverlap({posX: eyeT.posX + Tg.sizX / 2, posY: eyeT.posY + Tg.sizY / 2, sizX: eyeT.sizX, sizY: eyeT.sizY}, currTarget)){
                if(_.find(monit.fixationObjects, d => d.id === Tg.id) == undefined){
                    // add target to the interacted list
                    monit.fixationObjects.push({id: Tg.id, counter: 0});
                    addAnimation('targetOverlap', Tg, monit.fixationTime);
                    animationsStack.filter(a => a.type == 'targetOverlap' && a.obj.id == Tg.id)[0].circle = new Circle(Tg.posX, Tg.posY, monit.fixationTime);
                }else{
                    var temp = _.find(monit.fixationObjects, d => d.id === Tg.id);
                    temp.counter += 33.33;
                }
            }else{
                _.remove(monit.fixationObjects, d => d.id === Tg.id);
                _.remove(animationsStack, d => d.obj.id == Tg.id && d.type == 'targetOverlap');
            }
        }
    }

    this.draw = function(){

        if(this.showWhenStill && !players[0].isFalling()) {
            if(players[0].isMoving()) {
                this.display = false;
                this.hideCounter = 0;
            } else if (!players[0].isMoving() && players[0].wasMoving) {
                this.display = true;
            }
        }
        if(this.display) {// && !players[0].isMoving()) {

            if(this.displayCounter >= this.displayPeriod[this.iconName.replace('.png','')] && !animationsStack.filter(a => a.type == 'targetOverlap').length) {
                this.displayCounter = 0;
                this.display = false;
                if(this.iconName == 'stop.png') {
                    players[0].jumpSequence(1);
                }
            }

            this.checkOverlap();
            this.watchIcon();

            this.hideCounter = 0;
            this.displayCounter += 33.33;

        } else {

            if(this.hideCounter >= this.hidePeriod) {
                this.display = true
                this.hideCounter = 0;
            }

            this.displayCounter = 0;
            this.hideCounter += 33.33;
            this.hide();
        }

        players[0].wasMoving = players[0].isMoving();
    }

    this.watchIcon = function(){
        let me = this;
        var timeOffx = 100;
        monit.fixationObjects.forEach( f => {
            if(f.counter > monit.fixationTime){
                // action
                me.hide();
                if(me.iconName == 'freccia.png'){
                    // set character in moving state 
                    me.display = false;
                    players[0].jumpSequence(3);
                }else{
                    // fixation stop icon
                    me.display = false;
                    players[0].stopJumpSequence();
                    players[0].fall();
                    monit.playSound("wrong.wav");
                }
                _.remove(monit.fixationObjects, fr => f.id == fr.id);
                _.remove(animationsStack, d => d.obj.id == f.id && d.type == 'targetOverlap');
            }
        });
    }
};
// circle to fixation
Circle = function(x, y, dur){
    this.ind = 0;
    this.r = 100;
    this.x = x;
    this.y = y;
    this.done = 0;
    this.inc = (2 * PI) / (dur / 33);
    this.draw = function(){
        if(this.ind < 2 * PI){
            push();
            stroke(200);
            fill(255,100)
            ellipse(this.x, this.y, this.r, this.r);
            fill(255,210);
            arc(this.x, this.y, this.r, this.r, -PI/2 + this.ind, (3/2)*PI, PIE); //
            pop();

            this.ind += this.inc;
        }else{
            this.done = 1;
        }
    }
};

Item = function(itemDef,absStep,player,id){
    this.itemDef = itemDef;
    this.pairId = id;
    this.player = player;

    var posX = windowWidth/2 + step*(absStep-1) - step*players[0].stepCounter + players[this.player].offX;
    this.ds = monit.addDistractor(posX,tracksY[player],itemDef.img,40);
    
    this.catch = function() {
        itemDef.cb(this);
    }
};

ItemsEngine = function(isTutorial) {
    this.isTutorial = isTutorial ? isTutorial : false;
    this.currentItems = [];
    this.idCounter = 0;
    this.generateItem = function(itemName,absStep) {
        var itemDef = items.find((item)=>{return item.img == itemName});
        var id = this.getItemsPairId();
        this.currentItems.push(new Item(itemDef,absStep,0,id))
        this.currentItems.push(new Item(itemDef,absStep,1,id))
    }

    this.getItemsPairId = function () {
        return this.idCounter++;
    }

    this.checkOverlap = function () {
        // var itemToSpliceIndex = null;
        this.currentItems.forEach((item,index)=>{
            // console.log("item",item,"index",index);
            if(item.ds.posX <= players[item.player].ds.posX) {
                // console.log("PRESO L'ITEM ID",item.pairId," DA PLAYER",item.player,"perchè",item.ds.posX, "è minore di",players[item.player].ds.posX)
                item.catch();
                // this.currentItems.splice(index,1);
                // itemToSpliceIndex = index;
                // console.log("imposto l'indice da rimuovere a ",itemToSpliceIndex);
            }
        })
        // if(itemToSpliceIndex!=null){
        //     this.currentItems.splice(itemToSpliceIndex,1);
        //     // console.log("splico l'indice",itemToSpliceIndex);
        // }
        
    }
    this.run = function() {
        this.checkOverlap();
    }

    this.removeItem = function(pairId,player){
        var i = this.currentItems.findIndex(el=>el.pairId == pairId && el.player == player);
        if(i>=0) {
            monit.removeDistractor(this.currentItems[i].ds);
            this.currentItems.splice(i,1);
        }
    }

    this.generateOneItem = function(itemName,absStep) {
        var itemDef = items.find((item)=>{return item.img == itemName});
        var id = this.getItemsPairId();
        this.currentItems.push(new Item(itemDef,absStep,0,id))
    }

    this.init(this.isTutorial);
};

ItemsEngine.prototype.init = function(isTutorial) {
    if(!isTutorial) {
        levelSpecs.itemsPositions.forEach((item)=>{
            this.generateItem(item.name,item.step)
        });
    } else {
        levelSpecs.tutorial.itemsPositions.forEach((item)=>{
            this.generateOneItem(item.name,item.step)
        });
    }

};

// TutorialEngine = function(tutorialObj) {
//     this.
// }

// check if there are movement animations in execution  
function addAnimation(type, obj, end){
    var temp = _.cloneDeep(animations[type].obj);
    temp.timestamp = getCurrTM();
    temp.obj = obj;
    temp.end = monit.fixationTime;
    animationsStack.push(temp);
}
var flag = 0;
function drawGround(){
    if(monit.getDistractorByName('pista.png') == undefined){
        fixedObjectsID.push(monit.addDistractor(windowWidth / 2, windowHeight / 2, 'pista.png',-10, null, windowWidth, windowHeight).id);
        monit.addDistractor(windowWidth / 2, windowHeight / 2, 'pista-linee.png',-10, null, windowWidth, windowHeight);
        monit.addDistractor(windowWidth / 2 + step*(levelSpecs.totalSteps-1), windowHeight / 2, 'pista-linee.png',-10, null, windowWidth, windowHeight);

        //temp to populate the background in the beginning
        // var bgEl = backgroundElements.filter((el)=>{return el.img.startsWith("albero")});
        var bgEl = levelSpecs.backgroundElements;
        for(let i = 0;i<10;i++) {
            var treeEl = bgEl[randomNumber(0,bgEl.length)];
            if(random()>0.82) {
                monit.addDistractor(randomNumber(0,windowWidth),randomNumber(treeEl.posY[0]*monit.screenScaleFactor,treeEl.posY[1]*monit.screenScaleFactor),treeEl.img);
            }
        }
    }
    var track = monit.getDistractors().find(el=>el.img=="pista-linee.png");
    if (track.posX <= -track.sizX/2) {
        monit.removeDistractor(track);
    }
}

function drawBackground() {
    if(levelSpecs.backgrounds == undefined || !levelSpecs.backgrounds.length) {
        levelSpecs.backgrounds = [];
        levelSpecs.backgrounds.push(monit.addDistractor(windowWidth/2, windowHeight/2, levelSpecs.backgroundImg,-100, null, windowWidth, windowHeight))
    } else {
        var firstBg = levelSpecs.backgrounds[0];
        if (firstBg.posX < -firstBg.sizX/2) {
            monit.removeDistractor(levelSpecs.backgrounds.shift());
        } else if (firstBg.posX < windowWidth - firstBg.sizX/2 && levelSpecs.backgrounds.length == 1) {
            levelSpecs.backgrounds.push(monit.addDistractor(firstBg.posX + firstBg.sizX, windowHeight/2, levelSpecs.backgroundImg,-100, null, windowWidth, windowHeight))
            if(players[0].isMoving()) {
                var obj = _.cloneDeep(animationsStack.find((el)=>el.type == "slideBw"));
                obj.obj = levelSpecs.backgrounds[1];
                animationsStack.push(obj);
            }
        }
    }
}
var animations = [];
animations['selected'] = {
    cb: function(el){

        el.index += 1000/30;
        el.prog = map(el.index,0,el.end,0,1);
        var y = (1+sin(2*PI*el.prog-PI/2))/2;
        var x = ((1+sin(2*PI*el.prog-PI/2))/2+(1+sin(4*PI*el.prog-PI/2))/2)/(PI/2);


        push();
        ellipseMode(CENTER);
        strokeWeight(50*x);
        strokeCap(ROUND);
        strokeJoin(ROUND);
        stroke(0,0,255,map(y,0,1,0,255));
        noFill();
        ellipse(el.obj.posX,el.obj.posY,el.startSizX+50*x,el.startSizX+50*x);
        pop();
    },
    obj: {
        type: 'selected',
        index: 0,
        timestamp: 0,
        obj: {},
        prog:0,
        startSizX:0,
        end: 800
    }
}
animations['catched'] = {
    cb: function(el){
        el.index += 1000/30;
        el.prog = map(el.index,0,el.end,0,1);
        var x = ((1+sin(2*PI*el.prog-PI/2))/2+(1+sin(4*PI*el.prog-PI/2))/2)/(PI/2);
        el.obj.sizX = el.startSizX+el.startSizX*0.3*x;
        el.obj.sizY = el.startSizY+el.startSizY*0.3*x;
    },
    obj: {
        type: 'catched',
        index: 0,
        timestamp: 0,
        obj: {},
        prog:0,
        startSizX:0,
        startSizY:0,
        name: '',
        end: 800
    }
}
// completed animation
animations['completed'] = {
    cb: function(el){
        el.index += (el.end / 33);
    },
    obj: {
        type: 'completed',
        index: 0,
        timestamp: 0,
        obj: {},
        name: '',
        end: 3000
    }
}
animations['targetOverlap'] = { 
    cb: function(el) {
        el.circle.x = el.obj.posX;
        el.circle.y = el.obj.posY;
        el.circle.draw();
        el.index += 1000/30;//(el.end / 33);
    },
    obj: {
        type: 'targetOverlap', 
        id: '', 
        index: 0,
        obj: {}, 
        timestamp: 0, 
        end: 500
    }
};
animations['jump'] = { 
    cb: function(el) {

        el.index += 1000/30;
        el.prog=map(el.index,0,el.end,0,1);

        var ease = 1-sq(2*el.prog-1);
        var newPosY = el.startPosY -ease*el.distY;
        el.obj.posY = newPosY;
        // if(newPosY >= el.startPosY) {
        //     _.remove(animationsStack, d => d.obj.id == el.obj.id && d.type == "slideFw");
        // }
    },
    obj: {
        type: 'jump', 
        id: '', 
        index: 0,
        obj: {}, 
        timestamp: 0, 
        end: 1000,
        prog:0,
        startPosY:0,
        distY:100
    }
};
animations['slideFw'] = { 
    cb: function(el) {

        el.index += 1000/30;

        el.vel = el.distX / (30*el.end/1000);
        el.obj.posX += el.vel;
    },
    obj: {
        type: 'slideFw', 
        id: '', 
        index: 0,
        obj: {}, 
        timestamp: 0, 
        end: 1000,
        prog:0,
        distX:step,
        vel:0
    }
};
animations['slideBw'] = { 
    cb: function(el) {

        el.index += 1000/30;

        el.vel = - el.distX / (30*el.end/1000);
        el.obj.posX += el.vel;

    },
    obj: {
        type: 'slideBw', 
        id: '', 
        index: 0,
        obj: {}, 
        timestamp: 0, 
        end: 1000,
        prog:0,
        distX:step,
        vel:0
    }
};
// reset targets distractor and fixation objects
function resetAll(){
    // monit.fixationObjects = [];
    monit.clearTargets();
    monit.clearDistractors();
}
function drawAnimations(){
    animationsStack.slice().reverse().forEach((animObj,i) =>{
        if(animObj.index > animObj.end){
            _.remove(animationsStack, d => d.obj.id == animObj.obj.id && d.type == animObj.type);
        }else{
            animations[animObj.type].cb(animObj);
        }
    })
}

// -------------- PRELOAD --------------
function preload() {
    monit.loadJsonItems();
    loadMyJson("./corsa_coi_sacchi.json");
}
// -------------- SETUP ----------------
function setup() {
    createCanvas(windowWidth,windowHeight);
    step = windowWidth/6;
    monit.animationRate = 4;

    // loadTabletSettings();
    // redefine draw distractors and targets methods in order to manage a z-index of elements 
    // overrideMonit();
    noStroke();
    frameRate(30);
}

var timer = undefined,
isFirstLoop = true;
// -------------- DRAW -----------------
function draw() {
    // console.warn(monit.GameScene, GameSceneTypeFix, GameSceneStage);
    if(monit.loaded){

        if(monit.pState.bg != backgroundImg && !monit.GameScene){monit.setBg(backgroundImg);} // set first bg      
        monit.drawBg();
        if(levelSpecs && levelSpecs.bg) levelSpecs.bg.run();
 
        if (currentState < stateMachine.length) {
            stateMachine[currentState]();
        }
        // monit.drawDistractors();
        // monit.drawTargets();
        monit.drawAllItems(); 

        if(monit.trackerActive && monit.mode == 'mouse'){
            monit.drawTracker();
        }
        // monit.monitoring();
        //draw animations if present
        drawAnimations();
    }else{
        push();
        background('black');
        textSize(30);
        stroke('white');
        fill('white');
        textAlign(CENTER, CENTER);
        text('Caricamento in corso...', windowWidth / 2, windowHeight / 2);
        pop();
    }
}

var iconG = {};
var iconB = {};

function init() {

    if(isFirstLoop){
        //temp
        // monit.GameLevel = 2;
        overrideMonit();

        setButtonsState({next:true,back:false,cooldown:false});
        isFirstLoop = false;

        levelSpecs = specs.levels[monit.GameLevel];
        loadTabletSettings();
        levelSpecs.bg = new Bg(myFrameRate);
        levelSpecs.bg.rect.zIndex = -50;

        // fixedObjectsID.push(monit.addDistractor(windowWidth/2,windowHeight*3/4,"pista.png",-10).id);

        iconG = new DummyIcon('freccia');
        iconB = new DummyIcon('stop');
        players[0] = new Player({name:charName},true);
        itemsEngine = new ItemsEngine(true);
        // players[0].ds.posX = windowWidth/2;
        // players[0].ds.posY = windowHeight*3/4;
        // players[0].ds.sizX *= 1.5;
        // players[0].ds.sizY *= 1.5;
        fixedObjectsID.push(players[0].ds.id);
    }
    drawBackground();
    itemsEngine.run();
    iconG.run();
    iconB.run();
    players[0].draw();
}

function startScene() {

    monit.clearDistractors();
    monit.clearTargets();
    fixedObjectsID = [];
    players = [];
    levelSpecs.backgrounds = [];
    levelSpecs.bg = null;
    levelSpecs.bg = new Bg(myFrameRate);
    levelSpecs.bg.rect.zIndex = -50;

    if(levelSpecs.icon == undefined){levelSpecs.icon = new Icon(levelSpecs.iconObj)} // set assets dimension object 

    players[0] = new Player(levelSpecs.playerObj[0]);
    players[1] = new Player(levelSpecs.playerObj[1]);
    fixedObjectsID.push(players[0].ds.id);
    itemsEngine = new ItemsEngine();

    players[1].startJumpTimer();
    levelSpecs.icon.display = true;

    setButtonsState({next:false,back:false,cooldown:false});
    currentState = PLAY;
}

function play() {

    drawBackground();
    drawGround();
    players[0].draw();
    players[1].draw();
    
    itemsEngine.run();
    friendPlaceHolder();
    limitLead();
    generateBgElements();
    removeBgElements();
    if(!gameIsOver){
        checkVictory();
        levelSpecs.icon.draw();
    }

    // push();
    // fill(0);
    // text('distacco: ' + (players[0].stepCounter-players[1].stepCounter).toString(), 200, windowHeight - 10); 
    // pop();
}
function endGame() {
    if(levelSpecs.collection != null && monit.getDistractorByName("robot_mucchio.png")==undefined) monit.animateDistractor(players[3],"-hurra");

    if(players[0].posX >= windowWidth/2) {
        monit.animateDistractor(players[0],"-hurra");
        monit.animateDistractor(players[1],"-down");
    } else {
        monit.animateDistractor(players[0],"-down");
        monit.animateDistractor(players[1],"-hurra");
    }
}

// KEY PRESSED FUNCTION
function keyPressed(){
    // console.warn(keyCode)
    switch(keyCode){
        case UP_ARROW:
            monit.onAddTarget();
            break;
        case DOWN_ARROW:
            monit.onAttentionSig();
            break;
        case RIGHT_ARROW:
            
            break;
        case LEFT_ARROW:
            monit.onToggleTracker(monit.trackerActive ? 0 : 1);
            break;
        case 78:
            monit.onBackNext('next');
            break;
        case 66:
            monit.onBackNext('back');
            break;
        case 68:
            monit.onToggleFatigue();
            break;
    }
}
// check if an object is empty 
function isEmpty(obj) {
    for(var key in obj) {
        if(obj.hasOwnProperty(key))
            return false;
    }
    return true;
}
// conversion rgb to hex
function rgbToHex(r, g, b) {
    return "#" + ((1 << 24) + (r << 16) + (g << 8) + b).toString(16).slice(1);
}
// change appearence order of distractors
function reorderDis(){
    var currOb = monit.pState.distractors,
    keys = Object.keys(currOb),
    localPstate = {};
    keys = keys.sort(function(a, b){
        if(a.id < b.id){
            return 1;
        }else{
            return -1;
        }
    })
    keys.forEach( el => {
        localPstate[el] = monit.getDistractors().filter(d => d.id == el)[0];
    });
    monit.pState.distractors = localPstate;
}

function generateBgElements() {

    var element = levelSpecs.backgroundElements[randomNumber(0,levelSpecs.backgroundElements.length)];


    if(random() > element.rate && players[0].isMoving()) {
        var bgEl = monit.addDistractor(windowWidth + element.sizX/2,randomNumber(element.posY[0]*monit.screenScaleFactor,element.posY[1]*monit.screenScaleFactor),element.img,element.zIndex);
        var obj = _.cloneDeep(animationsStack.find((el)=>el.type == "slideBw"));
        obj.obj = bgEl;
        animationsStack.push(obj);
    }
}

function removeBgElements() {
    var distr = monit.getDistractors();
    var bgEl = distr.filter((el)=>{
        return levelSpecs.backgroundElements.filter((t)=>{
            return el.img == t.img;
        }).length;
    });

    bgEl.forEach((el)=>{
        if(el.posX < -el.sizX/2){
            monit.removeDistractorById(el.id);
        }
    })
}

function checkVictory() {

    players.forEach((player,index)=>{
        if (player.stepCounter >= levelSpecs.totalSteps) {
            setTimeout(()=>{gameOver(index)},1000);
            players[0].stopJumpSequence();
            players[1].stopJumpTimer();
            levelSpecs.icon.hide();
            gameIsOver = true;
        }
    })
}

function gameOver(player) {
    
    _.remove(animationsStack, d => d.type == 'targetOverlap');
    
    

    var collectionComplete = players[0].score.completed;

    players = [];
    monit.clearTargets();
    monit.clearDistractors();

    var bg = monit.addDistractor(windowWidth/2,windowHeight/2,levelSpecs.backgroundImg,-100, null, windowWidth, windowHeight);
    bg.sizX *=2;
    bg.sizY *=4;
    bg.posY = 0;
    var pista = monit.addDistractor(windowWidth/2,windowHeight/2,"pista-linee.png",-10, null, windowWidth, windowHeight);
    pista.sizX *=2;
    pista.sizY *=4;
    pista.posY = 0;
    tracksY = [windowHeight/2,windowHeight*3/4]

    var winnerPosX = windowWidth*4/6;
    var looserPosX = windowWidth/6;

    if(!player) {
        monit.playSound("yuppi.wav");
        monit.playSound("applause.wav");
        monit.playSound("completato.wav");
        players[0] = monit.addDistractor(winnerPosX,tracksY[0],charName,50);
        players[1] = monit.addDistractor(looserPosX,tracksY[1]-100,levelSpecs.playerObj[1].name,50);

    } else {
        monit.playSound("fail.wav");
        players[0] = monit.addDistractor(looserPosX,tracksY[0],charName,50);
        players[1] = monit.addDistractor(winnerPosX,tracksY[1]-100,levelSpecs.playerObj[1].name,50);
    }

    players.forEach((el)=>{
        el.sizX *= 1.5;
        el.sizY *= 1.5;
    })

    if (levelSpecs.collection != null && collectionComplete) {
        players[3] = monit.addDistractor(players[0].posX+windowWidth/5,tracksY[0],"robot.png",50);
    } else if (levelSpecs.collection != null && !collectionComplete) {
        players[3] = monit.addDistractor(players[0].posX+windowWidth/5,tracksY[0],"robot_mucchio.png",50);
    }

    currentState = ENDGAME;
}

function limitLead() {

    var currLead = players[0].stepCounter - players[1].stepCounter;
    var prevLead = levelSpecs.lead.prevLead  ? levelSpecs.lead.prevLead : 0;
    var min = levelSpecs.lead.limits.min;
    var max = levelSpecs.lead.limits.max;

    if(currLead <= min && prevLead > min) {
        //the player just get the maximum disadvantage
        players[1].stopJumpTimer();
    } else if (currLead > min && prevLead <= min) {
        // the player just resolved the maximum disadvantage
        players[1].startJumpTimer();
    } else if (currLead >= max && prevLead < max) {
        //the player just get the maximum advantage
        //TBD
    } else if (currLead < max && prevLead >= max) {
        // the player just loose the maximum advantage
        //TBD
    }
    //memory
    levelSpecs.lead.prevLead = currLead;
    // console.log("lead: ",lead)
}

function friendPlaceHolder() {
    
    var play = monit.getDistractorByName('play.png');
    var face = monit.getDistractors().filter(el=>el.img==levelSpecs.playerObj[1].name.replace("sacco.png","avatar.png") && el.id!=players[1].score.face.id)[0];
    
    if (players[1].ds.posX <= -players[1].ds.sizX*1/5) {
        if (play == undefined && face == undefined) {
            play = monit.addDistractor(104/2,tracksY[1],'play.png',100);
            face = monit.addDistractor(104*3/2,tracksY[1],levelSpecs.playerObj[1].name.replace("sacco.png","avatar.png"),100);
            play.rot = PI;
            fixedObjectsID.push(play.id);
            fixedObjectsID.push(face.id);
            players[1].mute = true;
        }
    } else if (players[1].ds.posX >= windowWidth + players[1].ds.sizX*1/5) {
        if (play == undefined && face == undefined) {
            play = monit.addDistractor(windowWidth - 104/2,tracksY[1],'play.png',100);
            face = monit.addDistractor(windowWidth - 104*3/2,tracksY[1],levelSpecs.playerObj[1].name.replace("sacco.png","avatar.png"),100);
            fixedObjectsID.push(play.id);
            fixedObjectsID.push(face.id);
            players[1].mute = true;
        }
    } else {
        if (play != undefined && face != undefined) {
            fixedObjectsID.splice(fixedObjectsID.findIndex((id)=>{return id == play.id}),1);
            fixedObjectsID.splice(fixedObjectsID.findIndex((id)=>{return id == face.id}),1);
            monit.removeDistractor(play);
            monit.removeDistractor(face);
            players[1].mute = false;
        }
    }
}

function loadTabletSettings(){
    let args = (new URL(location.href)).searchParams;
    levelSpecs.playerObj.jumpPeriod = args.get('avvSpeed') !== null ? parseInt(args.get('avvSpeed')) : 2500;
}

function randomNumber(min, max) {  
    return Math.floor(Math.random() * (max - min) + min); 
}


var DummyIcon = function(type) {
    this.type = type;
    this.posX = this.type == 'freccia' ? windowWidth/4 : windowWidth*3/4;
    this.posY = windowHeight/4;
    this.iconName = this.type == 'freccia' ? 'freccia.png' : 'stop.png';
    this.ds = monit.addTarget(this.posX,this.posY,this.iconName,100);
    this.display = true;
    this.init();
    this.run = function() {

        if(!players[0].isMoving() && !players[0].isFalling()) {
            this.show();
            this.checkOverlap();
            this.watchIcon();
        } else {
            this.hide();
        }

    }
    this.checkOverlap = function() {
        var Tg = monit.getTargetByName(this.iconName);
        if(monit.trackerActive && Tg !== undefined){
            var eyeT = monit.eyeTracker,
            currTarget = {
                posX: Tg.posX,
                posY: Tg.posY,
                sizX: Tg.sizX,
                sizY: Tg.sizY
            };
            // compensate offset for overlap
            if(checkOverlap({posX: eyeT.posX + Tg.sizX / 2, posY: eyeT.posY + Tg.sizY / 2, sizX: eyeT.sizX, sizY: eyeT.sizY}, currTarget)){
                if(_.find(monit.fixationObjects, d => d.id === Tg.id) == undefined){
                    // add target to the interacted list
                    monit.fixationObjects.push({id: Tg.id, counter: 0});
                    addAnimation('targetOverlap', Tg, monit.fixationTime);
                    animationsStack.filter(a => a.type == 'targetOverlap' && a.obj.id == Tg.id)[0].circle = new Circle(Tg.posX, Tg.posY, monit.fixationTime);
                }else{
                    var temp = _.find(monit.fixationObjects, d => d.id === Tg.id);
                    temp.counter += 33.33;
                }
            }else{
                _.remove(monit.fixationObjects, d => d.id === Tg.id);
                _.remove(animationsStack, d => d.obj.id == Tg.id && d.type == 'targetOverlap');
            }
        }
    }
    this.watchIcon = function(){
        let me = this;
        var timeOffx = 100;
        monit.fixationObjects.forEach( f => {
            if(f.counter > monit.fixationTime){
                // action
                me.hide();
                if(me.iconName == 'freccia.png'){
                    // set character in moving state 
                    me.display = false;
                    players[0].jump(1);
                }else{
                    // fixation stop icon
                    me.display = false;
                    players[0].stopJumpSequence();
                    players[0].fall();
                    monit.playSound("wrong.wav");
                }
                _.remove(monit.fixationObjects, fr => f.id == fr.id);
                _.remove(animationsStack, d => d.obj.id == f.id && d.type == 'targetOverlap');
            }
        });
    }
    this.show = function(){
        this.ds.posY = this.posY;
    }
    this.hide = function(){
        this.ds.posY = -1000;
    }
}

function setButtonsState(state) {
    if(state.next !== undefined) buttons.next = state.next;
    if(state.back !== undefined) buttons.back = state.back;
    if(state.cooldown !== undefined) buttons.cooldown = state.cooldown;
    monit.sendEvent({ts: new Date().getTime(), type: "event", event_type: "buttons_state", data:{cooldown:buttons.cooldown,next:buttons.next,back:buttons.back}});
}

DummyIcon.prototype.init = function() {
    fixedObjectsID.push(this.ds.id);
}

function loadMyJson(url) {
    fetch(url)
        .then(response => {
            return response.json()
        })
        .then(data => {
            specs = _.cloneDeep(data);
        });
}

function overrideMonit() {
    monit.drawDistractors = function(){
        var ds = _.values(this.pState.distractors),
        players = ds.filter(d => d.img.indexOf('bimbo') !== -1 || d.img.indexOf('drago') !== -1),
        others = ds.filter(d => d.img.indexOf('bimbo') == -1 && d.img.indexOf('drago') == -1);
        monit.drawItems(others.concat(players));
    }
    monit.drawTargets = function(){
        var tg = _.values(this.pState.targets),
        icons = tg.filter(t => t.img == 'freccia.png' || t.img == 'stop.png'),
        others = tg.filter(t => t.img != 'freccia.png' && t.img != 'stop.png')
        monit.drawItems(others.concat(icons));
    }
    monit.onRemoveTarget = (idT) => {

        if(currentState == PLAY) {
            levelSpecs.icon.hide();
            if(levelSpecs.icon.iconName == 'freccia.png'){
                // set character in moving state 
                levelSpecs.icon.display = false;
                players[0].jumpSequence(3);
            }else{
                // fixation stop icon
                levelSpecs.icon.display = false;
                players[0].stopJumpSequence();
                players[0].fall();
                monit.playSound("wrong.wav");
            }
        } else if (currentState == INIT) {
            var icona = monit.getTargets().find(el=>el.id==idT);
            if (icona.img == 'freccia.png') {
                players[0].jump(1);
            } else {
                players[0].stopJumpSequence();
                players[0].fall();
                monit.playSound("wrong.wav");
            }
        }
        _.remove(monit.fixationObjects, fr => idT == fr.id);
        _.remove(animationsStack, d => d.obj.id == idT && d.type == 'targetOverlap');

    }
    monit.onBackNext = (button) => {
        switch(button) {
            case "next":
                if(buttons.next) {
                    console.log("NEXT button pressed");
                    switch(currentState) {
                        case INIT:
                            currentState = STARTSCENE;
                            break;
                        default:
                            break;
                    }
                }
                break;
            case "back":
                if(buttons.back) {
                    console.log("back button pressed");
                }
                break;
            default:
                break;
        }
    }
    monit.onToggleFatigue = function() {
        if(buttons.cooldown) {
            console.log("cooldown button pressed");
        }
    }
    monit.onAddTarget = () => {
        // if(currentState == INIT) {
        //     currentState = STARTSCENE
        // }
    }
    monit.onMoveTarget = () => {

    }
    monit.onChangeDynamics = (data) => {
        console.log('data', data);
        var type = data.id;
        switch(type){
            case 'ft':
                monit.fixationTime = parseInt(data.value);
                console.log("fixation time: ",monit.fixationTime);
                break;
            case 'avvSpeed':

                players[1].jumpPeriod = parseInt(data.value);

                var friendJump = animationsStack.find(el=>el.type == 'slideFw');
                
                var time = 0;
                if(friendJump) time = friendJump.obj.end - friendJump.obj.index + 10;
                setTimeout(()=>{
                    players[1].stopJumpTimer();
                    players[1].jump(players[1].nextJumpLength);
                    players[1].startJumpTimer();
                },time);

                break;
            default:
                break;
        }
    }
    monit.onAttentionSig = () => {

        if(currentState == PLAY && levelSpecs.icon.display && levelSpecs.icon.iconName == 'freccia.png') {
            monit.playSound("richiamo.mp3");
            var obj = _.cloneDeep(animations["selected"].obj);
            obj.obj = levelSpecs.icon.icon;
            obj.startSizX = levelSpecs.icon.icon.sizX-20;
            animationsStack.push(obj);
        } else if (currentState == INIT) {
            monit.playSound("richiamo.mp3");
            var obj = _.cloneDeep(animations["selected"].obj);
            obj.obj = iconG.ds;
            obj.startSizX = iconG.ds.sizX-20;
            animationsStack.push(obj);
        }
    }
    monit.onToggleTracker = function (val) {
        if(val == 1) this.trackerActive = true;
        else this.trackerActive = false;
        if(levelSpecs && levelSpecs.bg) {
            levelSpecs.bg.fade();
        }
    }
}