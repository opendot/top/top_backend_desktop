var args = (new URL(location.href)).searchParams;
var clown,
sizeCharacter = {
    x: 200,
    y: 240
},
movingT = [],
animations = [],
transformation = 1,
// BACKGROUND COLOR FOR NORMAL AND TRANSFORMATION VIEW
// beige, green, rose, azure
bgColor = ["#f2ece2", "#acd88f", "#e8cadd", "#bedee5"],
bgAnimations = [],
trackingBgAnimations = [],
targetCount = 0,
toDelTgObject = [],
boxGame = {},
// json of game specification 
json = {},
targetDeleted = [],
loaded = 0,
nomi = [],
targetsTouched = [],
animationTargetTime = 700,
movingTargetDirs = [],
targetsAnimMovIndx = [],
GameSceneStep = 0,
remainingAnimals = [],
boxAnimIndx = 5,
assetsDim = {},
toggleSoundLimit = args.get('switchOffSound') == undefined ? true : args.get('switchOffSound') == 'true',
toggleCharacter = args.get('toggleCharacter') == undefined ? true : args.get('toggleCharacter') == 'true';
var characterStatus = ['', 'B', 'C'];
toggleCharacterCb = () =>{console.log('temp toggle character')}
firstStateButton = 0;
// VIBRATION BOX ON GAME START
BoxGame = function(x, y) {
    // this.sizX = size;
    // this.sizY = size;
    console.log(x, y)
    this.image = 'scatola-aperta.png'
    // monit.addImage('scatola-aperta-1')
    this.box = monit.addTarget(x, y, this.image);
    console.log(this.box)
    this.posX = this.box.posX;
    this.posY = this.box.posY;
    // this.siz = size;
    this.sinFreq = 5;
    this.amp = 0;
    this.vibInc = 1;
    this.vibIndex = 0;
    this.vibDur = 4000;
    this.vibration = false;
    this.current = 'scatola-aperta';
    var me = this;
    this.interval = setInterval(() => {
        me.vibration = !me.vibration;
    }, 1000);
    this.opened = 0;
    this.animals = [];
    this.animations = [];
    this.attentionTrigger = function(){
        var temp = _.cloneDeep(animations['attention'].obj);
        temp.timestamp = getCurrTM();
        temp.index = 255;
        this.animations.push(temp);
    }
    this.drawAnimations = function(){
        let me = this;
        this.animations.forEach(a =>{
            if(a.index <= 0){
            // if(getCurrTM() - animObj.timestamp > animObj.end){
                _.remove(me.animations, d => d.type == a.type)
                monit.getDistractors().concat(monit.getTargets()).forEach(el => {
                    jsonEl = monit.json.details.ingame_assets.filter(e => e.name == el.img)[0]
                    el.rot = 0;
                    if(el.img.indexOf('scatola') == -1){
                        el.sizX = (jsonEl.sizX / 2) * (window.innerWidth / monit.idealRes.width);
                        el.sizY = (jsonEl.sizY / 2) * (window.innerHeight / monit.idealRes.height);
                    }
                }); 
            }else{
                console.log(a)
                animations[a.type].cb(null, a);
            }
        })
    }
    this.draw = function() {
        // imageMode(CENTER);
        this.current = _.find(monit.getTargets(), d => monit.itemsNames[d.id] == this.image).img;
        rectMode(CENTER);
        fill(color('#f2ece2'));
        rect(this.box.posX - 50, this.box.posY + 50, this.box.sizX + 100, this.box.sizY + 20)
        noFill();
        // imageMode(CENTER);
        // image(monit.images[this.current], this.box.posX, this.box.posY, this.box.sizX, this.box.sizX);
        this.vibrate();
        this.drawAnimations();
    };
    // add alla character names
    // console.log(nomi)
    nomi.forEach(el =>{
        if(!monit.getDistractors().filter(d => monit.itemsNames[d.id] == el.name).length){
            let temp = monit.addDistractor(this.box.posX, this.box.posY, el.name);
            temp.sizX /= 2;
            temp.sizY /= 2;
        }
    });
    this.vibrate = function() {
        if(!this.opened){
            this.amp = 10*sin(map(this.vibIndex, 0, this.vibDur, 0, PI))
            this.box.posX = this.posX + this.amp * sin(map(this.vibIndex, 0, this.vibDur, 0, 5*2*PI))
            this.vibIndex += this.vibDur / 33;
            this.vibIndex %= this.vibDur;
        }else{
            if(this.box.posX != this.posX){
                this.box.posX = this.posX; 
            }
        }
    };
};
// class to create a jump in the center of the screen from the point where the target was
JumpingIn = function(start, obj){
    this.posX = start.x;
    this.posY = start.y;
    this.refX = start.x;
    this.refY = start.y;
    this.obj = obj;
    this.amp = this.refY - windowHeight / 2 < 0 ? 100 : windowHeight - (windowHeight - this.refY);
    this.dur = 1000;
    this.indS = 0;
    this.dir = this.posX > windowWidth / 2 ? -1 : 1;
    this.step = (Math.abs(this.refX - (windowWidth / 2))) / (this.dur / 33);
    this.coefficient = Math.abs(this.refX - (windowWidth / 2));
    this.done = 0;
    // draw jump in 
    this.draw = function(){
        switch(this.dir){
          case -1:
                if(this.obj.posX > windowWidth / 2){
                    this.obj.posX -= this.step
                    this.obj.posY = -this.amp * sin(map(this.obj.posX, this.refX, windowWidth / 2 , 0, PI)) + this.refY;
                }else{
                    this.done = 1;
                }
                break;
          case 1:
                if(this.obj.posX < windowWidth / 2){
                    this.obj.posX += this.step
                    this.obj.posY = -this.amp * sin(map(this.obj.posX, this.refX, windowWidth / 2 , 0, PI)) + this.refY;
                }else{
                    this.done = 1;
                }
                break;
        }
    }
}
var boxDistractorsAnim = [],
startGameState = 0,
rotationDisIndx = 0;
// ANIMATIONS TO GIVE TO CHARACTER CLASS
// me: character
// el: animation object
// a: animation suffix (for changing character)
// ATTENTION animation
animations['attention'] = {
    cb: function(me, el, a){
        var bigger = 20;
        // me.changeAnimation('attention');
        var els = monit.getDistractors().concat(monit.getTargets());
        // add character to animation
        var c = els.length;
        // els[c] = {posX: me.char.posX, posY: me.char.posY, sizX: me.sizX, sizY: me.sizY};
        // for(var e in els){
        //     fill(0,0,0, el.index);
        //     textSize(35);
        //     text('!!!', els[e].posX + els[e].sizX / 2, els[e].posY - els[e].sizY / 4);
        //     noFill();
        //     textSize(12);
        // }  
        els.forEach(tgds => {
            tgds.rot = (3.14 / 4) * sin(map(el.index, 255, 0, 0, 2 * PI));
            tgds.sizX = assetsDim[tgds.img].sizX + (40 * sin(map(el.index, 255, 0, 0, 2 * PI)))
            tgds.sizY = assetsDim[tgds.img].sizY + (40 * sin(map(el.index, 255, 0, 0, 2 * PI)) * (assetsDim[tgds.img].sizY / assetsDim[tgds.img].sizX))
        })
        el.index = el.index <= 0 ? 0 : el.index - el.max / ((el.end / 33));
    },
    obj: {
        type: 'attention', 
        index: 255, 
        max: 255,
        timestamp: getCurrTM(), 
        end: 1000
    }
};
animations['limitsReached'] = {
    cb: function(me, el, a) {
        let inc = 4
        // console.log('limits', el);
        // fill(200, 0, 0);
        // ellipse(me.char.posX, me.char.posY, 200);
        if(toggleSoundLimit) monit.playSound('limits.wav')
        if(getCurrTM() - el.timestamp < el.end / 2){
            el.index += 6;
        }else if(getCurrTM() - el.timestamp < el.end){
            el.index -= 6;
        }
        fill(el.color);
        noStroke();
        if(el.param == 'x'){
          // ellipse(windowWidth / 2, windowHeight / 2, 10 + el.index, 500);
            ellipse(el.posX, el.posY - 10, 10 + el.index, 500);
            me.char.posX += el.posX < windowWidth / 2 ? inc : -inc;
        }
        if(el.param == 'y'){
           // ellipse(windowWidth / 2, windowHeight / 2, 500, 10 + el.index); 
            ellipse(el.posX - 10, el.posY, 500, 10 + el.index); 
            me.char.posY += el.posY < windowHeight / 2 ? inc : -inc;
        }
        noFill();
        // console.log('effettone', el.posX, el.posY, el.index)
    },
    obj: {
        type: 'limitsReached', 
        index: 0,
        timestamp: getCurrTM(), 
        color: '',
        param: '',
        end: 500
    }
};
// TARGETS INTERACTION animation
animations['targetOverlap'] = { 
    cb: function(me, el, a) {
        fill(240, 240, 100, el.index);
        ellipse(el.pos.x, el.pos.y, 50 + ((-el.index + el.end) / (el.end / 200) ));
        el.index = el.index <= 0 ? 0 : el.index - (el.end / 30);
        // me.changeAnimation('happy' + a);
    },
    obj: {
        type: 'targetOverlap', 
        id: '', 
        index: 500,
        pos: {}, 
        timestamp: getCurrTM(), 
        end: 700
    }
};

animations['targetDisappear'] = {
    cb: function(me, el, a) {
        el.sizX = el.index < 4 ? 0 : el.index - el.max / ((el.end / 33));
        monit.pState.targets[el.id].sizX = el.index;
    },
    obj: {
        id: '',
        pos: {},
        type: 'targetDisappear',
        timestamp: getCurrTM(),
        max: 0,
        index: 500,
        end: 500
    }
}
// SLEEP CAUSE BY EYETRACKER DISCONNECTING
animations['sleep'] = {
    cb: function(targetCol, el, character){
        // remove all character animations
        character.trackingAnim = [];
        // remove check limit animation
        if(!isEmpty(clown)){
            _.remove(clown.trackingAnim, an => an.type == 'limitsReached');
        }
        var darker = hexToRgbA(targetCol);
        background(darker[0] - 40, darker[1] - 40, darker[2] - 40); 
        var tgs = monit.getTargets();
        var dis = monit.getDistractors();
        var els = tgs.concat(dis);
        // add eyetracker
        var last = els.length;
        if(character.char.visible){
            els[last] = {posX: character.char.posX, posY: character.char.posY, sizX: character.sizX, sizY: character.sizY};
        }
        var bgCol = hexToRgbA(targetCol);
        for(var x in els){
            // radius
            var r = 300;
            for (var y = 0; y < r; y++) {
                noStroke();
                fill(bgCol[0], bgCol[1] + 30, bgCol[2], y * (10 / r));
                ellipse(els[x].posX, els[x].posY, r - y, r - y);
            }
        }
        // set targets to sleep
        monit.getTargets().forEach(tg =>{
            if(tg.img != undefined){
                tg.img = monit.itemsNames[tg.id].replace('.png', '') + '-off.png';
            }
        });
        // set distractors to sleep
        monit.getDistractors().forEach(dis =>{
            if(dis.img != undefined){
                if(monit.itemsNames[dis.id].indexOf('char') !== -1){
                    dis.img = 'char-' + clown.currState + (clown.currState.length == 1 ? '-' : '') + 'off.png';
                }else{
                    dis.img = monit.itemsNames[dis.id].replace('.png', '') + '-off.png';
                }
            }
        });
        for(x in els){
            textSize(30);
            fill('#000');
            text('zZZ', els[x].posX - els[x].sizX / 2, els[x].posY - els[x].sizY / 2);
            textSize(12);
        }
        // empty tail on sleep
        character.tail = [];
    },
    obj: {
        type: 'sleep',
        timestamp: parseFloat(moment().add(1, 'years').format('x'))
    }
}
// ANIMATION FOR BACKGROUND
animations['bgTransformation'] = {
    cb: function(targetCol, el, charPos){
        // background(targetCol);
        monit.setBg(bgColor[transformation])
        var oldCol = transformation == 1 ? 3 : transformation - 1;
        fill(bgColor[oldCol]);
        ellipse(charPos.char.posX, charPos.char.posY, el.index * 2);
        el.index = el.index > 0 ? el.index -= el.max / ((el.end / 33)) : 0;
    },
    obj: {
        type: 'bgTransformation',
        timestamp: 0,
        index: Math.sqrt(window.innerWidth ** 2 + window.innerHeight ** 2),
        max: Math.sqrt(window.innerWidth ** 2 + window.innerHeight ** 2),
        end: 1000
    }
};

// CINEMA ANIMATION 
animations['cinema'] = {
    cb: function(col, el, me){
        // var inc = 2000 / (el.end / 30);
        // reopening
        if(el.index >= el.end / 4 ){
            var interval = assetsDim[boxGame.box.img].sizX,
            increment = interval / ((el.end / 7) / 33);
            if(boxGame.box.sizX < 10 || boxGame.box.sizY < 10){
                monit.removeTarget(boxGame.box);
            }else{
                boxGame.box.sizX -= increment;
                boxGame.box.sizY -= increment * (assetsDim[boxGame.box.img].sizY / assetsDim[boxGame.box.img].sizX);
            }
        }
        if(el.index >= el.end / 2){
            monit.setProcBg(bgColor[1]);
            if(monit.getTargetByName('scatola-aperta.png') !== undefined){
                monit.removeTarget(monit.getTargetByName('scatola-aperta.png'));
            }
            monit.drawTargets();
            // console.log('executing cinema', el.index, el.inc);
            if(!monit.GameScene){
                goToGameOne();
            }
        }
        push();
        fill(0,0,0,0);
        stroke('black')
        strokeWeight(4000)
        ellipse(el.posX, el.posY, map(el.index, 0, el.end, -8000, 0));
        pop();
        el.index += el.end / (el.end / 33);
    }, 
    obj:{
        type: 'cinema',
        index: 0,
        timestamp: 0,
        game: '',
        end: 4000
    }
}
// end game animation
animations['endGameBg'] = {
    cb: function(col, el, me){
        var startSiz = 200,
        endSiz = 5000,
        step = Math.abs(startSiz - endSiz) / (el.end / 33);
        push();
        imageMode(CENTER);
        image(monit.images['end-animation.png'], windowWidth / 2, windowHeight / 2, map(el.index, 0, el.end, 200, 6000), map(el.index, 0, el.end, 200, 6000));
        pop();
        if(el.index > el.end * (4/5)){
            monit.setBg(bgColor[0]);
        }
        el.index += step;
    },
    obj: {
        type: 'endGameBg',
        index: 0,
        timestamp: 0,
        end: 2000
    }
}


// DRAW BACKGROUND
function drawBg(){
    if(monit.pState.bg && monit.pState.bg != null){
        if(monit.trackerActive){
            monit.drawPrBg()
            // background(color(bgColor[transformation]));
            // draw background when eyetracker disconnected
            if(trackingBgAnimations.filter(d => d.type === 'sleep').length > 0){
                _.remove(trackingBgAnimations, d => d.type === 'sleep');
                // trackingBgAnimations.splice(trackingBgAnimations.filter(d => d.type === 'sleep')[0], 1);
            }
            // insert sleep animation if not present in background animation array
        }else{
            if(!trackingBgAnimations.filter(d => d.type === 'sleep').length && monit.GameScene){
                if(_.find(clown.trackingAnim, a => a.type == 'attention') != undefined){
                    console.log('inside')
                    resetRotationTgsDs();
                }
                var temp = _.cloneDeep(animations['sleep'].obj)
                temp.timestamp = parseFloat(moment().add(1, 'years').format('x'));
                trackingBgAnimations.push(temp);
            }
        }
    }
    // BACKGROUND ANIMATIONS
    trackingBgAnimations.forEach((el, i)=> {
        // console.log(getCurrTM() - el.timestamp)
        if(getCurrTM() - el.timestamp > el.end){
            trackingBgAnimations.splice(el, 1);
        }else{
            animations[el.type].cb(bgColor[transformation], el, clown);
        }
    });
}

// TRIGGER BACKGROUND ANIMATION
function triggerBgAnimation(){
    transformation = transformation == 3 ? 1 : transformation + 1;
    clown.currState = characterStatus[transformation - 1];
    monit.itemsNames[clown.char.id] = 'char' + (!clown.currState.length ? '' : '-' ) + clown.currState + '.png';
    var temp = _.cloneDeep(animations['bgTransformation'].obj);
    temp.timestamp = getCurrTM();
    temp.index = Math.sqrt(windowWidth ** 2 + windowHeight ** 2);
    // console.log(temp)
    trackingBgAnimations.push(temp);
}
// DRAW EYETRACKER ELEMENT
function drawEyeTracker(){
    if(monit.trackerActive){
        // monit.eyeTracker.posX += (mouseX - monit.eyeTracker.posX) * monit.eyeTracker.easing;
        // monit.eyeTracker.posY += (mouseY - monit.eyeTracker.posY) * monit.eyeTracker.easing;
        // console.log('prima', monit.eyeTracker.posX, monit.eyeTracker.posY)
        monit.eyeTracker.posX = monit.gaze[0] * window.innerWidth;
        monit.eyeTracker.posY = monit.gaze[1] * window.innerHeight;
        // console.log('dopo', monit.eyeTracker.posX, monit.eyeTracker.posY)
        // console.log(monit.eyeTracker);
        fill('rgba(255,0,0, 0.25)');
        ellipse(monit.eyeTracker.posX, monit.eyeTracker.posY, monit.eyeTracker.sizX, monit.eyeTracker.sizY);
    }
}
// HANDLE DELETE TARGET ANIMATION
function targetDelAnimation(curr){
    // draw disappear image
    curr.obj.sizX -= (curr.sizX / 7);
    curr.obj.sizY -= (curr.sizY / 7);
    imageMode(CENTER);
    // console.log(curr.obj.img);
    image(monit.images[curr.obj.img], curr.obj.posX, curr.obj.posY, curr.obj.sizX, curr.obj.sizY);
    curr.indx--;
    // console.log(curr.obj.sizX, curr.obj.sizY)
    if(curr.indx < 0){
        targetDeleted.splice(targetDeleted.indexOf(curr), 1);
    }
}
if (Tone.context.state !== 'running') {
    Tone.context.resume();
}
function setAssetsSizes(){
    monit.json.details.ingame_assets.forEach(el => {
        assetsDim[el.name] = {sizX: el.sizX * (window.innerWidth / monit.idealRes.width), sizY: el.sizY * (window.innerHeight / monit.idealRes.height)}
    });
    console.log('assets dimension', assetsDim);
}
// PRELOAD
function preload() {
    // monit.addImage('img/chiusa.png', 'chiusa');
    // monit.addImage('img/aperta.png', 'aperta');
    // can load only character A temorary
    // if(localStorage.character !== undefined && localStorage.character !== 'char-A.png') monit.loadCharEnable = false
    monit.loadJsonItems();
}
// SETUP
function setup() {
    createCanvas(windowWidth,windowHeight);
    // console.log(characterParam);
    // NEW CHARACTER CREATION
    noStroke();
    frameRate(30);
    monit.onChangeDynamics = data => {
        console.log('data', data);
        var type = data.id;
        switch(type){
            case 'ft':
                monit.fixationTime = parseInt(data.value);
                break;
            case 'characterFollow':
                monit.eyeTracker.easing = 20 / parseFloat(data.value);
                break;
            case 'switchOffSound':
                toggleSoundLimit = data.value;
                break;
            case 'toggleCharacter':
                toggleCharacter = data.value;
                toggleCharacterCb();
                break;
        }
    }
    monit.setProcBg(bgColor[0]);
    monit.setCharacterSize(sizeCharacter.x, sizeCharacter.y);

    monit.onAddTarget = (x,y,type) => {
        if(monit.GameScene == 1){
            monit.addTarget(x*window.innerWidth,y*window.innerHeight, type)
        }
    };
    monit.onRemoveTarget = (id) => {
        if(monit.GameScene == 1){
            monit.removeTargetById(id)
        }else if(!isEmpty(boxGame)){
            boxGame.opened = 1;
        }
    };
    monit.onMoveTarget = (ob) => {
        if(monit.GameScene == 1){
            monit.moveTarget(ob) 
        }
    };
    monit.onBackNext = dir => {
        if(dir == 'next' && !monit.GameScene){
            monit.GameScene = 1;
        }
    }
    monit.skipIntro = 0;
}
indexAttention = 0;
// DRAW
function draw() {
    if(monit.loaded){
        if(monit.skipIntro){
            goToGameOne();
            monit.skipIntro = 0;
        }else if(!firstStateButton){
            monit.sendEvent({ts: new Date().getTime(), type: "event", event_type: "buttons_state", data: {cooldown: false, back: false, next: true}});
            firstStateButton = 1;
        }
        if(isEmpty(assetsDim)){setAssetsSizes()}
        drawBg();
        switch(monit.GameScene){
            case 0:
                // GAME INTRO
                    // stare at box
                    if(isEmpty(boxGame)){
                        // add moving and transformation distractors to arrays
                        monit.json.levels[0].scenes[monit.GameScene].distractors.forEach( dis => {
                            nomi.push({name: dis.name, type: dis.type});
                        });
                        console.log('nomi', nomi);
                        boxGame = new BoxGame((windowWidth) / 2, (windowHeight) / 2);
                        // boxGame.box.sizX *= 2;
                        // boxGame.box.sizY *= 2;
                        monit.onAttentionSig = ()=>{
                            boxGame.attentionTrigger();
                            monit.playSound('richiamo.mp3');
                        }
                    }
                    // console.log(monit.eyeTracker.posX, monit.eyeTracker.posY, boxGame.box.posX, boxGame.box.posY)
                    // first step start game
                    if(!boxGame.opened){
                        var eyeT = monit.eyeTracker;
                        if(checkOverlap({posX: eyeT.posX + boxGame.box.sizX / 2, posY: eyeT.posY + boxGame.box.sizY / 2, sizX: eyeT.sizX, sizY: eyeT.sizY}, boxGame.box)){
                            console.log('vibration disabled');
                            clearInterval(boxGame.interval);
                            boxGame.vibration = false;
                            // box opened flag
                            boxGame.opened = 1;
                        }
                    }else{
                        if(boxGame.box.img !== 'scatola-aperta-5.png'){
                            // console.log('')
                            monit.animateTarget(boxGame.box, '', 5);
                        }else{
                            // boxGame.box = 'scatola'
                            // animals out one by one
                            let distractors = monit.getDistractors();
                            let time = -500;
                            monit.getDistractors().forEach((ds, i) =>{
                                if(boxGame.animals[ds.id] == undefined){
                                    boxGame.animals[ds.id] = 0;
                                    time += 500 + parseInt(random(250)) + 1;
                                    setTimeout(()=>{
                                        boxGame.animals[ds.id] = 1;
                                    }, time);
                                }
                                if(ds.posY < -ds.sizY){
                                    monit.removeDistractor(ds);
                                }
                                if(boxGame.animals[ds.id]){
                                    ds.posY -= 10;
                                    ds.sizX += 1;
                                    ds.sizY += 1;
                                }
                            });

                            if(!monit.getDistractors().length){
                                // add cinema animation
                                if(!trackingBgAnimations.filter( d => d.type == 'cinema').length){
                                    var animationCinema = _.cloneDeep(animations['cinema'].obj);
                                    animationCinema.timestamp = getCurrTM();
                                    animationCinema.posX = windowWidth / 2;
                                    animationCinema.posY = windowHeight / 2;
                                    trackingBgAnimations.push(animationCinema);
                                }
                                break;
                            }
                        }
                    }
                monit.drawDistractors();
                if(!isEmpty(boxGame)){
                    boxGame.draw();
                }
                monit.drawTargets();
                // drawEyeTracker()
                break;
            case 1: 
                // GAME
                if(isEmpty(clown)){
                    goToGameOne();   
                }
                // ATTENTION SIGNAL handling
                if(monit.attentionSig){
                    clown.attentionTrigger();
                    monit.attentionSig = 0;
                }
                if(!trackingBgAnimations.filter(a => a.type == 'cinema').length){
                    clown.draw();
                }else{
                    clown.char.posX = windowWidth / 2;
                    clown.char.posY = windowHeight / 2;
                }
                // insert a target inside animation target structure
                monit.fixationObjects.forEach( el => {
                    if(_.find(targetsTouched, touch => touch.id == el.id) == undefined){
                        // add fixation object to target animation array
                        // monit.sendEvent({ts: new Date().getTime(), type: 'event', event_type: 'reach_target', data: el.id});
                        targetsTouched.push(_.cloneDeep(el));
                    }
                });
                // TARGETS ANIMATIONS
                monit.getTargets().forEach( el => {
                    var currFixationObj = _.find(targetsTouched, d => d.id === el.id);
                    var currTgName = monit.itemsNames[el.id];
                    if(currFixationObj != undefined){
                        if(currFixationObj.counter > animationTargetTime){
                            // END FIXATION TIME TARGET
                            // monit.playSound('win.wav');
                            // add target to delete to another structure to make disappear animation
                            var crObj = _.cloneDeep(el)
                            targetDeleted.push({
                                obj: crObj, 
                                indx: 5,
                                sizX: crObj.sizX,
                                sizY: crObj.sizY
                            });
                            monit.removeTarget(el);
                            // make transformation if got a transformation target
                            console.log(currTgName)
                            if(nomi.filter(d => d.name == currTgName)[0].type == 'transformation'){
                                triggerBgAnimation();
                            }
                            targetCount++;
                            // woo sound
                            if(targetCount % 3 === 0){
                                monit.playSound('woo.wav');
                            }                            
                            // remove element from animation array
                            delete monit.targetsAnimIndx[el.id];
                            _.remove(monit.targetsAnimIndx, d => d == null);
                            // delete target from fixation objects
                            _.remove(monit.fixationObjects, (d) => d.id === el.id);
                            // delete target also in the animation target structure
                            _.remove(targetsTouched, (d) => d.id === el.id);
                        }else{
                            monit.playSound(currTgName.replace('.png', '.wav')); 
                            // animate target on 
                            monit.animateTarget(el)
                            currFixationObj.counter += 33;
                        }
                    // move moving target animation
                    }else if(nomi.filter(d => d.name == monit.itemsNames[el.id])[0].type == 'moving'){
                        if(monit.trackerActive){
                            moveTarget(el);
                        }
                    // sleep animation
                    }else if(monit.trackerActive){
                        if( monit.itemsNames[el.id] !==  el.img){
                            el.img = monit.itemsNames[el.id];
                            // remove element from animation array
                            delete monit.targetsAnimIndx[el.id];
                            _.remove(monit.targetsAnimIndx, d => d == null);
                        }
                    }
                    checkReachTarget();
                    // if(monit.json.levels[0].scenes[1].targets)
                });
                break;
            case 2: 
                // GAME END 
                switch(GameSceneStep){
                    case 0: 
                        var animationDur = 2000;
                        console.log('parole');
                        // add BOX
                        if(monit.getDistractorByName('scatola-aperta-5.png') === undefined){
                            var box = monit.addDistractor(windowWidth / 2, windowHeight / 2, 'scatola-aperta-5.png');
                        }else{
                            box = monit.getDistractorByName('scatola-aperta-5.png');
                        }
                        if(isEmpty(remainingAnimals)){
                            monit.getTargets().forEach( tg => {
                                remainingAnimals[tg.id] = {
                                    id: tg.id,
                                    startPosX: tg.posX, 
                                    startPosY: tg.posY, 
                                    endPosX: tg.posX > windowWidth / 2 ? parseInt(random((windowWidth / 4) * 3 - 200, ((windowWidth / 4) * 3) + 200)) : parseInt(random((windowWidth / 4) - 200, (windowWidth / 4) + 200)),
                                    endPosY: windowHeight / 2 + 20,
                                    done: 0
                                };
                            });
                            // console.log('dentro', remainingAnimals)
                            box.sizX *= 2;
                            box.sizY *= 2;
                        }
                        // console.log('remainingAnimals', remainingAnimals);
                        for(var x in remainingAnimals){
                            if(!remainingAnimals[x].done){
                                var incX = (remainingAnimals[x].endPosX - remainingAnimals[x].startPosX) / (animationDur / 33),
                                incY = (remainingAnimals[x].endPosY - remainingAnimals[x].startPosY) / (animationDur / 33);
                                if((incX <= 0 && monit.pState.targets[remainingAnimals[x].id].posX > remainingAnimals[x].endPosX) ||
                                    incX > 0 && monit.pState.targets[remainingAnimals[x].id].posX < remainingAnimals[x].endPosX){
                                    monit.pState.targets[remainingAnimals[x].id].posX += incX;
                                }else{
                                    remainingAnimals[x].done = 1;
                                }
                                if((incY <= 0 && monit.pState.targets[remainingAnimals[x].id].posY > remainingAnimals[x].endPosY) ||
                                    incY > 0 && monit.pState.targets[remainingAnimals[x].id].posY < remainingAnimals[x].endPosY){
                                    monit.pState.targets[remainingAnimals[x].id].posY += incY;
                                }else{
                                    remainingAnimals[x].done = 1;
                                }
                            }
                        };
                        clown.changeAnimation(clown.currState + '-hurra-3')
                        // at the end of end game animation, go to the next part
                        if(!trackingBgAnimations.filter(a => a.type = 'endGameBg').length){
                            GameSceneStep = 1;
                        }
                        break;
                        // animals go back into the box
                    case 1:
                        if(remainingAnimals.filter(a => a.done == 1).length == remainingAnimals.length){
                            var temp = [];
                            monit.getTargets().forEach( t => {
                                temp.push(new JumpingIn({x: t.posX, y: t.posY}, t));
                            });
                            remainingAnimals = temp;
                            GameSceneStep = 2;
                        }
                        break;
                    case 2:
                        remainingAnimals.forEach( an => {
                            an.draw();
                        });
                        if(remainingAnimals.filter(el => el.done == 1).length == remainingAnimals.length){
                            remainingAnimals.forEach( an => {
                                monit.removeTarget(an.obj);
                            });
                            box = monit.getDistractors().filter(d => d.img.indexOf('scatola-aperta') !== -1)[0];
                            if(boxAnimIndx > 0){
                                box.img = 'scatola-aperta-' + boxAnimIndx + '.png';   
                                boxAnimIndx--;
                            }else{
                                box.img = 'scatola-aperta.png';
                                if(box.sizX > 25 && box.sizY > 25){
                                    box.sizX -= 20;
                                    box.sizY -= 20 * (box.sizY / box.sizX);
                                }else{
                                    monit.removeDistractor(box);
                                    clown.startPosX = clown.char.posX,
                                    clown.startPosY = clown.char.posY,
                                    clown.endPosX = windowWidth / 2,
                                    clown.endPosY = windowHeight / 2;
                                    GameSceneStep = 3;
                                }
                            }
                        }
                        break;
                    case 3:
                        var incX = (clown.endPosX - clown.startPosX) / (1000 / 33),
                        incY = (clown.endPosY - clown.startPosY) / (1000 / 33);
                        if((incX <= 0 && clown.char.posX > clown.endPosX) ||
                            incX > 0 && clown.char.posX < clown.endPosX){
                            clown.char.posX += incX;
                        }else{
                            clown.done = 1;
                            GameSceneStep = 4;
                        }
                        if((incY <= 0 && clown.char.posY > clown.endPosY) ||
                            incY > 0 && clown.char.posY < clown.endPosY){
                            clown.char.posY += incY;
                        }else{
                            clown.done = 1;
                            GameSceneStep = 4;
                        }
                        monit.animateDistractor(clown.char, '-hurra');
                        break;
                    case 4:
                        if(clown.char.sizX > 15 && clown.char.sizY > 15){
                            monit.animateDistractor(clown.char, '-hurra');
                            clown.char.sizX -= 10;
                            clown.char.sizY -= 10 * (clown.char.sizY / clown.char.sizX);
                        }else{
                            monit.removeTarget(clown.char);
                            // clown
                            // monit.onEndSession();
                        }
                        break;
                }
                break;
        }
        // DRAW TARGETS, DISTRACTORS AND EYETRACKER
        monit.drawDistractors();
        monit.drawTargets();
        if(GameSceneStep == 2 || GameSceneStep == 1){
            monit.drawItems([monit.getDistractorByName('scatola-aperta-5.png')]);
        }
        // deleting target animation
        for(x in targetDeleted){
            targetDelAnimation(targetDeleted[x]);
        }
        // drawEyeTracker();
        // monit.monitoring();
        // monit.drawTracker();
    }else{
        background('black');
        textSize(30);
        stroke('white');
        fill('white');
        textAlign(CENTER, CENTER);
        text('Caricamento in corso...', windowWidth / 2, windowHeight / 2);
        textSize(12);
        noFill();
        noStroke();
    }
}
function goToGameOne(){
    console.log('go to fixation part');
    monit.pState.targets = {};
    monit.pState.distractors = {};
    monit.setProcBg(bgColor[1]);
    var characterParam = {posX: monit.eyeTracker.posX, posY: monit.eyeTracker.posY, sizX: 0.7, sizY: 0.7}
    clown = new Character(characterParam, animations, toggleCharacter);
    toggleCharacterCb = ()=>{clown.toggleCharacter()};
    // monit.eventconnection.send({'changeScene': 1});
    monit.GameScene = 1;
    // add targets to arrays
    nomi = [];
    // add targets name for the current scene
    monit.json.levels[0].scenes[monit.GameScene].targets.forEach( dis => {
        nomi.push({name: dis.name, type: dis.type});
    });
    monit.onAttentionSig = ()=>{clown.attentionTrigger();}
    // send change scene to tablet 
    monit.sendEvent({ts: new Date().getTime(), type: "event", event_type: "buttons_state", data: {cooldown: false, back: false, next: false}});
    changeScene();
}
var sendedChangeScene = 0;
function changeScene(){
    if(!sendedChangeScene){
        console.log('try change scene')
        setTimeout(()=>{
            if(monit.eventconnection.readyState === 1){
                monit.eventconnection.send(JSON.stringify({ts: new Date().getTime(), type: "event", event_type: "change_scene", data: 1}));
                sendedChangeScene = 1;
            }else{
                changeScene();
            }
        }, 100);
    }
}
function goToEnd(){
    monit.GameScene = 2;
    if(!monit.getTargets().length){
        GameSceneStep = 3;
        clown.startPosX = clown.char.posX;
        clown.startPosY = clown.char.posY;
        clown.endPosX = windowWidth / 2;
        clown.endPosY = windowHeight / 2;
    }else{
        monit.addDistractor(windowWidth / 2, windowHeight / 2, 'scatola-aperta-5.png');
    }
    var temp = _.cloneDeep(animations['endGameBg'].obj);
    temp.timestamp = getCurrTM();
    trackingBgAnimations.push(temp);
}
// setInterval(()=>{
//     var nome = nomi[parseInt(Math.random() * 10)];
//     var obj = targetsList.filter(d => monit.itemsNames[d.id] == nome)[0];
//     monit.addTarget(random(windowWidth - 100), random(windowHeight - 100), obj.sizX, obj.sizY, nome);
// }, 500)
// KEY PRESSED FUNCTION
function keyPressed(){
    // console.log(keyCode)
    switch(keyCode){
        case UP_ARROW:
            console.log('pushed up')
            // transformation = transformation == 3 ? 1 : transformation + 1;
            // clown.currState = characterStatus[transformation - 1];

            // triggerBgAnimation();
            monit.onAttentionSig();
            break;
        case DOWN_ARROW:
            console.log('pushed down');
            // transformation = transformation == 0 ? 3 : transformation - 1;
            // clown.currState = characterStatus[transformation - 1];
            triggerBgAnimation();
            break;
            // MANUAL SKIP INTRO
        case RIGHT_ARROW:
            console.log('right arrow');
            if(!monit.GameScene){
                goToGameOne();
            }else{
                goToEnd();
            }
            break;
        case SHIFT:
            // monit.addTarget(random(windowWidth - 100), random(windowHeight - 100), 180, 150, 'tartaruga');
            // var nome = "struzzo";
            var nome = nomi[parseInt(Math.random() * 10)];
            var addedTg = monit.addTarget(random(windowWidth - 100), random(windowHeight - 100), nome.name);
            console.log('type', nomi.filter(d => d.name == monit.itemsNames[addedTg.id])[0].type);
            // add target data to moving target structure
            if(nomi.filter(d => d.name == monit.itemsNames[addedTg.id])[0].type == 'moving' && movingTargetDirs[addedTg.id] === undefined){
                movingTargetDirs[addedTg.id] = {sx: 0, sy: 0, direction: 'right', indx: 0};
            }
            break;
    }
}
function isEmpty(obj) {
    for(var key in obj) {
        if(obj.hasOwnProperty(key))
            return false;
    }
    return true;
}
// function that allow targets to moving
function moveTarget(obj){
    var id = obj.id;
    // create structure for moving if not defined
    if(movingTargetDirs[id] == undefined){movingTargetDirs[id] = {sx: 0, sy: 0, direction: 'right', indx: 0};}
    if(monit.counter % 2 == 0){
        if(targetsAnimMovIndx[id] === undefined){
            targetsAnimMovIndx[id] = {inc: 1, val:1};
        }
        var counter = targetsAnimMovIndx[id].val;
        // ahead and back
        // if(counter > 4){
        //     targetsAnimMovIndx[id].inc = -1;
        // }else if(counter < 2){
        //     targetsAnimMovIndx[id].inc = 1;
        // }
        // ahead and reset 
        if(counter > 4){
            targetsAnimMovIndx[id].val = 1;
        }
        targetsAnimMovIndx[id].val += targetsAnimMovIndx[id].inc;
        monit.pState.targets[id].img = monit.itemsNames[id].split('.')[0] + '-movimento-' + counter + '.png';
    }
    // assign a new direction every 15 cicle of draw
    _.remove(movingTargetDirs, d => d === null)
    if(movingTargetDirs[id].indx == 30){
        movingTargetDirs[id].direction = randMov(monit.pState.targets[id], movingTargetDirs[id].direction);
        movingTargetDirs[id].indx = 0
    }
    targetEaseMove(movingTargetDirs[id], movingTargetDirs[id].direction);
    monit.pState.targets[id].posX += movingTargetDirs[id].sx;
    monit.pState.targets[id].posY += movingTargetDirs[id].sy;
    movingTargetDirs[id].indx++;
    // console.log('moving', monit.pState.targets[id].nam);
}

// fading movement of distractors
function targetEaseMove(main, dir){
    var acc = 0.4, maxSp = 3, inert = 0.2;
    switch(dir){
        case 'right': 
            if(main.sy < 0){
                main.sy += inert
            }
            if(main.sy > 0){
                main.sy -= inert
            }
            if(main.sx < maxSp){
                main.sx += acc;
            }
            break;
        case 'left': 
            if(main.sy < 0){
                main.sy += inert
            }
            if(main.sy > 0){
                main.sy -= inert
            }
            if(main.sx > -maxSp){
                main.sx -= acc;
            }
            break;
        case 'up':
            if(main.sx < 0){
                main.sx += inert
            }
            if(main.sx > 0){
                main.sx -= inert
            }
            if(main.sy > -maxSp){
                main.sy -= acc;
            }
            break;    
        case 'down':
            if(main.sx < 0){
                main.sx += inert
            }
            if(main.sx > 0){
                main.sx -= inert
            }
            if(main.sy < maxSp){
                main.sy += acc;
            }
            break;
    }
}

// change movement of ditractors
function randMov(main, currAnim){
    var ar = ['right', 'left', 'up', 'down'],
    offx = 100;
    if(main.posX > windowWidth - offx){
        // ar.splice(ar.indexOf('right'), 1);
        ar = ['left'];
    }
    if(main.posX < offx){
        // ar.splice(ar.indexOf('left'), 1);
        ar = ['right'];
    }   
    if(main.posY > windowHeight - offx){
        // ar.splice(ar.indexOf('down'), 1);
        ar = ['up'];
    }
    if(main.posY < offx){
        // ar.splice(ar.indexOf('up'), 1);
        ar = ['down'];
    }
    currAnim = ar[parseInt(random(ar.length))]
    // console.log('go', currAnim);
    return currAnim;
}
var currReachedTargets = [];
function checkReachTarget(){
    var eyet = monit.eyeTracker;
    monit.getTargets().forEach(tg => {
        var overlapF = checkOverlap({posX: eyet.posX + tg.sizX / 2, posY: eyet.posY + tg.sizY / 2, sizX: eyet.sizX, sizY: eyet.sizY}, tg)
        if(overlapF && currReachedTargets.indexOf(tg.id) == -1){
            monit.sendEvent({ts: new Date().getTime(), type: "event", event_type: "start_fixation", data: tg.id});
            currReachedTargets.push(tg.id)
        }
        if(!overlapF && currReachedTargets.indexOf(tg.id) != -1){
            monit.sendEvent({ts: new Date().getTime(), type: "event", event_type: "end_fixation", data: tg.id});
            _.remove(currReachedTargets, e => e == tg.id);
        }
    })
}
function resetRotationTgsDs(){
    monit.getDistractors().concat(monit.getTargets()).forEach(el => {
        jsonEl = monit.json.details.ingame_assets.filter(e => e.name == el.img)[0]
        el.rot = 0
        el.sizX = jsonEl.sizX * (window.innerWidth / monit.idealRes.width);
        el.sizY = jsonEl.sizY * (window.innerHeight / monit.idealRes.height);
    });  
}