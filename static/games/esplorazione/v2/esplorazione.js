var sizeCharacter = {
    x: 200,
    y: 240
},
animations = [],
backgroundImg = '#fff',
// backgroundImg = 'sfondo-intro.png',
animationsStack = [],
// type of game (rocket, submarine, car)
GameSceneTypeFix = 0,
// game stage ('choose vehicle' -> 'game-fixation' -> 'game-transition-animation' -> 'game-return-path')
GameSceneStage = 0,
GameTransitionAnimStep = 0,
loaded = 0,
indx = 0,
assetsDim = {},
instrumentsData = [],
instrumentsStack = [],
completedInstruments = [],
indxHue = 0,
rainbowHaloDim = 800,
elDimensions = parseFloat((new URL(location.href)).searchParams.get('elDimensions')),
firstStateButtons = 0,
firstEmittedSoundTime = new Date().getTime(),
quantizeSoundTo = 500;
var bgEyeTracker = {};
if(elDimensions == 0){
    elDimensions += 0.2;
}
function quantizeSound(audio){
    setTimeout(()=>{
        monit.playSound(audio);
    }, quantizeSoundTo - (new Date().getTime()) % quantizeSoundTo);
}
// GUITAR
instrumentsData['guitar.png'] = {
    name: 'guitar.png',
    parts:{
        'handle.png':  {
            name : 'handle.png',
            posX : 0,
            posY : -100
        },
        'body.png':  {
            name : 'body.png',
            posX : 0,
            posY : 100
        },
        'hole.png':  {
            name : 'hole.png',
            posX : 0,
            posY : 50
        },
        'bone.png':  {
            name : 'bone.png',
            posX : 0,
            posY : 160
        },
        'keys.png':  {
            name : 'keys.png',
            posX : 0,
            posY : -290
        },
        'strings.png':  {
            name : 'strings.png',
            posX : 0,
            posY : -75
        }
    },
    global: {
        posX: 500,
        posY: 700,
        sizX: 200,
        sizY: 600,
        audio: ["guitar-1.wav", "guitar-2.wav", "guitar-3.wav", "guitar-4.wav", "guitar-5.wav", "guitar-6.wav", "guitar-7.wav", "guitar-8.wav", "guitar-9.wav", "guitar-10.wav"]
    }
};
// MARACAS
instrumentsData['maracas.png'] = {
    name: 'maracas.png',
    parts: {
        'handle-right-mar.png':  {
            name : 'handle-right-mar.png',
            posX : 250,
            posY : 150
        },
        'handle-left-mar.png':  {
            name : 'handle-left-mar.png',
            posX : -230,
            posY : 150
        },
        'egg-right.png':  {
            name : 'egg-right.png',
            posX : 170,
            posY : -50
        },
        'egg-left.png':  {
            name : 'egg-left.png',
            posX : -130,
            posY : -50
        },
        'stripes-right.png':  {
            name : 'stripes-right.png',
            posX : 170,
            posY : -35
        },
        'stripes-left.png':  {
            name : 'stripes-left.png',
            posX : -130,
            posY : -35
        },   
    },
    global: {
        posX: 200,
        posY: 200,
        sizX: 600,
        sizY: 400,
        audio: ['maracas1.wav', 'maracas2.wav']
    }
};
// PIANO
instrumentsData['piano.png'] = {
    name: 'piano.png',
    parts: {
        'keyboard.png':  {
            name : 'keyboard.png',
            posX : 25,
            posY : -10
        },
        'piano-1.png':  {
            name : 'piano-1.png',
            posX : -270,
            posY : 30
        }, 
        'piano-2.png':  {
            name : 'piano-2.png',
            posX : -63,
            posY : 30
        }, 
        'piano-3.png':  {
            name : 'piano-3.png',
            posX : 145,
            posY : 30
        }, 
        'piano-4.png':  {
            name : 'piano-4.png',
            posX : 352,
            posY : 30
        }, 
        'piano-mid-1.png':  {
            name : 'piano-mid-1.png',
            posX : -270,
            posY : 10
        }, 
        'piano-mid-2.png':  {
            name : 'piano-mid-2.png',
            posX : -65,
            posY : 10
        }, 
        'piano-mid-3.png':  {
            name : 'piano-mid-3.png',
            posX : 145,
            posY : 10
        }, 
        'piano-mid-4.png':  {
            name : 'piano-mid-4.png',
            posX : 350,
            posY : 10
        }
    },
    global: {
        posX: 900,
        posY: 200,
        sizX: 800,
        sizY: 300,
        audio: ["piano-1.wav", "piano-2.wav", "piano-3.wav", "piano-4.wav", "piano-5.wav", "piano-6.wav", "piano-7.wav", "piano-8.wav"] 
    }
};
// SAXOPHONE
instrumentsData['saxophone.png'] = {
    name: 'saxophone.png',
    parts: {
        'saxophone-body.png':{
            name: "saxophone-body.png",
            posX: 0,
            posY: 0
        },
        'sax-hole1.png':{
            name: "sax-hole1.png",
            posX:  74,
            posY:  -200,
        },
        'sax-hole2.png':{
            name: "sax-hole2.png",
            posX:  74,
            posY:  -160
        },
        'sax-hole3.png':{
            name: "sax-hole3.png",
            posX:  73,
            posY:  -110,
        },
        'sax-hole4.png':{
            name: "sax-hole4.png",
            posX:  73,
            posY:  -20,
        },
        'sax-hole5.png':{
            name: "sax-hole5.png",
            posX:  72,
            posY:  70,
        },
        'sax-hole6.png':{
            name: "sax-hole6.png",
            posX:  72,
            posY:  170,
        },
        'sax-hole7.png':{
            name: "sax-hole7.png",
            posX:  -90,
            posY:  60,
        },
        'sax-hole8.png':{
            name: "sax-hole8.png",
            posX:  -75,
            posY:  150,
        }
    },
    global: {
        posX: 700,
        posY: 400,
        sizX: 700,
        sizY: 200,
        audio: ["sax-1.wav", "sax-2.wav", "sax-3.wav", "sax-4.wav", "sax-5.wav", "sax-6.wav", "sax-7.wav"]
    }
};
// DRUM
instrumentsData['drum.png'] = {
    name: 'drum.png',
    parts:{
        'leather.png':{
            name: "leather.png",
            posX: 0,
            posY: -100
        },
        'wood.png':{
            name: "wood.png",
            posX: 0,
            posY: 90
        },
        'skin-1.png':{
            name: "skin-1.png",
            posX: -205,
            posY: 118
        },
        'skin-2.png':{
            name: "skin-2.png",
            posX: 0,
            posY: 140
        },
        'skin-3.png':{
            name: "skin-3.png",
            posX: 205,
            posY: 118
        }
    },
    global: {
        posX: 1000,
        posY: 400,
        sizX: 500,
        sizY: 400,
        audio: ["drum-1.wav", "drum-2.wav", "drum-3.wav", "drum-4.wav"]
    }
};
// TRUMPET
instrumentsData['trumpet.png'] = {
    name: 'trumpet.png',
    parts:{
        'rod.png':{   
            name: "rod.png",
            posX: 40,
            posY: -40
        },
        'lead-pipe.png':{
            name: "lead-pipe.png",
            posX: 0,
            posY: 70
        },
        'bell.png':{   
            name: "bell.png",
            posX: 355,
            posY: -40
        },
        'pumps.png':{   
            name: "pumps.png",
            posX: -30,
            posY: 20
        },
        'buttons.png':{   
            name: "buttons.png",
            posX: -30,
            posY: -100
        },
        'reed-trumpet.png':{   
            name: "reed-trumpet.png",
            posX: -330,
            posY: 20
        }
    },
    global: {
        posX: 1000,
        posY: 400,
        sizX: 500,
        sizY: 400,
        audio: ["trumpet-1.wav", "trumpet-2.wav", "trumpet-3.wav", "trumpet-4.wav", "trumpet-5.wav", "trumpet-6.wav", "trumpet-7.wav"]
    }
};
// XYLOPHONE
instrumentsData['xylophone.png'] = {
    name: 'xylophone.png',
    parts:{
        'xil-bar1.png':{
            name: "xil-bar1.png",
            posX: 0,
            posY: -50
        },
        'xil-bar2.png':{
            name: "xil-bar2.png",
            posX: 0,
            posY: 60
        },
        'xil-1.png':{   
            name: "xil-1.png",
            posX: -250,
            posY: 0
        },
        'xil-2.png':{   
            name: "xil-2.png",
            posX: -150,
            posY: 0
        },
        'xil-3.png':{   
            name: "xil-3.png",
            posX: -50,
            posY: 0
        },
        'xil-4.png':{   
            name: "xil-4.png",
            posX: 50,
            posY: 0
        },
        'xil-5.png':{   
            name: "xil-5.png",
            posX: 150,
            posY: 0
        },
        'xil-6.png':{   
            name: "xil-6.png",
            posX: 250,
            posY: 0
        },
    },
    global: {
        posX: 500,
        posY: 400,
        sizX: 500,
        sizY: 400,
        audio: ["xil-1.wav", "xil-2.wav", "xil-3.wav", "xil-5.wav", "xil-6.wav", "xil-7.wav"]
    }
};

// INSTRUMENT CLASS
Instrument = function(c){
    var me = this;
    this.name = c.name;
    this.posX = c.global.posX;
    this.posY = c.global.posY;
    // this.sizX = c.global.sizX;
    // this.sizY = c.global.sizY;
    this.audio = c.global.audio;
    // instrument parts
    this.parts = c.parts;
    this.partsNames = Object.keys(this.parts);
    this.overPartsNames = [];
    this.partsTgs = [];
    this.partsNames.forEach( el=> {
        me.overPartsNames.push(el.replace('.png', '-overcast.png'));
    });
    this.counter = 0;
    // overcast flag
    this.overcast = 0;
    this.ratio = 1;
    this.playing = 0;
    _.values(this.parts).forEach( p => {
        p.posX *= (windowWidth / 1920);
        p.posY *= (windowHeight / 1080);
        let currTg =  monit.addTarget(p.posX, p.posY, p.name);
        currTg.posX += this.posX;
        currTg.posY += this.posY;
        me.partsTgs.push(currTg);
        p.sizX = currTg.sizX;
        p.sizY = currTg.sizY;
    })
    // MOVE INSTRUMENT
    this.move = function(posX, posY){
        this.posX = posX;
        this.posY = posY;
        _.values(this.parts).forEach( p => {
            let currTg =  getTargetByName(p.name);
            currTg.posX = (((this.parts[p.name].posX) * this.ratio) + this.posX);
            currTg.posY = (((this.parts[p.name].posY) * this.ratio) + this.posY);
        })
    }
    // PULSE INSTRUMENT
    this.pulse = function(index){
        _.values(this.parts).forEach( p => {
            let currTg =  getTargetByName(p.name);
            currTg.sizX = (this.parts[p.name].sizX * this.ratio) + index;
            currTg.sizY = (this.parts[p.name].sizY * this.ratio) + index;
            currTg.posX = (((this.parts[p.name].posX) * this.ratio) + this.posX);
            currTg.posY = (((this.parts[p.name].posY) * this.ratio) + this.posY);
        });
    }
    // CHECK INTERACTION WITH PARTS OF INSTRUMENT
    this.checkInteractions = function(){
        let me = this;
        monit.getTargets().filter(t => this.partsNames.indexOf(t.img) !== -1 || this.overPartsNames.indexOf(t.img) !== -1).forEach( Tg =>{
            var eyeT = monit.eyeTracker;
            // compensate offset for overlap
            if(checkOverlap({posX: eyeT.posX + Tg.sizX / 2, posY: eyeT.posY + Tg.sizY / 2, sizX: eyeT.sizX, sizY: eyeT.sizY}, Tg)){
                if(Tg.img.indexOf('overcast') === -1){
                    Tg.img = Tg.img.replace('.png', '-overcast.png');
                    monit.sendEvent({ts: new Date().getTime(), type: "event", event_type: "reach_target", data: me.name});
                    monit.playSound('colorato.wav');
                    me.counter++;
                    if(me.counter == _.values(me.parts).length){
                        me.overcast = 1;
                        completedInstruments.push(me.name);
                        console.log('yeeeee', me.name);
                        monit.sendEvent({ts: new Date().getTime(), type: "event", event_type: "completed_targets_group", data: me.name});
                        monit.playSound('completato.wav');
                        var temp = _.cloneDeep(animations['completed'].obj);
                        temp.timestamp = getCurrTM();
                        // temp.obj = monit.getTargets().filter(t => this.partsNames.indexOf(t.img.replace('-overcast.png', '.png')) !== -1)[0];
                        temp.name = me.name;
                        animationsStack.push(temp);
                    }
                }else{
                    var playing = _.values(monit.sounds).filter(s => me.audio.indexOf(s.src) !== -1 && s.pl.state == 'started');
                    // console.log('playing', playing)
                    if(me.overcast && !animationsStack.filter(a => 
                        (a.type == 'completed' && me.name == a.name) || 
                        (a.type == 'pulsating' && me.name == a.name)).length && 
                        !playing.length){
                        // console.log('sound', me.audio[parseInt(random(me.audio.length))]);
                        var temp = _.cloneDeep(animations['pulsating'].obj);
                        temp.timestamp = getCurrTM();
                        temp.instrument = this;
                        temp.refRatio = this.ratio;
                        temp.name = me.name;
                        animationsStack.push(temp);
                        quantizeSound(me.audio[parseInt(random(me.audio.length))]);
                    }
                }
            }
        });
    }
    // SUGGEST RANDOM COMPONENT TO HOVER
    this.suggest = function(){
        var mineUnovercasts = monit.getTargets().filter(t => this.partsNames.indexOf(t.img) !== -1 && t.img.indexOf('overcast') == -1);
        if(mineUnovercasts.length){
            // console.log('lunghezza')
            var temp = _.cloneDeep(animations['suggest'].obj);
            temp.timestamp = getCurrTM();
            temp.obj = mineUnovercasts[parseInt(random(mineUnovercasts.length))];
            animationsStack.push(temp);
        }
    }
    // change dimensions of instrument
    this.changeRatio = function(n){
        let me = this;
        this.ratio = n;
        this.partsTgs.forEach( t => {
            let refTg = _.values(me.parts).filter(p => t.img.indexOf(p.name.replace('.png', '')) !== -1)[0]
            // console.log(refTg)
            t.sizX = refTg.sizX * me.ratio;
            t.sizY = refTg.sizY * me.ratio;
        });
        // _.values(this.parts).forEach( t => {
            // t.posX = t.posX * this.ratio;
            // t.posY = t.posY * this.ratio;
        //     t.sizX = t.sizX * me.ratio;
        //     t.sizY = t.sizY * me.ratio;
        // });
        this.move(this.posX, this.posY);
        rainbowHaloDim = 800 * this.ratio;
    }
    // at the end of the constructor, execute calculus of ratio
    this.changeRatio(elDimensions);
};
// array of pulsating objects
var pulsatingObjs = [],
surfaceMov = {},
currInstr = {};
// targets interaction
animations['targetOverlap'] = { 
    cb: function(el) {
        fill(map(el.index, el.end, 0, 200, 255), map(el.index, el.end, 0, 150, 50), map(el.index, el.end, 0, 150, 50), 150);
        ellipse(el.pos.x, el.pos.y, map(el.index, el.end, 0, 100, 400));
        el.index = el.index <= 0 ? 0 : el.index - (el.end / 30);
        // me.changeAnimation('happy' + a);
    },
    obj: {
        type: 'targetOverlap', 
        id: '', 
        index: 500,
        pos: {}, 
        timestamp: 0, 
        end: 500
    }
};
// bouncing animation when an instrument is all overcast
animations['bouncing'] = {
    cb: function(el){
        // fill('red');
        // noStroke();
        // ellipse(mouseX, -300 * Math.abs(sin(map(el.index, 0, 2000, 0, 3 * (PI / 2)))) + mouseY, 30);
        // -300 * Math.abs(sin(map(el.index, 0, 2000, 0, 3 * (PI / 2)))) + el.ref.posY;
        el.obj.move(el.ref.posX, -100 * Math.abs(sin(map(el.index, 0, el.end, 0, 3 * (PI / 2)))) + el.ref.posY)
        el.index += (el.end / 33);
    },
    obj:{
        type: 'bouncing',
        timestamp: getCurrTM(),
        index: 0,
        obj: {},
        ref: {},
        end: 2000
    }
}
animations['pulsating'] = {
    cb: function(el){
        el.index += el.end / (el.end / 33);
        el.instrument.changeRatio((el.refRatio) + 0.1 * sin(map(el.index, 0, el.end, 0, 2 * PI)));
    },
    obj: {
        type: 'pulsating',
        timestamp: getCurrTM(),
        instrument: {},
        refRatio: 0,
        obj: {},
        index: 0,
        end: 1000
    }
}
// suggest animation
animations['suggest'] = {
    cb: function(el){
        push();
        fill('rgba(255, 0, 0, 0.5)');
        ellipse(el.obj.posX, el.obj.posY, 30 * sin(map(el.index, 0, el.end, 0, 2 * (2 * PI))) + 90);
        el.index += (el.end / 33)
        pop();
    },
    obj: {
        type: 'suggest',
        index: 0,
        timestamp: 0,
        obj: {},
        end: 3000
    }
}
// completed animation
animations['completed'] = {
    cb: function(el){
        el.index += (el.end / 33);
    },
    obj: {
        type: 'completed',
        index: 0,
        timestamp: 0,
        obj: {},
        name: '',
        end: 3000
    }
}
// reset targets distractor and fixation objects
function resetAll(){
    // monit.fixationObjects = [];
    monit.clearTargets();
    monit.clearDistractors();
}
function drawAnimations(){
    animationsStack.forEach(animObj =>{
        if(animObj.index > animObj.end){
            if(animObj.type == 'completed' || animObj.type == 'pulsating'){
                if(animObj.type == 'completed'){
                    _.remove(completedInstruments, c => c == animObj.name);
                }
                _.remove(animationsStack, d => d.name == animObj.name)
                instrumentsStack.forEach( ins => {
                    ins.changeRatio(elDimensions);
                });
            }else{
                _.remove(animationsStack, d => d.obj.id == animObj.obj.id && d.type == animObj.type);
            }
        }else{
            animations[animObj.type].cb(animObj);
        }
    })
}
// get specified distractor
function getDistractorByName(dis){
    return _.find(monit.getDistractors(), d => monit.itemsNames[d.id] === dis);
}
// get specified target
function getTargetByName(tg){
    return _.find(monit.getTargets(), d => monit.itemsNames[d.id] === tg);
}
// SET GAME STATE 
function setGameState(gs, gss, gstf){
    monit.GameScene = gs;
    GameSceneStage = gss;
    GameSceneTypeFix = gstf;
} 
// trigger all overcast
function triggerBouncing(instrument){
    var temp = _.cloneDeep(animations['bouncing'].obj);
    temp.timestamp = getCurrTM();
    temp.obj = instrument;
    temp.ref = {posX: instrument.posX, posY: instrument.posY};
    animationsStack.push(temp);
}
// add target from instrument name
function addInstrument(x, y, name){
    if(name.indexOf('.png') == -1){
        name += '.png';
    }
    console.log('name', name);
    if(!instrumentsStack.filter(el => el.name == name).length){
        var instrData = _.cloneDeep(instrumentsData[name]);
        instrData.global.posX = x*window.innerWidth;
        instrData.global.posY = y*window.innerHeight;
        instrumentsStack.push(new Instrument(instrData));
    }
}
function removeInstrument(id){
    var tg = monit.pState.targets[id];
    if(tg !== undefined){
        var selectedInstr = instrumentsStack.filter(i => i.partsNames.indexOf(tg.img) !== -1 || i.overPartsNames.indexOf(tg.img) !== -1)[0];
        var inToDel = _.find(instrumentsStack, i => i.name == selectedInstr.name);
        console.log(inToDel);
        inToDel.partsTgs.forEach( el => {
            // console.log(el, el.img)
            monit.removeTarget(el);
        });
        _.remove(instrumentsStack, i => i.name == inToDel.name);
    } 
}
function overcastInstrument(id){
	var tg = monit.pState.targets[id];
    if(tg !== undefined){
        var selectedInstr = instrumentsStack.filter(i => i.partsNames.indexOf(tg.img) !== -1 || i.overPartsNames.indexOf(tg.img) !== -1)[0];
        if(selectedInstr !== undefined){
        	var tgToOvercast = selectedInstr.partsTgs.filter(t => t.img.indexOf('overcast') === -1)[0];
        	if(tgToOvercast !== undefined){
        		tgToOvercast.img = tgToOvercast.img.replace('.png', '-overcast.png');
        		selectedInstr.counter++;
        	}
			if(selectedInstr.counter == _.values(selectedInstr.parts).length){
				selectedInstr.overcast = 1;
		        completedInstruments.push(selectedInstr.name);
		        console.log('yeeeee', selectedInstr.name);
		        monit.playSound('completato.wav');
		        var temp = _.cloneDeep(animations['completed'].obj);
		        temp.timestamp = getCurrTM();
		        temp.obj = monit.getTargets().filter(t => selectedInstr.partsNames.indexOf(t.img.replace('-overcast.png', '.png')) !== -1)[0];
		        temp.name = selectedInstr.name;
		        animationsStack.push(temp);
			}
        }
        // selectedInstr.partsTgs.filter(t => t.img.indexOf('overcast') == -1).forEach(tg => {
        // 	tg.img = tg.img.replace('.png', '-overcast.png');
        // 	selectedInstr.counter++;
        // });
    }
}
function actionOnInstrument(id){
	var tg = monit.pState.targets[id];
    if(tg !== undefined && !animationsStack.filter(a => a.type == 'completed').length){
        var selectedInstr = instrumentsStack.filter(i => i.partsNames.indexOf(tg.img) !== -1 || i.overPartsNames.indexOf(tg.img) !== -1)[0];
        _.remove(animationsStack, a => (a.type == 'pulsating') && a.name == selectedInstr.name)
        if(selectedInstr.overcast){
        	removeInstrument(id);
        }else{
        	overcastInstrument(id);
        }
    }
}
// fill assets dimension object
function setAssetsSizes(){
    monit.json.details.ingame_assets.forEach(el => {
        assetsDim[el.name] = {sizX: el.sizX * (window.innerWidth / monit.idealRes.width), sizY: el.sizY * (window.innerHeight / monit.idealRes.height)}
    });
    console.log('assets dimension', assetsDim);
}
// -------------- PRELOAD --------------
function preload() {
    monit.loadJsonItems();
}
// -------------- SETUP ----------------
function setup() {
    createCanvas(windowWidth,windowHeight);
    noStroke();
    frameRate(30);
    monit.onChangeDynamics = function(data){
        var type = data.id;
        switch(type){
            case 'elDimensions':
                if(animationsStack.filter(a => a.type == 'pulsating')){
                    _.remove(animationsStack, a => a.type == 'pulsating')
                }
                elDimensions = parseFloat(data.value) + 0.2;
                instrumentsStack.forEach(inst => {
                    inst.changeRatio(elDimensions);
                })
                break;
        }
    }
    // redefine monit.play sound
    monit.playSound = (name)=>{
        if(monit.sounds[name] !== undefined){
            if(name == 'colorato.wav' || name == 'completato.wav'){
                monit.sounds[name].pl.start();
            }else{
                if(monit.sounds[name].pl.state !== 'started'){
                    monit.sounds[name].pl.start();  
                }
                // send message to event socket when play sound
                monit.eventconnection.send(JSON.stringify({type: 'event', 'event_type': 'play_sound', data: name}));
            }
        }
    }
    monit.onAttentionSig = ()=>{
        instrumentsStack.forEach( ins => {
            if(!ins.overcast){
                ins.suggest();
            }else{
                addSuggestAnimation(ins)
            }
        });
        if(instrumentsStack.length) monit.playSound('richiamo.mp3');
    }
    monit.onAddTarget = addInstrument;
    monit.onRemoveTarget = actionOnInstrument;
    monit.onMoveTarget = (obj) => {
        if(obj !== undefined && !animationsStack.filter(a => a.type == 'completed').length && !animationsStack.filter(a => a.type == 'pulsating').length){
            console.log('moving')
            var img = monit.pState.targets[obj.id].img;
            // console.log(img)
            var selectedInstr = instrumentsStack.filter(i => i.partsNames.indexOf(img) !== -1 || i.overPartsNames.indexOf(img) !== -1)[0];
            var inToMove = _.find(instrumentsStack, i => i.name == selectedInstr.name);
            selectedInstr.move(obj.x*window.innerWidth, obj.y*window.innerHeight);
        }
    }
}

// -------------- DRAW -----------------
function draw() {
    // console.log(monit.GameScene, GameSceneTypeFix, GameSceneStage);
    if(monit.loaded){
        if(monit.pState.bg != backgroundImg && !monit.GameScene){monit.setBg(backgroundImg);} // set first bg      
        if(isEmpty(assetsDim)){setAssetsSizes()} // set assets dimension object 
        if(!firstStateButtons){
            monit.sendEvent({ts: new Date().getTime(), type: "event", event_type: "buttons_state", data: {cooldown: false, back: false, next: false}});
            firstStateButtons = 1;
        }
        if(isEmpty(bgEyeTracker)) {bgEyeTracker = new Bg(frameRate())}
        monit.drawBg();
        bgEyeTracker.run();
        switch(monit.GameScene){
            // INSTRUMENT APPEARING 
            case 0:
                if(monit.trackerActive){
                    instrumentsStack.forEach( i=> {
                        i.checkInteractions();
                    });
                    checkReachTarget();
                }
                break;
        }
        monit.drawDistractors();
        // rotate animation on completed
        if(animationsStack.filter(a => a.type == 'completed').length || animationsStack.filter(a => a.type == 'pulsating' && a.instrument.overcast).length){
            instrumentsStack.forEach( el => {
                instrName = el.name
                // push();
                // blendMode(DIFFERENCE);
                if(completedInstruments.indexOf(instrName) !== -1 || animationsStack.filter(a => a.type == 'pulsating' && a.instrument.overcast && a.instrument.name == el.name).length){
                    var instrToRot = instrumentsStack.filter(e => e.name == instrName)[0];
                    // animData = animationsStack.filter(a => a.type == 'completed' && a.name == instrToRot.name)[0];
                    // draw rainbow with gradient
                    push();
                    colorMode(HSB);
                    indxHue = indxHue > 350 ? 0 : indxHue += 5;
                    fill(indxHue, 100, 100);
                    ellipse(instrToRot.posX, instrToRot.posY, 800 * el.ratio);
                    imageMode(CENTER);
                    image(monit.images['white.png'], instrToRot.posX, instrToRot.posY, 800 * el.ratio, 800 * el.ratio);
                    pop();
                }
                // pop()
            });
            instrumentsStack.forEach( el => {
                instrName = el.name
                if(completedInstruments.indexOf(instrName) !== -1){
                    // console.log('yes', el.name);
                    // console.log('rotating', el);
                    var instrToRot = instrumentsStack.filter(e => e.name == instrName)[0],
                    animData = animationsStack.filter(a => a.type == 'completed' && a.name == instrToRot.name)[0];
                    instrToRot.partsTgs.forEach( t => {
                        push();
                        translate(instrToRot.posX, instrToRot.posY);
                        rotate( 0.5 * sin(map(animData.index, 0, animData.end, 0, 2 * (2 * PI))));
                        imageMode(CENTER);
                        translate(-instrToRot.posX, -instrToRot.posY)
                        image(monit.images[t.img], t.posX, t.posY, t.sizX, t.sizY);
                        pop();
                    });
                }else{
                    // console.log('nope', el.name);
                    el.partsTgs.forEach( tg => {
                        push();
                        imageMode(CENTER);
                        image(monit.images[tg.img], tg.posX, tg.posY, tg.sizX, tg.sizY);
                        pop();
                    });
                }
            });
        }else{
            monit.drawTargets();
        }
        if(monit.trackerActive){

            // monit.drawTracker();

        }
        // monit.monitoring();
        bgEyeTracker.fade();
        //draw animations if present
        drawAnimations();
    }else{
        push();
        background('black');
        textSize(30);
        stroke('white');
        fill('white');
        textAlign(CENTER, CENTER);
        text('Caricamento in corso...', windowWidth / 2, windowHeight / 2);
        pop();
    }
}
// KEY PRESSED FUNCTION
function keyPressed(){
    // console.log(keyCode)
    switch(keyCode){
        case UP_ARROW:
            console.log('sopra');
            if(!instrumentsStack.length){
                monit.onAddTarget(400 / window.innerWidth, 400 / window.innerHeight,'guitar.png');
                monit.onAddTarget(1000 / window.innerWidth, 400 / window.innerHeight,'trumpet.png');
                monit.onAddTarget(0.6, 0.8,'xylophone.png');
                monit.onAddTarget(0.8, 0.8,'piano.png');
            }
            break;
        case LEFT_ARROW:

            break;
    }
}
// check if an object is empty 
function isEmpty(obj) {
    for(var key in obj) {
        if(obj.hasOwnProperty(key))
            return false;
    }
    return true;
}
// conversion rgb to hex
function rgbToHex(r, g, b) {
    return "#" + ((1 << 24) + (r << 16) + (g << 8) + b).toString(16).slice(1);
}

function monitorHoveredColor(){
    push();
    fill('black');
    var pxs = get(mouseX - 20, mouseY - 20);
    text(pxs[0] + ' ' + pxs[1] + ' ' + pxs[2] + ' ', windowWidth - 50, 50);
    stroke('black');
    rectMode(CENTER);
    fill(pxs);
    rect(windowWidth - 100, 100, 100, 100);
    pop();
}

function addSuggestAnimation(instr){
    var temp = _.cloneDeep(animations['suggest'].obj);
    temp.timestamp = getCurrTM();
    temp.obj = instr;
    animationsStack.push(temp);
}

var currReachedTargets = [];
function checkReachTarget(){
    var eyet = monit.eyeTracker;
    monit.getTargets().forEach(tg => {
        var overlapF = checkOverlap({posX: eyet.posX + tg.sizX / 2, posY: eyet.posY + tg.sizY / 2, sizX: eyet.sizX, sizY: eyet.sizY}, tg)
        if(overlapF && currReachedTargets.indexOf(tg.id) == -1){
            monit.sendEvent({ts: new Date().getTime(), type: "event", event_type: "start_fixation", data: tg.id});
            currReachedTargets.push(tg.id)
        }
        if(!overlapF && currReachedTargets.indexOf(tg.id) != -1){
            monit.sendEvent({ts: new Date().getTime(), type: "event", event_type: "end_fixation", data: tg.id});
            _.remove(currReachedTargets, e => e == tg.id);
        }
    })
}