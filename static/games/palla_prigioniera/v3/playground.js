var Playground = function(dir, animalName, actionDuration){
    // STATES
    // 'attack'
    // 'animationAttack'
    // 'defense'
    // 'animationDefense'
    // 'charVictory'
    this.direction = dir;
    this.charName = 'char.png';
    // this.charName = 'char-' + this.direction + '.png';
    this.animalName = animalName;
    this.animalNameAvatar = animalName.replace('.png', '-avatar.png');
    this.charNameAvatar = 'char-avatar.png';
    this.targetAtt = 'target.png';
    this.targetDef = 'target_defense.png';
    this.heightHeader = 150;
    this.constrainChar = this.direction == 'left' ? [this.heightHeader, windowWidth / 2, windowHeight, 0] : [this.heightHeader, windowWidth, windowHeight, windowWidth / 2];
    this.constrainAnimal = this.direction == 'left' ? [this.heightHeader, windowWidth, windowHeight, windowWidth / 2] : [this.heightHeader, windowWidth / 2, windowHeight, 0];
    this.strokePG = 10;
    this.protections = [];
    this.obstacles = [];
    this.boxes = [];
    this.usedProtection = {};
    this.obstacleOverlapped = {};
    this.boxOverlapped = {};
    this.obstDur = 1000;
    this.obstTimer = 0;
    this.timActionDuration = actionDuration * 1000;
    // draw action explanation 
    this.drawExplanation = true;
    // amount of time remaining until the end of the action 
    this.intervalAnimActionDone = 0;
    this.prevEyetrackerActive = monit.trackerActive;
    this.terapistMovement = {};
    this.endOfGameCounterAnim = 0;
    this.numMaxExtraEls = 3;
    this.extraElsTimer = new Date().getTime();
    // set lifes of players
    switch (monit.GameLevel) {
        case 3:
            this.nPoints = 5;
            break;
        case 4:
            this.nPoints = 7;
            break;
        default: 
            this.nPoints = 3;
            break;
    }
    // this.nPoints = 3;
    // game dynamic: 
    // 0: trance of 5 balls          
    // 1: trance of 3 balls         
    // 2: trance of 1 ball, normal game         
    this.gameDynamic = monit.GameLevel;
    // this.nBallThrow = 0;
    switch (this.gameDynamic) {
        case 0:
            this.nBallThrow = (monit.GameScene == 0 ? 1 : 3);
            break;
        default:
            this.nBallThrow = 1;
            break;
    }
    // set match state from attack to defense
    this.matchState = 0;
    this.counterBall = 0;
    // stare char object, during defense, same as fixationObject 
    this.stareCharCounter = 0;
    this.createWiseRandomItem = function(name){
        let bottomX = this.constrainChar[3] + (assetsDim[name].sizX / 1);
        let topX = this.constrainChar[1] - (assetsDim[name].sizX / 1);
        let bottomY = this.constrainChar[0] + (assetsDim[name].sizY / 1);
        let topY = this.constrainChar[2] - (assetsDim[name].sizY / 1);
        var rule = true;
        var values = undefined;
        var els = this.protections.concat(this.obstacles).concat(this.boxes);
        var counter = 0;
        var rndValX = random(bottomX, topX);
        var rndValY = random(bottomY, topY);
        if(els.length){
            while(rule){
                var rndValX = random(bottomX, topX);
                var rndValY = random(bottomY, topY);
                var counter = 0;
                for (var x in els) {
                    if (
                        Math.abs(els[x].posX - rndValX) < els[x].sizX + 10 &&
                        Math.abs(els[x].posY - rndValY) < els[x].sizY + 10
                    ) {
                        counter++;
                    }
                }
                if(!counter) rule = false;
            }
        }
        console.log(counter);
        return {posX: rndValX, posY: rndValY};
    }
    this.addItem = function(name, type, posX, posY){
        if(posX == undefined || posY == undefined){
            let randomItem = this.createWiseRandomItem(name);
            posX = randomItem.posX;
            posY = randomItem.posY;
        }
        var item = monit.addDistractor(posX, posY, name, 2)
        switch (type) {
            case 'protection':
                this.protections.push(item);
                break;
            case 'obstacle':
                this.obstacles.push(item);
                break;
            case 'box':
                this.boxes.push(item);
                break;
        }
    }
     // draw points for the first time 
    this.createPlayersPoints = function(){
        var offx = 100;
        // char
        for (var x = 0; x < this.nPoints; x++){
            monit.addTarget(this.direction == 'left' ? ((x * offx) + (assetsDim[this.charNameAvatar].sizX * 2)) : windowWidth - ((x * offx) + (assetsDim[this.charNameAvatar].sizX * 2)),
            this.heightHeader / 2,
            this.charNameAvatar, 
            10
             )
        }
        // animal
        for (x = 0; x < this.nPoints; x++){
            monit.addTarget(this.direction == 'left' ? windowWidth - ((x * offx) + (assetsDim[this.charNameAvatar].sizX * 2)) : ((x * offx) + (assetsDim[this.charNameAvatar].sizX * 2)),
            this.heightHeader / 2,
            this.animalNameAvatar,
            10
            )
        }
    }
    // GAME SETTINGS
    this.initGame = function(){
        monit.clearTargets();
        monit.clearDistractors();
        this.currState = 'attack';
        // values: 1=hit with success, 2=missed
        this.launchingBall = 0;
        // report what's happened in the last action. Values: 'hit animal' 'hit char' 'missed animal' 'missed char'
        this.lastAction = '';
        this.startPosChar = [(windowWidth / 4) * (this.direction == 'left' ? 1 : 3), (windowHeight / 2) + (this.heightHeader / 2)];
        this.startPosAnimal = [(windowWidth / 4) * (this.direction == 'left' ? 3 : 1), (windowHeight / 2) + (this.heightHeader / 2)];

        this.startPosXAvatarCharacter = (this.direction == 'left' ? windowWidth : 0) - (this.heightHeader / 2);
        this.startPosYAvatarCharacter = this.heightHeader / 2;
        this.startPosXAvatarAnimal = this.heightHeader / 2;
        this.startPosYAvatarAnimal = this.heightHeader / 2;
        // create avatars for score points table
        // create targets
        this.animal = new Player(monit.addTarget(...this.startPosAnimal, this.animalName, 1));
        this.char = new Player(monit.addTarget(...this.startPosChar, this.charName, 1));
        console.log(this.char.item.sizX, this.char.item.sizY)
        // add ball near the character
        this.ball = monit.addDistractor(this.startPosChar[0] + ((this.direction == 'left' ? -1 : 1) * (assetsDim[this.charName].sizX / 2)), this.startPosChar[1] - (assetsDim[this.charName].sizY / 2), 'ball.png', 10);
        // add the target following the eyetracker
        this.target = monit.addDistractor(monit.eyeTracker.posX, monit.eyeTracker.posY, this.targetAtt, 4);
        // path on which the target and the animal moves
        this.movingPath = movingPath.path;
        // index of the movement along the path 
        this.movingInd = 0;
        this.playgroundAnimationBucket = [];
        this.timeoutAction = null;
        // this.timActionDuration = 15000;
        // timestamp indicating the start of an action
        this.startActionTime = null;
        this.setExplanationState = 0;
        this.actionAnimation = [
            {
                id: 0,
                startX: 0,
                startY: 0,
                endX: 0,
                endY: 0,
                duration: 1000,
                index: 0
            }
        ];
        this.protections = [];
        this.obstacles = [];
        this.boxes = [];
        this.usedProtection = {};
        this.obstacleOverlapped = {};
        this.boxOverlapped = {};
        // CHECK INTERACTION WITH TARGETS (add/remove elements to/from fixation object array)
        this.checkInteractionTargets = function(rule){
            if(monit.trackerActive){
                // check only for elements still to choose
                monit.getTargets().filter(rule).forEach( Tg => {
                    // var Tg = tgs[x];
                    var eyeT = monit.eyeTracker,
                    currTarget = {
                        posX: Tg.posX,
                        posY: Tg.posY,
                        sizX: Tg.sizX,
                        sizY: Tg.sizY
                    };
                    // compensate offset for overlap
                    if(checkOverlap({posX: eyeT.posX + Tg.sizX / 2, posY: eyeT.posY + Tg.sizY / 2, sizX: eyeT.sizX, sizY: eyeT.sizY}, currTarget)){
                        if(_.find(monit.fixationObjects, d => d.id === Tg.id) == undefined){
                            // monit.sendEvent({ts: new Date().getTime(), type: "event", event_type: "start_fixation", data: Tg.id})
                            removeAnimation('timerAnimation', this.target.id)
                            // add target to the interacted list
                            monit.fixationObjects.push({id: Tg.id, counter: 0});
                            var temp = _.cloneDeep(animations['watchedPlayer'].obj);
                            temp.timestamp = getCurrTM();
                            temp.obj = Tg;
                            temp.end = monit.fixationTime;
                            // temp.circle = new Circle(Tg.posX, Tg.posY, temp.end);
                            animationsStack.push(temp)
                        }else{
                            var temp = _.find(monit.fixationObjects, d => d.id === Tg.id);
                            temp.counter += 33.33;
                        }
                    }else{
                        if(monit.fixationObjects.filter(f => f.id == Tg.id).length > 0){
                            _.remove(monit.fixationObjects, d => d.id === Tg.id);
                            // monit.sendEvent({ts: new Date(), type: "event", event_type: "end_fixation", data: Tg.id});
                        }
                        removeAnimation('watchedPlayer', Tg.id)
                    }
                });
            }
        }
        if(monit.GameScene) {
            this.createPlayersPoints();
        }
        var numExtraEls = monit.GameScene == 0 ? 1 : this.numMaxExtraEls;
        switch(monit.GameLevel){
            case 2:
                for(var x = 0; x < numExtraEls; x++) this.addItem('pupazzo-neve.png', 'protection');
                break;
            case 3:
                for(x = 0; x < numExtraEls; x++) this.addItem('telo-1.png', 'obstacle');
                break;
            case 4:
                for(x = 0; x < numExtraEls; x++) this.addItem('forziere.png', 'box');
                break;
        }
    }
    this.initGame();
    // removing targets and distractors
    this.removeGameTgDis = function(){
        monit.removeTarget(this.char.item);
        monit.removeTarget(this.animal.item);

        monit.removeDistractor(this.animalAvatar);
        monit.removeDistractor(this.charAvatar);

        monit.removeDistractor(this.ball);
        monit.removeDistractor(this.target);
    }
    this.addGameToActionAnimation = function(el){
        // this.removeGameTgDis();
        // remove watched animation
        if(this.currState == 'attack'){
            _.remove(animationsStack, a => a.type == 'watchedPlayer');
        }
        // adding animation target
        var temp = _.cloneDeep(this.actionAnimation[0]);
        var myTarget = monit.getTargetByName(el);
        if(myTarget !== undefined){
            // console.log('last action', this.lastAction)
            let amountMovementX = this.lastAction.indexOf('hit') !== -1 ? 200 : 0;
            temp.id = myTarget.id;
            temp.startX = myTarget.posX;
            temp.startY = myTarget.posY;
            // temp.endX = myTarget.posX + (((el == this.animalName && this.animalName.indexOf('left') != -1) ||
            //     (el == this.charName && this.charName.indexOf('right') != -1)) ? amountMovementX : -amountMovementX);
            temp.endX = myTarget.posX + (((el == this.animalName && this.direction == 'left') ||
                (el == this.charName && this.direction == 'right')) ? amountMovementX : -amountMovementX);
            temp.endY = myTarget.posY;
            this.playgroundAnimationBucket.push(temp);
            monit.removeDistractor(this.target);
            if(!isEmpty(this.usedProtection)){
                var temp3 = _.cloneDeep(animations['protectionUsed'].obj);
                temp3.obj = this.usedProtection;
                temp3.index = 0;
                temp3.color = this.usedProtection.img == 'pupazzo-neve.png' ? 'rgba(255, 255, 255,1)' : 'rgba(0,0,0,1)';
                animationsStack.push(temp3)
                monit.playSound(this.usedProtection.img == 'pupazzo-neve.png' ? 'destroy_snowman.wav' : 'shield_used.wav');
                _.remove(this.protections, el => el.id == this.usedProtection.id)
                monit.removeDistractorById(this.usedProtection.id);
                this.usedProtection = {};
            }else if(this.launchingBall == 2){
                monit.playSound('wrong.wav');
            }
            // add ball bouncing animation
            var temp2 = _.cloneDeep(animations['bouncing'].obj);
            temp2.timestamp = getCurrTM();
            temp2.obj = monit.getDistractorByName('ball.png');
            temp2.refY = temp2.obj.posY;
            temp2.refX = temp2.obj.posX;
            temp2.orientation = this.direction;
            temp2.currState = this.currState;
            animationsStack.push(temp2);
        }   

        // temp[0].id = monit.addDistractor(temp[0].startX, temp[0].startY, elToAdd1).id;
        // temp[1].id = monit.addDistractor(temp[1].startX, temp[1].startY, elToAdd2).id;
    }
    this.actionAnimationToGame = function(){
        this.animalAvatar = monit.addTarget(this.startPosXAvatarAnimal, this.startPosYAvatarAnimal, this.charNameAvatar, 0);
        this.charAvatar = monit.addTarget(this.startPosXAvatarCharacter, this.startPosYAvatarCharacter, this.animalNameAvatar, 0);
        // create targets
        this.animal.item = monit.addTarget(...this.startPosAnimal, this.animalName, 1);
        this.char.item = monit.addTarget(...this.startPosChar, this.charName, 1);
        // add ball near the character
        this.ball = monit.addDistractor(0, this.startPosChar[1] - (assetsDim[this.charName].sizX / 2), 'ball.png', 10);
        // add the target following the eyetracker
        this.target = monit.addDistractor(monit.eyeTracker.posX, monit.eyeTracker.posY, this.currState == 'attack' ?  this.targetAtt : this.targetDef, 4);
    }
    this.drawEndOfGame = function(){
        var winner = this.char.score == this.nPoints ? this.charName : this.animalName;
        // removing targets and distractors
        if(monit.getDistractorByName('ball.png') !== undefined){
            monit.clearTargets();
            monit.clearDistractors();
            // create targets            
            this.char.item = monit.addTarget(windowWidth / 2, windowHeight / 2, winner);
            monit.playSound(winner.indexOf('char') !== -1 ? 'completato.wav' : 'fail.wav');
        }
        winnerTg = monit.getTargetByName(winner);
        monit.animateTarget(monit.getTargetByName(winner), (winner.indexOf('char') == -1 ? '-' + this.direction : '') + '-hurra', 5);
        winnerTg.posY = (windowHeight / 2) - 200*sin(map(this.endOfGameCounterAnim, 0, 30, 0, PI));
        this.endOfGameCounterAnim = this.endOfGameCounterAnim >= 30 ? 0 : this.endOfGameCounterAnim + 1;
        
    }
    // -------- DRAW GAME -------------
    this.draw = function(){
        // reset character rotation after fallen animation
        if(this.char.item.rot != 0 && !thereIsThisAnimation('fallen')){
            this.char.item.rot = 0;
        }
        // draw playground and points only during the real match 
        if(['charDefeat', 'charVictory'].indexOf(this.currState) == -1 && monit.GameScene){
            this.drawPlayground();
            this.checkPoints();
        }
        if(this.prevEyetrackerActive != monit.trackerActive){
            if(monit.trackerActive == false){
                if(thereIsThisAnimation('timerAnimation')){
                    this.intervalAnimActionDone = animationsStack.filter(a => a.type == 'timerAnimation')[0].index;
                    console.log('freeze', this.timActionDuration, animationsStack.filter(a => a.type == 'timerAnimation')[0].index)
                }
            }else{
                if(thereIsThisAnimation('timerAnimation')){
                    this.startActionTime = new Date().getTime() - this.intervalAnimActionDone;
                    console.log('difference', new Date().getTime() - this.startActionTime)
                }
            }
            this.prevEyetrackerActive = monit.trackerActive;
        }
        if(monit.trackerActive){
            switch(this.currState){
                case 'attack':
                    if(this.matchState == 1) {
                        this.resetActionPositions();
                        this.currState = 'defense';
                    }else{
                        if(!thereIsThisAnimation('hitPlayer') && this.launchingBall){
                            this.char.score += this.launchingBall == 1 ? 1 : 0;
                            this.lastAction = this.launchingBall == 1 ? 'hit animal' : 'missed animal';
                            this.currState = 'animationAttack';
                            this.addGameToActionAnimation(this.animalName);
                            if(this.launchingBall == 1) {
                                monit.playSound('bounce.wav');
                                monit.playSound('animal-ouch.wav');
                                setTimeout(()=>{
                                    monit.playSound('yuppi.wav');
                                }, 300);
                            }else{
                                monit.playSound('wrong.wav');
                                setTimeout(()=>{
                                    monit.playSound('evil-laugh.wav');
                                }, 300);
                            }
                            this.launchingBall = 0;
                            break;
                        }
                        this.addPeriodicallyExtraEls();
                        this.attack();
                        checkReachTarget(monit.getTargetByName(this.animalName).id);
                    }
                    break;
                case 'animationAttack':
                    removeAnimation('timerAnimation');
                    this.drawActionAnimation(() => {
                        if(this.char.score == this.nPoints && monit.GameScene !== 0){
                            this.currState = 'charVictory';
                        }else{
                            // this.ball.posX = monit.getTargetByName('animal.png').posX + (assetsDim['animal.png'].sizX / 2);
                            // this.ball.posY = monit.getTargetByName('animal.png').posY - (assetsDim['animal.png'].sizY / 2);
                            // this.currState = 'defense';
                            if(this.matchState == 0){
                                this.currState = 'attack';
                                this.target = monit.addDistractor(windowWidth / 2, windowHeight / 2, this.targetAtt, 4);
                                this.resetActionPositions();
                            }else{
                                this.target = monit.addDistractor(windowWidth / 2, windowHeight / 2, this.targetDef, 4);
                                if(this.drawExplanation) {
                                    this.currState = 'transition_attack2defense';
                                }else{
                                    this.currState = 'defense';
                                    this.resetActionPositions();
                                }
                            }
                        }
                    });
                    break;
                case 'transition_attack2defense':
                    this.drawDefenseExplanation();
                    break;
                case 'defense':
                    if(this.matchState == 0){
                        this.ball.posX = monit.getTargetByName(this.charName).posX + ((this.direction == 'left' ? -1 : 1) * (assetsDim[this.animalName].sizX / 2));
                        this.ball.posY = monit.getTargetByName(this.charName).posY - (assetsDim[this.charName].sizY / 2);
                        this.startActionTime = new Date().getTime();
                        this.currState = 'attack'
                    }else{
                        if(!thereIsThisAnimation('hitPlayer') && this.launchingBall){
                            console.log('launchingBall', this.launchingBall);
                            this.animal.score += this.launchingBall == 1 ? 1 : 0;
                            this.lastAction = this.launchingBall == 1 ? 'hit char' : 'missed char'
                            // console.log(this.launchingBall);
                            if(this.launchingBall == 1) {
                                monit.playSound('bounce.wav');
                                monit.playSound('char-ouch.wav');
                                setTimeout(()=>{
                                    monit.playSound('evil-laugh.wav');
                                }, 300);
                            }else{
                                setTimeout(()=>{
                                    monit.playSound('yuppi.wav');
                                }, 300);
                            }
                            this.currState = 'animationDefense';
                            this.addGameToActionAnimation(this.charName);
                            this.launchingBall = 0;
                            break;
                        }
                        this.addPeriodicallyExtraEls();
                        this.defense();
                    }   
                    break;
                case 'animationDefense':
                    removeAnimation('timerAnimation');
                    this.drawActionAnimation(() => {
                        // remove hit/missing animation distractors
                        if(this.animal.score == this.nPoints && monit.GameScene != 0){
                            this.currState = 'charDefeat'; 
                        }else{
                            // this.ball.posX = monit.getTargetByName('char.png').posX - (assetsDim['char.png'].sizX / 2);
                            // this.ball.posY = monit.getTargetByName('char.png').posY - (assetsDim['char.png'].sizY / 2);
                            // this.currState = 'attack';
                            if(this.matchState == 1){
                                this.currState = 'defense';
                                this.target = monit.addDistractor(windowWidth / 2, windowHeight / 2, this.targetDef, 4);
                                this.resetActionPositions();
                            }else {
                                this.target = monit.addDistractor(windowWidth / 2, windowHeight / 2, this.targetAtt, 4);
                                if(this.drawExplanation){
                                    this.currState = 'transition_defense2attack';
                                }else {
                                    this.currState = 'attack';
                                    this.resetActionPositions();
                                }
                            }
                        }
                    });
                    break;
                case 'transition_defense2attack':
                    this.drawAttackExplanation();
                    break;
                case 'charVictory':
                    this.drawEndOfGame();
                    break;
                case 'charDefeat':
                    this.drawEndOfGame();
                    break;
            }
            bgEyeTracker.fade();
        }else{
            // if(animationsStack.filter(a => a.type == 'timerAnimation').length){
            //     removeAnimation('timerAnimation');
            // }
            this.startActionTime = new Date().getTime();
            bgEyeTracker.fade();
        }
        pulseObjects.forEach( el => {
            el.draw();
        });
    }
    // draw playground
    this.createPlayground = true;
    this.drawPlayground = function(){
        if(this.createPlayground){
            monit.addDistractor(
                windowWidth / 2, 
                this.heightHeader - (this.strokePG / 2), 
                'rect', 
                1, 
                '#000', 
                windowWidth, 
                this.strokePG
            );
            monit.addDistractor(
                windowWidth / 2, 
                this.strokePG / 2, 
                'rect', 
                1, 
                '#000', 
                windowWidth, 
                this.strokePG
            );
            monit.addDistractor(
                windowWidth / 2, 
                (windowHeight / 2) + (this.heightHeader / 2), 
                'rect',
                1, 
                '#000', 
                this.strokePG, 
                windowHeight - (this.heightHeader - this.strokePG)
            );
            // white rect
            monit.addTarget(
                windowWidth / 2, 
                (this.heightHeader / 2) - this.strokePG, 
                'rect',
                0, 
                '#fff', 
                windowWidth, 
                (this.heightHeader) 
            );

        }
        this.createPlayground = false;
        // playground
        // push();
        // noFill();
        // strokeWeight(this.strokePG);
        // stroke('black');
        // line(windowWidth / 2, 0, windowWidth / 2, windowHeight);
        // // header
        // rectMode(CENTER);
        // fill('#fff');
        // rect(windowWidth / 2, this.heightHeader / 2, windowWidth - this.strokePG, this.heightHeader - this.strokePG);
        // pop();

        // this.drawPoints();
    }
    // draw points on header
    this.removePoint = function(player){
        var limit = player == this.charNameAvatar ? 0 : windowWidth,
        min = windowWidth,
        elected = {};
        monit.getTargets().filter(d => d.img == player).forEach((el, i, ar) => {
            var calc = Math.abs(limit - el.posX);
            if(calc < min){
                elected = el;
                min = calc;
            }
        });
        monit.removeDistractor(elected);
    }
    // draw attack part of the game
    this.attack = function(){
        if(!thereIsThisAnimation('hitPlayer')){
            // enable timeout action
            if(this.startActionTime == null){
                this.startActionTime = new Date().getTime();
            }
            if(new Date().getTime() - this.startActionTime > this.timActionDuration){
                removeAnimation('watchedPlayer')
                this.addHitPlayerAnimation(2);
                console.log('i\'m the timer');
                this.startActionTime = new Date().getTime();
                monit.playSound('launch_ball.wav');
            }
            // moving animal
            if(monit.GameScene == 2){
                this.animal.item.posX = (this.movingPath[this.movingInd].x * (windowWidth / 2)) + (this.direction == 'left' ? (windowWidth / 2) : 0);
                this.animal.item.posY = (this.movingPath[this.movingInd].y * (windowHeight - 300)) + this.heightHeader;
                this.movingInd += 10;
                if(this.movingInd >= this.movingPath.length){
                    this.movingInd = this.movingInd % this.movingPath.length;
                }
            }else{
                this.animal.posX = this.startPosAnimal[0]
                this.animal.posY = this.startPosAnimal[1]
            }
            // moving target
            if(isEmpty(this.terapistMovement)){
                if(!monit.fixationObjects.filter(f => f.id == this.animal.item.id).length){
                    this.target.posX += (monit.eyeTracker.posX - this.target.posX) * 0.5;
                    this.target.posY += (monit.eyeTracker.posY - this.target.posY) * 0.5;
                    this.constrainItem(this.target, ...this.constrainAnimal);
                }else{
                    this.target.posX = this.animal.item.posX;
                    this.target.posY = this.animal.item.posY;
                }
                this.checkInteractionTargets(tg => tg.id == this.animal.item.id);
            }else if(!this.terapistMovement.endReached){
                // removeAnimation('timerAnimation');
                this.terapistMovement.move();
            }else{
                // hit player after the target reached it
                this.addHitPlayerAnimation(1);
                this.terapistMovement = {}
            }
            monit.animateTarget(monit.getTargetByName(this.charName), '-' + this.direction + '-palla', 5);
            monit.animateTarget(monit.getTargetByName(this.animalName), '-' + this.direction + '-scappa', 5);
            this.drawTimerAnimation();
        }else{
            removeAnimation('timerAnimation');
            removeAnimation('watchedPlayer');
        }
        this.watchPlayer();
    }
    // draw defense part
    this.defense = function(){
        if(!thereIsThisAnimation('hitPlayer')){
            if(this.startActionTime == null){
                this.startActionTime = new Date().getTime();
            }
            if(new Date().getTime() - this.startActionTime > this.timActionDuration){
            // enable timeout action
                removeAnimation('watchedPlayer')
                this.startActionTime = new Date().getTime();
                this.addHitPlayerAnimation(2);
                console.log('i\'m the timer');
            }
            // moving animal
            this.target.posX += (this.char.item.posX - this.target.posX) * 0.01;
            this.target.posY += (this.char.item.posY - this.target.posY) * 0.01;
            // get previous positions
            this.char.prevPosX = this.char.item.posX;
            this.char.prevPosY = this.char.item.posY;
            // moving target
            var boxOv = this.checkOverlapItem(this.charName, 'box')
            if(boxOv.res){
                var probability = random(100);
                if(probability > 0 && probability < 50) this.addItem('slime.png', 'obstacle', this.char.item.posX, this.char.item.posY)
                else this.addItem('scudo.png', 'protection', this.char.item.posX, this.char.item.posY)
                _.remove(this.boxes, b => b.id == this.boxOverlapped.id);
                monit.removeDistractorById(this.boxOverlapped.id);
                this.boxOverlapped = {};
            }
            // check for obstacle elements
            if(this.obstTimer == 0){
                var obst = this.checkOverlapItem(this.charName, 'obstacle');
                if(obst.res){
                    this.obstTimer = new Date().getTime();
                    var temp = _.cloneDeep(animations['fallen'].obj);
                    temp.index = 0;
                    temp.obj = this.char.item;
                    temp.end = this.obstDur;
                    animationsStack.push(temp);
                    _.remove(this.obstacles, el => el.id == this.obstacleOverlapped.id);
                    monit.removeDistractorById(this.obstacleOverlapped.id);
                    this.obstacleOverlapped = {}
                    monit.playSound('slip.wav');
                    var temp3 = _.cloneDeep(animations['protectionUsed'].obj);
                    temp3.obj = this.char.item;
                    temp3.index = 0;
                    temp3.color = 'rgba(255, 255, 255,1)';
                    temp3.end = 1000;
                    animationsStack.push(temp3)
                }else if(isEmpty(this.terapistMovement)){
                    this.char.item.posX += (monit.eyeTracker.posX - this.char.item.posX) * 0.8;
                    this.char.item.posY += (monit.eyeTracker.posY - this.char.item.posY) * 0.8;
                    // check target and character interaction
                    this.constrainItem(this.char.item, ...this.constrainChar);
                    this.checkCharAndTargetOverlap();
                }else if(!this.terapistMovement.endReached){
                    this.terapistMovement.move();
                }else{
                    // hit player after the target reached it
                    this.addHitPlayerAnimation(1);
                    this.terapistMovement = {}
                }
            }else if(new Date().getTime() - this.obstTimer > this.obstDur){
                console.log('stopping', new Date().getTime() - this.obstTimer)
                this.obstTimer = 0;
                // check target and character interaction
                this.constrainItem(this.char.item, ...this.constrainChar);
                this.checkCharAndTargetOverlap();
            }
            // animating character ant animal
            monit.animateTarget(monit.getTargetByName(this.charName), '-' + this.direction + '-scappa', 5);
            monit.animateTarget(monit.getTargetByName(this.animalName), '-' + this.direction + '-palla', 5);
            this.drawTimerAnimation();
        }else{
            removeAnimation('timerAnimation');
            removeAnimation('watchedPlayer');
        }
        this.watchChar();
    }
    // add animal hitting animation 
    this.addWatchedCharAnimation = function(){
        var temp = _.cloneDeep(animations['watchedPlayer'].obj);
        temp.timestamp = getCurrTM();
        temp.obj = this.char.item;
        animationsStack.push(temp);
    }
    // add animal hitting animation 
    this.addHitPlayerAnimation = function(state){
        var temp = _.cloneDeep(animations['hitPlayer'].obj);

        temp.timestamp = getCurrTM();
        temp.obj = this.ball;
        // let tgToHit = this.currState == 'attack' ? this.animal.item : this.char.item;
        temp.movingObj = {
            rX: this.ball.posX,
            rY: this.ball.posY
        };
        console.log(state)
        if(state == 2 && ((this.target.posX == this.char.item.posX && this.target.posY == this.char.item.posY ) ||
            (this.target.posX == this.animal.item.posX && this.target.posY == this.animal.item.posY ))){
            temp.movingObj.tX = this.target.posX + (this.target.posX < windowWidth / 2 ? 300 : -300);
            temp.movingObj.tY = this.target.posY;
        }else{
            temp.movingObj.tX = this.target.posX;
            temp.movingObj.tY = this.target.posY;
        }
        // check for protection elements
        if(this.currState == 'defense'){
            if(state == 1){
                var pr = this.isInTrajectory(temp.movingObj);
                if(pr.res) {
                    temp.movingObj.tX = pr.obj.posX;
                    temp.movingObj.tY = pr.obj.posY;
                    this.launchingBall = 2;
                    this.usedProtection = pr.obj;
                }else{
                    this.launchingBall = state;
                }
            }else{
                this.launchingBall = state;   
            }
        }else{
            this.launchingBall = state;
        }
        this.startActionTime = null;
        this.timeoutAction = null;
        removeAnimation('timerAnimation');
        animationsStack.push(temp);
        monit.playSound('launch_ball.wav');
    }
    // check end fixation on animal
    this.watchPlayer = function(){
        let me = this;
        monit.fixationObjects.forEach( f => {
            if(f.counter > monit.fixationTime){
                me.addHitPlayerAnimation(1); 
                // move the ball 
                _.remove(monit.fixationObjects, fr => f.id == fr.id);
                removeAnimation('targetOverlap', f.id);
                monit.sendEvent({ts: new Date().getTime(), type:"event", event_type: "fixation_time_reached", data: f.id});
            }
        });
    }
    // handle watchPlayer animation in order to trigger character hit animation
    this.watchChar = function(){
        if(this.stareCharCounter > animations['watchedPlayer'].obj.end){
            _.remove(animationsStack, a => a.type == 'watchedPlayer');
            this.addHitPlayerAnimation(1);
            this.stareCharCounter = 0;
        }
    }
    // constrain object inside given limits
    this.constrainItem = function(item, top, right, bottom, left){
        // horizontal
        var refDim = assetsDim[item.img];
        if(item.posX - (refDim.sizX / 2) < left){
            item.posX = left + (refDim.sizX / 2);
        }else if(item.posX + (refDim.sizX / 2) > right){
            item.posX = right - (refDim.sizX / 2);
        }
        // vertical
        if(item.posY - (refDim.sizY / 2) < top){
            item.posY = top + (refDim.sizY / 2);
        }else if(item.posY + (refDim.sizY / 2) > bottom){
            item.posY = bottom - (refDim.sizY / 2);
        }
    }
    // check if target interact with character
    this.checkCharAndTargetOverlap = function(){
        // console.log(Math.abs(this.char.prevPosX - this.char.item.posX))
        var isRunningAway = Math.abs(this.char.prevPosX - this.char.item.posX) > 8 ? 1 : 0;
        isRunningAway = Math.abs(this.char.prevPosX - this.char.item.posX) > 8 || isRunningAway ? 1 : 0;
        this.char.prevPosX = this.char.item.posX;
        this.char.prevPosY = this.char.item.posY;
        if(checkHalfOverlap(this.target, this.char.item) && !isRunningAway){
            this.target.posX = this.char.item.posX;
            this.target.posY = this.char.item.posY;
            if(_.find(monit.fixationObjects, d => d.id === this.char.item.id) == undefined){
                // add target to the interacted list
                this.stareCharCounter += (animations['watchedPlayer'].obj.end / (animations['watchedPlayer'].obj.end / 33));
                if(!thereIsThisAnimation('watchedPlayer')){
                    this.addWatchedCharAnimation();
                    removeAnimation('timerAnimation', this.target.id);
                }
            }
        }else{
            removeAnimation('watchedPlayer');
            this.stareCharCounter = 0;
        }
    }
    // draw action attack and defense 
    this.drawActionAnimation = function(cb){
        var elToRemove = [];
        let me = this;
        this.playgroundAnimationBucket.forEach(el => {
            if(el.index < el.duration){
                // monit.getTargets().filter( d => d.id == el.id).forEach(d => {
                var currTg = monit.pState.targets[el.id];
                currTg.posX = map(el.index, 0, el.duration, el.startX, el.endX);
                currTg.posY = map(el.index, 0, el.duration, el.startY, el.endY);
                var animateWhichWith = [];
                switch(me.lastAction){
                    case 'hit animal':
                        animateWhichWith = [me.animalName, '-' + me.direction + '-down'];
                        me.constrainItem(currTg, ...me.constrainAnimal);
                        break;
                    case 'missed animal':
                        animateWhichWith = [me.animalName, '-' + me.direction + '-hurra'];
                        me.constrainItem(currTg, ...me.constrainAnimal);
                        break;
                    case 'hit char':
                        animateWhichWith = [me.charName, '-' + me.direction + '-down'];
                        me.constrainItem(currTg, ...me.constrainChar);
                        break;
                    case 'missed char':
                        animateWhichWith = [me.charName, '-hurra'];
                        me.constrainItem(currTg, ...me.constrainChar);
                        break;
                }
                monit.animateTarget(monit.getTargetByName(animateWhichWith[0]), animateWhichWith[1], 5);
                // animate the other player
                if(animateWhichWith[1].indexOf('-down') !== -1){
                    var hurraAnimation = (
                        animateWhichWith[0] == me.charName ? 
                        [me.animalName, '-' + me.direction + '-hurra'] : 
                        [me.charName, '-hurra'])
                    monit.animateTarget(monit.getTargetByName(hurraAnimation[0]), hurraAnimation[1], 5)
                }
                el.index += el.duration / (el.duration / 33)
            }else{
                elToRemove.push(el);
            }
        });
        elToRemove.forEach(el => {
            me.counterBall++;
            if(me.counterBall == me.nBallThrow){
                me.counterBall = 0;
                me.matchState = me.matchState == 0 ? 1 : 0;
            }
            _.remove(me.playgroundAnimationBucket, obj => obj.id == el.id);
        });
        if(!this.playgroundAnimationBucket.length){
            cb();
        }
    }
    // check points and remove 
    this.checkPoints = function(){
        var drawnPoints = monit.getTargets().filter( d => d.img.indexOf(this.charNameAvatar) != -1).length;
        if(drawnPoints > (this.nPoints - this.animal.score)){
            monit.removeTarget(monit.getTargetByName(this.charNameAvatar));
        }
        drawnPoints = monit.getTargets().filter( d => d.img.indexOf(this.animalNameAvatar) != -1).length;
        if(drawnPoints > (this.nPoints - this.char.score)){
            monit.removeTarget(monit.getTargetByName(this.animalNameAvatar));
        }
    }
    // add action timer animation
    this.drawTimerAnimation = function(){
        if(!thereIsThisAnimation('watchedPlayer', this.currState == 'attack' ? this.animal.item.id : this.char.item.id) && !thereIsThisAnimation('timerAnimation', this.target.id)){
            console.log('timer animation creation')
            var temp = _.cloneDeep(animations['timerAnimation'].obj);
            temp.timestamp = getCurrTM();
            temp.end = this.timActionDuration;
            temp.obj = this.target;
            temp.index = new Date().getTime() - this.startActionTime;
            temp.circle = new Circle(temp.obj.posX, temp.obj.posY, temp.end - temp.index)
            animationsStack.push(temp);
        }
    }
    // draw explanation of defense
    this.drawDefenseExplanation = function(){
        if(!this.setExplanationState){
            this.setExplanationState = 1;
            monit.removeDistractor(monit.getDistractorByName('ball.png'));
            this.char.item.posX = windowWidth / 4;
            this.char.item.posY = windowHeight / 2;
            pulseObjects.push(new Pulse(this.char.item))
            setTimeout(() => {
                this.currState ='defense';
                this.setExplanationState = 0;
                this.animal.item.posX = this.startPosAnimal[0];
                this.animal.item.posY = this.startPosAnimal[1];
                this.ball = monit.addDistractor(0, 0, 'ball.png', 10);
                this.ball.posX = monit.getTargetByName(this.animalName).posX + ((this.direction == 'left' ? 1 : -1) * (assetsDim[this.animalName].sizX / 2));
                this.ball.posY = monit.getTargetByName(this.animalName).posY - (assetsDim[this.animalName].sizY / 2);
                this.startActionTime = new Date().getTime();
                pulseObjects[0].obj.sizX = pulseObjects[0].refX;
                pulseObjects[0].obj.sizY = pulseObjects[0].refY;
                pulseObjects = [];
            }, 3000);
        }   
        monit.animateTarget(monit.getTargetByName(this.charName), '-' + this.direction + '-scappa', 5)
        this.target.posX += this.direction == 'left' ? -2 : 2;
        // this.char.item.posX -= 2;
        this.char.prevPosX = this.char.item.posX;
        this.char.prevPosY = this.char.item.posY;
        // moving target
        this.char.item.posX += (monit.eyeTracker.posX - this.char.item.posX) * 0.8;
        this.char.item.posY += (monit.eyeTracker.posY - this.char.item.posY) * 0.8;
        this.constrainItem(this.char.item, ...this.constrainChar);
    }
    // draw explanation of attack
    this.drawAttackExplanation = function(){
        if(!this.setExplanationState){
            this.setExplanationState = 1;
            monit.removeDistractor(monit.getDistractorByName('ball.png'));
            this.animal.item.posX = (windowWidth / 4) * (this.direction == 'left' ? 3 : 1);
            this.animal.item.posY = windowHeight / 2;
            pulseObjects.push(new Pulse(this.target))
            setTimeout(() => {
                this.currState ='attack';
                this.setExplanationState = 0;
                this.char.item.posX = this.startPosChar[0];
                this.char.item.posY = this.startPosChar[1];
                this.ball = monit.addDistractor(0, 0, 'ball.png', 10);
                this.ball.posX = monit.getTargetByName(this.charName).posX + ((this.direction == 'left' ? -1 : 1) * (assetsDim[this.animalName].sizX / 2));
                this.ball.posY = monit.getTargetByName(this.charName).posY - (assetsDim[this.charName].sizY / 2);
                this.startActionTime = new Date().getTime();
                pulseObjects[0].obj.sizX = pulseObjects[0].refX;
                pulseObjects[0].obj.sizY = pulseObjects[0].refY;
                pulseObjects = [];
            }, 3000);
        }   
        monit.animateTarget(monit.getTargetByName(this.animalName), '-' + this.direction + '-scappa', 5)
        this.target.posX += this.direction == 'left' ? 2 : -2;
        this.animal.item.posX += this.direction == 'left' ? 2 : -2;
    }
    this.ballPosition = function(item, itemPosition){
        switch(itemPosition){
            case 'left':
                return [
                    monit.getTargetByName(item).posX - (assetsDim[item].sizX / 2),
                    monit.getTargetByName(item).posY - (assetsDim[item].sizY / 2)
                ]
                break;
            case 'right':
                return [
                    monit.getTargetByName(item).posX + (assetsDim[item].sizX / 2),
                    monit.getTargetByName(item).posY - (assetsDim[item].sizY / 2)
                ]
                break;
        }
    }
    
    // check if a protection con 
    this.isInTrajectory = function(path){
        var stepLen = 50;
        var lenX = path.tX - path.rX;
        var lenY = path.tY - path.rY;
        // console.log('distanze', lenX, lenY);
        var stepX = lenX / stepLen;
        var stepY = lenY / stepLen;
        for(var z in this.protections){
            var x = y = 0;
            for(x = 0; x < stepLen; x++){
                var movObj = {
                    posX: path.rX + (x * stepX) + (this.ball.sizX / 2),
                    posY: path.rY + (y * stepY) + (this.ball.sizY / 2),
                    sizX: this.ball.sizX,
                    sizY: this.ball.sizY
                }
                if(checkOverlap(movObj, this.protections[z])){
                    return {res: true, obj: this.protections[z]}; 
                }
                y++;
            }
        }
        return {res: false};
    }
    this.checkOverlapItem = function(name, type){
        var player = monit.getTargetByName(name);
        switch(type){
            case 'obstacle':
                var arr = _.cloneDeep(this.obstacles); 
                break;
            case 'box':
                var arr = _.cloneDeep(this.boxes); 
                break;
        }
        if(player !== undefined){
            for(var x in arr){
                playerObj = {
                    posX: player.posX, 
                    posY: player.posY,
                    sizX: assetsDim[player.img].sizX / 2,
                    sizY: assetsDim[player.img].sizY / 2
                };
                if(checkOverlap(playerObj, arr[x])){
                    switch(type){
                        case 'obstacle':
                            this.obstacleOverlapped = arr[x];
                            break;
                        case 'box':
                            this.boxOverlapped = arr[x];
                            break;
                    }
                    return {res: true, obj: arr[x]};
                }
            };
            return {res: false};
        }
    }
    this.resetActionPositions = function(){
        if(this.currState == 'animationAttack' || this.currState == 'defense'){
            this.ball.posX = monit.getTargetByName(this.animalName).posX + ((this.direction == 'left' ? 1 : -1) * (assetsDim[this.animalName].sizX / 2));
            this.ball.posY = monit.getTargetByName(this.animalName).posY - (assetsDim[this.animalName].sizY / 2);
            this.startActionTime = new Date().getTime();
        }else {
            this.ball.posX = monit.getTargetByName(this.charName).posX + ((this.direction == 'left' ? -1 : 1) * (assetsDim[this.animalName].sizX / 2));
            this.ball.posY = monit.getTargetByName(this.charName).posY - (assetsDim[this.charName].sizY / 2);
            this.startActionTime = new Date().getTime();   
        }
    }
    this.addPeriodicallyExtraEls = function(){
        let interval = 10000;
        if(monit.GameScene){
            if(new Date().getTime() - this.extraElsTimer > interval){
                if([2,3,4].indexOf(monit.GameLevel) != -1){
                    switch(monit.GameLevel){
                        case 2: // protection
                            if(this.protections.length < this.numMaxExtraEls){
                                this.addItem('pupazzo-neve.png', 'protection');
                            }else{
                                this.extraElsTimer = new Date().getTime();
                            }
                            break;
                        case 3: // obstacle
                            if(this.obstacles.length < this.numMaxExtraEls){
                                this.addItem('telo-1.png', 'obstacle');
                            }else{
                                this.extraElsTimer = new Date().getTime();
                            }
                            break;
                        case 4: // surprise
                            if(this.boxes.length < this.numMaxExtraEls){
                                this.addItem('forziere.png', 'box');
                            }else{
                                this.extraElsTimer = new Date().getTime();
                            }
                            break;
                    }
                }
                this.extraElsTimer = new Date().getTime();
            }
        }
    }
}

var currReachedTargets = [];
function checkReachTarget(id){
    var eyet = monit.eyeTracker;
    // monit.pState.targets[id].forEach(tg => {
        tg = monit.pState.targets[id]
        var overlapF = checkOverlap({posX: eyet.posX + tg.sizX / 2, posY: eyet.posY + tg.sizY / 2, sizX: eyet.sizX, sizY: eyet.sizY}, tg)
        if(overlapF && currReachedTargets.indexOf(tg.id) == -1){
            monit.sendEvent({ts: new Date().getTime(), type: "event", event_type: "start_fixation", data: tg.id});
            currReachedTargets.push(tg.id);
        }
        if(!overlapF && currReachedTargets.indexOf(tg.id) != -1){
            monit.sendEvent({ts: new Date().getTime(), type: "event", event_type: "end_fixation", data: tg.id});
            _.remove(currReachedTargets, e => e == tg.id);
        }
    // })
}
var pulseObjects = [];
var Pulse = function(obj){
    this.obj = obj;
    this.refX = obj.sizX;
    this.refY = obj.sizY;
    this.sInd = 0;
    this.amp = 30;
    this.draw = function(obj){
        this.sInd = this.sInd > 2 * PI ? 0 : this.sInd + 0.30;
        this.obj.sizX = this.refX + this.amp * sin(this.sInd);
        this.obj.sizY = this.refY + this.amp * sin(this.sInd) * (this.refY / this.refX);
    }
}
var ItemMovements = function(el, stX, stY, enX, enY, time){
    this.obj = el;
    this.stX = stX;
    this.stY = stY;
    this.enX = enX;
    this.enY = enY;
    this.time = time;
    this.spStepX = (this.enX - this.stX) / (this.time / 33);
    this.spStepY = (this.enY - this.stY) / (this.time / 33);
    this.endReached = false;
    this.move = function(){
        var ruleX = this.enX < this.stX ? this.obj.posX > this.enX : this.obj.posX < this.enX;
        var ruleY = this.enY < this.stY ? this.obj.posY > this.enY : this.obj.posY < this.enY;
        if(ruleX || ruleY){
            this.obj.posX += this.spStepX;
            this.obj.posY += this.spStepY;
        }else{
            this.endReached = true;
        }
    }
}