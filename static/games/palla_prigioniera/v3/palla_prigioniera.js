var animations = [];
var backgroundGame = ['parchetto.png', 'parchetto.png', 'montagna.png', 'spiaggia2.png', 'castello2.png'];
var animalNames = ['elefante', 'leone', 'yeti', 'tricheco', 'animal'];
var animationsStack = [];
var GameTransitionAnimStep = 0;
var assetsDim = {};
var catAndObjsJson = {};
var catAndObjsJsonFl = 0;
var correctObjectMove = 0;
var rotatingObjs = [];
var countDownIndx = 0;
var suggestFl = 0;
var movingPath = {};
var pathJsonFl = 0;
// levels objec;
var levels = {};
var urlParams = (new URL(location.href)).searchParams;
var orientation = urlParams.get('side') == 'true' ? 'left' : 'right';
var level = urlParams.get('level') == undefined ? 0 : urlParams.get('level'); 
var bgEyeTracker = {};
var firstStateButtons = 0;
var charName = 'char-' + orientation + '.png';
var animalName = animalNames[level] + '.png';
// var animalName = animalNames[level] + '-' + orientation + '.png';
var actionDur = urlParams.get('actionDur');
if(actionDur == undefined) actionDur = 15
else actionDur = urlParams.get('actionDur')

// three parameters of the game board
// LEVELS CLASS in order to handle
var Player = function(tg){
    this.item = tg;
    this.score = 0;
    this.prevPosX = 0;
    this.prevPosY = 0;
}
var playground = {};
// circle to fixation
var Circle = function(x, y, dur){
    this.ind = 0;
    this.r = 100;
    this.x = x;
    this.y = y;
    this.done = 0;
    this.inc = (2 * PI) / (dur / 33);
    this.draw = function(){
        if(this.ind < 2 * PI){
            push();
            stroke(200);
            fill(255,100)
            ellipse(this.x, this.y, this.r, this.r);
            fill(255,210);
            arc(this.x, this.y, this.r, this.r, -PI/2 + this.ind, (3/2)*PI, PIE); //
            pop();
            if(monit.trackerActive) this.ind += this.inc;
        }else{
            this.done = 1;
        }
    }
}

var countdown = {};
var CountDown = function(to){
    this.dur = 3000;
    this.index = 0;
    this.count = function(){
        var currInd = this.index % (this.dur / 3),
        currImg = 3;
        push();
        imageMode(CENTER);
        var sizX = map(currInd, 0, (this.dur / 3), assetsDim['1.png'].sizX, assetsDim['1.png'].sizX + 200),
        sizY = sizX * (assetsDim['1.png'].sizY / assetsDim['1.png'].sizX)
        if(this.index > 0 && this.index < this.dur / 3){
            currImg = 3;
        }else if(this.index < (this.dur / 3) * 2){
            currImg = 2;
        }else if(this.index < this.dur){
            currImg = 1;
        }else{ // end timeout
            pop();
            setGameState(to);
            return;
        }
        image(monit.images[currImg + '.png'], windowWidth / 2, windowHeight / 2, sizX, sizY);
        fill('rgba(0,0,0,' + map(currInd, 0, (this.dur / 3), 0, 1) + ')');
        rect(0, 0, windowWidth, windowHeight);
        pop();
        this.index += this.dur / (this.dur / 33);
    }
}

// array of pulsating objects
var pulsatingObjs = [];
var pulseTimer = 0;
var surfaceMov = {}
currInstr = {};
animations['targetOverlap'] = { 
    cb: function(el) {
        el.circle.x = el.obj.posX;
        el.circle.y = el.obj.posY;
        el.circle.draw();
        el.index += (el.end / 33);
    },
    obj: {
        type: 'targetOverlap', 
        id: '', 
        index: 0,
        obj: {}, 
        timestamp: 0, 
        end: 500
    }
};
animations['timerAnimation'] = { 
    cb: function(el) {
        // el.circle.x = el.obj.posX;
        // el.circle.y = el.obj.posY;
        el.circle.x = windowWidth / 2;
        el.circle.y = (playground.heightHeader / 2);
        el.circle.draw();
        if(monit.trackerActive) {
            el.index += el.end / (el.end / 33);
        }
    },
    obj: {
        type: 'timerAnimation', 
        index: 0,
        obj: {}, 
        timestamp: 0, 
        end: 500
    }
};
// bouncing animation on character
animations['bouncing'] = {
    cb: function(el){
        el.obj.posY = el.refY - 200*sin(map(el.index, 0, el.end, 0, PI));
        el.obj.posX = el.refX + map(el.index, 0, el.end, 0, ((el.currState == 'animationAttack' && el.orientation == 'left') ||
                                                             (el.currState != 'animationAttack' && el.orientation == 'right')) ? -100 : 100)
        el.index += el.end / (el.end / 33);
    },
    obj:{
        type: 'bouncing',
        timestamp: getCurrTM(),
        currState: '',
        index: 0,
        obj: {},
        refY: 0,
        refX: 0,
        orientation: '',
        end: 1000
    }
}
// bouncing animation on character
animations['hitPlayer'] = {
    cb: function(el){
        var incX = (el.movingObj.tX - el.movingObj.rX) / (el.end  / 33), 
        incY = (el.movingObj.tY - el.movingObj.rY) / (el.end  / 33);
        // background(bg + map(el.index, 0, el.end, 0, 0.99).toString() + ')')
        el.obj.posX += incX;
        el.obj.posY += incY;//+ 10*sin(map(el.index, 0, el.end, 0, PI));
        el.index += el.end / (el.end / 33);
    },
    obj:{
        type: 'hitPlayer',
        timestamp: getCurrTM(),
        index: 0,
        obj: {},
        movingObj: {},
        end: 1000
    }
}

animations['wrong'] = {
    cb: function(el){
        el.obj.posX = sin(map(el.index, 0, el.end, 0, PI)) * 10 * sin(map(el.index, 0, el.end, 0, 5 * (2 * PI))) + el.refX;
        el.index += (el.end / 33);
    },
    obj: {
        type: 'wrong',
        index: 0,
        refX: 0,
        timestamp: 0,
        obj: {},
        end: 1000
    }
}
// watched char
animations['watchedPlayer'] = {
    cb: function(el){
        let col = map(el.end, 100, 1000, 150, 50),
        dim = map(el.end, 100, 1000, 250, 400)
        fill(map(el.index, 0, el.end, 200, 255), map(el.index, 0, el.end, 150, col), map(el.index, 0, el.end, 150, col), 150);
        ellipse(el.obj.posX, el.obj.posY, map(el.index, 0, el.end, 100, dim));
        el.index += el.end / (el.end / 33);
    },
    obj: {
        type: 'watchedPlayer',
        index: 0,
        refX: 0,
        timestamp: 0,
        obj: {},
        end: 1000
    }
}
// slipe animation
animations['fallen'] = {
    cb: function(el){
        var bigger = 20;
        el.obj.rot = (3.14 / 4) * sin(map(el.index, 255, 0, 0, PI));
        // el.obj.sizX = assetsDim[el.obj.img].sizX + (40 * sin(map(el.index, 255, 0, 0, 2 * PI)))
        // el.obj.sizY = assetsDim[el.obj.img].sizY + (40 * sin(map(el.index, 255, 0, 0, 2 * PI)) * (assetsDim[el.obj.img].sizY / assetsDim[el.obj.img].sizX))
        el.index += el.end / (el.end / 33);
    },
    obj: {
        type: 'fallen', 
        index: 255, 
        timestamp: getCurrTM(), 
        end: 1000
    }
};
animations['protectionUsed'] = {
    cb: function(el){
        let dim = map(el.end, 0, 1000, 250, 400)
        let col = color(el.color);
        push();
        // console.log(alpha(map(el.index, 0, el.end, 0.7, 0)))
        fill('rgba(' + red(col) + ', ' + green(col) + ', ' + blue(col) + ', ' + map(el.index, 0, el.end, 0.7, 0) + ')');
        ellipse(el.obj.posX, el.obj.posY, map(el.index, 0, el.end, 100, dim));
        pop();
        el.index += el.end / (el.end / 33);  
    },
    obj: {
        type: 'protectionUsed', 
        index: 0, 
        color: '',
        timestamp: getCurrTM(), 
        end: 1000   
    }
}
// reset targets distractor and fixation objects
function resetAll(){
    // monit.fixationObjects = [];
    monit.clearTargets();
    monit.clearDistractors();
}
function drawAnimations(){
    animationsStack.forEach(animObj =>{
        // console.log(animObj);
        if(animObj.index > animObj.end){
            _.remove(animationsStack, d => d.obj.id == animObj.obj.id && d.type == animObj.type);
        }else{
            animations[animObj.type].cb(animObj);
        }
    })
}
// fill assets dimension object
function setAssetsSizes(){
    monit.json.details.ingame_assets.forEach(el => {
        assetsDim[el.name] = {sizX: el.sizX * (window.innerWidth / monit.idealRes.width), sizY: el.sizY * (window.innerHeight / monit.idealRes.height)}
    });
    console.log('assets dimension', assetsDim);
}
// load path of moving player
function loadPlayerPath(){
    fetch('path.json').then(res => {return res.json()}).then(data =>{
        movingPath = data;            
        pathJsonFl = 1;
    });
}
// -------------- PRELOAD --------------
function preload() {
    monit.loadJsonItems();
}
// -------------- SETUP ----------------
function setup() {
    createCanvas(windowWidth,windowHeight);
    frameRate(30);
    setGameState(0);
    monit.skipIntro = 1;
    monit.onChangeDynamics = (data) => {
        console.log('data', data);
        var type = data.id;
        switch(type){
            case 'ft':
                monit.fixationTime = parseInt(data.value);
                break;
            case 'actionDur':
                if(playground != undefined){
                    console.log('changed action duration', parseInt(data.value));
                    playground.timActionDuration = parseInt(data.value) * 1000;
                    removeAnimation('timerAnimation');
                }
                break;
        }
    }
    monit.onBackNext = function(){
        playground = {};
        setGameState(2);
        monit.sendEvent({ts: new Date().getTime(), type: "event", event_type: "buttons_state", data: {cooldown: false, back: false, next: false}});
    }
    function makeAction() {
        if(playground != undefined){
            if(playground.currState == 'attack' || playground.currState == 'defense'){
                if(monit.GameScene != 0){
                    // removeAnimation('watchedPlayer');
                    // removeAnimation('timerAnimation');
                    // playground.startActionTime = null;
                    // playground.addHitPlayerAnimation(2);
                    // playground.startActionTime = new Date().getTime();
                }else if(isEmpty(playground.terapistMovement)){
                    // add automatically reach and hit player animation
                    // var targetToMove = monit.getDistractorByName(playground.target = monit.addDistractor(windowWidth / 2, windowHeight / 2, playground.targetDef, 4));

                    // var playerToHit = monit.getTargetByName(playground.currState == 'attack' ? animalName : charName) 
                    // playground.terapistMovement = new ItemMovements(
                    //     targetToMove,
                    //     targetToMove.posX,
                    //     targetToMove.posY,
                    //     playerToHit.posX,
                    //     playerToHit.posY,
                    //     1500
                    // )
                }
            }//else if(playground.currState == 'charVictory' || playground.currState == 'charDefeat'){
            //     playground.currState = 'attack';
            //     playground.initGame();
            // }
        }
    }
    monit.onRemoveTarget = makeAction;
    monit.onAddTarget = makeAction;
    monit.onMoveTarget = () =>{};
    monit.onToggleTracker = val => {
        if(!isEmpty(playground)){
            if((monit.GameScene == 2 || !monit.GameScene) && (playground.currState == 'attack' || playground.currState == 'defense') && !playground.launchingBall){
                if(val == 1) monit.trackerActive = true;
                else monit.trackerActive = false;
            }
        }
    };
    monit.onAttentionSig = () =>{
        if(!isEmpty(playground)){
            switch (playground.currState) {
                case 'attack':
                    if(!pulsatingObjs.filter(p => p.obj.id == playground.animal.item.id).length){
                        pulsatingObjs.push(new Pulse(playground.animal.item));  
                    }     
                    break;
                case 'defense':
                    if(!pulsatingObjs.filter(p => p.obj.id == playground.char.item.id).length){
                        pulsatingObjs.push(new Pulse(playground.char.item)); 
                    }
                    break;
            }
            pulseTimer = new Date().getTime();
            monit.playSound('richiamo.mp3');
        }
    }
    // monit.setBg('#c1e5ca');
}
// animation states: 'launching ball', 'hit player'
var attackAndDefenseAnimState = 'launching-ball';
var attDefAnimDur = 1000;
var movingBall = {};
var movingAnimal = {};
var movingChar = {};
var animAD = {};
var chAD = {};
var ballAD = {}; 
// var movingPlayer = {};

function drawAttackDefense(){
    // add players
    if(monit.getDistractorByName(charName) == undefined){
        chAD = monit.addDistractor((windowWidth / 4) * (orientation == 'right' ? 3 : 1), windowHeight / 2, charName);
        animAD = monit.addDistractor((windowWidth / 4) * (orientation == 'left' ? 3 : 1), windowHeight / 2, animalName);
    }
    switch(monit.GameLevel){
        case 0: // only ATTACK
            switch(attackAndDefenseAnimState){
                case 'launching-ball':
                    monit.animateDistractor(chAD, '-' + orientation + '-palla', 5);
                    monit.animateDistractor(animAD, '-' + orientation + '-scappa', 5);
                    if(isEmpty(movingBall)){
                        ballAD = monit.addDistractor(chAD.posX - (assetsDim[charName].sizX / 2), chAD.posY - (assetsDim[charName].sizY / 2), 'ball.png');
                        movingBall = new ItemMovements(
                            ballAD, 
                            ballAD.posX, 
                            ballAD.posY, 
                            (windowWidth / 4) * (orientation == 'left' ? 3 : 1), 
                            windowHeight / 2, 
                            attDefAnimDur
                        );
                    }else{
                        if(!movingBall.endReached){
                            movingBall.move();
                        }else{
                            // monit.removeDistractor(monit.getDistractorByName('ball.png'));
                            movingBall = {};
                            attackAndDefenseAnimState = 'hit-player';
                            // ball.posX = monit.getDistractorByName(charName).posX - (assetsDim[charName].sizX / 2);
                            // ball.posY = monit.getDistractorByName(charName).posY - (assetsDim[charName].sizY / 2);
                        }
                    }
                    break;
                case 'hit-player':
                    monit.animateDistractor(chAD, '-' + orientation + '-palla', 5);
                    if(isEmpty(movingAnimal)){
                        movingAnimal = new ItemMovements(
                            animAD,
                            animAD.posX,
                            animAD.posY,
                            animAD.posX + ((orientation == 'left' ? 1 : -1) * 200),
                            animAD.posY,
                            attDefAnimDur                           
                        )
                    }
                    if(!movingAnimal.endReached){
                        movingAnimal.move();
                        monit.animateDistractor(monit.getDistractorByName(animalName), '-' + orientation + '-down', 5);
                        if(!thereIsThisAnimation('bouncing')){
                            var temp = _.cloneDeep(animations['bouncing'].obj);
                            temp.timestamp = getCurrTM();
                            temp.obj = ballAD;
                            temp.refY = temp.obj.posY;
                            temp.refX = temp.obj.posX;
                            temp.orientation = orientation;
                            temp.currState = 'animationAttack';
                            temp.end = attDefAnimDur;
                            animationsStack.push(temp);
                        }
                    }else{
                        movingAnimal = {};
                        attackAndDefenseAnimState = 'launching-ball';
                        setGameState(0)
                    }
                    break;
            }
            break;
        case 1: // only DEFENSE
            switch(attackAndDefenseAnimState){
                case 'launching-ball':
                    monit.animateDistractor(chAD, '-' + orientation + '-scappa', 5);
                    monit.animateDistractor(animAD, '-' + orientation + '-palla', 5);
                    if(isEmpty(movingBall)){
                        ballAD = monit.addDistractor(animAD.posX - (assetsDim[animalName].sizX / 2), animAD.posY - (assetsDim[animalName].sizY / 2), 'ball.png');
                        movingBall = new ItemMovements(
                            ballAD, 
                            ballAD.posX, 
                            ballAD.posY, 
                            (windowWidth / 4) * (orientation == 'left' ? 1 : 3), 
                            windowHeight / 4, 
                            attDefAnimDur
                        );
                    }else{
                        if(!movingBall.endReached){
                            movingBall.move();
                        }else{
                            // monit.removeDistractor(monit.getDistractorByName('ball.png'));
                            movingBall = {};
                            if(!thereIsThisAnimation('bouncing')){
                                var temp = _.cloneDeep(animations['bouncing'].obj);
                                temp.timestamp = getCurrTM();
                                temp.obj = ballAD;
                                temp.refY = temp.obj.posY;
                                temp.refX = temp.obj.posX;
                                temp.orientation = orientation;
                                temp.currState = 'animationDefense';
                                temp.end = attDefAnimDur;
                                animationsStack.push(temp);
                            }
                            attackAndDefenseAnimState = 'miss-character';
                            // ball.posX = monit.getDistractorByName(charName).posX - (assetsDim[charName].sizX / 2);
                            // ball.posY = monit.getDistractorByName(charName).posY - (assetsDim[charName].sizY / 2);
                        }
                    }
                    break;
                case 'miss-character':
                    monit.animateDistractor(animAD, '-' + orientation + '-palla', 5);
                    monit.animateDistractor(chAD, '-' + orientation + '-hurra', 5);
                    if(!thereIsThisAnimation('bouncing')){
                        movingBall = {};
                        attackAndDefenseAnimState = 'launching-ball';
                        setGameState(0);
                    }
                    break;
            }
            break;
    }
}
// -------------- DRAW -----------------
function draw() {
    noStroke();
    if(isEmpty(movingPath)) loadPlayerPath();
    if(monit.loaded && pathJsonFl){
        if(isEmpty(assetsDim)) setAssetsSizes()  // set assets dimension object 
        if(!firstStateButtons){
            monit.sendEvent({ts: new Date().getTime(), type: "event", event_type: "buttons_state", data: {cooldown: false, back: false, next: true}});
            firstStateButtons = 1;
        }
        if(monit.pState.bg == undefined) monit.setBg(backgroundGame[monit.GameLevel]);
        if(isEmpty(bgEyeTracker)) bgEyeTracker = new Bg(frameRate());
        monit.drawBg();
        bgEyeTracker.run();
        switch(monit.GameScene){
            case 0:
                // setGameState(2)
                if(isEmpty(playground)){
                    playground = new Playground(orientation, animalName, actionDur);
                    playground.drawExplanation = false;
                }
                playground.draw();
                // drawAttackDefense();
                break;
            case 1:
                if(monit.skipIntro){
                    setGameState(2)
                }else{
                    if(isEmpty(countdown)){
                        countdown = new CountDown(1);
                    }
                    countdown.count();
                }
                break;
            
            case 2:
                if(isEmpty(playground)){
                    playground = new Playground(orientation, animalName, actionDur);
                }
                playground.draw();
                break;
        }
        monit.drawAllItems();
        // monit.drawTargets();
        // monit.drawDistractors();
        if(!isEmpty(playground)){
            if((playground.currState.indexOf('transition_attack2defense') !== -1 && orientation == 'left') || 
                (playground.currState.indexOf('transition_defense2attack') !== -1 && orientation == 'right')){
                push();
                fill('rgba(0, 0, 0, 0.5)')
                rectMode(CORNER)
                rect(windowWidth / 2, 0, windowWidth / 2, windowHeight)
                pop();
            } 
            if((playground.currState.indexOf('transition_defense2attack') !== -1 && orientation == 'left') ||
                (playground.currState.indexOf('transition_attack2defense') !== -1 && orientation == 'right')){
                push();
                fill('rgba(0, 0, 0, 0.5)')
                rectMode(CORNER)
                rect(0, 0, windowWidth / 2, windowHeight)
                pop();
            } 
        }
        // monit.drawTracker();
        // monit.monitoring();
        //draw animations if present
        if(new Date().getTime() - pulseTimer < 1000){
            console.log('inside drawing');
            pulsatingObjs.forEach( el => {
                el.draw();
            });
        }else{
            pulsatingObjs.forEach( el => {
                el.obj.sizX = el.refX;
                el.obj.sizY = el.refY;
            });
            pulsatingObjs = [];
        }
        drawAnimations();
    }else{
        loading();
    }
}
// restart game part
function gameAgain(){
    levels.currCategory = 0;
    board = {};
    startMessage = {};
    setGameState(1);
    tgsHurra = [];
    highlight = {};
}
function loading(){
    push();
    background('black');
    textSize(30);
    stroke('white');
    fill('white');
    textAlign(CENTER, CENTER);
    text('Caricamento in corso...', windowWidth / 2, windowHeight / 2);
    pop();
}

// KEY PRESSED FUNCTION
function keyPressed(){
    // console.log(keyCode)
    switch(keyCode){
        case UP_ARROW:
            break;
        case RIGHT_ARROW:
            playground = {};
            setGameState(2);
            // if(!isEmpty(playground)){
            //     if(playground.currState == 'charVictory' || playground.currState == 'charDefeat' ){
            //         playground.currState = 'attack';
            //         playground.initGame();
            //     }
            // }else if(monit.GameScene == 0){
            //     setGameState(2);
            // }
            break;
    }
}
// SET GAME STATE 
function setGameState(gs){
    noLoop();
    // console.log(gs);
    monit.GameScene = gs;
    // monit.setBg(backgroundGame[monit.GameScene]);
    monit.fixationObjects = [];
    animationsStack = [];
    switch(monit.GameScene){
        case 0: 
            monit.sendEvent({ts: new Date().getTime(), type: "event", event_type: "buttons_state", data: {cooldown: false, back: false, next: true}});
            break;
        case 2:
            monit.sendEvent({ts: new Date().getTime(), type: "event", event_type: "buttons_state", data: {cooldown: false, back: false, next: false}});
            break;
        case 4:
            break;
    }
    resetAll();
    loop();
}

function goNextScene(){
    var next = monit.GameScene > 3 ? 0 : monit.GameScene + 1;
    setGameState(next);
}
function checkHalfOverlap(el1, el2){
    return el1.posX + (el1.sizX / 2)  >= el2.posX &&
            el1.posX <= el2.posX + (el2.sizX / 2) &&
            el1.posY + (el1.sizY / 2) >= el2.posY && 
            el1.posY <= el2.posY + (el2.sizY / 2);
}
function thereIsThisAnimation(anim, id){
    if(id !== undefined){
        return animationsStack.filter(a => a.type == anim && a.obj.id == id).length;
    }else{
        return animationsStack.filter(a => a.type == anim).length;
    }
}
function removeAnimation(anim, id){
    if(id !== undefined){
        if(animationsStack.filter(a => a.type == anim && a.obj.id == id).length){
            _.remove(animationsStack, a => a.type == anim && a.obj.id == id);
        }
    }else{
        if(animationsStack.filter(a => a.type == anim).length){
            _.remove(animationsStack, a => a.type == anim);
        }
    }
}