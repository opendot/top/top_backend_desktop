# Create your views here.
import subprocess
from django.contrib.auth.models import User
from django.http import HttpResponse
from rest_framework.renderers import JSONRenderer
import requests
from django.conf import settings
from tog.models import CareReceiver, Game, Sync, Session
from tog.utils import getIP, zipdir, createGuestUserandCareReceiver
from rest_framework import viewsets
from django.contrib.auth import authenticate
from django.views.decorators.csrf import csrf_exempt
from rest_framework.authtoken.models import Token
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny
from rest_framework.authentication import TokenAuthentication
from rest_framework.views import APIView
from rest_framework.status import (
    HTTP_400_BAD_REQUEST,
    HTTP_404_NOT_FOUND,
    HTTP_200_OK,
    HTTP_422_UNPROCESSABLE_ENTITY,
    HTTP_500_INTERNAL_SERVER_ERROR)
from rest_framework.response import Response
from django.shortcuts import render
from tog.serializers import UserSerializer, CareReceiverSerializer, GameSerializer, SyncSerializer, SessionSerializer, SyncSessionSerializer
from qr_code.qrcode.utils import QRCodeOptions
import re, os, json
import shutil
from zipfile import ZipFile
from django.core import serializers



class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    AUTH: token
    """
    authentication_classes = (TokenAuthentication,)
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer


class SessionViewSet(viewsets.ModelViewSet):
    authentication_classes = (TokenAuthentication,)
    #queryset = Session.objects.all()
    serializer_class = SessionSerializer

    def get_queryset(self):
        queryset = Session.objects.filter(careGiver=self.request.user).select_related()
        return queryset


class CareReceiverViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows careReceivers to be viewed or edited.
    AUTH: token
    """
    authentication_classes = (TokenAuthentication,)
    #queryset = CareReceiver.objects.all()
    serializer_class = CareReceiverSerializer

    def get_queryset(self):
        if self.request.user.username == "guest":
            queryset = CareReceiver.objects.filter(name = "Anonymous", last_name = "CareReceiver")
        else:
            queryset = CareReceiver.objects.exclude(name = "Anonymous", last_name = "CareReceiver")
        return queryset


class GameViewSet(viewsets.ModelViewSet):
    """
    API endpoint to retireve games on the platform
    AUTH: token
    """
    authentication_classes = (TokenAuthentication,)
    queryset = Game.objects.all()
    serializer_class = GameSerializer


class LastSyncView(APIView):
    """
    API endpoint that returns the last successful sync
    AUTH: token
    """

    authentication_classes = (TokenAuthentication,)

    def get(self, request):
        try:
            lastsync = Sync.objects.filter(success=True).latest("completed_at")
            serializer = SyncSerializer(lastsync)
            return Response(data=serializer.data)
        except Sync.DoesNotExist:
            return Response({})




@csrf_exempt
@api_view(["POST"])
@permission_classes((AllowAny,))
def login(request):
    print("login requested")
    print(request.data)

    # guestCareReciver to be returned in case of guest login
    guestCareReceiver = None

    username = request.data.get("username")
    password = request.data.get("password")
    if username is None or password is None:
        return Response({'error': 'Please provide both username and password'},
                        status=HTTP_400_BAD_REQUEST)

    elif username == "guest" and password == "guest":
        
        # get or create guest caregiver and carereceiver 
        guestCareReceiver = createGuestUserandCareReceiver(username,password)

    user = authenticate(username=username, password=password)
    if not user:
        return Response({'error': 'Invalid Credentials'},
                        status=HTTP_404_NOT_FOUND)
    token, _ = Token.objects.get_or_create(user=user)

    # existing careGiver login response
    if guestCareReceiver is None:
        return Response({'token': token.key, 'username': user.username, 'user_id': user.id, 'complete_name': user.get_full_name()},
                    status=HTTP_200_OK)
    
    # guest login response, includes guest carereceiver
    else:
        return Response({'token': token.key, 'username': user.username, 'user_id': user.id, 'complete_name': user.get_full_name(), 'guest':True, 'careReceiver': CareReceiverSerializer(guestCareReceiver).data},
                    status=HTTP_200_OK)


@api_view(["GET"])
@permission_classes((AllowAny,))
def getServerIP(request):
    return Response({'ip': getIP()},
                    status=HTTP_200_OK)


@api_view(["GET"])
@permission_classes((AllowAny,))
def checkClosedSession(request):

    ses = ""

    if not "session" in request.query_params:
        return Response({'error': "specificare una sessione valida"},
                 status=HTTP_422_UNPROCESSABLE_ENTITY)

    ses = request.query_params["session"]

    if not os.path.exists(os.getcwd() + '/sessions/'+ ses):
        return Response({'error': "errore durante la registrazione della sessione"},
                 status=HTTP_422_UNPROCESSABLE_ENTITY)

    sespath = os.getcwd() + '/sessions/'+ ses+"/"

    if not os.path.isfile(sespath + "gaze.csv"):
        return Response({'error': "errore durante la registrazione della sessione"},
                 status=HTTP_422_UNPROCESSABLE_ENTITY)

    if os.path.getsize(sespath + "gaze.csv") < 50:
        return Response({'error': "errore durante la registrazione della sessione"},
                 status=HTTP_422_UNPROCESSABLE_ENTITY)

    if not os.path.isfile(sespath + "render.csv"):
        return Response({'error': "errore durante la registrazione della sessione"},
                 status=HTTP_422_UNPROCESSABLE_ENTITY)

    if os.path.getsize(sespath + "render.csv") < 50:
        return Response({'error': "errore durante la registrazione della sessione"},
                 status=HTTP_422_UNPROCESSABLE_ENTITY)

    if not os.path.isfile(sespath + "events.csv"):
        return Response({'error': "errore durante la registrazione della sessione"},
                 status=HTTP_422_UNPROCESSABLE_ENTITY)

    if os.path.getsize(sespath + "events.csv") < 20:
        print("this should raise error")
        return Response({'error': "errore durante la registrazione della sessione"},
                 status=HTTP_422_UNPROCESSABLE_ENTITY)

    session = Session.objects.get(id=ses)
    session.processed = True
    session.save()

    return Response("ok", status=HTTP_200_OK)


@api_view(["GET"])
@permission_classes((AllowAny,))
def syncupload(request):
    sessions = Session.objects.filter(processed=True, synced=False)
    serializer = SyncSessionSerializer(sessions, many=True)
    json_sessions = JSONRenderer().render(serializer.data)

    # prepare json with sessions to upload
    with open("tmp/upload.json", "w") as jsonFile:
        jsonFile.write(json_sessions.decode("utf-8"))

    # prepare zipfile containing csv files
    zipf = ZipFile('tmp/upload.zip', 'w')
    for ses in sessions:
        sespath = os.getcwd() + '/sessions/' + str(ses.id) + "/"
        zipdir(sespath, zipf, str(ses.id))

    # write zip
    zipf.write("tmp/upload.json", "upload.json")

    #close zip
    zipf.close()

    # send zip to remote
    url = settings.REMOTE_SERVER + "/receivesync/"
    files = {'file': open("tmp/upload.zip", 'rb')}
    r = requests.post(url, files=files)

    # wait for response
    if r.status_code == requests.codes.ok:
        return Response("ok", status=HTTP_200_OK)

    # response cases
    else:
        return Response("ko", status=HTTP_500_INTERNAL_SERVER_ERROR)


@api_view(["GET"])
@permission_classes((AllowAny,))
def syncdownload(request):
    usrdata = User.objects.all().values_list('id', flat=True)
    gamedata = Game.objects.all().values_list('slug', flat=True)

    obj = {"users": list(usrdata), "games": list(gamedata)}

    print(json.dumps(obj))

    # send zip to remote
    url = settings.REMOTE_SERVER + "/receivedownsync/"
    r = requests.post(url, data=json.dumps(obj))

    # wait for response
    if r.status_code == requests.codes.ok:
        return Response("ok", status=HTTP_200_OK)

    # response cases
    else:
        return Response("ko", status=HTTP_500_INTERNAL_SERVER_ERROR)

    return Response("ok", status=HTTP_200_OK)


@api_view(["POST"])
@permission_classes((AllowAny,))
def receivesync(request):

    f = request.FILES['file']
    archive = ZipFile(f, 'r')
    archive.extractall("tmp/upload")
    archive.close()

    for p in os.listdir(os.getcwd() + '/tmp/upload'):
        if os.path.isdir(os.getcwd() + '/tmp/upload/' + p):
            shutil.move(os.getcwd() + '/tmp/upload/' + p, os.getcwd() + "/sessions_rem/")

    with open(os.getcwd()+'/tmp/upload/upload.json') as json_file:
        data = json.load(json_file)
        objs = SyncSessionSerializer(data=data)

        # save to db
        print(objs)

        if (objs.is_valid() == False):
            return Response(objs.errors, status=HTTP_400_BAD_REQUEST)

        new_ses = objs.save()
        return Response(json.dumps(data), status=HTTP_200_OK)



def getSSID():
    """
        Windows only, to be refined
        Gets the current SSID
    """
    ps = subprocess.Popen(("netsh wlan show interfaces"), stdout=subprocess.PIPE)
    try:
        output = subprocess.check_output(('findstr', 'SSID'), stdin=ps.stdout)
    except subprocess.CalledProcessError as e:
        return None
    ps.wait()
    output = output.decode("utf-8")
    if not len(output):
        return None
    ssid = re.search(".*? : (.*?)\\r\\n", output)
    ssid = ssid.group(1)

    return ssid


def showQRIp(request):
    ip = getIP() + ":" + request.META['SERVER_PORT']
    ssid = getSSID()
    print(ssid)
    context = dict(
        qr_options=QRCodeOptions(size='L', border=6, error_correction='L'),
        ip=ip,
        ssid=ssid
    )
    return render(request, 'qr/index.html', context=context)


def pingview(request):
    return HttpResponse(status=200)
