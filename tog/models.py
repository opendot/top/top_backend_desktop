from django.db import models
from django.contrib.auth.models import User
from django.core.validators import MinValueValidator
import uuid


class CareReceiver(models.Model):
    GENDER_CHOICES = (
        ('M', 'Male'),
        ('F', 'Female'),
    )

    METHOD_CHOICES = (
        ('wisc-iv', 'wisc-iv'),
        ('leiter-r', 'leiter-r'),
        ('griffiths', 'gri'),
    )
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=30, blank=False)
    last_name = models.CharField(max_length=30, blank=False)
    gender = models.CharField(max_length=1, choices=GENDER_CHOICES)
    birthdate = models.DateField(max_length=8)
    syndrome = models.CharField(max_length=80)
    iq = models.CharField(max_length=30, null = True)
    iq_method =  models.CharField(max_length=20, choices=METHOD_CHOICES, null=True)
    created_at = models.DateTimeField(auto_now_add=True, blank=True)


class Game(models.Model):
    GAME_TYPES = (
        ('training', 'Training'),
        ('cognitive', 'Cognitive'),
        ('entertainment', 'Entertainment'),
        ('evaluation', 'Evaluation'),
    )
    version = models.CharField(max_length=30, blank=False)
    slug = models.CharField(primary_key=True, max_length=30)
    safename = models.CharField(max_length=30, blank=False)
    title = models.CharField(max_length=30, blank=False)
    description = models.TextField(max_length=300, blank=False)
    image = models.CharField(max_length=50, blank=False)
    url = models.CharField(max_length=30, blank=True)
    type = models.CharField(max_length=1, choices=GAME_TYPES)
    created_at = models.DateTimeField(auto_now_add=True, blank=True)
    path = models.CharField(max_length=200, default="games", blank=False)


class GameSettings(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    game = models.ForeignKey(Game, on_delete=models.PROTECT)
    careReceiver = models.ForeignKey(CareReceiver, on_delete=models.PROTECT)
    settings = models.TextField(blank=True)
    created_at = models.DateTimeField(auto_now_add=True, blank=True)


class Session(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    careReceiver = models.ForeignKey(CareReceiver, on_delete=models.PROTECT)
    careGiver = models.ForeignKey(User, on_delete=models.PROTECT)
    game = models.ForeignKey(Game, on_delete=models.PROTECT)
    processed = models.BooleanField(default=False)
    files_sent = models.BooleanField(default=False)
    level = models.IntegerField(default=1)
    synced = models.BooleanField(default=False)
    diffFile = models.FilePathField(match=".*\.zip$",blank=True)
    gazeFile = models.FilePathField(match=".*\.zip$",blank=True)
    eventFile = models.FilePathField(match=".*\.zip$",blank=True)
    created_at = models.DateTimeField(auto_now_add=True, blank=True)


class Sync(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    created_at = models.DateTimeField(auto_now_add=True, blank=True)
    completed_at = models.DateTimeField(blank=True)
    success = models.BooleanField(default=False)
    error = models.TextField(max_length=300, blank=True)


class Pid(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    number = models.IntegerField(default=0)