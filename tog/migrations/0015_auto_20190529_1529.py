# Generated by Django 2.1.5 on 2019-05-29 13:29

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('tog', '0014_auto_20190529_1528'),
    ]

    operations = [
        migrations.AlterField(
            model_name='carereceiver',
            name='iq_method',
            field=models.CharField(choices=[('wisc-iv', 'wisc-iv'), ('leiter-r', 'leiter-r'), ('griffiths', 'gri')], max_length=20, null=True),
        ),
    ]
