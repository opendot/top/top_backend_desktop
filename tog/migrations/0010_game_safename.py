# Generated by Django 2.1.5 on 2019-03-11 16:14

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('tog', '0009_auto_20190311_1639'),
    ]

    operations = [
        migrations.AddField(
            model_name='game',
            name='safename',
            field=models.CharField(default='name', max_length=30),
            preserve_default=False,
        ),
    ]
