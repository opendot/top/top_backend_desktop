# Generated by Django 2.1.5 on 2019-03-11 17:24

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('tog', '0011_auto_20190311_1805'),
    ]

    operations = [
        migrations.AlterField(
            model_name='game',
            name='image',
            field=models.CharField(max_length=50),
        ),
    ]
