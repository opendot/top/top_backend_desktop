# Generated by Django 2.1.5 on 2019-05-29 13:28

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('tog', '0013_auto_20190412_1258'),
    ]

    operations = [
        migrations.AddField(
            model_name='carereceiver',
            name='iq',
            field=models.CharField(max_length=30, null=True),
        ),
        migrations.AddField(
            model_name='carereceiver',
            name='iq_method',
            field=models.CharField(choices=[('wisc-iv', 'wisc-iv'), ('leiter-r', 'leiter-r'), ('griffiths', 'gri')], max_length=1, null=True),
        ),
        migrations.AlterField(
            model_name='carereceiver',
            name='syndrome',
            field=models.CharField(max_length=80),
        ),
    ]
