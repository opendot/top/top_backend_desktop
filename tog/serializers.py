from django.contrib.auth.models import User, Group
from tog.models import CareReceiver, Game, Session, GameSettings, Sync
from rest_framework import serializers


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'url', 'username', 'email', 'groups')


class CareReceiverSerializer(serializers.ModelSerializer):
    class Meta:
        model = CareReceiver
        fields = ('id', 'name', 'last_name', 'gender', 'birthdate', 'syndrome')


class GameSerializer(serializers.ModelSerializer):
    class Meta:
        model = Game
        fields = ('slug', 'title', 'version', 'description', 'image', 'url', 'type', 'safename')


class SyncSerializer(serializers.ModelSerializer):
    class Meta:
        model = Sync
        fields = ('created_at', 'completed_at', 'success', 'error')


class SessionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Session
        fields = ('id', 'careGiver', 'careReceiver', 'game', 'level', 'created_at')


class SyncSessionSerializer(serializers.ModelSerializer):
    game_slug = serializers.CharField(source='game.slug')

    class Meta:
        model = Session
        fields = ('id', 'careGiver', 'careReceiver', 'game_slug', 'level', 'created_at')



