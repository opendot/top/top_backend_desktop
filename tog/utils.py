import socket
import os
import datetime
from django.contrib.auth.models import User
from tog.models import CareReceiver

def getIP():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(("8.8.8.8", 80))
    ip = s.getsockname()[0]
    s.close()

    return ip


def zipdir(path, ziph, sesid):
    # ziph is zipfile handle
    for root, dirs, files in os.walk(path):
        for file in files:
            ziph.write(os.path.join(root, file), sesid+"/"+file)

def createGuestUserandCareReceiver(usr,passw):

    newuser, created = User.objects.get_or_create(username=usr)
    if created:
            # user was created
            # set the password here
            newuser.set_password(passw)
            newuser.first_name = ""
            newuser.last_name = ""
            newuser.save()
    
    newReceiver, created = CareReceiver.objects.get_or_create(
        name = "Anonymous",
        last_name = "CareReceiver",
        gender = 'F',
        birthdate = datetime.date.today(),
        syndrome = "Unknown"
    )

    return newReceiver
