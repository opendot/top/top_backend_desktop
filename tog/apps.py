from django.apps import AppConfig
import os


class TogConfig(AppConfig):
    name = 'tog'
    verbose_name = "Tog Eye Games"

    # write django PID for tasks listeners
    def ready(self):
        # the following part it's obsolete since "strartserver"
        # command has been introduced (but harmless)
        from tog import signals
        from tog.models import Pid
        import sys
        print(sys.argv)
        if len(sys.argv) == 1 and sys.argv[0] == "starter.py":
            print(sys.argv)
            currpid = os.getpid()
            print(currpid)
            p = Pid.objects.first()
            if p is None:
                Pid.objects.create(number=currpid)
            else:
                p.number = currpid
                p.save()
