import tobii_research as tr
import os
import copy
import math
import time, datetime
import threading

CSV_PATH = os.path.join(os.getcwd(), 'sessions/')


# UTILITIES

# average of a list of 2d tuples
def compute_avg(lst):
    sumx = 0
    sumy = 0

    for l in lst:
        sumx = sumx + l[0]
        sumy = sumy + l[1]

    sumx = sumx / len(lst)
    sumy = sumy / len(lst)

    return sumx, sumy

# euclidean distance
def dist(a,b):
    distance = math.sqrt(sum([(x - y) ** 2 for x, y in zip(a, b)]))
    return distance

def t_write_to_file(data,session):
    sespath = os.path.join(CSV_PATH, session)
    with open(sespath + '/gaze.csv', 'w', newline='') as file:
        # print(data)
        file.write(str(data[0])+","+str(data[1])+","+str(data[2])+'\n')
        file.close()

# -------- END UTILITIES -----------


# PRO DRIVER CLASS

LICENSE_PATH = os.path.join(os.getcwd(), 'licenses/license_key_00462841__-__Together_To_Go_Onlus_KS05M0T00670400383TM02')


class Prodriver:

    writing = False
    calibrating = False
    f = None
    fw = None
    session = None
    count = 0
    orig_count = 0
    buffer = []
    buffcopy = []
    ts = None
    trackerts = None
    systime = None
    tobiitime = None
    orig_left = []
    orig_right = []
    found_eyetrackers = []

    # constructor
    def __init__(self, queue, cal_queue):

        self.q = queue
        self.cal_queue = cal_queue
        
        #check for eyetrackers
        while len(self.found_eyetrackers) <= 0:
            print("tobii finding trackers")  
            self.found_eyetrackers = tr.find_all_eyetrackers()
            time.sleep(1)

        self.my_eyetracker = self.found_eyetrackers[0]
        print("Address: " + self.my_eyetracker.address)
        print("Model: " + self.my_eyetracker.model)
        
        #test if tracker has a valid license
        try:
            self.calibration = tr.ScreenBasedCalibration(self.my_eyetracker)
            self.calibration.enter_calibration_mode()
            self.calibration.leave_calibration_mode()
        except tr.EyeTrackerSavedLicenseFailedToApplyError:
            raise
        except tr.EyeTrackerLicenseError:
            raise

        # apply license for TOG model
        if self.my_eyetracker.serial_number == "KS05M0T00670400383TM02":
            with open(LICENSE_PATH, "rb") as f:
                license = f.read()
                failed_licenses_applied_as_key = self.my_eyetracker.apply_licenses(tr.LicenseKey(license))

                if len(failed_licenses_applied_as_key) == 0:
                    print("Successfully applied license from single key.")
                else:
                    print("Failed to apply license from single key. Validation result: {0}.".format(failed_licenses_applied_as_key[0].validation_result))

        # subscribe to eyegaze data events
       
        self.sync_time()
       
        try:
            self.my_eyetracker.subscribe_to(tr.EYETRACKER_GAZE_DATA, self.gaze_data_callback, as_dictionary=True)

        except Exception as e:
            print("exception", e)

    # get average point being gazed on screen
    def get_avg_gaze(self, gaze_data):
        res = None
        if gaze_data['left_gaze_point_validity'] == 1 and gaze_data['right_gaze_point_validity'] == 1:
            res = [(gaze_data['left_gaze_point_on_display_area'][0] + gaze_data['right_gaze_point_on_display_area'][0]) / 2,
                   (gaze_data['left_gaze_point_on_display_area'][1] + gaze_data['right_gaze_point_on_display_area'][1]) / 2]

        elif gaze_data['left_gaze_point_validity'] == 1 and gaze_data['right_gaze_point_validity'] == 0:
            res = [gaze_data['left_gaze_point_on_display_area'][0], gaze_data['left_gaze_point_on_display_area'][1]]

        elif gaze_data['left_gaze_point_validity'] == 0 and gaze_data['right_gaze_point_validity'] == 1:
            res = [gaze_data['right_gaze_point_on_display_area'][0], gaze_data['right_gaze_point_on_display_area'][1]]

        return res


    def get_avg_pupil_diameter(self, gaze_data):
        res = None
        if gaze_data['left_pupil_validity'] == 1 and gaze_data['right_pupil_validity'] == 1:
            res = (gaze_data['left_pupil_diameter'] + gaze_data['right_pupil_diameter']) / 2

        elif gaze_data['left_pupil_validity'] == 1 and gaze_data['right_pupil_validity'] == 0:
            res = gaze_data['left_pupil_diameter']

        elif gaze_data['left_pupil_validity'] == 0 and gaze_data['right_pupil_validity'] == 1:
            res = gaze_data['right_pupil_diameter']

        return res

    # write rows to csv file
    def write_to_file(self):
        self.fw.writerows(self.buffcopy)
        self.f.flush()
        self.buffcopy = []

    def sync_time(self):
        self.systime = int(round(time.time() * 1000))
        self.tobiitime = tr.get_system_time_stamp() / 1000

    def compute_timestamp(self, micros):
        dif = micros/1000 -self.tobiitime
        return self.systime + dif

    # callback when a new eyegaze datum is available
    def gaze_data_callback(self, gaze_data):
        
        a = datetime.datetime.now()
        data = self.get_avg_gaze(gaze_data)
        pupdata = self.get_avg_pupil_diameter(gaze_data)
        if data is not None:
            data = [round(x, 8) for x in data]

        if pupdata is not None:
            pupdata = round(pupdata, 8)

        micros = gaze_data['system_time_stamp']
        data = [self.compute_timestamp(micros)] + data + [pupdata]


        self.orig_count += 1
        if self.orig_count >= 0:
            self.orig_left = gaze_data['left_gaze_origin_in_trackbox_coordinate_system']
            self.orig_right = gaze_data['right_gaze_origin_in_trackbox_coordinate_system']
            self.orig_left = [round(x, 3) for x in self.orig_left]
            self.orig_right = [round(x, 3) for x in self.orig_right]
            self.orig_count = -9

        data = data + list(self.orig_left)
        data = data + list(self.orig_right)
        data = [x if not math.isnan(x) else None for x in data]

        #print(data)

        # print(data)

        if self.writing:
            self.buffer.append(copy.copy(data))
            # print("append to buffer")

            if len(self.buffer) >= 50:
                self.buffcopy = copy.copy(self.buffer)
                self.buffer = []
                t1 = threading.Thread(target=self.write_to_file)
                t1.start()


        self.count = self.count + 1

        if self.count >= 3:
            if not self.q.full():
                data.pop(0)
                try:
                    self.q.put({"type": "gaze", "data": data}, False)
                except:
                    print("queue is full")
                self.count = 0

        b = datetime.datetime.now()
        c = b - a

    # calibrate function
    def calibrate(self):

        # print("calling calibrate")
        self.calibration = tr.ScreenBasedCalibration(self.my_eyetracker)

        self.calibration.enter_calibration_mode()
        self.calibrating = True

        # call first calibration
        points_to_calibrate = [(0.5, 0.5), (0.15, 0.15), (0.15, 0.85), (0.85, 0.15), (0.85, 0.85)]
        # print("calibrating these points: ")
        # print(points_to_calibrate)
        self.q.put({"type": "calibrate", "data":points_to_calibrate})
        yield from(self.calibrate_points(points_to_calibrate))

        # leave calibration
        self.calibration.leave_calibration_mode()
        self.q.put({"type": "end_calibration"})
        self.cal_queue.put({"type": "end_calibration"})

    def calibrate_points(self, points_to_calibrate):
        # wait for tablet and desktop to be ready
        discarded = []
        # print("starting point by point")
        # cycle through points
        for point in points_to_calibrate:
            # send position to ws
            self.q.put({"type": "calibrate_point", "data": point})
            yield(print("Show a point on screen at {0}.".format(point)))
            # adjust time according to transition, or maybe wait for a message from ws
            print("Collecting data at {0}.".format(point))

            if self.calibration.collect_data(point[0], point[1]) != tr.CALIBRATION_STATUS_SUCCESS:
                # print("still calibrating")
                self.calibration.collect_data(point[0], point[1])

        # print("Computing and applying calibration.")
        self.q.put({"type": "computing_calibration"})
        # evaluate calibration
        calibration_result = self.calibration.compute_and_apply()
        count = 0

        # check calibration accuracy for every point
        for p in calibration_result.calibration_points:
            count = count + 1
            valsSx = []
            valsDx = []

            # print(str(count))
            # print(p.position_on_display_area)
            # print("---")
            for p1 in p.calibration_samples:
                # print(p1.left_eye.position_on_display_area)
                valsSx.append(p1.left_eye.position_on_display_area)
                # print(p1.right_eye.position_on_display_area)
                valsDx.append(p1.right_eye.position_on_display_area)

            meanSx = compute_avg(valsSx)
            meanDx = compute_avg(valsDx)
            totMean = compute_avg([meanSx, meanDx])

            # print(dist(p.position_on_display_area, totMean))

            # if distance between calibration point and collected data is small enough, ok
            if dist(p.position_on_display_area, totMean) < 0.1:
                print("point ok!")

            # else recalibrate that point
            else:
                # print("point not ok!")
                self.calibration.discard_data(p.position_on_display_area[0], p.position_on_display_area[1])
                discarded.append(p.position_on_display_area)

            # print("---------------")

        # check if recalibration is needed
        if len(discarded) > 0:
            # print("recalibrating" + str(len(discarded)) + " points")
            self.q.put({"type": "recalibrate", "data": discarded})
            yield from(self.calibrate_points(discarded))