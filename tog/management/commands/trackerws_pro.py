from django.core.management.base import BaseCommand
from tog.models import Pid
import psutil
import os
import threading
import json
import asyncio
import queue
import csv
from websockets.exceptions import ConnectionClosed
from sanic import Sanic
from ..helpers.prodriver import Prodriver

DRIVER = os.path.join(os.getcwd(), 'nopro_driver/Tog_noPro_driver.exe')
CSV_PATH = os.path.join(os.getcwd(), 'sessions/')
q = queue.Queue(maxsize=1)
calibration_queue = queue.Queue(maxsize=0)
session = None
calibrating = False


def start_driver(q, cal_q):

    driver = Prodriver(q, cal_q)
    driver.writing = False
    driver.f = None
    driver.fw = None
    cal_generator = None
    global session
    global calibrating

    while True:

        # check if calibration going on
        if calibrating:
            print("calibrating")

            # a new claibration stated
            if not driver.calibrating:
                driver.calibrating = True
                print("start calibration")
                cal_generator = driver.calibrate()
                # print(cal_generator)
                next(cal_generator)

            # go through the calibration generator
            if cal_generator:
                try:
                    msg = cal_q.get()
                    if msg["type"] == "calibration_point_in_position":
                        print("calibration point in position")
                        next(cal_generator)
                    elif msg["type"] == "end_calibration":
                        print("end_calibration")
                        driver.calibrating = False
                        calibrating = False

                        with cal_q.mutex:
                            size = len(cal_q.queue)
                            cal_q.queue.clear()
                            cal_q.unfinished_tasks -= size

                    elif msg["type"] == "interrupt_calibration":
                        print("interrupt_calibration")
                        driver.calibrating = False
                        driver.calibration.leave_calibration_mode()
                        calibrating = False

                        with cal_q.mutex:
                            size = len(cal_q.queue)
                            cal_q.queue.clear()
                            cal_q.unfinished_tasks -= size
                except:
                    pass

        # for interrupting calibration
        elif not calibrating and driver.calibrating:
            print("not calibrating")
            driver.calibration.leave_calibration_mode()
            driver.calibrating = False
            print("Interrupting calibration")

        if driver.writing:

            # session closed, reset
            if session is None:
                driver.writing = False
                driver.f = None
                driver.fw = None
                # print("close session")

        else:
            # a session started, initialize the csv
            if session is not None:

                # check that file was correctly closed
                if driver.f is not None and not driver.f.closed:
                    driver.f.close()

                # initialize writer
                driver.writing = True
                # print("new session")
                driver.session = session
                driver.sync_time()
                driver.buffer = []
                sespath = os.path.join(CSV_PATH, session)

                driver.f = open(sespath + '/gaze.csv', 'w', newline='')
                driver.fw = csv.writer(driver.f)
                driver.fw.writerow(['ts', 'x', 'y', 'p', 'lx', 'ly', 'lz', 'rx', 'ry', 'rz'])
                # print("created file")
                driver.f.flush()


def serve_app(loop, q, cal_q):
    asyncio.set_event_loop(loop)
    app = Sanic()
    connections = set()
    mobile = set()
    desktop = set()


    @app.websocket('/')
    async def feed(request, websocket):
        global session
        global calibrating
        connections.add(websocket)

        while True:
            message = await websocket.recv()
            msg = json.loads(message)
            # print(" + received messasge: " + message)

            if msg['type'] == "room":
                room = msg['data']
                if room == "mobile":
                    # print("added to mobile")
                    mobile.add(websocket)
                elif room == "desktop":
                    # print("added to desktop")
                    desktop.add(websocket)

            elif msg['type'] == "test":
                print(msg['data'])

            elif msg['type'] == "run_calibration":
                for conn in desktop.copy():
                    try:
                        await conn.send(json.dumps(msg))
                    except ConnectionClosed:
                        desktop.remove(conn)

            elif msg['type'] == "start_calibration":
                calibrating = True
                # print(msg)

            elif msg['type'] == "interrupt_calibration":

                cal_q.put(msg)
                for conn in desktop.copy():
                    try:
                        await conn.send(json.dumps(msg))
                    except ConnectionClosed:
                        desktop.remove(conn)
                # print(msg)

            elif msg['type'] == "calibration_point_in_position":
                # print(msg)
                cal_q.put(msg)

            elif msg['type'] == "start_session":
                session = msg['data']
                # print('new session ', msg['data'])

            elif msg['type'] == "new_session":
                for conn in desktop.copy():
                    try:
                        await conn.send(json.dumps(msg))
                    except ConnectionClosed:
                        desktop.remove(conn)

                # print('start the new session ', msg['data'])

            elif msg['type'] == "end_session":
                session = None
                # print('end session')

    async def sendgaze():
        while True:
            try:
                data = q.get(False)
                for conn in connections.copy():
                    try:
                        await conn.send(json.dumps(data))
                    except ConnectionClosed:
                        connections.remove(conn)

            except queue.Empty:
                await asyncio.sleep(0.030)

    app.add_task(sendgaze())
    app.run(host="0.0.0.0", port=5001)


class Command(BaseCommand):
    help = 'Runs the eye tracker socket server'

    def handle(self, *args, **options):

        # get server process
        p = Pid.objects.first()
        # print(p.number)

        new_loop = asyncio.new_event_loop()

        wst = threading.Thread(target=serve_app, args=(new_loop, q, calibration_queue,))
        wst.daemon = True
        wst.start()

        dri = threading.Thread(target=start_driver, args=(q,calibration_queue,))
        dri.daemon = True
        dri.start()

        while True:
            if not psutil.pid_exists(p.number):
                # print("goodbye")
                break
