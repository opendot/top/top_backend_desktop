from django.core.management.base import BaseCommand
import tobii_research as tr
import os, sys
import multiprocessing
import threading
import subprocess
import json
import asyncio
import queue
import csv
import psutil
import time
from sanic.websocket import WebSocketProtocol
from websockets.exceptions import ConnectionClosed
from sanic import Sanic
from ..helpers.prodriver import Prodriver

DRIVER = os.path.join(os.getcwd(), 'nopro_driver/Tog_noPro_driver.exe')
CSV_PATH = os.path.join(os.getcwd(), 'sessions/')
licensed = multiprocessing.Value('i', -1)
q = multiprocessing.Queue(maxsize=1)
calibration_queue = multiprocessing.Queue(maxsize=0)
t_exit = multiprocessing.Value('i', 0)

# used by the no pro version
def output_reader(proc, track_queue):

    for line in iter(proc.stdout.readline, b''):
        if t_exit.value > 0:
            break

        data = [float(s) for s in line.decode("utf-8").strip().split(",")]
        data.pop(0)
        try:
            track_queue.put({"type": "gaze", "data": data},False)
        except queue.Full:
            pass

 # used by the pro version
def prodriver_routine(q,cal_q, driver):
    driver.writing = False
    #csv file to be written with data 
    driver.f = None
    #csv file writer instance
    driver.fw = None
    #generator for following calibration steps
    cal_generator = None
    session = None
    calibrating = False

    while True:

        try:
            msg = cal_q.get()

            if msg["type"] == "start_session":
                session = msg['data']

            elif msg["type"] == "end_session":
                session = None

            elif msg["type"] == "start_calibration":
                calibrating = True

            elif msg["type"] == "calibration_point_in_position":
                print("calibration point in position")
                next(cal_generator)

            elif msg["type"] == "end_calibration":
                print("end_calibration")
                driver.calibrating = False
                calibrating = False

                with cal_q.mutex:
                    size = len(cal_q.queue)
                    cal_q.queue.clear()
                    cal_q.unfinished_tasks -= size

            elif msg["type"] == "interrupt_calibration":
                print("interrupt_calibration")
                driver.calibrating = False
                driver.calibration.leave_calibration_mode()
                calibrating = False

                with cal_q.mutex:
                    size = len(cal_q.queue)
                    cal_q.queue.clear()
                    cal_q.unfinished_tasks -= size

            elif msg['type'] == "terminate":
                # get server process
                p = Pid.objects.first()
                server = psutil.Process(p.number)
                server.terminate()
        except:
            pass

        # check if calibration going on
        if calibrating:
            print("calibrating")

            # a new claibration stated
            if not driver.calibrating:
                driver.calibrating = True
                print("start calibration")
                cal_generator = driver.calibrate()
                next(cal_generator)

        # for interrupting calibration
        elif not calibrating and driver.calibrating:
            print("not calibrating")
            driver.calibration.leave_calibration_mode()
            driver.calibrating = False
            print("Interrupting calibration")

        if driver.writing:

            # session closed, reset
            if session is None:
                driver.writing = False
                driver.f = None
                driver.fw = None

        else:
            # a session started, initialize the csv
            if session is not None:
                print(session)

                # check that file was correctly closed
                if driver.f is not None and not driver.f.closed:
                    driver.f.close()

                # initialize writer
                driver.writing = True
                driver.session = session
                driver.sync_time()
                driver.buffer = []
                sespath = os.path.join(CSV_PATH, session)

                driver.f = open(sespath + '/gaze.csv', 'w', newline='')
                driver.fw = csv.writer(driver.f)
                driver.fw.writerow(['ts', 'x', 'y', 'p', 'lx', 'ly', 'lz', 'rx', 'ry', 'rz'])
                driver.f.flush()

def start_driver(q, cal_q,license, t_exit):

    # try running the pro version
    try:
        
        driver = Prodriver(q, cal_q)
        license.value = 1
        prodriver_routine(q,cal_q,driver)

    # if it fails, run the no pro version
    except:
        
        print("no valid license, switching to no license version")
        license.value = 0
        pdr = subprocess.Popen([DRIVER], shell=False, bufsize=-1, stdout=subprocess.PIPE)
        t = threading.Thread(target=output_reader, args=(pdr, q,))
        t.start()

        while True:
            if(t_exit.value > 0):
                print("exited!")
                pdr.terminate()
                pdr.wait()

# runs the websocket routine
def serve_app(loop, q, cal_q,proc):

    asyncio.set_event_loop(loop)
    app = Sanic()
    connections = {}
    mobile = {}
    desktop = {}

    @app.websocket('/')
    async def feed(request, websocket):

        print("request:",request)
        print("-------------")
        print("websocket:", hash(websocket))

        connections[hash(websocket)] = websocket

        while True:
            
            try:
                message = await websocket.recv()
                msg = json.loads(message)
                # print(" + received messasge: " + message)

                if msg['type'] == "room":
                    room = msg['data']
                    if room == "mobile":
                        print("new mobile client")
                        mobile[hash(websocket)] = websocket

                        try:
                            await websocket.send(json.dumps({"type":"license","data": bool(licensed.value)}))
                        except ConnectionClosed:
                            print("connection closed")

                        for conn in desktop.copy().values():
                            try:
                                await conn.send(json.dumps({"type": "new_mobile_client"}))
                            except ConnectionClosed:
                                print("connection closed")

                    elif room == "desktop":
                        print("new desktop client")
                        desktop[hash(websocket)] = websocket

                elif msg['type'] == "test":
                    print(msg['data'])

                elif msg['type'] == "run_calibration":
                    for conn in desktop.copy().values():
                        try:
                            await conn.send(json.dumps(msg))
                        except ConnectionClosed:
                            print("connection closed")

                elif msg['type'] == "start_calibration":
                    cal_q.put(msg)

                elif msg['type'] == "interrupt_calibration":

                    cal_q.put(msg)
                    for conn in desktop.copy().values():
                        try:
                            await conn.send(json.dumps(msg))
                        except ConnectionClosed:
                            print("connection closed")

                elif msg['type'] == "calibration_point_in_position":
                    cal_q.put(msg)

                elif msg['type'] == "start_session":
                    cal_q.put(msg)

                elif msg['type'] == "new_session":
                    for conn in desktop.copy().values():
                        try:
                            await conn.send(json.dumps(msg))
                        except ConnectionClosed:
                            print("connection closed")

                elif msg['type'] == "end_session":
                    cal_q.put(msg)

                elif msg['type'] == "terminate":
                    # get server process
                    p = Pid.objects.first()
                    server = psutil.Process(p.number)
                    server.terminate()

            except Exception as e:
                print("client quit", e, hash(websocket))

                connections.pop(hash(websocket), None)
                desktop.pop(hash(websocket), None)
                mobile.pop(hash(websocket), None)

                if len(list(connections.values())) == 1:
                    await list(connections.values())[0].send(json.dumps({"type": "last_client"}))
                break

            

    # sends gaze data through websocket
    async def sendgaze(proc):

        while True:

            # if app is closed, exit the command
            if not psutil.pid_exists(p.number):
                t_exit.value = 1
                app.stop()
                break

            # get last data from queue, dump to json and send to all clients 
            try:
                data = q.get(False)
                for conn in connections.values():
                    try:
                        await conn.send(json.dumps(data))
                    except Exception as e:
                        print("connection closed")

            except queue.Empty:
                await asyncio.sleep(0.030)
            
            except SystemExit:
                print("system gone :/")
            


    from tog.models import Pid
    p = Pid.objects.first()
    
    app.add_task(sendgaze(proc))
    app.run(host="0.0.0.0", port=5001,protocol=WebSocketProtocol)


class Command(BaseCommand):
    help = 'Runs the eye tracker socket server'
    def add_arguments(self, parser):

        # add test option for skipping license check
        parser.add_argument(
                '--test',
                help="Test version that doesn't wait for eyetracker license",
            )

    def handle(self, *args, **options):

        time.sleep(1)
        from tog.models import Pid
        p = Pid.objects.first()
        # run the driver
        dri = multiprocessing.Process(target=start_driver, args=(q, calibration_queue,licensed,t_exit,))
        dri.daemon = True
        dri.start()

        # set license to pro for testing purposes
        if options['test']:
            licensed.value = 1
        
        # wait until license type is set
        while licensed.value == -1:
            time.sleep(1)
            
            if not psutil.pid_exists(p.number):
                sys.exit()

        # run the websocket server
        new_loop = asyncio.new_event_loop()
        serve_app(new_loop,q,calibration_queue,p)