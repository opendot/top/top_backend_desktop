from django.core.management.base import BaseCommand
import os

import cherrypy
import django
from django.conf import settings
from django.core.handlers.wsgi import WSGIHandler
from tog.models import Pid

class DjangoApplication(object):
    HOST = "0.0.0.0"
    PORT = 5000

    def mount_static(self, url, root):
        config = {
            'tools.staticdir.on': True,
            'tools.staticdir.dir': root,
            'tools.expires.on': True,
            'tools.expires.secs': 86400,
            'tools.staticdir.index': "index.html"
        }
        cherrypy.tree.mount(None, url, {'/': config})

    def run(self):
        cherrypy.config.update({
            'server.socket_host': self.HOST,
            'server.socket_port': self.PORT,
            'engine.autoreload_on': False,
            'log.screen': True
        })

        self.mount_static(settings.STATIC_URL, settings.STATIC_ROOT)

        # save the process id - moved here from tog.apps
        currpid = os.getpid()
        print(currpid)
        p = Pid.objects.first()
        if p is None:
            Pid.objects.create(number=currpid)
        else:
            p.number = currpid
            p.save()
        #################################################

        cherrypy.log("Load application app")
        cherrypy.tree.graft(WSGIHandler())
        cherrypy.engine.start()
        cherrypy.engine.block()

        

class Command(BaseCommand):
    help = 'Run the server'

    def handle(self, *args, **options):

        # what should I do on process termination?
        def on_terminate(proc):
            print("process {} terminated with exit code {}".format(proc, proc.returncode))
        
        django.setup()
        DjangoApplication().run()

        


