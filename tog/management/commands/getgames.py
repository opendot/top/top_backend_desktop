from django.core.management.base import BaseCommand
from tog.models import Game
import os
import json
from django.conf import settings

GAMES_FOLDER = DRIVER = os.path.join(os.getcwd(), 'static/games')

class Command(BaseCommand):
    help = 'get games data and write it to the database'

    def handle(self, *args, **options):

        for path, dirs, files in os.walk(GAMES_FOLDER):
            for f in files:
                if f == "gamespecs.json":
                    fp = os.path.join(path, f)
                    with open(fp,encoding='utf-8') as f:
                        data = json.load(f)
                        gameurl = os.path.relpath(path, os.getcwd()+"\static").replace("\\", "/")
                        print(data)
                        defaults = {
                            'title': data['info']['title'],
                            'safename': data['info']['safename'],
                            'version': data['info']['version'],
                            'description': data['info']['description'],
                            'type': data['info']['type'],
                            'url': settings.STATIC_URL + gameurl + "/",
                            'image': settings.STATIC_URL + gameurl + "/img/icon.png",
                            'path': path.replace("/", "\\")
                        }
                        obj, created = Game.objects.update_or_create(
                            slug=data['info']['safename'] + "_v" + str(data['info']['version']),
                            defaults=defaults
                        )
                        if created:
                            print("new game! ", obj.title)




