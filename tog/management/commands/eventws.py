from django.core.management.base import BaseCommand
from tog.models import Pid
import psutil
import os
import threading
from sanic import Sanic
import asyncio
import queue
import csv
import json
import functools
import operator
import copy
from websockets.exceptions import ConnectionClosed
import time

CSV_PATH = os.path.join(os.getcwd(), 'sessions/')
rq = queue.Queue(maxsize=0)
eq = queue.Queue(maxsize=0)
session = None


def start_rndr_writer(rq, eq):
    writing = False
    f = None
    fw = None
    fe = None
    few = None
    global session

    while True:

        # am I actually writing?
        if writing:
            # session closed, reset
            if session is None:
                # print("closing session")
                writing = False
                f.close()
                fe.close()
                f = None
                fw = None
                fe = None
                few = None
            else:

                # write new render data on csv
                try:
                    data = rq.get(False)
                    # print('- get new render message from queue:' + str(data))
                    if "lhs" in data:
                        del data["lhs"]
                    if "rhs" not in data:
                        data["rhs"] = ""
                    else:
                        try:
                            data['rhs'] = json.dumps(data['rhs'])
                        except:
                            pass
                    fw.writerow([data["ts"], data["kind"], data["path"], data["rhs"]])
                    # print('wrote render message on file: '+ str(data))
                    f.flush()
                # queue is empty
                except queue.Empty:
                    pass

                # write new event data on csv
                try:
                    data = eq.get(False)
                    # print('- get new event message from queue:' + str(data))
                    if not "ts" in data:
                        data["ts"] = int(round(time.time() * 1000))
                    del data['type']
                    # print(data)
                    few.writerow([data["ts"], data["event_type"], data["data"]])
                    # print('wrote event message on file: ' + str(data))
                    fe.flush()
                # queue is empty
                except queue.Empty:
                    pass

        # so I'm not writing
        else:
            # a session started, initialize the csv
            if session is not None:
                writing = True

                # empty the queues to avoid old messages to be written in the csv
                #while not eq.empty():
                #    eq.get()
                #while not rq.empty():
                #    rq.get()

                    # check that files were correctly closed
                if f is not None and not f.closed:
                    f.close()
                if fe is not None and not fe.closed:
                    fe.close()

                # print("opening session")

                with rq.mutex:
                    size = len(rq.queue)
                    rq.queue.clear()
                    rq.unfinished_tasks -= size

                with eq.mutex:
                    size = len(eq.queue)
                    eq.queue.clear()
                    eq.unfinished_tasks -= size


                # print("clear queus")

                # create new files
                sespath = os.path.join(CSV_PATH, session)
                try:
                    f = open(sespath + '/render.csv', 'w', newline='')
                    fw = csv.writer(f)
                    fw.writerow(['ts', 'change', 'path', 'data'])
                    f.flush()
                    # print("created render csv file")
                except Exception as e:
                    print(e)

                try:
                    fe = open(sespath + '/events.csv', 'w', newline='')
                    few = csv.writer(fe)
                    few.writerow(['ts', 'type', 'data'])
                    fe.flush()
                    # print("created events csv file")
                except Exception as e:
                    print(e)


def serve_app(loop, rq, eq):
    lrq = queue.Queue(maxsize=5)
    asyncio.set_event_loop(loop)
    app = Sanic()

    def getFromDict(dataDict, mapList):
        return functools.reduce(operator.getitem, mapList, dataDict)

    def setInDict(dataDict, mapList, value):
        getFromDict(dataDict, mapList[:-1])[mapList[-1]] = value

    def addToDict(dataDict, mapList, value):
        try:
            rem = mapList.pop()
            ob = getFromDict(dataDict, mapList)
            ob[rem] = value
        except:
            print("add error")

    def remFromDict(dataDict, mapList):
        rem = mapList.pop()
        ob = getFromDict(dataDict, mapList)
        ob.pop(rem)

    def updateState(ob):
        if ob['kind'] == "N":
            addToDict(pstate, ob['path'], ob['rhs'])
        elif ob['kind'] == "E":
            setInDict(pstate, ob['path'], ob['rhs'])
        elif ob['kind'] == "D":
            remFromDict(pstate, ob['path'])

    pstate = {}

    connections = set()
    mobile = set()
    desktop = set()

    async def sendToMobile(msg):
        for conn in mobile.copy():
            try:
                await conn.send(msg)
            except ConnectionClosed:
                mobile.remove(conn)

    async def sendToDesktop(msg):
        for conn in desktop.copy():
            try:
                await conn.send(msg)
            except ConnectionClosed:
                desktop.remove(conn)

    async def sendToAll(msg):
        for conn in connections.copy():
            try:
                await conn.send(msg)
            except ConnectionClosed:
                connections.remove(conn)

    async def multicast(msg, ws):
        for conn in connections.copy():
            if conn != ws:
                try:
                    await conn.send(msg)
                    # # print("multicast send", msg)
                except ConnectionClosed:
                    connections.remove(conn)


    @app.websocket('/')
    async def feed(request,websocket):
        global session
        connections.add(websocket)
        while True:
            message = await websocket.recv()
            # print('+ received new message:' + message )
            msg = json.loads(message)
            if msg['type'] == "room":
                room = msg['data']
                if room == "mobile":
                    mobile.add(websocket)
                    # print("new mobile connection")
                elif room == "desktop":
                    desktop.add(websocket)
                    # print("new desktop connection")

            elif msg['type'] == "test":
                print(msg['data'])

            elif msg['type'] == "start_session":
                session = msg['data']
                # print('new session ', msg['data'])

            elif msg['type'] == "end_session":
                session = None
                # print('end session')

            elif msg['type'] == "event":
                # print(msg)
                eq.put(msg)
                await multicast(message, websocket)
                if msg['event_type'] == "end_session":
                    session = None
                    # print('end session')

            elif msg['type'] == "render":
                for l in msg['data']:
                    rq.put(copy.deepcopy(l))
                    updateState(l)
                await sendToMobile(json.dumps({"type": "render", "data": pstate}))

            elif msg['type'] == "terminate":
                # get server process
                p = Pid.objects.first()
                server = psutil.Process(p.number)
                server.terminate()

    # start the websocket service
    app.run(host="0.0.0.0", port=5002)


class Command(BaseCommand):
    help = 'Runs the eye tracker socket server'

    def handle(self, *args, **options):

        # get server process
        p = Pid.objects.first()
        # print(p.number)

        new_loop = asyncio.new_event_loop()

        wst = threading.Thread(target=serve_app, args=(new_loop, rq, eq,))
        wst.daemon = True
        wst.start()

        r_wri = threading.Thread(target=start_rndr_writer, args=(rq,eq))
        r_wri.daemon = True
        r_wri.start()

        while True:
            if not psutil.pid_exists(p.number):
                break
