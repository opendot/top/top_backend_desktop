from django.core.management.base import BaseCommand
from tog.models import Pid
import psutil
import subprocess
import os

CHROME = os.path.join(os.getcwd(), 'chrome-win32/chrome.exe')


class Command(BaseCommand):
    help = 'Runs the browser and listens to the server process'

    def handle(self, *args, **options):

        # what should I do on process termination?
        def on_terminate(proc):
            print("process {} terminated with exit code {}".format(proc, proc.returncode))
            try:
                browser.terminate()
            except (psutil.ZombieProcess, psutil.AccessDenied, psutil.NoSuchProcess):
                print("browser gone already")

            try:
                server.terminate()
            except (psutil.ZombieProcess, psutil.AccessDenied, psutil.NoSuchProcess):
                print("server gone already")

        # get server process
        p = Pid.objects.first()
        server = psutil.Process(p.number)
        # launch browser and get process
        b = subprocess.Popen([CHROME,'http://localhost:5000/qr', '--start-fullscreen', '--disable-domain-reliability', '--disable-infobars', '--disable-notifications', '--disable-pinch', '--disable-pull-to-refresh-effect', '--overscroll-history-navigation=0', '--autoplay-policy=no-user-gesture-required', '--disable-gpu', '-incognito'])  # add flags and launch page
        browser = psutil.Process(b.pid)

        # listener on termination of both processes
        gone, alive = psutil.wait_procs([server, browser], callback=on_terminate)


