from django.core.management.base import BaseCommand
from tog.models import Pid
import psutil
import os
import threading
from aiohttp import web
import socketio
import subprocess
import asyncio
import queue
import csv


DRIVER = os.path.join(os.getcwd(), 'nopro_driver/Tog_noPro_driver.exe')
CSV_PATH = os.path.join(os.getcwd(), 'sessions/')
q = queue.Queue(maxsize=1)
session = None


def start_driver(q):
    count = 0
    writing = False
    f = None
    fw = None
    global session
    p = subprocess.Popen([DRIVER], shell=False, bufsize=-1, stdout=subprocess.PIPE)
    while True:

        if writing:
            # session closed, reset
            if session is None:
                writing = False
                f = None
                fw = None

        else:
            # a session started, initialize the csv
            if session is not None:
                writing = True
                sespath = os.path.join(CSV_PATH, session)
                try:
                    os.makedirs(sespath)
                except FileExistsError:
                    print("there already")
                f = open(sespath + '/gaze.csv', 'w', newline='')
                fw = csv.writer(f)
                fw.writerow(['ts', 'x', 'y'])
                f.flush()

        output = p.stdout.readline()
        # if output == '' and p.poll() is not None:
        #    break
        if output:
            data = [float(s) for s in output.decode("utf-8").strip().split(",")]
            if writing:
                fw.writerow(data)
                f.flush()
            count = count + 1

            if count >= 3:
                if not q.full():
                    data.pop(0)
                    q.put(data)
                    count = 0
            p.stdout.flush()

    #rc = p.poll()
    #return rc


def serve_app(loop, q):
    asyncio.set_event_loop(loop)
    sio = socketio.AsyncServer(ping_timeout=10, ping_interval=10, allow_upgrades=False)
    app = web.Application()
    sio.attach(app)

    @sio.on('connect')
    async def connect(sid, environ):
        print('connect ', sid)

    @sio.on('message')
    async def message(sid, data):
        print('message ', data)

    @sio.on('start_session')
    async def message(sid, data):
        global session
        session = data
        print('new session ', data)

    @sio.on('end_session')
    async def message(sid):
        global session
        session = None
        print('close session')

    @sio.on('disconnect')
    async def disconnect(sid):
        print('disconnect ', sid)

    async def sendgaze():
        while True:
            try:
                data = q.get(False)
                await sio.emit('gaze', data)
            except queue.Empty:
                await sio.sleep(0.015)

    sio.start_background_task(sendgaze)
    web.run_app(app, host="0.0.0.0", port=5001)


class Command(BaseCommand):
    help = 'Runs the eye tracker socket server'

    def handle(self, *args, **options):

        # get server process
        p = Pid.objects.first()
        print(p.number)

        new_loop = asyncio.new_event_loop()

        wst = threading.Thread(target=serve_app, args=(new_loop, q,))
        wst.daemon = True
        wst.start()

        dri = threading.Thread(target=start_driver, args=(q,))
        dri.daemon = True
        dri.start()

        while True:
            if not psutil.pid_exists(p.number):
                break
