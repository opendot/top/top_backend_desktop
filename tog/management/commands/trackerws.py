from django.core.management.base import BaseCommand
from tog.models import Pid
import psutil
import os
import threading
import subprocess
import json
import asyncio
import queue
import csv
import copy
from websockets.exceptions import ConnectionClosed
from sanic import Sanic
import sys


DRIVER = os.path.join(os.getcwd(), 'nopro_driver/Tog_noPro_driver.exe')
CSV_PATH = os.path.join(os.getcwd(), 'sessions/')
q = queue.Queue(maxsize=1)
session = None
t_exit = False


def output_reader(proc, track_queue):
    global t_exit

    for line in iter(proc.stdout.readline, b''):
        if t_exit:
            break
        data = [float(s) for s in line.decode("utf-8").strip().split(",")]
        try:
            track_queue.put_nowait(data)
        except queue.Full:
            pass


def start_driver(q):
    count = 0
    track_q = queue.Queue(maxsize=0)
    writing = False
    buffer = []
    f = None
    fw = None
    global session
    global t_exit
    p = subprocess.Popen([DRIVER], shell=False, bufsize=-1, stdout=subprocess.PIPE)
    t = threading.Thread(target=output_reader, args=(p, track_q,))
    t.start()

    while True:
        if t_exit:
            break
        if writing:
            # session closed, reset
            if session is None:
                try:
                    print("closing file")
                    writing = False
                    f.close()
                    f = None
                    fw = None
                except Exception as e:
                    print(e)


        else:
            # a session started, initialize the csv
            if session is not None:
                writing = True
                try:
                    print("creating csv")
                    sespath = os.path.join(CSV_PATH, session)
                    f = open(sespath + '/gaze.csv', 'w', newline='')
                    fw = csv.writer(f)
                    fw.writerow(['ts', 'x', 'y'])
                    f.flush()
                except Exception as e:
                    print(e)

        try:
            data = track_q.get_nowait()

            if writing:
                try:
                    buffer.append(copy.copy(data))
                    if len(buffer) >= 50:
                        fw.writerows(buffer)
                        f.flush()
                        buffer = []
                except Exception as e:
                    print(e)
            count = count + 1

            if count >= 3:
                if not q.full():
                    data.pop(0)
                    q.put(data)
                    count = 0

        except queue.Empty:
            pass


def serve_app(loop, q):
    asyncio.set_event_loop(loop)
    app = Sanic()
    connections = set()
    mobile = set()
    desktop = set()


    @app.websocket('/')
    async def feed(request, websocket):
        global session
        connections.add(websocket)

        while True:
            message = await websocket.recv()
            msg = json.loads(message)
            print(msg)

            if msg['type'] == "room":
                room = msg['data']
                if room == "mobile":
                    print("added to mobile")
                    mobile.add(websocket)
                    for conn in desktop.copy():
                        try:
                            await conn.send(json.dumps({"type": "new_mobile_client"}))
                        except ConnectionClosed:
                            print("connection closed")
                elif room == "desktop":
                    print("added to desktop")
                    desktop.add(websocket)

            elif msg['type'] == "test":
                print(msg['data'])

            elif msg['type'] == "start_session":
                session = msg['data']
                print('new session ', msg['data'])

            elif msg['type'] == "new_session":
                for conn in desktop.copy():
                    try:
                        await conn.send(json.dumps(msg))
                    except ConnectionClosed:
                        desktop.remove(conn)

                print('start the new session ', msg['data'])

            elif msg['type'] == "end_session":
                session = None
                print('end session')
            elif msg['type'] == "terminate":
                # get server process
                p = Pid.objects.first()
                server = psutil.Process(p.number)
                server.terminate()

    async def sendgaze():
        global t_exit

        while True:
            if t_exit:
                break
            try:
                data = q.get(False)
                for conn in connections.copy():
                    try:
                        await conn.send(json.dumps({"type": "gaze", "data": data}))
                    except ConnectionClosed:
                        connections.remove(conn)

            except queue.Empty:
                await asyncio.sleep(0.015)

    app.add_task(sendgaze())
    app.run(host="0.0.0.0", port=5001)


class Command(BaseCommand):
    help = 'Runs the eye tracker socket server'


    def handle(self, *args, **options):

        global t_exit
        # get server process
        p = Pid.objects.first()
        # print(p.number)

        new_loop = asyncio.new_event_loop()

        wst = threading.Thread(target=serve_app, args=(new_loop, q,))
        wst.daemon = True
        wst.start()

        dri = threading.Thread(target=start_driver, args=(q,))
        dri.daemon = True
        dri.start()

        while True:
            if not psutil.pid_exists(p.number):
                print("goodbye!")
                t_exit = True
                sys.exit()
                break
