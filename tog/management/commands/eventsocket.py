from django.core.management.base import BaseCommand
from tog.models import Pid
import psutil
import os
import threading
from sanic import Sanic
import socketio
import asyncio
import queue
import csv
import time

CSV_PATH = os.path.join(os.getcwd(), 'sessions/')
rq = queue.Queue(maxsize=0)
eq = queue.Queue(maxsize=0)
session = None


def start_rndr_writer(rq):
    writing = False
    f = None
    fw = None
    global session

    while True:

        # print(session)
        # am I actually writing?
        if writing:
            # session closed, reset
            if session is None:
                writing = False
                f = None
                fw = None
            else:
                # print("ok, so?")
                # write new data on csv
                try:
                    data = rq.get(False)
                    fw.writerow(data.values())
                    f.flush()
                # queue is empty
                except queue.Empty:
                    # print("queue empty")
                    continue

        # so I'm not writing
        else:
            # a session started, initialize the csv
            if session is not None:
                writing = True
                sespath = os.path.join(CSV_PATH, session)
                try:
                    os.makedirs(sespath)
                except FileExistsError:
                    print("there already")
                f = open(sespath + '/render.csv', 'w', newline='')
                fw = csv.writer(f)
                fw.writerow(['ts', 'change', 'path', 'data'])
                f.flush()


def start_evt_writer(eq):
    writing = False
    fe = None
    few = None
    global session

    while True:
        # print(session)
        # am I actually writing?
        if writing:
            # session closed, reset
            if session is None:
                writing = False
                fe = None
                few = None
            else:
                # print("ok, so?")
                # write new data on csv
                try:
                    data = eq.get(False)
                    few.writerow(data.values())
                    fe.flush()
                # queue is empty
                except queue.Empty:
                    # print("queue empty")
                    continue

        # so I'm not writing
        else:
            # a session started, initialize the csv
            if session is not None:
                writing = True
                sespath = os.path.join(CSV_PATH, session)
                try:
                    os.makedirs(sespath)
                except FileExistsError:
                    print("there already")
                fe = open(sespath + '/events.csv', 'w', newline='')
                few = csv.writer(fe)
                few.writerow(['ts', 'type', 'data'])
                fe.flush()

def serve_app(loop, rq, eq):
    lrq = queue.Queue(maxsize=5)
    asyncio.set_event_loop(loop)
    sio = socketio.AsyncServer(async_mode='sanic', ping_timeout=20,allow_upgrades=False, engineio_logger=False)
    app = Sanic()
    sio.attach(app)

    @sio.on('connect')
    async def connect(sid, environ):
        print('connect ', sid)

    @sio.on('room')
    async def connect(sid, room):
        sio.enter_room(sid,room)
        print("sid entered room ", room)

    @sio.on('message')
    async def message(sid, data):
        print('message ', data)

    @sio.on('start_session')
    async def message(sid, data):
        global session
        session = data
        print('new session ', data)

    @sio.on('end_session')
    async def message(sid):
        global session
        session = None
        print('close session')

    @sio.on('render')
    async def message(sid, data):
        #await sio.emit('render', data)
        lrq.put(data)
        #rq.put(data)

    @sio.on('event')
    async def message(sid, data):
        eq.put(data)
        print('received event ', data)

    @sio.on('disconnect')
    async def disconnect(sid):
        print('disconnect ', sid)

    async def sendRender():

        import functools
        import operator

        def getFromDict(dataDict, mapList):
            return functools.reduce(operator.getitem, mapList, dataDict)

        def setInDict(dataDict, mapList, value):
            getFromDict(dataDict, mapList[:-1])[mapList[-1]] = value

        def addToDict(dataDict, mapList, value):
            try:
                rem = mapList.pop()
                ob = getFromDict(dataDict, mapList)
                ob[rem] = value
            except:
                print("add error")
        def remFromDict(dataDict, mapList):
            rem = mapList.pop()
            ob = getFromDict(dataDict, mapList)
            ob.pop(rem)

        def updateState(ob):
            if ob['kind'] == "N":
                addToDict(state, ob['path'], ob['rhs'])
            elif ob['kind'] == "E":
                setInDict(state, ob['path'], ob['rhs'])
            elif ob['kind'] == "D":
                remFromDict(state, ob['path'])

        state = {}

        while True:
            try:
                start = time.time()
                cr = lrq.get(False)
                try:

                    for o in cr:
                        updateState(o)

                except:
                    print("error parsing", cr)

                await sio.emit('render', state, room="mobile")
                end = time.time()
                print(end-start)
            except queue.Empty:
                #print('queue empty')
                await sio.sleep(0.015)

    @app.listener('before_server_start')
    def before_server_start(sanic, loop):
        print("before start")
        sio.start_background_task(sendRender)


    app.run(host="0.0.0.0", port=5002)


class Command(BaseCommand):
    help = 'Runs the eye tracker socket server'

    def handle(self, *args, **options):

        # get server process
        p = Pid.objects.first()
        print(p.number)

        new_loop = asyncio.new_event_loop()

        wst = threading.Thread(target=serve_app, args=(new_loop, rq, eq,))
        wst.daemon = True
        wst.start()

        '''r_wri = threading.Thread(target=start_rndr_writer, args=(rq,))
        r_wri.daemon = True
        r_wri.start()

        e_wri = threading.Thread(target=start_evt_writer, args=(eq,))
        e_wri.daemon = True
        e_wri.start()'''

        while True:
            if not psutil.pid_exists(p.number):
                break
