from django.contrib import admin
from .models import CareReceiver, Game, Session, Sync


class CareReceiverAdmin(admin.ModelAdmin):
    model = CareReceiver
    list_display = ['name', 'last_name', 'gender']


class GameAdmin(admin.ModelAdmin):
    model = Game
    list_display = ['title', 'version', 'description']


class SessionAdmin(admin.ModelAdmin):
    model = Session
    list_display = ['get_name', 'game', 'created_at']

    def get_name(self, obj):
        return obj.careReceiver.name + " " + obj.careReceiver.last_name

    get_name.admin_order_field = 'careReceiver__last_name'  # Allows column order sorting
    get_name.short_description = 'Care Receiver'  # Renames column head


class SyncAdmin(admin.ModelAdmin):
    model = Sync
    list_display = ['created_at', 'completed_at', 'success', 'error']


# Register your models here.
admin.site.register(CareReceiver, CareReceiverAdmin)
admin.site.register(Game, GameAdmin)
admin.site.register(Session, SessionAdmin)
admin.site.register(Sync, SyncAdmin)
