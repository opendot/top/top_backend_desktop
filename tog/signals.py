from django.db.models.signals import post_save
from django.dispatch import receiver
from .models import Session
import time
from datetime import datetime
import os
import csv

CSV_PATH = os.path.join(os.getcwd(), 'sessions/')

@receiver(post_save, sender=Session)
def write_to_csv(sender, instance, created, **kwargs):
    if created:
        sespath = os.path.join(CSV_PATH, str(instance.id))
        try:
            os.makedirs(sespath)
        except FileExistsError:
            print("there already")

        exists = os.path.isfile(os.path.join(CSV_PATH, 'sessions.csv'))
        if exists:
            print("file exists, appending")
            with open(os.path.join(CSV_PATH, 'sessions.csv'), 'a', newline='') as file:
                writer = csv.writer(file)
                row = [str(instance.id), instance.careGiver.first_name + " " + instance.careGiver.last_name,
                       str(instance.careGiver.id), instance.careReceiver.name + " " + instance.careReceiver.last_name,
                       str(instance.careReceiver.id), instance.game.safename, instance.game.version, int(time.time()), datetime.now().strftime('%Y-%m-%d %H:%M:%S')]
                writer.writerow(row)
                file.close()
        else:
            print("creating sessions csv")
            with open(os.path.join(CSV_PATH, 'sessions.csv'), 'w', newline='') as file:
                writer = csv.writer(file)
                row = ["session_id", "caregiver_name", "caregiver_id",
                       "carereceiver_name", "carereceiver_id", "game_name","game_version", "timestamp", "date"]

                writer.writerow(row)

                row = [str(instance.id), instance.careGiver.first_name + " " + instance.careGiver.last_name,
                       str(instance.careGiver.id), instance.careReceiver.name + " " + instance.careReceiver.last_name,
                       str(instance.careReceiver.id), instance.game.safename, instance.game.version, int(time.time()), datetime.now().strftime('%Y-%m-%d %H:%M:%S')]


                writer.writerow(row)
                file.close()
